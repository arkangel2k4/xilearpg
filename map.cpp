#include "mmo.h"
#include "map.h"
#include "mob.h"
#include "Monster.h"

struct block_list bl_head;
struct block_list *bl_tail = &bl_head;
int bl_count=0;

struct map_data maps[1];

int map_init(){
	bl_head.id=0;
	bl_head.next=0;
	bl_head.prev=0;
	return 0;
}

int map_spawn(int *map_id, int *map_stage){
	int map = *map_id;
	int stage = *map_stage;
	//map finished?
	if(stage >= maps[map].stages.size()){
		*map_id = maps[map].next_map;
		*map_stage = 0;
		map_spawn(map_id,map_stage);
		return 1;
	}else{
		//spawn points?
		Vector2 pos[4];
		pos[0].x = maps[map].size.x/2;	pos[0].y = 100;
		pos[1].x = maps[map].size.x-50;	pos[1].y = maps[map].size.y/2;
		pos[2].x = maps[map].size.x/2;	pos[2].y = maps[map].size.y;
		pos[3].x = 50;					pos[3].y = maps[map].size.y/2;
		int nextpos;
		for(int i=0 ; i<maps[map].stages[stage].quant ; i++){
			nextpos=(int)(RAND()*3.99999);
			new Monster(pos[nextpos].x, pos[nextpos].y, maps[map].stages[stage].mob_id);
		}
		return 0;
	}

}

void map_add_stage(int map_id, int mob_id, int quantity){
	struct map_stage newstage;
	newstage.mob_id=mob_id;
	newstage.quant=quantity;
	maps[map_id].stages.push_back(newstage);
}

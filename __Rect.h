#ifndef ___RECT_H_
#define ___RECT_H_

class __Rect
{
public:

	int left;
	int top;
	int width;
	int height;

	__Rect()
	{
		left = 0; top = 0; width = 0; height = 0;
	}

	__Rect(int _left, int _top, int _width, int _height)
	{
		left = _left;
		top = _top;
		width = _width;
		height = _height;
	}

	bool intersect(const __Rect &rhs)
	{
		int X1, Y1, X2, Y2;
		int X1Left, X1Right, Y1Top, Y1Bottom, X2Left, X2Right, Y2Top, Y2Bottom;

		//grab top left X and Y
		X1 = left;
		Y1 = top;
		X2 = rhs.left;
		Y2 = rhs.top;

		//Figure out the box details for Object1
		X1Left = X1;
		X1Right = X1Left + width;
		Y1Top = Y1;
		Y1Bottom=Y1Top + height;

		//Figure out the box details for Object2
		X2Left = X2;
		X2Right = X2Left + rhs.width;
		Y2Top = Y2;
		Y2Bottom= Y2Top + rhs.height;
		
		if ((((Y1Top <= Y2Bottom) && (Y1Bottom >= Y2Top)) || 
			((Y2Top <= Y1Bottom) && (Y2Bottom >= Y1Top))) && 
			(((X1Right >= X2Left) && (X1Left <= X2Right)) || 
			((X2Right >= X1Left) && (X2Left <= X1Right))))
		{
			return true;
		}

		return false;
	}
};

#endif
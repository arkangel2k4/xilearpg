struct item_data {
	char name[ITEM_NAME_LENGTH];
	//Do not add stuff between value_buy and wlv (see how getiteminfo works)
	int value_buy;
	int value_sell;
	int type;
	int equip_index;
	int weight;
	int bonus[NUM_STATS];
	int slots;
};

extern struct item_data item_db[NUM_ITEMS];

#include <string.h>

// "server side" stuff
#include "mmo.h"
#include "map.h"
#include "mob.h"

// "client side" stuff
#include "Resources.h"
#include "AnimObject.h"
#include "GameObject.h"
#include "Player.h"
#include "Monster.h"
#include "Configuration.h"
#include "Interface.h"
#include "battle.h"
#include "item.h"
#include "SaveData.h"

unsigned int frameCount=0;
int frameCountInv=0;
bool appLoaded=false;
bool avatarsLoaded=false;
bool resourcesLoaded=false;
bool gameStarted=false;
Player* player=NULL;

int levelToLoad=0;

int skillTimeCount[NUM_SKILLS];

bool skillIsActive[NUM_SKILLS];
bool skillIsPressed[NUM_SKILLS];
bool directionIsPressed=false;

AnimObject *weather=NULL;

//-------------------------------------------------------

void restart_game(){
	//delete monsters...
	block_list *aux=bl_head.next, *lastaux;
	while(aux){
		lastaux=aux;
		aux=aux->next;
		delete ((Monster*)lastaux->obj);
	}
	// clear map cells (to be safe)
	for(int i=0 ; i<1+(int)(sizeBg.y/BLOCK_SIZE) ; i++)
		for(int j=0 ; j<1+(int)(sizeBg.x/BLOCK_SIZE) ; j++)
			mapCells[i][j]=NULL;

	//spawn monsters...
	map_spawn(&player->map, &player->map_stage);

	//player...
	player->status.hp = player->status.max_hp;
	player->invisibleTimeCount=0;
	player->lasthitTimeCount=0;
	player->setDead(false);
	player->_pos.x = 250; player->_pos.y = 200;
	centerPlayer();
	player->idle();
}

void load_resources(int level_id){

	if(level_id==-1 && !avatarsLoaded){
		load_player_resources_avatar();
		load_head_resources();
		load_interface_player();

		load_item_resources(); //TODO: load when going to inventory; only if not loaded yet
		avatarsLoaded=true;

		panelSetVisible(PANEL_CHAR_SELECT, true, TRANSITION_BLACK);
		panelSetVisible(PANEL_LOADING, false, TRANSITION_BLACK);
	}else if(level_id==0){
		//load images...
		load_player_resources();
		load_mob_resources();
		load_weather_resources();
		weather->init();

		//handles...
		init_object_handles_storage();

		//start game
		load_game();
		gameStarted=true;

		panelSetVisible(PANEL_LOADING, false, TRANSITION_NORMAL);
	}else{
		panelSetVisible(PANEL_LOADING, false, TRANSITION_NORMAL);
	}

	//done...
	resourcesLoaded=true;
}

void load_level(){
	Mp3Stop();
	//Mp3Loop(music[SONG_LVL1]);

	centerPlayer();

	// martim: map cells
	mapCells.resize( 1+(int)(sizeBg.y/BLOCK_SIZE), 1+(int)(sizeBg.x/BLOCK_SIZE) );
	for(int i=0 ; i<mapCells.rows() ; i++)
		for(int j=0 ; j<mapCells.cols() ; j++)
			mapCells[i][j]=NULL;
	/*
	mapCells = new GameObject**[1+(int)(sizeBg.y/BLOCK_SIZE)];
	for(int i=0 ; i<1+(int)(sizeBg.y/BLOCK_SIZE) ; i++){
		mapCells[i] = new GameObject*[1+(int)(sizeBg.x/BLOCK_SIZE)];
		for(int j=0 ; j<1+(int)(sizeBg.x/BLOCK_SIZE) ; j++)
			mapCells[i][j]=NULL;
	}
	*/

	//spawn monsters...
	map_spawn(&player->map, &player->map_stage);

	// martim: skill variables
	player->skillInUse=-1;
	for(int i=0;i<NUM_SKILLS;i++){
		skillTimeCount[i]=-1;
		skillIsActive[i]=false;
		skillIsPressed[i]=false;
	}
}

void load_game(){
	wViewSetVisible(viewBlack,0);
	if(player) delete player;
	player = new Player(250, 200); //create new player
	player->status.level = gdGlobal.player_slot[selectedPlayer].level;
	player->setHead(gdGlobal.player_slot[selectedPlayer].id_head);
	if(!gdGlobal.player_slot[selectedPlayer].free){
		memcpy(player->inventory, gdGlobal.player_slot[selectedPlayer].inventory, MAX_INVENTORY*sizeof(struct item));
	}
	player->computeStats();

	//fill in equipment slots
	inventoryDisplay(true);
	inventoryDisplay(false);

	load_level();
}

void unload_level(){
	//delete stuff...
	block_list *aux=bl_head.next, *lastaux;
	while(aux){
		lastaux=aux;
		aux=aux->next;
		delete ((Monster*)lastaux->obj);
	}
	/*
	if(mapCells){
		for(int i=0 ; i<1+(int)(sizeBg.y/BLOCK_SIZE) ; i++){
			if(mapCells[i]) delete[] mapCells[i];
		}
		delete[] mapCells;
		mapCells=NULL;
	}
	*/
}

void unload_game(){
	gameStarted=false;

	//save game...
	if(player)
		player->save();

	//unload everything...
	Mp3Stop();
	unload_level();
	if(player){ delete player; player=NULL; }
}

//===============================================
void AppMain()
{
	DragonWrapperStart(WRAPPER_LOG_WARNINGS);
	LandscapeMode();
	posBg.x=0; posBg.y=0;

	//background
	strcpy(maps[0].background, "Images/Maps/Background.png");
	viewBg = wViewAdd(maps[0].background, 0, 0);
	sizeBg.x=ViewGetWidth(viewBg); sizeBg.y=ViewGetHeight(viewBg);
	maps[0].size = sizeBg;

	//weather
	weather = new AnimObject();
	weather->setCurrentAnim(0);
	weather->setVisible(false);

	//resources
	map_init();
	load_interface_resources();
	load_interface();
	load_config();
	load_sounds();
	load_menu();

	LoadGameData("ixile_save");
	//gdGlobal.player_slot[0].free=true;
	//gdGlobal.player_slot[1].free=true;
	//gdGlobal.player_slot[2].free=true;

	appLoaded=true;

	return;
}

void AppExit()
{
	DragonWrapperEnd();
	unload_game();
	if(weather){ delete weather; }
}

void updateZorder(){
	block_list *aux;
	GameObject **sortedObjs = new GameObject*[bl_count+1], *obj;
	int i=0, swaps=0;

	//temporary vectors (with all monsters and the player)
	aux=bl_head.next;
	while(aux){
		sortedObjs[i++] = aux->obj;
		aux=aux->next;
	}
	sortedObjs[i] = (GameObject*)player;
	
	//sort handles? No. that array should already be sorted...

	//sort objects
	do{
		swaps=0;
		for(i=0;i<bl_count;i++)
			if(sortedObjs[i]->_pos.y > sortedObjs[i+1]->_pos.y){
				obj=sortedObjs[i]; sortedObjs[i]=sortedObjs[i+1]; sortedObjs[i+1]=obj; swaps++;
			}
	}while(swaps>0);

	//swap handles
	for(i=0;i<bl_count+1;i++){
		if(sortedObjs[i]->handles.view[0] != used_handles[i].view[0]){
			sortedObjs[i]->handles = used_handles[i];
			sortedObjs[i]->reloadText();
		}
	}

	delete[] sortedObjs; //i should...right?
	return;
}

void restrictPlayer(){
	//check off-screen limits
	int playerw=ViewGetWidth(player->handles.view[0]);
	int playerh=ViewGetHeight(player->handles.view[0]);
	if(player->_pos.x < 0.5*playerw - posBg.x)
		player->_pos.x = 0.5*playerw - posBg.x;
	if(player->_pos.x > 480-0.5*playerw - posBg.x)
		player->_pos.x = 480-0.5*playerw - posBg.x;
	if(player->_pos.y < playerh - posBg.y)
		player->_pos.y = playerh - posBg.y;
	if(player->_pos.y > 320 - posBg.y)
		player->_pos.y = 320 - posBg.y;
}

//===============================================
void OnTimer()
{
	int i;

	//loading screen
	if(panelVisible[PANEL_LOADING] && panelAnimNext==-1){
		if(!resourcesLoaded)
			load_resources(levelToLoad);
	}

	//menu fading
	if(panelAnimCurrent!=-1){ //first finish fade out
		panelAnimCurrent_alpha-=20;
		if(panelAnimCurrent_alpha>0)
			panelSetAlpha(panelAnimCurrent, panelAnimCurrent_alpha);
		else
			panelSetVisible(panelAnimCurrent, false, TRANSITION_NOP);
	}else if(panelAnimNext!=-1){ //then fade in
		panelAnimNext_alpha+=20;
		if(panelAnimNext_alpha<=100)
			panelSetAlpha(panelAnimNext, panelAnimNext_alpha);
		else
			panelAnimNext=-1;
	}
	//zoom fading
	if(zoomFade>=0){
		if(zoomFade==100){
			panelSetVisible(PANEL_CLEAR, true, TRANSITION_BLACK);
			panelSetAlpha(PANEL_CLEAR, 0);
		}else if(zoomFade>50){
			panelSetAlpha(PANEL_CLEAR, 200-2*zoomFade);
		}else if(zoomFade>0){
			if(zoomFade==50){
				ViewSetSize(viewBg, (int)(sizeBg.x*zoomPer/100.0), (int)(sizeBg.y*zoomPer/100.0));
				centerPlayer();
				player->update();
			}
			panelSetAlpha(PANEL_CLEAR, 2*zoomFade);
		}else{
			panelSetVisible(PANEL_CLEAR, false, TRANSITION_NORMAL);
			gameStarted=true;
		}
		zoomFade-=10;
	}

	//game is not running
	if(!gameStarted){
		//inventory animations
		if(panelVisible[PANEL_INV]){
			if ((frameCountInv++)%3 != 0) return;
			if(viewPlayerRot=='L'){
				if(--viewPlayerDir <= -1) viewPlayerDir=7;
				wViewSetImage(viewPlayer, player->_frames[viewPlayerDir][0]);
				wViewSetImage(viewPlayerHead, player->_heads[viewPlayerDir]);
				ViewSetxy(viewPlayer,240-0.5*ViewGetWidth(viewPlayer),180-ViewGetHeight(viewPlayer));
				ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
			}else if(viewPlayerRot=='R'){
				if(++viewPlayerDir >= 8) viewPlayerDir=0;
				wViewSetImage(viewPlayer, player->_frames[viewPlayerDir][0]);
				wViewSetImage(viewPlayerHead, player->_heads[viewPlayerDir]);
				ViewSetxy(viewPlayer,240-0.5*ViewGetWidth(viewPlayer),180-ViewGetHeight(viewPlayer));
				ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
			}
		}
		//stop here.
		return;
	}

	//------------------------------------------------------
	// Game is running from here on

	frameCount++;

	// invisible for teleport skill... max 5secs
	if(skillIsActive[SKILL_TELEPORT] && player->invisibleTimeCount > 15*5){
		TouchSetVisible(view_touch,0);
		player->setVisible(true);
		player->invisibleTimeCount=0;
		for(int i=0 ; i<MENU1 ; i++)
			wViewSetVisible(buttonHandle[i],1);
		wViewSetVisible(buttonHandle[JOYSTICK],1);
	}

	// last time player was hit for hp regen... after 5secs no hit, regen 10HP every 2secs
	if(!player->isDead && player->lasthitTimeCount - 15*5 > 0 && (player->lasthitTimeCount - 15*5)%(15*2)==0){
		player->status.hp += 10;
		if(player->status.hp > player->status.max_hp)
			player->status.hp = player->status.max_hp;
	}

	// fire skill active ? (pressed or counting delay)
	if(skillIsActive[SKILL_FIRE]){
		// if delay has passed...
		if(skillTimeCount[SKILL_FIRE] >= skillDelay[SKILL_FIRE]){
			skillTimeCount[SKILL_FIRE] = 0;
			// attack if button is pressed
			if(skillIsPressed[SKILL_FIRE]){
				block_list *aux=bl_head.next;
				while(aux){
					attack(SKILL_FIRE,player, aux->obj);
					aux=aux->next;
				}
				showSkillAnim(player, SKILL_FIRE);
				SoundPlay(sound[SOUND_ATK0]);
			}
			// deactivate
			skillIsActive[SKILL_FIRE] = false;
		}
		skillTimeCount[SKILL_FIRE]++;
		// activate if button is pressed
		if(skillIsPressed[SKILL_FIRE])
			skillIsActive[SKILL_FIRE] = true;
	}

	// process skill delays...
	if(player->isVisible() && !player->isDead)
	for(i=0;i<NUM_SKILLS;i++){
		if(skillIsActive[i] && i!=SKILL_FIRE){
			skillTimeCount[i]++;
			/*
			// stop animation after x frames
			if(skillTimeCount[i] == 9){
				if(directionIsPressed)
					player->setCurrentAnim(directionAnim[currentButtonId]);
				else
					showSkillAnim(player, -1);
			}
			*/
			// unlock skill after "delay" frames
			if(skillTimeCount[i] == skillDelay[i]){
				skillIsActive[i]=false;
				wViewSetImage(buttonHandle[buttonIdOfSkill[i]], imgbuttonHandle1[buttonIdOfSkill[i]]);
			}
		}
	}

	//auto-target
	if(autoTarget!=NULL && autoAttack && !player->isDead){
		if(inRange(autoNextSkill, player, autoTarget, NULL)){
			//autoTarget=NULL;
			SkillPressed(buttonIdOfSkill[autoNextSkill],1,0,0);
		}else{
			reachObject(player, autoTarget);
		}
	}

	//walk
	if(!player->isDead && !player->isSitting)
		player->_pos += joystick_dir*4*(player->status.speed/100.0);
	player->checkPositionLimits();

	//map movement...
	centerPlayer();

	//update animations 15fps
	if (frameCount % 2 == 0)
	{
		//update panels...
		updateHPbar(100 * player->status.hp / player->status.max_hp); //TODO: update only when damaged
		updateEXPbar(player->status.exp);
		textLogUpdate();

		char level_txt[3];
		sprintf(level_txt,"%2d",player->status.level);
		wTextSetText(panel_level, level_txt);

		//update Z-order...
		updateZorder();

		//update monsters...
		block_list *aux=bl_head.next, *lastaux; int num_monsters=0;
		while(aux){
			//count monsters. TODO: keep it on a global var?
			if(aux->obj->type==TYPE_MOB) num_monsters++;

			//attack/walk...
			((Monster*)aux->obj)->runAI();
			((Monster*)aux->obj)->update();

			lastaux=aux;
			aux=aux->next;
			//delete monster if it was dead
			if(lastaux->obj->isDead && !lastaux->obj->expiryDate) 
				delete ((Monster*)lastaux->obj);
		}

		//map stage completed?
		if(num_monsters==0){
			player->map_stage++;
			map_spawn(&player->map, &player->map_stage);
		}
		
		//update player...
		player->update();

		if(weather) weather->update();

		//game over?
		if(player->isDead && player->expiryDate==0)
			restart_game();

		//frameCount = 0;
	}	
}

#include <math.h>
#include <float.h>

// "server side" stuff
#include "mmo.h"
#include "map.h"
#include "mob.h"
#include "battle.h"
#include "item.h"

// "client side" stuff
#include "Resources.h"
#include "GameObject.h"
#include "Player.h"
#include "Monster.h"
#include "Interface.h"
#include "SaveData.h"

//background
int viewBg;
int viewBlack;

int zoomPer=100;
int zoomFade=-1;

bool autoAttack=true;
int autoNextSkill=-1;
GameObject *autoTarget=NULL;

//fonts
int viewFont[NUM_FONTS];

//panels
int imgPanel[NUM_PANELS];
int viewPanel[NUM_PANELS];
bool panelVisible[NUM_PANELS];
int panelIdOfButton[NUM_BUTTONS];
int panelAnimCurrent=-1;
int panelAnimNext=-1;
int panelAnimCurrent_alpha;
int panelAnimNext_alpha;
int inventorySection=0;

//player
int selectedPlayer=0;
int viewPlayer, viewPlayerHead;
int viewPlayerDir=0;
char viewPlayerRot=' ';
int viewCharSelectPlayer[3], viewCharSelectPlayerHead[3];
int char_create_head=0;

//buttons
int panel_black;
int panel_avatar, view_panel_avatar;
int panel_hp, view_panel_hp;
int panel_hp_hide, view_panel_hp_hide, view_hp_black;
int panel_exp, view_panel_exp;
int panel_exp_hide, view_panel_exp_hide, view_exp_black;
int panel_joystick, view_panel_joystick;
int view_touch;

//text
int edit_name;
int textCharSelectName[3];
int textCharSelectLevel[3];
int textCharSelectClass[3];
int panel_level;
int text_inv_section;
int text_log1[2], text_log2[2];
int text_log_timer=0;

//joystick
Vector2 joystick_dir;

//buttons
int currentButtonId=-1;
int buttonHandle[NUM_BUTTONS];
int imgbuttonHandle1[NUM_BUTTONS];
int imgbuttonHandle2[NUM_BUTTONS];
int buttonHandleInv[NUM_BUTTONS_INV];
int buttonHandleEqu[NUM_BUTTONS_EQU];
int buttonInvId[NUM_BUTTONS_INV];
int buttonEquId[NUM_BUTTONS_EQU];
int skillIdOfButton[NUM_BUTTONS];
int buttonIdOfSkill[NUM_SKILLS];
int buttonMinX[NUM_BUTTONS];
int buttonMaxX[NUM_BUTTONS];
int buttonMinY[NUM_BUTTONS];
int buttonMaxY[NUM_BUTTONS];

int directionAnim[8];
int directionAnimIdle[8];
int directionAnimSit[8];

//--------------------------------------------------------------

class ButtonHistory {
public:
	ButtonHistory(int history_size);
	void addToHistory(int button, int timestamp);
	bool checkCombo(std::vector<int> combo, int time_between);
private:
	int lastButtonIdx;
	std::vector<int> history;
	std::vector<int> timestamps;
};

ButtonHistory::ButtonHistory(int history_size){
	for(int i=0 ; i<history_size ; i++){
		history.push_back(-1);
		timestamps.push_back(-1);
	}
	lastButtonIdx = history_size-1;
}
void ButtonHistory::addToHistory(int button, int timestamp){
	if(history.size()<=0) return;
	lastButtonIdx = (lastButtonIdx+1) % history.size();
	history[lastButtonIdx] = button;
	timestamps[lastButtonIdx] = timestamp;
	
	int i=lastButtonIdx, c = 3;
	while(c>0){
		printf("%d ", history[i]);
		i = (i>0)?(i-1):(history.size()-1);
		c--;
	}
	printf("\n");
}
bool ButtonHistory::checkCombo(std::vector<int> combo, int time_between){
	if(combo.size()>history.size()) 
		return false;
	int i=lastButtonIdx, j, c=combo.size()-1;
	while(c>=0){
		if(history[i]!=combo[c])
			return false;
		c--;
		j=i; 
		i = (i>0)?(i-1):(history.size()-1);
		if(c>=0 && timestamps[j]-timestamps[i] > time_between)
			return false;
	}
	return true;
}

ButtonHistory buttonHistory(3);

//--------------------------------------------------------------

void load_menu(){
	//menu...
	Mp3Loop(music[SONG_MENU]);
	panelVisible[PANEL_MENU] = true;
}

void load_interface_resources(){  //imgbuttonhandles (1 for normal, 2 for down)
	//main menu
	imgPanel[PANEL_MENU] = wImageAdd("Images/Interface/menu_background.png");
	imgbuttonHandle1[MENU1] = wImageAdd("Images/Interface/play01.png");
	imgbuttonHandle1[MENU2] = wImageAdd("Images/Interface/online01.png");
	imgbuttonHandle1[MENU3] = wImageAdd("Images/Interface/options01.png");
	imgbuttonHandle1[MENU4] = wImageAdd("Images/Interface/help01.png");
	imgbuttonHandle1[MENU5] = wImageAdd("Images/Interface/shop01.png");
	imgbuttonHandle2[MENU1] = wImageAdd("Images/Interface/play02.png");
	imgbuttonHandle2[MENU2] = wImageAdd("Images/Interface/online02.png");
	imgbuttonHandle2[MENU3] = wImageAdd("Images/Interface/options02.png");
	imgbuttonHandle2[MENU4] = wImageAdd("Images/Interface/help02.png");
	imgbuttonHandle2[MENU5] = wImageAdd("Images/Interface/shop02.png");
	//loading panel
	imgPanel[PANEL_LOADING] = wImageAdd("Images/iXileMenus/iXile_Load.png");
	//pause
	imgbuttonHandle1[BTN_PAUSE] = wImageAdd("Images/iXileMenus/Pause_Button.png");
	imgbuttonHandle2[BTN_PAUSE] = wImageAdd("Images/iXileMenus/Pause_Button_Select.png");
	//sit and stand
	imgbuttonHandle1[BTN_SIT] = wImageAdd("Images/iXileMenus/Sit_Button.png");
	imgbuttonHandle2[BTN_SIT] = wImageAdd("Images/iXileMenus/Stand_Button.png");
	//pause menu
	imgPanel[PANEL_PAUSE] = wImageAdd("Images/iXileMenus/PauseMenu/iXile_Menu_Screen_Static.png");
	imgbuttonHandle1[BTN_PAUSE_CHAR] = wImageAdd("Images/iXileMenus/PauseMenu/Character_Button.png");
	imgbuttonHandle2[BTN_PAUSE_CHAR] = wImageAdd("Images/iXileMenus/PauseMenu/Character_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_EQUIP] = wImageAdd("Images/iXileMenus/PauseMenu/Equipment_Button.png");
	imgbuttonHandle2[BTN_PAUSE_EQUIP] = wImageAdd("Images/iXileMenus/PauseMenu/Equipment_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_QUESTS] = wImageAdd("Images/iXileMenus/PauseMenu/Quests_Button.png");
	imgbuttonHandle2[BTN_PAUSE_QUESTS] = wImageAdd("Images/iXileMenus/PauseMenu/Quests_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_SKILLS] = wImageAdd("Images/iXileMenus/PauseMenu/Skills_Button.png");
	imgbuttonHandle2[BTN_PAUSE_SKILLS] = wImageAdd("Images/iXileMenus/PauseMenu/Skills_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_SYSTEM] = wImageAdd("Images/iXileMenus/PauseMenu/System_Button.png");
	imgbuttonHandle2[BTN_PAUSE_SYSTEM] = wImageAdd("Images/iXileMenus/PauseMenu/System_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_RETURN] = wImageAdd("Images/iXileMenus/ReturnButton/Return_Button.png");
	imgbuttonHandle2[BTN_PAUSE_RETURN] = wImageAdd("Images/iXileMenus/ReturnButton/Return_Button_Select.png");
	//character select menu
	imgPanel[PANEL_CHAR_SELECT] = wImageAdd("Images/iXileMenus/CharSelect/iXile_Character_Selection_Screen_BLANK.png");
	imgbuttonHandle1[BTN_CHAR_S_SELECT1] = wImageAdd("Images/iXileMenus/CharSelect/Character_Select_WND.png");
	imgbuttonHandle2[BTN_CHAR_S_SELECT1] = wImageAdd("Images/iXileMenus/CharSelect/Character_Select_WND_Select.png");
	imgbuttonHandle1[BTN_CHAR_S_SELECT3] = imgbuttonHandle1[BTN_CHAR_S_SELECT2]=imgbuttonHandle1[BTN_CHAR_S_SELECT1];
	imgbuttonHandle2[BTN_CHAR_S_SELECT3] = imgbuttonHandle2[BTN_CHAR_S_SELECT2]=imgbuttonHandle2[BTN_CHAR_S_SELECT1];
	imgbuttonHandle1[BTN_CHAR_S_RETURN] = imgbuttonHandle1[BTN_PAUSE_RETURN];
	imgbuttonHandle2[BTN_CHAR_S_RETURN] = imgbuttonHandle2[BTN_PAUSE_RETURN];
	//character create menu
	imgPanel[PANEL_CHAR_CREATE] = wImageAdd("Images/iXileMenus/CharCreation/iXile_Character_Creation_Screen.png");
	imgbuttonHandle1[BTN_CHAR_C_LEFT1] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Left.png");
	imgbuttonHandle2[BTN_CHAR_C_LEFT1] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Left_Select.png");
	imgbuttonHandle1[BTN_CHAR_C_LEFT4] = imgbuttonHandle1[BTN_CHAR_C_LEFT3]=imgbuttonHandle1[BTN_CHAR_C_LEFT2]=imgbuttonHandle1[BTN_CHAR_C_LEFT1];
	imgbuttonHandle2[BTN_CHAR_C_LEFT4] = imgbuttonHandle2[BTN_CHAR_C_LEFT3]=imgbuttonHandle2[BTN_CHAR_C_LEFT2]=imgbuttonHandle2[BTN_CHAR_C_LEFT1];
	imgbuttonHandle1[BTN_CHAR_C_RIGHT1] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Right.png");
	imgbuttonHandle2[BTN_CHAR_C_RIGHT1] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Right_Select.png");
	imgbuttonHandle1[BTN_CHAR_C_RIGHT4] = imgbuttonHandle1[BTN_CHAR_C_RIGHT3]=imgbuttonHandle1[BTN_CHAR_C_RIGHT2]=imgbuttonHandle1[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle2[BTN_CHAR_C_RIGHT4] = imgbuttonHandle2[BTN_CHAR_C_RIGHT3]=imgbuttonHandle2[BTN_CHAR_C_RIGHT2]=imgbuttonHandle2[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle1[BTN_CHAR_C_NAME] = wImageAdd("Images/iXileMenus/CharCreation/Name_Plate_Entry.png");
	imgbuttonHandle2[BTN_CHAR_C_NAME] = wImageAdd("Images/iXileMenus/CharCreation/Name_Plate_Entry_Select.png");
	imgbuttonHandle1[BTN_CHAR_C_CREATE] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Right.png");
	imgbuttonHandle2[BTN_CHAR_C_CREATE] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Right_Select.png");
	imgbuttonHandle1[BTN_CHAR_C_RETURN] = imgbuttonHandle1[BTN_PAUSE_RETURN];
	imgbuttonHandle2[BTN_CHAR_C_RETURN] = imgbuttonHandle2[BTN_PAUSE_RETURN];
	//inventory menu
	imgPanel[PANEL_INV] = wImageAdd("Images/iXileMenus/Inventory/iXile_Character_Menu.png");
	imgbuttonHandle1[BTN_INV_ROTLEFT] = wImageAdd("Images/iXileMenus/Inventory/Arrow_Rotate_Left_Button.png");
	imgbuttonHandle2[BTN_INV_ROTLEFT] = wImageAdd("Images/iXileMenus/Inventory/Arrow_Rotate_Left_Button_Select.png");
	imgbuttonHandle1[BTN_INV_ROTRIGHT] = wImageAdd("Images/iXileMenus/Inventory/Arrow_Rotate_Right_Button.png");
	imgbuttonHandle2[BTN_INV_ROTRIGHT] = wImageAdd("Images/iXileMenus/Inventory/Arrow_Rotate_Right_Button_Select.png");
	imgbuttonHandle1[BTN_INV_PREV] = imgbuttonHandle1[BTN_CHAR_C_LEFT1];
	imgbuttonHandle2[BTN_INV_PREV] = imgbuttonHandle2[BTN_CHAR_C_LEFT1];
	imgbuttonHandle1[BTN_INV_NEXT] = imgbuttonHandle1[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle2[BTN_INV_NEXT] = imgbuttonHandle2[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle1[BTN_INV_HOTKEY1] = wImageAdd("Images/iXileMenus/Inventory/Hotkey_Button.png");
	imgbuttonHandle2[BTN_INV_HOTKEY1] = wImageAdd("Images/iXileMenus/Inventory/Hotkey_Button_Select.png");
	imgbuttonHandle1[BTN_INV_HOTKEY2] = imgbuttonHandle1[BTN_INV_HOTKEY1];
	imgbuttonHandle2[BTN_INV_HOTKEY2] = imgbuttonHandle2[BTN_INV_HOTKEY1];
	imgbuttonHandle1[BTN_INV_RETURN] = imgbuttonHandle1[BTN_PAUSE_RETURN];
	imgbuttonHandle2[BTN_INV_RETURN] = imgbuttonHandle2[BTN_PAUSE_RETURN];
	//game buttons
	imgbuttonHandle1[BTN_ZOOMOUT] = imgbuttonHandle1[BTN_CHAR_C_LEFT1];
	imgbuttonHandle2[BTN_ZOOMOUT] = imgbuttonHandle2[BTN_CHAR_C_LEFT1];
	imgbuttonHandle1[BTN_ZOOMIN] = imgbuttonHandle1[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle2[BTN_ZOOMIN] = imgbuttonHandle2[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle1[JOYSTICK] = wImageAdd("Images/Interface/interface_joystick_pod.png");
	imgbuttonHandle2[JOYSTICK] = imgbuttonHandle1[JOYSTICK];
	imgbuttonHandle1[PB_LEFT_BTN] = wImageAdd("Images/controller/left1.png");
	imgbuttonHandle2[PB_LEFT_BTN] = wImageAdd("Images/controller/left2.png");
	imgbuttonHandle1[PB_RIGHT_BTN] = wImageAdd("Images/controller/right1.png");
	imgbuttonHandle2[PB_RIGHT_BTN] = wImageAdd("Images/controller/right2.png");
	imgbuttonHandle1[PB_TOP_BTN] = wImageAdd("Images/controller/up1.png");
	imgbuttonHandle2[PB_TOP_BTN] = wImageAdd("Images/controller/up2.png");
	imgbuttonHandle1[PB_DOWN_BTN] = wImageAdd("Images/controller/down1.png");
	imgbuttonHandle2[PB_DOWN_BTN] = wImageAdd("Images/controller/down2.png");
	imgbuttonHandle1[PB_NW_BTN] = wImageAdd("Images/controller/nw1.png");
	imgbuttonHandle2[PB_NW_BTN] = wImageAdd("Images/controller/nw2.png");
	imgbuttonHandle1[PB_NE_BTN] = wImageAdd("Images/controller/ne1.png");
	imgbuttonHandle2[PB_NE_BTN] = wImageAdd("Images/controller/ne2.png");
	imgbuttonHandle1[PB_SW_BTN] = wImageAdd("Images/controller/sw1.png");
	imgbuttonHandle2[PB_SW_BTN] = wImageAdd("Images/controller/sw2.png");
	imgbuttonHandle1[PB_SE_BTN] = wImageAdd("Images/controller/se1.png");
	imgbuttonHandle2[PB_SE_BTN] = wImageAdd("Images/controller/se2.png");
	imgbuttonHandle1[PB_FIRE_BTN] = wImageAdd("Images/controller/fire1.png");
	imgbuttonHandle2[PB_FIRE_BTN] = wImageAdd("Images/controller/fire2.png");
	imgbuttonHandle1[PB_BESERK_BTN] = wImageAdd("Images/controller/beserk1.png");
	imgbuttonHandle2[PB_BESERK_BTN] = wImageAdd("Images/controller/beserk2.png");
	imgbuttonHandle1[PB_BUSTER_BTN] = wImageAdd("Images/controller/buster1.png");
	imgbuttonHandle2[PB_BUSTER_BTN] = wImageAdd("Images/controller/buster2.png");
	imgbuttonHandle1[PB_REPEL_BTN] = wImageAdd("Images/controller/repel1.png");
	imgbuttonHandle2[PB_REPEL_BTN] = wImageAdd("Images/controller/repel2.png");
	imgbuttonHandle1[PB_TELEPORT_BTN] = wImageAdd("Images/controller/teleport1.png");
	imgbuttonHandle2[PB_TELEPORT_BTN] = wImageAdd("Images/controller/teleport2.png");
}

void load_interface(){
	//where should this be?
	for(int i=0;i<NUM_BUTTONS;i++)
		panelIdOfButton[i] = -1;

	//fonts
	viewFont[FONT_NORMAL] = FontAdd("Arial","Regular",18,0xFFFFFF); //FontAdd("Images/font-numbers");
	viewFont[FONT_CHAR] = FontAdd("Arial","Regular",16,0x000000);
	viewFont[FONT_CHAR2] = FontAdd("Arial","Regular",11,0x000000);
	viewFont[FONT_DMG] = FontAdd("Arial","Regular",18,0xFF0000);
	viewFont[FONT_DMG_SHADOW] = FontAdd("Arial","Regular",18,0x000000);
	viewFont[FONT_CRIT] = FontAdd("Arial","Regular",26,0xFFFF00);
	viewFont[FONT_CRIT_SHADOW] = FontAdd("Arial","Regular",26,0x000000);
	viewFont[FONT_LOG1] = FontAdd("Arial","Regular",20,0xECCA11);
	//viewFont[FONT_LOG2] = FontAdd("Arial","Regular",20,0x1E9A05);
	viewFont[FONT_LOG_SHADOW] = FontAdd("Arial","Regular",20,0x000000);

	//add panels
	view_panel_avatar = wViewAdd("Images/Interface/interface_avatar.png", 0, 0);
	view_panel_hp = wViewAdd("Images/Interface/progress_life.png", 81, 6);
	view_panel_hp_hide = wViewAdd("Images/Interface/hit1.png", 213, 2);
	view_hp_black = wViewAdd("Images/Interface/black.png", 213, 9);
	view_panel_exp = wViewAdd("Images/Interface/progress_exp.png", 83, 31);
	view_panel_exp_hide = wViewAdd("Images/Interface/exp1.png", 211, 27);
	view_exp_black = wViewAdd("Images/Interface/black.png", 211, 30);

	panel_level = wTextAdd(4,1,"99", viewFont[FONT_NORMAL]);
	updateHPbar(100);

	//text log
	text_log1[1] = wTextAdd(190+2,280+2,"Loot: ", viewFont[FONT_LOG_SHADOW]);
	text_log1[0] = wTextAdd(190,280,"Loot: ", viewFont[FONT_LOG1]);
	text_log2[1] = wTextAdd(240+2,280+2,"          ", viewFont[FONT_LOG_SHADOW]);
	text_log2[0] = wTextAdd(240,280,"          ", viewFont[FONT_LOG1]);
	TextSetVisible(text_log1[0], 0);
	TextSetVisible(text_log1[1], 0);
	TextSetVisible(text_log2[0], 0);
	TextSetVisible(text_log2[1], 0);

	//add actual control buttons with respective callback functions
	buttonHandle[BTN_ZOOMIN] = wViewAdd("Images/dummy.png", 40, 65, ZoomPressed, BTN_ZOOMIN);
	buttonHandle[BTN_ZOOMOUT] = wViewAdd("Images/dummy.png", 0, 65, ZoomPressed, BTN_ZOOMOUT);
	buttonHandle[JOYSTICK] = wViewAdd("Images/dummy.png", 9, 199, JoystickPressed, JOYSTICK);
	buttonHandle[PB_LEFT_BTN] = wViewAdd("Images/dummy.png", 13, 241);
	buttonHandle[PB_RIGHT_BTN] = wViewAdd("Images/dummy.png", 82, 241);
	buttonHandle[PB_TOP_BTN] = wViewAdd("Images/dummy.png", 50, 210);
	buttonHandle[PB_DOWN_BTN] = wViewAdd("Images/dummy.png", 50, 265);
	buttonHandle[PB_NW_BTN] = wViewAdd("Images/dummy.png", 28, 218);
	buttonHandle[PB_NE_BTN] = wViewAdd("Images/dummy.png", 92, 218);
	buttonHandle[PB_SW_BTN] = wViewAdd("Images/dummy.png", 26, 282);
	buttonHandle[PB_SE_BTN] = wViewAdd("Images/dummy.png", 91, 281);
	buttonHandle[PB_FIRE_BTN] = wViewAdd("Images/dummy.png", 410, 255, SkillPressed, PB_FIRE_BTN);
	buttonHandle[PB_BESERK_BTN] = wViewAdd("Images/dummy.png", 350, 275, SkillPressed, PB_BESERK_BTN);
	buttonHandle[PB_BUSTER_BTN] = wViewAdd("Images/dummy.png", 363, 234, SkillPressed, PB_BUSTER_BTN);
	buttonHandle[PB_REPEL_BTN] = wViewAdd("Images/dummy.png", 394, 205, SkillPressed, PB_REPEL_BTN);
	buttonHandle[PB_TELEPORT_BTN] = wViewAdd("Images/dummy.png", 435, 195, SkillPressed, PB_TELEPORT_BTN);
	buttonHandle[BTN_SIT] = wViewAdd("Images/dummy.png", 435, 45, SitPressed, BTN_SIT);
	buttonHandle[BTN_PAUSE] = wViewAdd("Images/dummy.png", 435, 7, PausePressed, BTN_PAUSE);

	//black background
	viewBlack = wViewAdd("Images/Interface/black.png", 0, 0);
	ViewSetSize(viewBlack,480,320);

	// martim: panels
	viewPanel[PANEL_MENU] = wViewAdd(imgPanel[PANEL_MENU], 0, 0);
	viewPanel[PANEL_LOADING] = wViewAdd(imgPanel[PANEL_LOADING], 0, 0);
	viewPanel[PANEL_PAUSE] = wViewAdd(imgPanel[PANEL_PAUSE], 0, 0);
	viewPanel[PANEL_CHAR_SELECT] = wViewAdd(imgPanel[PANEL_CHAR_SELECT], 0, 0);
	viewPanel[PANEL_CHAR_CREATE] = wViewAdd(imgPanel[PANEL_CHAR_CREATE], 0, 0);
	viewPanel[PANEL_INV] = wViewAdd(imgPanel[PANEL_INV], 0, 0);
	buttonHandle[MENU1] = wViewAdd("Images/Interface/play01.png", 370, 10, MenuPressed, MENU1);
    buttonHandle[MENU2] = wViewAdd("Images/Interface/online01.png", 370, 70, MenuPressed, MENU2);
    buttonHandle[MENU3] = wViewAdd("Images/Interface/options01.png", 370, 130, MenuPressed, MENU3);
    buttonHandle[MENU4] = wViewAdd("Images/Interface/help01.png", 370, 190, MenuPressed, MENU4);
    buttonHandle[MENU5] = wViewAdd("Images/Interface/shop01.png", 370, 250, MenuPressed, MENU5);
	buttonHandle[BTN_PAUSE_CHAR] = wViewAdd("Images/dummy.png", 240-145, 45, MenuPressed, BTN_PAUSE_CHAR);
	buttonHandle[BTN_PAUSE_EQUIP] = wViewAdd("Images/dummy.png", 240-145, 90, MenuPressed, BTN_PAUSE_EQUIP);
	buttonHandle[BTN_PAUSE_QUESTS] = wViewAdd("Images/dummy.png", 240-145, 135, MenuPressed, BTN_PAUSE_QUESTS);
	buttonHandle[BTN_PAUSE_SKILLS] = wViewAdd("Images/dummy.png", 240-145, 180, MenuPressed, BTN_PAUSE_SKILLS);
	buttonHandle[BTN_PAUSE_SYSTEM] = wViewAdd("Images/dummy.png", 240-145, 225, MenuPressed, BTN_PAUSE_SYSTEM);
	buttonHandle[BTN_PAUSE_RETURN] = wViewAdd("Images/dummy.png", 48, 258, MenuPressed, BTN_PAUSE_RETURN);
	buttonHandle[BTN_CHAR_C_LEFT1] = wViewAdd("Images/dummy.png", 250, 116, MenuPressed, BTN_CHAR_C_LEFT1);
	buttonHandle[BTN_CHAR_C_RIGHT1] = wViewAdd("Images/dummy.png", 338, 116, MenuPressed, BTN_CHAR_C_RIGHT1);
	buttonHandle[BTN_CHAR_C_LEFT2] = wViewAdd("Images/dummy.png", 250, 146, MenuPressed, BTN_CHAR_C_LEFT2);
	buttonHandle[BTN_CHAR_C_RIGHT2] = wViewAdd("Images/dummy.png", 338, 146, MenuPressed, BTN_CHAR_C_RIGHT2);
	buttonHandle[BTN_CHAR_C_LEFT3] = wViewAdd("Images/dummy.png", 250, 176, MenuPressed, BTN_CHAR_C_LEFT3);
	buttonHandle[BTN_CHAR_C_RIGHT3] = wViewAdd("Images/dummy.png", 338, 176, MenuPressed, BTN_CHAR_C_RIGHT3);
	buttonHandle[BTN_CHAR_C_LEFT4] = wViewAdd("Images/dummy.png", 250, 206, MenuPressed, BTN_CHAR_C_LEFT4);
	buttonHandle[BTN_CHAR_C_RIGHT4] = wViewAdd("Images/dummy.png", 338, 206, MenuPressed, BTN_CHAR_C_RIGHT4);
	buttonHandle[BTN_CHAR_C_NAME] = wViewAdd("Images/dummy.png", 125, 215, MenuPressed, BTN_CHAR_C_NAME);
	buttonHandle[BTN_CHAR_C_CREATE] = wViewAdd("Images/dummy.png", 420, 290, MenuPressed, BTN_CHAR_C_CREATE);
	buttonHandle[BTN_CHAR_C_RETURN] = wViewAdd("Images/dummy.png", 48, 258, MenuPressed, BTN_CHAR_C_RETURN);
	buttonHandle[BTN_CHAR_S_SELECT1] = wViewAdd("Images/dummy.png", 85, 116, MenuPressed, BTN_CHAR_S_SELECT1);
	buttonHandle[BTN_CHAR_S_SELECT2] = wViewAdd("Images/dummy.png", 190, 116, MenuPressed, BTN_CHAR_S_SELECT2);
	buttonHandle[BTN_CHAR_S_SELECT3] = wViewAdd("Images/dummy.png", 295, 116, MenuPressed, BTN_CHAR_S_SELECT3);
	buttonHandle[BTN_CHAR_S_RETURN] = wViewAdd("Images/dummy.png", 48, 258, MenuPressed, BTN_CHAR_S_RETURN);
	buttonHandle[BTN_INV_ROTLEFT] = wViewAdd("Images/dummy.png", 195, 170, MenuPressed, BTN_INV_ROTLEFT);
	buttonHandle[BTN_INV_ROTRIGHT] = wViewAdd("Images/dummy.png", 252, 170, MenuPressed, BTN_INV_ROTRIGHT);
	buttonHandle[BTN_INV_PREV] = wViewAdd("Images/dummy.png", 295, 241, MenuPressed, BTN_INV_PREV);
	buttonHandle[BTN_INV_NEXT] = wViewAdd("Images/dummy.png", 355, 241, MenuPressed, BTN_INV_NEXT);
	buttonHandle[BTN_INV_HOTKEY1] = wViewAdd("Images/dummy.png", 200, 270, MenuPressed, BTN_INV_HOTKEY1);
	buttonHandle[BTN_INV_HOTKEY2] = wViewAdd("Images/dummy.png", 250, 270, MenuPressed, BTN_INV_HOTKEY2);
	buttonHandle[BTN_INV_RETURN] = wViewAdd("Images/dummy.png", 48, 258, MenuPressed, BTN_INV_RETURN);
	panelIdOfButton[MENU1] = PANEL_MENU;
	panelIdOfButton[MENU2] = PANEL_MENU;
	panelIdOfButton[MENU3] = PANEL_MENU;
	panelIdOfButton[MENU4] = PANEL_MENU;
	panelIdOfButton[MENU5] = PANEL_MENU;
	panelIdOfButton[BTN_PAUSE_CHAR] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_EQUIP] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_QUESTS] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_SKILLS] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_SYSTEM] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_RETURN] = PANEL_PAUSE;
	panelIdOfButton[BTN_CHAR_C_LEFT1] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_LEFT2] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_LEFT3] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_LEFT4] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RIGHT1] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RIGHT2] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RIGHT3] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RIGHT4] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_NAME] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RETURN] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_CREATE] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_S_SELECT1] = PANEL_CHAR_SELECT;
	panelIdOfButton[BTN_CHAR_S_SELECT2] = PANEL_CHAR_SELECT;
	panelIdOfButton[BTN_CHAR_S_SELECT3] = PANEL_CHAR_SELECT;
	panelIdOfButton[BTN_CHAR_S_RETURN] = PANEL_CHAR_SELECT;
	panelIdOfButton[BTN_INV_ROTLEFT] = PANEL_INV;
	panelIdOfButton[BTN_INV_ROTRIGHT] = PANEL_INV;
	panelIdOfButton[BTN_INV_PREV] = PANEL_INV;
	panelIdOfButton[BTN_INV_NEXT] = PANEL_INV;
	panelIdOfButton[BTN_INV_HOTKEY1] = PANEL_INV;
	panelIdOfButton[BTN_INV_HOTKEY2] = PANEL_INV;
	panelIdOfButton[BTN_INV_RETURN] = PANEL_INV;

	// inventory
	for(int i=0 ; i<NUM_BUTTONS_INV ; i++){
		buttonInvId[i] = -1;
		buttonHandleInv[i] = wViewAdd("Images/dummy.png", 297+35*(i%3), 97+35*(i/3), InventoryPressed, i);
		wViewSetVisible(buttonHandleInv[i],0);
	}
	for(int i=0 ; i<NUM_BUTTONS_EQU ; i++){
		buttonEquId[i] = -1;
		buttonHandleEqu[i] = wViewAdd("Images/dummy.png", 89+35*(i%3), 98+45*(i/3), EquipmentPressed, i);
		wViewSetVisible(buttonHandleEqu[i],0);
	}

	// inventory and char create player preview
	viewPlayer = wViewAdd("Images/dummy.png",0,0);
	viewPlayerHead = wViewAdd("Images/dummy.png",0,0);
	wViewSetVisible(viewPlayer,0);
	wViewSetVisible(viewPlayerHead,0);

	// char select player preview
	for(int i=0 ; i<3 ; i++){
		viewCharSelectPlayer[i] = wViewAdd("Images/dummy.png",0,0);
		viewCharSelectPlayerHead[i] = wViewAdd("Images/dummy.png",0,0);
		wViewSetVisible(viewCharSelectPlayer[i],0);
		wViewSetVisible(viewCharSelectPlayerHead[i],0);
	}

	// martim: text boxes
	edit_name = EditAdd(135, 217, 75);
	EditSetLabel(edit_name, "Name");
	textCharSelectName[0] = wTextAdd(123,100,"     ",viewFont[FONT_CHAR]);
	textCharSelectName[1] = wTextAdd(227,100,"     ",viewFont[FONT_CHAR]);
	textCharSelectName[2] = wTextAdd(333,100,"     ",viewFont[FONT_CHAR]);
	textCharSelectLevel[0] = wTextAdd(130,213,"     ",viewFont[FONT_CHAR2]);
	textCharSelectLevel[1] = wTextAdd(235,213,"     ",viewFont[FONT_CHAR2]);
	textCharSelectLevel[2] = wTextAdd(340,213,"     ",viewFont[FONT_CHAR2]);
	textCharSelectClass[0] = wTextAdd(130,223,"     ",viewFont[FONT_CHAR2]);
	textCharSelectClass[1] = wTextAdd(235,223,"     ",viewFont[FONT_CHAR2]);
	textCharSelectClass[2] = wTextAdd(340,223,"     ",viewFont[FONT_CHAR2]);
	text_inv_section = wTextAdd(336,240,"00", viewFont[FONT_CHAR]);
	TextSetVisible(text_inv_section, 0);

	// hide panels
	panelSetVisible(PANEL_LOADING, false, TRANSITION_BLACK);
	panelSetVisible(PANEL_PAUSE, false, TRANSITION_BLACK);
	panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
	panelSetVisible(PANEL_CHAR_CREATE, false, TRANSITION_BLACK);
	panelSetVisible(PANEL_INV, false, TRANSITION_BLACK);

	// martim: screen touch
	view_touch = TouchAdd(0,0,480,320,function_touch,TOUCH);
	TouchSetVisible(view_touch,0);

	// martim: which animation when button is pressed
	directionAnim[PB_LEFT_BTN] = (int)WALK_LEFT;
	directionAnim[PB_RIGHT_BTN] = (int)WALK_RIGHT;
	directionAnim[PB_TOP_BTN] = (int)WALK_UP;
	directionAnim[PB_DOWN_BTN] = (int)WALK_DOWN;
	directionAnim[PB_NW_BTN] = (int)WALK_NW;
	directionAnim[PB_NE_BTN] = (int)WALK_NE;
	directionAnim[PB_SW_BTN] = (int)WALK_SW;
	directionAnim[PB_SE_BTN] = (int)WALK_SE;
	directionAnimIdle[PB_LEFT_BTN] = (int)IDLE_LEFT;
	directionAnimIdle[PB_RIGHT_BTN] = (int)IDLE_RIGHT;
	directionAnimIdle[PB_TOP_BTN] = (int)IDLE_UP;
	directionAnimIdle[PB_DOWN_BTN] = (int)IDLE_DOWN;
	directionAnimIdle[PB_NW_BTN] = (int)IDLE_NW;
	directionAnimIdle[PB_NE_BTN] = (int)IDLE_NE;
	directionAnimIdle[PB_SW_BTN] = (int)IDLE_SW;
	directionAnimIdle[PB_SE_BTN] = (int)IDLE_SE;
	directionAnimSit[PB_LEFT_BTN] = (int)SIT_LEFT;
	directionAnimSit[PB_RIGHT_BTN] = (int)SIT_RIGHT;
	directionAnimSit[PB_TOP_BTN] = (int)SIT_UP;
	directionAnimSit[PB_DOWN_BTN] = (int)SIT_DOWN;
	directionAnimSit[PB_NW_BTN] = (int)SIT_NW;
	directionAnimSit[PB_NE_BTN] = (int)SIT_NE;
	directionAnimSit[PB_SW_BTN] = (int)SIT_SW;
	directionAnimSit[PB_SE_BTN] = (int)SIT_SE;

	// martim: buttonId
	currentButtonId=-1;

	// martim: link skills to buttons
	skillIdOfButton[PB_FIRE_BTN]=SKILL_FIRE;
	skillIdOfButton[PB_BESERK_BTN]=SKILL_BESERK;
	skillIdOfButton[PB_BUSTER_BTN]=SKILL_BUSTER;
	skillIdOfButton[PB_REPEL_BTN]=SKILL_REPEL;
	skillIdOfButton[PB_TELEPORT_BTN]=SKILL_TELEPORT;
	buttonIdOfSkill[SKILL_FIRE]=PB_FIRE_BTN;
	buttonIdOfSkill[SKILL_BESERK]=PB_BESERK_BTN;
	buttonIdOfSkill[SKILL_BUSTER]=PB_BUSTER_BTN;
	buttonIdOfSkill[SKILL_REPEL]=PB_REPEL_BTN;
	buttonIdOfSkill[SKILL_TELEPORT]=PB_TELEPORT_BTN;

	// martim: set button images and get bounding box of each button
	for(int i=0;i<NUM_BUTTONS-1;i++){ //(last button is TOUCH, no image)
		wViewSetImage(buttonHandle[i], imgbuttonHandle1[i]);
		buttonMinX[i] = ViewGetx(buttonHandle[i]);
		buttonMinY[i] = ViewGety(buttonHandle[i]);
		buttonMaxX[i] = buttonMinX[i] + ViewGetWidth(buttonHandle[i]);
		buttonMaxY[i] = buttonMinY[i] + ViewGetHeight(buttonHandle[i]);
	}
}

void load_interface_player(){
	// inventory and char create player preview
	wViewSetImage(viewPlayer, playerResources[0]._frames[IDLE_DOWN][0]);
	wViewSetImage(viewPlayerHead, playerResources[0]._heads[0][DIR_S]);
	wViewSetVisible(viewPlayer,0);
	wViewSetVisible(viewPlayerHead,0);

	// char select player preview
	for(int i=0 ; i<3 ; i++){
		wViewSetImage(viewCharSelectPlayer[i], playerResources[0]._frames[IDLE_DOWN][0]);
		wViewSetImage(viewCharSelectPlayerHead[i], playerResources[0]._heads[0][DIR_S]);
		wViewSetVisible(viewCharSelectPlayer[i],0);
		wViewSetVisible(viewCharSelectPlayerHead[i],0);
	}
}

void updateHPbar(int hp){
	//hp in percentage...
	if(hp>96){
		wViewSetVisible(view_hp_black,0);
		wViewSetVisible(view_panel_hp_hide,0);
		wViewSetVisible(view_panel_hp,1);
	}else if(hp>=1){
		wViewSetVisible(view_hp_black,1);
		wViewSetVisible(view_panel_hp_hide,1);
		wViewSetVisible(view_panel_hp,1);
		int pos = (214-96*(214-92)/(96-1)) + hp*(214-92)/(96-1);
		//change hp bar
		ViewSetxy(view_hp_black, pos, 9);
		ViewSetSize(view_hp_black, 214-pos, 15);
	}else{
		wViewSetVisible(view_hp_black,0);
		wViewSetVisible(view_panel_hp_hide,1);
		wViewSetVisible(view_panel_hp,0);
	}
}

void updateEXPbar(int exp){
	//exp in percentage...
	if(exp>96){
		wViewSetVisible(view_exp_black,0);
		wViewSetVisible(view_panel_exp_hide,0);
		wViewSetVisible(view_panel_exp,1);
	}else if(exp>=1){
		wViewSetVisible(view_exp_black,1);
		wViewSetVisible(view_panel_exp_hide,1);
		wViewSetVisible(view_panel_exp,1);
		int pos = (212-96*(212-92)/(96-1)) + exp*(212-92)/(96-1);
		//change exp bar
		ViewSetxy(view_exp_black, pos, 30);
		ViewSetSize(view_exp_black, 212-pos, 10);
	}else{
		wViewSetVisible(view_exp_black,0);
		wViewSetVisible(view_panel_exp_hide,1);
		wViewSetVisible(view_panel_exp,0);
	}
}

void textLogUpdate(){
	if(text_log_timer<1) return;
	text_log_timer--;
	if(text_log_timer==0){
		TextSetVisible(text_log1[0], 0);
		TextSetVisible(text_log1[1], 0);
		TextSetVisible(text_log2[0], 0);
		TextSetVisible(text_log2[1], 0);
	}
}

void textLogLoot(int item_id){
	wTextSetText(text_log1[0], "Loot:");
	wTextSetText(text_log1[1], "Loot:");
	wTextSetText(text_log2[0], item_db[item_id].name);
	wTextSetText(text_log2[1], item_db[item_id].name);
	TextSetVisible(text_log1[0], 1);
	TextSetVisible(text_log1[1], 1);
	TextSetVisible(text_log2[0], 1);
	TextSetVisible(text_log2[1], 1);
	text_log_timer=60;
}

void centerPlayer(){
	int bgminx=-(sizeBg.x*zoomPer/100.0)+480;
	int bgminy=-(sizeBg.y*zoomPer/100.0)+320;
	posBg.x = -(player->_pos.x*zoomPer/100.0) + 240;
	posBg.y = -(player->_pos.y*zoomPer/100.0) + 200;

	if(posBg.x < bgminx) posBg.x = bgminx;
	if(posBg.y < bgminy) posBg.y = bgminy;
	
	if(posBg.x > 0) posBg.x = 0;
	if(posBg.y > 0) posBg.y = 0;

	ViewSetxy(viewBg, posBg.x, posBg.y);	
}

void mapPanning(){
	int playerw=ViewGetWidth(player->handles.view[0]);
	int playerh=ViewGetHeight(player->handles.view[0]);
	int bgy=ViewGety(viewBg);
	int bgminy=-ViewGetHeight(viewBg)+320;
	int bgx=ViewGetx(viewBg);
	int bgminx=-ViewGetWidth(viewBg)+480;
	//map panning when near borders
	if(player->_pos.x < 0.5*playerw - posBg.x){
		if(bgx!=0)
			ViewSetxy(viewBg, (bgx<-4 ? bgx+4 : 0), bgy);
	}
	if(player->_pos.x > 480-0.5*playerw - posBg.x){
		if(bgx!=bgminx)
			ViewSetxy(viewBg, (bgx>bgminx+4 ? bgx-4 : bgminx), bgy);
	}
	bgx=ViewGetx(viewBg);
	if(player->_pos.y < playerh - posBg.y){
		if(bgy!=0)
			ViewSetxy(viewBg, bgx, (bgy<-4 ? bgy+4 : 0));
	}
	if(player->_pos.y > 320 - posBg.y){
		if(bgy!=bgminy)
			ViewSetxy(viewBg, bgx, (bgy>bgminy+4 ? bgy-4 : bgminy));
	}
	//move objects as well
	posBg.x = ViewGetx(viewBg);
	posBg.y = ViewGety(viewBg);
}

void showSkillAnim(GameObject *obj, int skill){
	if(obj->isDead) return;

	if(obj->type==TYPE_PLAYER){
		((Player*)obj)->useSkill(skill);
		return;
	}

	switch(skill){
	case -1:
		if(obj->direction==RIGHT) obj->setCurrentAnim(IDLE_RIGHT);
		else obj->setCurrentAnim(IDLE_LEFT);
		break;
	case SKILL_FIRE:
		if(obj->direction==RIGHT) obj->setCurrentAnim(SHOOT_RIGHT);
		else obj->setCurrentAnim(SHOOT_LEFT);
		break;
	case SKILL_BESERK:
		if(obj->direction==RIGHT) obj->setCurrentAnim(BESERK_RIGHT);
		else obj->setCurrentAnim(BESERK_LEFT);
		break;
	case SKILL_BUSTER:
		if(obj->direction==RIGHT) obj->setCurrentAnim(BUSTER_RIGHT);
		else obj->setCurrentAnim(BUSTER_LEFT);
		break;
	case SKILL_REPEL:
		if(obj->direction==RIGHT) obj->setCurrentAnim(REPEL_RIGHT);
		else obj->setCurrentAnim(REPEL_LEFT);
		break;
	case SKILL_TELEPORT:
		if(obj->direction==RIGHT) obj->setCurrentAnim(TELEPORT_RIGHT);
		else obj->setCurrentAnim(TELEPORT_LEFT);
		break;
	}
}

void showDamage(GameObject *dst, int damage, int font){
	sprintf(dst->textStr,"%d", damage);
	//hide last dmg
	if(dst->textTimer>0 && dst->dmgType!=font){
		if(font==FONT_DMG){
			TextSetVisible(dst->textHandlerCrit[0], 0);
			TextSetVisible(dst->textHandlerCrit[1], 0);
		}else if(font==FONT_CRIT){
			TextSetVisible(dst->textHandlerDmg[0], 0);
			TextSetVisible(dst->textHandlerDmg[1], 0);
		}
	}
	//show new dmg
	if(font==FONT_DMG){
		wTextSetText(dst->textHandlerDmg[0], dst->textStr);
		TextSetVisible(dst->textHandlerDmg[0], 1);
		wTextSetText(dst->textHandlerDmg[1], dst->textStr);
		TextSetVisible(dst->textHandlerDmg[1], 1);
	}else if(font==FONT_CRIT){
		wTextSetText(dst->textHandlerCrit[0], dst->textStr);
		TextSetVisible(dst->textHandlerCrit[0], 1);
		wTextSetText(dst->textHandlerCrit[1], dst->textStr);
		TextSetVisible(dst->textHandlerCrit[1], 1);
	}
	dst->dmgType = font;
	dst->textTimer = 30;
}

int SkillPressed(int id,int event,int x,int y){
	//button history
	if(event==1 && x!=0 && y!=0){
		buttonHistory.addToHistory(id, frameCount);
		std::vector<int> combo; 
		combo.push_back(PB_BESERK_BTN);
		combo.push_back(PB_BUSTER_BTN);
		combo.push_back(PB_BESERK_BTN);
		if(buttonHistory.checkCombo(combo, 30)){
			printf("Beserk-Buster-Beserk combo!\n");
			if(player) player->addItem(8,1);
		}
	}

	if(!gameStarted || !player || player->isDead || player->lockedForAnimation) return 0;

	int skill_id = skillIdOfButton[id];

	// auto-target...
	if(autoAttack && skill_id!=SKILL_TELEPORT){
		//only if skill cooldown is over. fire can always be re-cast
		if(skill_id==SKILL_FIRE || !skillIsActive[skill_id]){
			if(event==2){
				return 0;
			}else if(event==1){
				block_list *aux=bl_head.next;
				float d, min_d=FLT_MAX;

				autoTarget=NULL;
				autoNextSkill=skill_id;
				// is no monster in range?
				while(aux){
					if(aux->obj->isDead){ aux=aux->next; continue; }
					if(inRange(skill_id, player, aux->obj, &d)){
						autoTarget=NULL;
						break;
					}else{
						//target the closest monster
						if(d<min_d){
							min_d=d;
							autoTarget=aux->obj;
						}
					}
					aux=aux->next;
				}
			}

			if(autoTarget!=NULL){
				//buttons are only shown as pressed when we are actually attacking
				player->skillInUse=-1;
				skillIsPressed[SKILL_FIRE]=false;
				wViewSetImage(buttonHandle[PB_FIRE_BTN], imgbuttonHandle1[PB_FIRE_BTN]);
				return 0;
			}
		}
	}

	// fire skill is special (spammable)
	if(skill_id==SKILL_FIRE){
/*		//if moved outside button [removed] fire sticks now...
		if(event==3 || !(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id])){
			skillIsPressed[skill_id]=false;
			wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
			if(player->direction==RIGHT) player->setCurrentAnim(IDLE_RIGHT);
			else player->setCurrentAnim(IDLE_LEFT);
		}else{
*/
			if(!skillIsActive[skill_id])
				skillTimeCount[skill_id]=skillDelay[skill_id];
			skillIsPressed[skill_id]=true;
			skillIsActive[skill_id]=true;
			wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
//		}
		return 0;
	}
		
	//if you press any skill, stop attack
	if(event==1 && skillIsPressed[SKILL_FIRE]){
		skillIsPressed[SKILL_FIRE]=false;
		player->skillInUse=-1;
		wViewSetImage(buttonHandle[PB_FIRE_BTN], imgbuttonHandle1[PB_FIRE_BTN]);
		if(player->direction==RIGHT) player->setCurrentAnim(IDLE_RIGHT);
		else player->setCurrentAnim(IDLE_LEFT);
	}

	// if we're not touching anymore...
	if(event==3)
		skillIsPressed[skill_id]=false;

	// if cooldown is over, and we released the button...
	if((event == 1 || event == 2) && skillIsActive[skill_id]==false /*&& skillIsPressed[skill_id]==false*/)
	{
		skillIsPressed[skill_id]=true;
		skillIsActive[skill_id]=true;
		skillTimeCount[skill_id]=0;

		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);

		block_list *aux=bl_head.next;

		switch(id){
		case PB_FIRE_BTN:
			break;
		case PB_BESERK_BTN:
			while(aux){
				attack(skill_id, player, aux->obj);
				aux=aux->next;
			}
			break;
		case PB_BUSTER_BTN:
			while(aux){
				attack(skill_id, player, aux->obj);
				aux=aux->next;
			}
			break;
		case PB_REPEL_BTN:
			while(aux){
				attack(skill_id, player, aux->obj);
				aux=aux->next;
			}
			break;
		case PB_TELEPORT_BTN:
			player->setVisible(false);
			TouchSetVisible(view_touch,1);
			for(int i=0 ; i<MENU1 ; i++)
				wViewSetVisible(buttonHandle[i],0);
			wViewSetVisible(buttonHandle[JOYSTICK],0);
			break;
		}

		showSkillAnim(player, skill_id);
	}

	return 0;
}

int JoystickPressed(int id,int event,int x,int y)
{
	if(!gameStarted || id!=JOYSTICK || player->lockedForAnimation) return 0;

	//if you press any direction, stop attack
	if(event==1 && skillIsPressed[SKILL_FIRE]){
		player->skillInUse=-1;
		skillIsPressed[SKILL_FIRE]=false;
		wViewSetImage(buttonHandle[PB_FIRE_BTN], imgbuttonHandle1[PB_FIRE_BTN]);
		//player->setCurrentAnim(directionAnimIdle[player->dir]);
	}

	//if you press any direction, stop auto-target
	if(event==1 && autoAttack){
		autoTarget=NULL;
	}

	int w = ViewGetWidth(buttonHandle[id])/2;
	int dx = x - (ViewGetx(buttonHandle[id]) + w);
	int dy = y - (ViewGety(buttonHandle[id]) + ViewGetHeight(buttonHandle[id])/2);

	joystick_dir.x = dx;
	joystick_dir.y = dy;

	// moved away...
	if((event==1 || event==2) && joystick_dir.Length()>w+4){
		//event=3; //old version
		joystick_dir = joystick_dir/(w+4); //new version. let user go out of the joystick area
	}

/*	if(event==1 || event==2){
		//moved away...
		if( !(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]) ){
			event=3;
		}
	}
*/
	//release touch...
	if(event==3){
		joystick_dir.x = 0;
		joystick_dir.y = 0;

		if(currentButtonId!=-1){
			player->idle();
			wViewSetImage(buttonHandle[currentButtonId], imgbuttonHandle1[currentButtonId]);
			currentButtonId=-1;
		}
		return 0;
	}

	
	joystick_dir.Unitize();

	float ang = 180 + atan2((float)dy,(float)-dx)*180/3.14;
	int lastid = currentButtonId;
	int headDir;

	if(ang <= 22.5 || ang > 337.5){
		headDir = DIR_E;
		currentButtonId = PB_RIGHT_BTN;
	}else if(ang <= 67.5 && ang > 22.5){
		headDir = DIR_NE;
		currentButtonId = PB_NE_BTN;
	}else if(ang <= 112.5 && ang > 67.5){
		headDir = DIR_N;
		currentButtonId = PB_TOP_BTN;
	}else if(ang <= 157.5 && ang > 112.5){
		headDir = DIR_NW;
		currentButtonId = PB_NW_BTN;
	}else if(ang <= 202.5 && ang > 157.5){
		headDir = DIR_W;
		currentButtonId = PB_LEFT_BTN;
	}else if(ang <= 247.5 && ang > 202.5){
		headDir = DIR_SW;
		currentButtonId = PB_SW_BTN;
	}else if(ang <= 292.5 && ang > 247.5){
		headDir = DIR_S;
		currentButtonId = PB_DOWN_BTN;
	}else{
		headDir = DIR_SE;
		currentButtonId = PB_SE_BTN;
	}

	if(lastid!=currentButtonId){
		if(lastid!=-1) wViewSetImage(buttonHandle[lastid], imgbuttonHandle1[lastid]);
		wViewSetImage(buttonHandle[currentButtonId], imgbuttonHandle2[currentButtonId]);
	}
	player->setDir(headDir);
	player->setHeadDir(headDir);
	if(!player->isSitting) player->setCurrentAnim(directionAnim[player->dir]);
	else player->setCurrentAnim(directionAnimSit[player->dir]);

	return 0;
}

int DirectionPressed(int id,int event,int x,int y)
{
	if(!player || player->isDead || !gameStarted) return 0;
	int i;

	// which button are we pressing?
	if(currentButtonId==-1)
		currentButtonId=id;
	else 
		id=currentButtonId;

	//if(currentButtonId<0 || currentButtonId>=NUM_DIRECTIONS) return 0;

	// if we're not touching anymore...
	if(event==3)
		currentButtonId=-1;

	// touch move...
	if(event==2){
		if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
			;
		}else{
			//if we moved to another button...
			for(i=0;i<NUM_DIRECTIONS;i++){
				if(x>buttonMinX[i] && x<buttonMaxX[i] && y>buttonMinY[i] && y<buttonMaxY[i]){
					wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
					currentButtonId=id=i; break;
				}
			}
			//no button...
			if(i>=NUM_DIRECTIONS)
				event=3;
		}
	}

	if(event==3){
		directionIsPressed = false;
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		player->setCurrentAnim(directionAnimIdle[id]);
	}else{
		directionIsPressed = true;
		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
		player->setCurrentAnim(directionAnim[id]);

		switch(id){
		case PB_LEFT_BTN:
		case PB_NW_BTN:
		case PB_SW_BTN:
			player->direction = LEFT; break;
		case PB_RIGHT_BTN:
		case PB_NE_BTN:
		case PB_SE_BTN:
			player->direction = RIGHT; break;
		case PB_TOP_BTN:
		case PB_DOWN_BTN:
			break;
		}
	}

	return 0;
}

int function_touch(int id,int event,int x,int y){
	if(id!=TOUCH || !gameStarted) return 0;

	// if we touch
	if(event==1){
		TouchSetVisible(view_touch,0);
		player->setVisible(true);
		player->_pos.x = -posBg.x + x;
		player->_pos.y = -posBg.y + y;
		centerPlayer();
		showSkillAnim(player, SKILL_TELEPORT);

		for(int i=0 ; i<MENU1 ; i++)
			wViewSetVisible(buttonHandle[i],1);
		wViewSetVisible(buttonHandle[JOYSTICK],1);
	}

	return 0;
}

void panelSetVisible(int panel, bool vis, int transition_type){
	//black background for transition
	if(transition_type==TRANSITION_BLACK){
		wViewSetVisible(viewBlack,1);
		ViewSetAlpha(viewBlack,100);
	}else if(transition_type==TRANSITION_NORMAL){
		wViewSetVisible(viewBlack,0);
	}

	//zoom (exception)
	if(panel==PANEL_CLEAR){
		//player
		if(player) player->setVisible(!vis);
		//monsters
		block_list *aux=bl_head.next;
		while(aux){
			aux->obj->setVisible(!vis);
			aux=aux->next;
		}
		return;
	}

	//text and items disappear right away
	if(!vis && panelAnimCurrent==-1){
		if(panel==PANEL_INV){
			inventoryDisplay(false);
		}else if(panel==PANEL_CHAR_CREATE){
			EditSetVisible(edit_name,false);
		}else if(panel==PANEL_CHAR_SELECT){
			for(int i=0 ; i<3 ; i++){
				TextSetVisible(textCharSelectName[i],false);
				TextSetVisible(textCharSelectLevel[i],false);
				TextSetVisible(textCharSelectClass[i],false);
			}
		}
	}

	//alpha...
	if(appLoaded){
		//set up transitions
		if(vis){
			panelAnimNext = panel;
			panelAnimNext_alpha = 0;
			panelSetAlpha(panel, 0);
		}else{
			if(panelAnimCurrent==-1){
				panelAnimCurrent = panel;
				panelAnimCurrent_alpha = 100;
				return;
			}else{
				panelAnimCurrent=-1;
			}
		}
	}

	//if we're opening a panel or going back to game...
	if(vis || panelAnimNext==-1){
		//player
		if(player) player->setVisible(!vis);
		//monsters
		block_list *aux=bl_head.next;
		while(aux){
			aux->obj->setVisible(!vis);
			aux=aux->next;
		}
	}

	//panel
	wViewSetVisible(viewPanel[panel], vis);
	panelVisible[panel]=vis;

	//buttons
	for(int i=0 ; i<NUM_BUTTONS ; i++){
		if(panelIdOfButton[i]==panel)
			wViewSetVisible(buttonHandle[i], vis);
	}
	//player previews
	if(panel==PANEL_INV){
		wViewSetVisible(viewPlayer,vis);
		wViewSetVisible(viewPlayerHead,vis);
		ViewSetxy(viewPlayer,240-0.5*ViewGetWidth(viewPlayer),180-ViewGetHeight(viewPlayer));
		ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
		//if(!vis) inventoryDisplay(false);
	}else if(panel==PANEL_CHAR_CREATE){
		wViewSetVisible(viewPlayer,vis);
		wViewSetVisible(viewPlayerHead,vis);
		ViewSetxy(viewPlayer,165-0.5*ViewGetWidth(viewPlayer),210-ViewGetHeight(viewPlayer));
		ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
	}else if(panel==PANEL_CHAR_SELECT){
		int head_, view_, name_, level_, class_;
		char str[10];
		for(int i=0 ; i<3 ; i++){
			head_=viewCharSelectPlayerHead[i]; view_=viewCharSelectPlayer[i]; name_=textCharSelectName[i]; level_=textCharSelectLevel[i]; class_=textCharSelectClass[i];
			if(gdGlobal.player_slot[i].free){
				//...
			}else{
				wViewSetVisible(view_,vis);
				wViewSetVisible(head_,vis);
				sprintf(str,"%d",gdGlobal.player_slot[i].level);
				ViewSetxy(view_,85+48+(105*i)-0.5*ViewGetWidth(view_),218-ViewGetHeight(view_));
				ViewSetxy(head_,ViewGetx(view_),ViewGety(view_));
				wTextSetText(name_,gdGlobal.player_slot[i].name);
				wTextSetText(level_,str);
				wTextSetText(class_, const_cast<char*> CLASS_STR(gdGlobal.player_slot[i].id_class) );
				if(vis) wViewSetImage(head_, playerResources[0]._heads[gdGlobal.player_slot[i].id_head][DIR_S]);
			}
		}
	}
}

void panelSetAlpha(int panel, int alpha){
	//zoom (exception)
	if(panel==PANEL_CLEAR){
		ViewSetAlpha(viewBlack, alpha);
		return;
	}

	//items and text show when alpha=100
	if(alpha==100){
		if(panel==PANEL_INV){
			inventoryDisplay(true);
		}else if(panel==PANEL_CHAR_CREATE){
			EditSetVisible(edit_name,true);
		}else if(panel==PANEL_CHAR_SELECT){
			for(int i=0 ; i<3 ; i++){
				TextSetVisible(textCharSelectName[i],true);
				TextSetVisible(textCharSelectLevel[i],true);
				TextSetVisible(textCharSelectClass[i],true);
			}
		}
	}
	//panel
	ViewSetAlpha(viewPanel[panel], alpha);
	//buttons
	for(int i=0 ; i<NUM_BUTTONS ; i++){
		if(panelIdOfButton[i]==panel)
			ViewSetAlpha(buttonHandle[i], alpha);
	}
	//player previews
	if(panel==PANEL_INV){
		ViewSetAlpha(viewPlayer,alpha);
		ViewSetAlpha(viewPlayerHead,alpha);
		ViewSetxy(viewPlayer,240-0.5*ViewGetWidth(viewPlayer),180-ViewGetHeight(viewPlayer));
		ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
		//if(alpha==100) inventoryDisplay(true);
	}else if(panel==PANEL_CHAR_CREATE){
		ViewSetAlpha(viewPlayer,alpha);
		ViewSetAlpha(viewPlayerHead,alpha);
		ViewSetxy(viewPlayer,165-0.5*ViewGetWidth(viewPlayer),210-ViewGetHeight(viewPlayer));
		ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
		//EditSetVisible(edit_name,vis);
	}else if(panel==PANEL_CHAR_SELECT){
		for(int i=0 ; i<3 ; i++){
			if(gdGlobal.player_slot[i].free){
				//...
			}else{
				ViewSetAlpha(viewCharSelectPlayer[i],alpha);
				ViewSetAlpha(viewCharSelectPlayerHead[i],alpha);
			}
		}
	}
}

void inventoryDisplay(bool vis){
	if(!player) return;
	
	if(!vis){
		int i;
		TextSetVisible(text_inv_section,0);
		for(i=0 ; i<NUM_BUTTONS_INV ; i++){
			wViewSetVisible(buttonHandleInv[i], 0);
		}
		for(i=0 ; i<NUM_BUTTONS_EQU ; i++){
			wViewSetVisible(buttonHandleEqu[i], 0);
		}
		return;
	}
	
	TextSetVisible(text_inv_section,1);
	char str[5]; sprintf(str, "%02d", inventorySection+1);
	wTextSetText(text_inv_section, str);

	int i, inv=0, num_notequipped=0;
	for(i=0 ; i<MAX_INVENTORY ; i++){
		if(player->inventory[i].item_id!=-1)
			if(!player->inventory[i].equip){
				//not equipped
				if(num_notequipped < inventorySection*NUM_BUTTONS_INV){
					num_notequipped++;
					continue;
				}
				if(inv>=NUM_BUTTONS_INV) 
					continue;
				buttonInvId[inv] = i;
				wViewSetImage(buttonHandleInv[inv], inventoryResources[ player->inventory[i].item_id ]);
				wViewSetVisible(buttonHandleInv[inv], 1);
				inv++;
			}else{
				//equipped
				int id=player->inventory[i].item_id;
				int equip_index=item_db[id].equip_index;
				buttonEquId[equip_index] = i;
				wViewSetImage(buttonHandleEqu[equip_index], inventoryResources[id]);
				wViewSetVisible(buttonHandleEqu[equip_index], 1);
			}
	}
	if(inv<NUM_BUTTONS_INV)
		for( ; inv<NUM_BUTTONS_INV ; inv++){
			buttonInvId[inv] = -1;
			wViewSetVisible(buttonHandleInv[inv], 0);
		}
}

void inventoryUnequip(int equip_index){
	if(!player) return;

	int inv_index = buttonEquId[equip_index];
	buttonEquId[equip_index] = -1;
	
	ViewSetxy(buttonHandleEqu[equip_index], 89+35*(equip_index%3), 98+45*(equip_index/3));
	wViewSetVisible(buttonHandleEqu[equip_index], 0);

	player->inventory[inv_index].equip = false;
	player->computeStats();
	inventoryDisplay(true);
}

void inventoryEquip(int inv){
	if(!player) return;

	int inv_index = buttonInvId[inv];
	int equip_index = item_db[player->inventory[inv_index].item_id].equip_index;
	//if something else is already equipped
	if(buttonEquId[equip_index]!=-1) inventoryUnequip(equip_index);
	
	buttonEquId[equip_index] = inv_index;
	ViewSetxy(buttonHandleEqu[equip_index], 89+35*(equip_index%3), 98+45*(equip_index/3));
	wViewSetVisible(buttonHandleEqu[equip_index], 1);

	player->inventory[inv_index].equip = true;
	player->computeStats();
	inventoryDisplay(true);
}

int EquipmentPressed(int id,int event,int x,int y){
	if(!player) return 0;
	if(event==2){
		ViewSetxy(buttonHandleEqu[id],x-12,y-12);
	}else if(event==3){
		ViewSetxy(buttonHandleEqu[id], 89+35*(id%3), 98+45*(id/3));
		if(!(x>84 && x<189 && y>64 && y<247)){
			inventoryUnequip(id);
		}
	}
	return 0;
}

int InventoryPressed(int id,int event,int x,int y){
	if(!player) return 0;
	//move item with finger
	if(event==2){
		ViewSetxy(buttonHandleInv[id],x-12,y-12);
	}else if(event==3){
		/*
		int i;
		for(i=BTN_INV_HOTKEY1 ; i<=BTN_INV_HOTKEY2 ; i++){
			if(x>buttonMinX[i] && x<buttonMaxX[i] && y>buttonMinY[i] && y<buttonMaxY[i]){
				ViewSetxy(buttonHandleInv[id], buttonMinX[i], buttonMinY[i]);
				break;
			}
		}
		*/
		int j;
		for(j=0 ; j<9 ; j++)
			if(x>84 && x<189 && y>64 && y<247){
				inventoryEquip(id);
				break;
			}
		//if(i>BTN_INV_HOTKEY2)
		ViewSetxy(buttonHandleInv[id], 297+35*(id%3), 97+35*(id/3));
	}
	return 0;
}

int MenuPressed(int id,int event,int x,int y){
	// which button are we pressing?
	if(currentButtonId==-1)
		currentButtonId=id;
	else 
		id=currentButtonId;

	// fading in process?
	if(panelAnimCurrent!=-1 || panelAnimNext!=-1)
		return 0;

	// init
	viewPlayerRot=' ';

	// check touch
	if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
		// if we're touching
		if(event==1 || event==2){
			switch(currentButtonId){
			case BTN_INV_ROTLEFT:
				viewPlayerRot='L';
				break;
			case BTN_INV_ROTRIGHT:
				viewPlayerRot='R';
				break;
			}
		}
	}else{
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		//if we moved to another button...
		int i;
		for(i=MENU1;i<=BTN_PAUSE_RETURN;i++)
			if(panelIdOfButton[i]!=-1 && panelVisible[panelIdOfButton[i]] && x>buttonMinX[i] && x<buttonMaxX[i] && y>buttonMinY[i] && y<buttonMaxY[i]){
				wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
				currentButtonId=id=i; break;
			}
		if(i>BTN_PAUSE_RETURN)
			return 0;
	}

	// if we release
	if(event==3){
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		currentButtonId=-1;
		
		switch(id){
		case MENU1:
			levelToLoad=-1;
			if(!avatarsLoaded)
				panelSetVisible(PANEL_LOADING, true, TRANSITION_BLACK);
			else
				panelSetVisible(PANEL_CHAR_SELECT, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_MENU, false, TRANSITION_BLACK);
			break;
		case MENU2:
			AppExit();
			exit(0);
			break;
		case BTN_PAUSE_EQUIP:
			wViewSetImage(viewPlayerHead, player->_heads[viewPlayerDir]);
			panelSetVisible(PANEL_INV, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_PAUSE, false, TRANSITION_BLACK);
			break;
		case BTN_PAUSE_SYSTEM:
			panelSetVisible(PANEL_MENU, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_PAUSE, false, TRANSITION_BLACK);
			unload_game();
			Mp3Loop(music[SONG_MENU]);
			break;
		case BTN_PAUSE_RETURN:
			panelSetVisible(PANEL_PAUSE, false, TRANSITION_NORMAL);
			gameStarted = !gameStarted;
			break;
		case BTN_INV_PREV:
			if(inventorySection>0) inventorySection--;
			inventoryDisplay(true);
			break;
		case BTN_INV_NEXT:
			inventorySection++;
			if(inventorySection>MAX_INVENTORY/12) inventorySection=0;
			inventoryDisplay(true);
			break;
		case BTN_INV_RETURN:
			panelSetVisible(PANEL_PAUSE, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_INV, false, TRANSITION_BLACK);
			break;
		case BTN_CHAR_C_RETURN:
			panelSetVisible(PANEL_CHAR_SELECT, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_CHAR_CREATE, false, TRANSITION_BLACK);
			break;
		case BTN_CHAR_S_RETURN:
			panelSetVisible(PANEL_MENU, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
			break;
		case BTN_CHAR_S_SELECT1:
			selectedPlayer=0;
			if(gdGlobal.player_slot[0].free){
				char_create_head=0;
				wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
				panelSetVisible(PANEL_CHAR_CREATE, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
			}else{
				panelSetVisible(PANEL_LOADING, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
				levelToLoad=0;
				resourcesLoaded=false;
			}
			break;
		case BTN_CHAR_S_SELECT2:
			selectedPlayer=1;
			if(gdGlobal.player_slot[0].free){
				char_create_head=0;
				wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
				panelSetVisible(PANEL_CHAR_CREATE, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
			}else{
				panelSetVisible(PANEL_LOADING, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
				levelToLoad=0;
				resourcesLoaded=false;
			}
			break;
		case BTN_CHAR_S_SELECT3:
			selectedPlayer=2;
			if(gdGlobal.player_slot[0].free){
				char_create_head=0;
				wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
				panelSetVisible(PANEL_CHAR_CREATE, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
			}else{
				panelSetVisible(PANEL_LOADING, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
				levelToLoad=0;
				resourcesLoaded=false;
			}
			break;
		case BTN_CHAR_C_LEFT3:
			if(char_create_head>0) char_create_head--;
			wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
			break;
		case BTN_CHAR_C_RIGHT3:
			if(char_create_head<NUM_HEADS-1) char_create_head++;
			wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
			break;
		case BTN_CHAR_C_CREATE:
			EditGetText(edit_name, gdGlobal.player_slot[selectedPlayer].name);
			if(gdGlobal.player_slot[selectedPlayer].name[0]=='\0')
				return 0;
			gdGlobal.player_slot[selectedPlayer].free=false;
			gdGlobal.player_slot[selectedPlayer].level=1;
			gdGlobal.player_slot[selectedPlayer].id_class=CLASS_KNIGHT;
			gdGlobal.player_slot[selectedPlayer].id_sex=0;
			gdGlobal.player_slot[selectedPlayer].id_body=0;
			gdGlobal.player_slot[selectedPlayer].id_head=char_create_head;
			for(int i=0 ; i<MAX_INVENTORY ; i++){
				gdGlobal.player_slot[selectedPlayer].inventory[i].item_id = -1;
				gdGlobal.player_slot[selectedPlayer].inventory[i].equip = false;
				gdGlobal.player_slot[selectedPlayer].inventory[i].amount = 0;
			}
			//for(int i=0 ; i<16 ; i++){
			//	gdGlobal.player_slot[selectedPlayer].inventory[i].item_id = i/5;
			//	gdGlobal.player_slot[selectedPlayer].inventory[i].equip = false;
			//	gdGlobal.player_slot[selectedPlayer].inventory[i].amount = 1;
			//}
			panelSetVisible(PANEL_CHAR_SELECT, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_CHAR_CREATE, false, TRANSITION_BLACK);
			break;
		default:
			break;
		}
	}

	return 0;
}

int ZoomPressed(int id,int event,int x,int y){
	if(id!=BTN_ZOOMIN && id!=BTN_ZOOMOUT) return 0;

	if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
	}else{
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		return 0;
	}

	// if we release
	if(event==3){
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);

		if(id==BTN_ZOOMIN) 
			zoomPer += 20;
		else if(id==BTN_ZOOMOUT) 
			zoomPer -= 20;
		
		if(zoomPer<80) zoomPer=80;
		if(zoomPer>120) zoomPer=120;

		zoomFade = 100;
		gameStarted=false;
	}

	return 0;
}

int SitPressed(int id,int event,int x,int y){
	if(!gameStarted || !player) return 0;

	if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
		;
	}else{
		;
		return 0;
	}
	
	// stop attacking
	if(event==1 && skillIsPressed[SKILL_FIRE]){
		player->skillInUse=-1;
		skillIsPressed[SKILL_FIRE]=false;
		wViewSetImage(buttonHandle[PB_FIRE_BTN], imgbuttonHandle1[PB_FIRE_BTN]);
	}else if(event==1 && autoAttack){
		autoTarget=NULL;
	}

	// if we release
	if(event==3){
		player->isSitting = !player->isSitting;
		for(int i=PB_FIRE_BTN ; i<BTN_ZOOMIN ; i++)
			wViewSetVisible(buttonHandle[i], !player->isSitting);
		if(player->isSitting){
			wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
			player->setCurrentAnim(directionAnimSit[player->dir]);
		}else{
			wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
			player->setCurrentAnim(directionAnimIdle[player->dir]);
		}
	}

	return 0;
}

int PausePressed(int id,int event,int x,int y){
	if(id!=BTN_PAUSE || !gameStarted) return 0;

	if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
	}else{
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		return 0;
	}
	
	// if we release
	if(event==3){
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		panelSetVisible(PANEL_PAUSE, true, TRANSITION_NORMAL);
		gameStarted = !gameStarted;
	}

	return 0;
}

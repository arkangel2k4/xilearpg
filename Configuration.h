extern int skillDelay[NUM_SKILLS];
extern int skillRange[NUM_SKILLS];

extern int mobStats[NUM_MOBS][NUM_STATS];
extern int itemStats[NUM_ITEMS][NUM_STATS];

extern int statusDuration[NUM_STATUS];

void load_config();
void load_map_config();
void load_skill_config();
void load_mob_config();
void load_item_config();
void load_status_config();

void load_mob_config_single(int id, int atk, int vit, int cri, int def, int exp, int speed, bool loop);
void load_item_config_single(int id, char* name, int equip_index, int atk, int vit, int cri, int def, int exp, int speed);

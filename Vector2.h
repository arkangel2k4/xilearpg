#ifndef _Vector2_H_
#define _Vector2_H_
/*****************************************************************************/

class Vector2
{

public:

	//constructors & destructors
	Vector2();
	Vector2(double x, double y);
	Vector2(const Vector2 &v);
	~Vector2();

	//assignment & comparison
    Vector2& operator= (const Vector2 &v);
    bool operator== (const Vector2 &v) const;
    bool operator!= (const Vector2 &v) const;

    //arithmetic & unary operations
	Vector2 operator+ (const Vector2 &v) const;
	Vector2 operator- (const Vector2 &v) const;
	Vector2 operator* (double f) const;
	Vector2 operator/ (double f) const;
	Vector2 operator- () const;

	//arithmetic with assignment operations
	Vector2& operator+= (const Vector2 &v);
	Vector2& operator-= (const Vector2 &v);
	Vector2& operator*= (double f);
	Vector2& operator/= (double f);
 
    //vector operations
    double Length() const;
    double LengthSquared() const;
    double Dot(const Vector2 &v) const;
    double Unitize();
	double Distance(const Vector2 &v) const;
	double DistanceSquared(const Vector2 &v) const;

 	//linear interpolation
	static Vector2 Lerp(const Vector2 &v0, const Vector2 &v1, double T);

	//special vectors
    static const Vector2 ZERO;
    static const Vector2 UNIT_X;
    static const Vector2 UNIT_Y;
 
	//special constants
	static const double EPSILON;
	static const double _INFINITY;

	//data
	double x;
	double y;
};

//these need to be global
Vector2 operator* (double f, const Vector2 &v);
Vector2 operator/ (double f, const Vector2 &v);

/*****************************************************************************/
#endif
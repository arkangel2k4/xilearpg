//buttons...
enum BUTTON_IDS
{
/*
	PB_LEFT_BTN,
	PB_RIGHT_BTN,
	PB_TOP_BTN,
	PB_DOWN_BTN,
	PB_NW_BTN,
	PB_NE_BTN,
	PB_SW_BTN,
	PB_SE_BTN,
*/
	PB_DOWN_BTN,
	PB_SW_BTN,
	PB_LEFT_BTN,
	PB_NW_BTN,
	PB_TOP_BTN,
	PB_NE_BTN,
	PB_RIGHT_BTN,
	PB_SE_BTN,
	PB_FIRE_BTN,
	PB_BESERK_BTN,
	PB_BUSTER_BTN,
	PB_REPEL_BTN,
	PB_TELEPORT_BTN,
	BTN_ZOOMIN,
	BTN_ZOOMOUT,
	BTN_SIT,
	BTN_PAUSE,
	MENU1,
	MENU2,
	MENU3,
	MENU4,
	MENU5,
	BTN_CHAR_C_LEFT1,
	BTN_CHAR_C_RIGHT1,
	BTN_CHAR_C_LEFT2,
	BTN_CHAR_C_RIGHT2,
	BTN_CHAR_C_LEFT3,
	BTN_CHAR_C_RIGHT3,
	BTN_CHAR_C_LEFT4,
	BTN_CHAR_C_RIGHT4,
	BTN_CHAR_C_NAME,
	BTN_CHAR_C_CREATE,
	BTN_CHAR_C_RETURN,
	BTN_CHAR_S_SELECT1,
	BTN_CHAR_S_SELECT2,
	BTN_CHAR_S_SELECT3,
	BTN_CHAR_S_RETURN,
	BTN_INV_ROTLEFT,
	BTN_INV_ROTRIGHT,
	BTN_INV_PREV,
	BTN_INV_NEXT,
	BTN_INV_HOTKEY1,
	BTN_INV_HOTKEY2,
	BTN_INV_RETURN,
	BTN_PAUSE_CHAR,
	BTN_PAUSE_EQUIP,
	BTN_PAUSE_QUESTS,
	BTN_PAUSE_SKILLS,
	BTN_PAUSE_SYSTEM,
	BTN_PAUSE_RETURN,
	JOYSTICK,
	//this one should stay just before NUM_BUTTONS:
	TOUCH,
	//leave the line below:
	NUM_BUTTONS
};
#define NUM_BUTTONS_INV 12
#define NUM_BUTTONS_EQU 9

//panels
enum PANEL_IDS
{
	PANEL_MENU,
	PANEL_LOADING,
	PANEL_PAUSE,
	PANEL_CLEAR,
	PANEL_CHAR_SELECT,
	PANEL_CHAR_CREATE,
	PANEL_INV,
	//leave the line below:
	NUM_PANELS
};
enum TRANSITION_TYPE
{
	TRANSITION_NOP, //NOP=do nothing
	TRANSITION_NORMAL,
	TRANSITION_BLACK,
	//leave the line below:
	NUM_TRANSITIONS
};

//fonts
enum FONT_IDS
{
	FONT_NORMAL,
	FONT_CHAR,
	FONT_CHAR2,
	FONT_DMG,
	FONT_DMG_SHADOW,
	FONT_CRIT,
	FONT_CRIT_SHADOW,
	FONT_LOG1,
	FONT_LOG2,
	FONT_LOG_SHADOW,
	//leave the line below:
	NUM_FONTS
};

//external to Interface
void load_game();
void unload_game();
void load_resources();
extern unsigned int frameCount;
extern Player* player;
extern bool appLoaded;
extern bool avatarsLoaded;
extern bool resourcesLoaded;
extern bool gameStarted;
extern int levelToLoad;
extern int skillTimeCount[NUM_SKILLS];
extern int skillDelay[NUM_SKILLS];
extern int skillRange[NUM_SKILLS];
extern bool skillIsActive[NUM_SKILLS];
extern bool skillIsPressed[NUM_SKILLS];
extern bool directionIsPressed;
//--end

void load_interface_resources();
void load_interface();
void load_menu();
void load_interface_player();

void panelSetVisible(int panel, bool vis, int transition_type);
void panelSetAlpha(int panel, int alpha);
void updateHPbar(int hp);
void updateEXPbar(int exp);
void textLogUpdate();
void textLogLoot(int item_id);
void centerPlayer();
void mapPanning();
void showSkillAnim(GameObject *obj, int skill);
void showDamage(GameObject *dst, int damage, int font);

void inventoryDisplay(bool vis);

int EquipmentPressed(int id,int event,int x,int y);
int InventoryPressed(int id,int event,int x,int y);
int MenuPressed(int id,int event,int x,int y);
int SkillPressed(int id,int event,int x,int y);
int JoystickPressed(int id,int event,int x,int y);
int DirectionPressed(int id,int event,int x,int y);
int function_touch(int id,int event,int x,int y);
int ZoomPressed(int id,int event,int x,int y);
int SitPressed(int id,int event,int x,int y);
int PausePressed(int id,int event,int x,int y);

extern int viewBg;
extern int viewBlack;

extern int zoomPer;
extern int zoomFade;

extern bool autoAttack;
extern int autoNextSkill;
extern GameObject *autoTarget;

extern int viewFont[NUM_FONTS];
extern int viewPanel[NUM_PANELS];
extern int imgPanel[NUM_PANELS];
extern bool panelVisible[NUM_PANELS];
extern int panelIdOfButton[NUM_BUTTONS];
extern int panelAnimCurrent;
extern int panelAnimNext;
extern int panelAnimCurrent_alpha;
extern int panelAnimNext_alpha;

extern int selectedPlayer;
extern int viewPlayer, viewPlayerHead;
extern int viewPlayerDir;
extern char viewPlayerRot;

extern int panel_black;
extern int panel_avatar, view_panel_avatar;
extern int panel_hp, view_panel_hp;
extern int panel_hp_hide, view_panel_hp_hide, view_hp_black;
extern int panel_exp, view_panel_exp;
extern int panel_exp_hide, view_panel_exp_hide, view_exp_black;
extern int panel_level;
extern int panel_joystick, view_panel_joystick;
extern int view_touch;
extern Vector2 joystick_dir;

extern int currentButtonId;
extern int buttonHandle[NUM_BUTTONS];
extern int imgbuttonHandle1[NUM_BUTTONS];
extern int imgbuttonHandle2[NUM_BUTTONS];
extern int buttonEquId[NUM_BUTTONS_EQU];

extern int directionAnim[8];
extern int directionAnimIdle[8];
extern int directionAnimSit[8];

extern int skillIdOfButton[NUM_BUTTONS];
extern int buttonIdOfSkill[NUM_SKILLS];
extern int buttonMinX[NUM_BUTTONS];
extern int buttonMaxX[NUM_BUTTONS];
extern int buttonMinY[NUM_BUTTONS];
extern int buttonMaxY[NUM_BUTTONS];


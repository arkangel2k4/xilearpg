#include <vector>

template <typename T>
class Matrix
{
public:
  Matrix(){};
  Matrix(int rows, int cols)
  {
	rows_=rows;
	cols_=cols;
    for(int i=0; i<rows; ++i)
    {
      data_.push_back(std::vector<T>(cols));
    }
  }
  
  int rows(){ return rows_; }
  int cols(){ return cols_; }

  // other ctors ....

  inline std::vector<T> & operator[](int i) { return data_[i]; }

  inline const std::vector<T> & operator[] (int i) const { return data_[i]; }

  // other accessors, like at() ...

  void resize(int rows, int cols)
  {
    data_.resize(rows);
    for(int i = 0; i < rows; ++i)
      data_[i].resize(cols);
  }

  // other member functions, like reserve()....

private:
  std::vector<std::vector<T> > data_;
  int rows_,cols_;
};

#ifndef ___POINT_H_
#define ___POINT_H_

class __Point
{
public:

	int x;
	int y;

	__Point()
	{
		x = 0; y = 0;
	}

	__Point(int _x, int _y)
	{
		x = _x; y = _y;
	}
};

#endif
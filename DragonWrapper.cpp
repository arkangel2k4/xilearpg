#include <stdio.h>
#include <string>
#include "DragonFireSDK.h"

/* TODO: switch to this writing system... (to close the file before app crashes)
long FileSeekStart(int fh){ return FileSeek(fh,0); }
long FileSize(int fh){
	FileSeekStart(fh);
	int iBytesRead,iPos; char szByte[1];
	for (iPos=0;;iPos++){ iBytesRead = FileRead(fh, szByte, 1); if (iBytesRead == 0) return iPos; }
	return 0;
}
long FileSeekEnd(int fh){ FileSeekStart(fh); return FileSeek(fh,FileSize(fh)); }
long FileAppend(int fh, char *buf){ FileSeekEnd(fh); return FileWrite(fh, buf, strlen(buf)); }

int fp_dbg=-1;
void opendbg(){ if(fp_dbg==-1) fp_dbg = FileOpen("debug.txt"); }
void closedbg(){ if(fp_dbg!=-1) FileClose(fp_dbg); fp_dbg=-1; }
void initdbg(){ fp_dbg=FileOpen("debug.txt"); if(fp_dbg==0) fp_dbg=FileCreate("debug.txt"); closedbg(); }
void printdbg(const char *txt){ opendbg(); FileAppend(fp_dbg, (char*)txt); closedbg(); }
void vardbg(int var){ opendbg(); char txt[100]; sprintf(txt,"%d",var); FileAppend(fp_dbg, txt); closedbg(); }
*/

int fout = 0;
char sout[1000];
bool uselessLogsActivated = false;
bool warningLogsActivated = false;

//-------------------------------------------------------

void DragonWrapperStart(int logType){
	if(logType==WRAPPER_LOG_WARNINGS)
		warningLogsActivated = true;
	if(logType==WRAPPER_LOG_EVERYTHING)
		uselessLogsActivated = true;

	if(logType==WRAPPER_LOG_WARNINGS || logType==WRAPPER_LOG_EVERYTHING)
		fout = FileCreate("wrapper_log.txt");

	if(fout==0){
		uselessLogsActivated = false;
		warningLogsActivated = false;
	}
}
void DragonWrapperEnd(){
	if(fout!=0)
		FileClose(fout);
}

int wImageAdd(char *filename){
	int r = ImageAdd(filename);
	if(uselessLogsActivated){
		sprintf(sout, "ImageAdd(%s) = %d\r\n", filename, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0){
			sprintf(sout, "ImageAdd(%s) = %d\r\n", filename, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}

int wViewAdd(int im,int x,int y){
	int r = ViewAdd(im,x,y);
	if(uselessLogsActivated){
		sprintf(sout, "ViewAdd(%d,%d,%d) = %d\r\n", im, x, y, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0 || im==0){
			sprintf(sout, "ViewAdd(%d,%d,%d) = %d\r\n", im, x, y, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wViewAdd(char *filename,int x,int y){
	int r = ViewAdd(filename,x,y);
	if(uselessLogsActivated){
		sprintf(sout, "ViewAdd(%s,%d,%d) = %d\r\n", filename, x, y, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0){
			sprintf(sout, "ViewAdd(%s,%d,%d) = %d\r\n", filename, x, y, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wViewAdd(char *filename,int x,int y,int (*callback)(int,int,int,int),int id){
	int r = ViewAdd(filename,x,y,callback,id);
	if(uselessLogsActivated){
		sprintf(sout, "ViewAdd(%s,%d,%d,callback_func,%d) = %d\r\n", filename, x, y, id, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0){
			sprintf(sout, "ViewAdd(%s,%d,%d,callback_func,%d) = %d\r\n", filename, x, y, id, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wViewSetImage(int vw,int im){
	int r = ViewSetImage(vw,im);
	if(uselessLogsActivated){
		sprintf(sout, "ViewSetImage(%d,%d) = %d\r\n", vw, im, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0 || vw==0 || im==0){
			sprintf(sout, "ViewSetImage(%d,%d) = %d\r\n", vw, im, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wViewSetVisible(int vw,int flag){
	int r = ViewSetVisible(vw,flag);
	if(uselessLogsActivated){
		sprintf(sout, "ViewSetVisible(%d,%d) = %d\r\n", vw, flag, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0 || vw==0){
			sprintf(sout, "ViewSetVisible(%d,%d) = %d\r\n", vw, flag, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}

int wTextAdd(int x,int y,char *text,int font){
	int r = TextAdd(x,y,text,font);
	if(uselessLogsActivated){
		sprintf(sout, "TextAdd(%d,%d,%s,%d) = %d\r\n", x, y, text, font, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0){
			sprintf(sout, "TextAdd(%d,%d,%s,%d) = %d\r\n", x, y, text, font, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wTextSetText(int tx,char *text){
	int r = TextSetText(tx,text);
	//if(uselessLogsActivated) 
	//	fprintf(fout, "TextSetText(%d,%s) = %d\r\n", tx, text, r);
	return r;
}

#ifndef _RESOURCES_H_
#define _RESOURCES_H_
/*****************************************************************************/

//#include "DragonFireSDK.h"
//#define MAX_ANIMS 50

//sounds...
enum SONG_IDS
{
	SONG_MENU,
	SONG_LVL1,
	//leave the line below:
	NUM_SONGS
};
enum SOUND_IDS
{
	SOUND_ATK0,
	SOUND_ATK1,
	SOUND_ATK2,
	//leave the line below:
	NUM_SOUNDS
};

//animations...
enum PLAYER_ANIM
{
	IDLE_DOWN,
	IDLE_SW,
	IDLE_LEFT,
	IDLE_NW,
	IDLE_UP,
	IDLE_NE,
	IDLE_RIGHT,
	IDLE_SE,
	WALK_DOWN,
	WALK_SW,
	WALK_LEFT,
	WALK_NW,
	WALK_UP,
	WALK_NE,
	WALK_RIGHT,
	WALK_SE,
	SIT_DOWN,
	SIT_SW,
	SIT_LEFT,
	SIT_NW,
	SIT_UP,
	SIT_NE,
	SIT_RIGHT,
	SIT_SE,
	DEATH_RIGHT,
	DEATH_LEFT,
	SKILL_RIGHT,
	SKILL_LEFT,
	SHOOT_RIGHT,
	SHOOT_LEFT,
	WALK_SHOOT_RIGHT,
	WALK_SHOOT_LEFT,
	BESERK_RIGHT,
	BESERK_LEFT,
	BUSTER_RIGHT,
	BUSTER_LEFT,
	REPEL_RIGHT,
	REPEL_LEFT,
	TELEPORT_RIGHT,
	TELEPORT_LEFT,
	//leave the line below:
	NUM_ANIMS
};

class Resources
{
public:
	//data
	int **_frames;
	int _numFrames[NUM_ANIMS];
	double _frameTime;
	double _maxTime;
	int _heads[NUM_HEADS][NUM_DIRECTIONS];
	
	//constructors & destructors
	//All images that matach the baseFilename will be loaded,
	//starting with index 0, so for example:
	//image0.png, image1.png, image2.png
	Resources();
	~Resources();
	
	void addHead(int animId, int dir, char *filename);
	void addAnim(int animId, const char *baseFilename);
};

//handles...
typedef struct {
	int view[NUM_VIEWHANDLES_PER_OBJ];
	int text[NUM_TEXTHANDLES_PER_OBJ];
} ObjectHandles;

extern std::vector<ObjectHandles> used_handles;

void init_object_handles_storage();
ObjectHandles popHandles();
void pushHandles(ObjectHandles hnd);

//vars...
extern int music[NUM_SONGS];
extern int sound[NUM_SOUNDS];
void load_sounds();
extern Resources playerResources[NUM_PLAYERS];
void load_head_resources_id(int id);
void load_head_resources();
void load_player_resources_avatar();
void load_player_resources_id(int id);
void load_player_resources();
extern Resources mobResources[NUM_MOBS];
void load_mob_resources_id(int id);
void load_mob_resources();
extern Resources weatherResources[NUM_WEATHER];
void load_weather_resources();
extern Resources itemResources[NUM_ITEMS];
extern int inventoryResources[NUM_ITEMS];
void load_item_resources();

/*****************************************************************************/
#endif
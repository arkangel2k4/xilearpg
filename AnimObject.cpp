#include <vector>
#include "DragonFireSDK.h"
#include "mmo.h"
#include "AnimObject.h"
#include "Resources.h"

extern Vector2 posBg;
extern Vector2 sizeBg;
extern int zoomPer;

AnimObject::AnimObject(double frameTime, bool loop) : 
	_rotDeg(0),_visible(false), _frames(0),
	_curTime(0), _curFrame(0), _maxTime(0),
	_viewHandle(0), _iterations(0), _paused(false)
{
	_isLoop = loop;	
	currentAnim = 0;
	_frameTime = frameTime;
	init();
	_viewHandle = wViewAdd("Images/dummy.png",0,0);
	_imagepos.x = 0;
	_imagepos.y = 0;
}

AnimObject::~AnimObject()
{
	//delete [] _frames; //martim... we have resources on memory now
	_frames = 0;
	setVisible(false); //martim... can't delete the view??
}

void AnimObject::init()
{
	_frames = weatherResources[0]._frames;
	_numFrames = weatherResources[0]._numFrames;
	_frameTime = weatherResources[0]._frameTime;
	_maxTime = weatherResources[0]._maxTime;
}

void AnimObject::setCurrentAnim(int animId)
{
	if(animId == currentAnim)
		return;

	currentAnim = animId;
	_curTime = 0;
	_maxTime = _numFrames[animId] * _frameTime;
}

void AnimObject::addAnim(int animId, char *baseFilename)
{
	//open files starting with 0 and open as many as can be found
	//we assume a .png extension
	int im = 0;
	int im_cnt = 0;
	char fn[512];
	int tmp[1000];

	sprintf(fn,"%s0.png",baseFilename,0);
	im = wImageAdd(fn);

	while(im && im_cnt < 1000)
	{
		tmp[im_cnt] = im;
		im_cnt++;
		
		sprintf(fn,"%s%d.png",baseFilename,im_cnt);
		im = wImageAdd(fn);
	}
	
	//copy over
	if(im_cnt > 0)
	{
		_frames[animId] = new int[im_cnt];
		_numFrames[animId] = im_cnt;

		for(int i=0; i<im_cnt; i++)
		{
			_frames[animId][i] = tmp[i];
		}
	}

	_maxTime = _numFrames[animId] * _frameTime;

}

void AnimObject::setVisible(bool vis)
{
	_visible = vis;
	wViewSetVisible(_viewHandle, vis);
}

bool AnimObject::isVisible() const
{
	return _visible;
}

bool AnimObject::setPosition(int _x, int _y)
{
	//dont leave borders... how to deal with this?
	if(_x < 0) _x = 0;
	if(_y < 0) _y = 0;
	if(_x >= sizeBg.x) _x = sizeBg.x;
	if(_y >= sizeBg.y) _y = sizeBg.y;

	//update
	_imagepos.x = _x;
	_imagepos.y = _y;
	return true;
}

bool AnimObject::isPaused() const
{
	return _paused;
}

int AnimObject::getIterations()
{
	return _iterations;
}

void AnimObject::setLoop(bool loop)
{
	_isLoop = loop;
}

bool AnimObject::getLoop() const
{
	return _isLoop;
}

///////////////////////////////////////////////////////////////////////////////
// start / stop / reset
///////////////////////////////////////////////////////////////////////////////
void AnimObject::start()
{
	_paused = false;
	update(0.0);
}

void AnimObject::stop()
{
	_paused = true;
	update(0.0);
}

void AnimObject::reset()
{
	_curTime = 0;
	update(0.0);
}

///////////////////////////////////////////////////////////////////////////////
// update
///////////////////////////////////////////////////////////////////////////////
void AnimObject::update(double dt)
{
	if(!isVisible())
		return;

	if(_paused){
		//martim: viewHandle could have changed from Z-order... update image even if paused
		reloadView();
		return;
	}else
		_curTime += dt;

	//compute the current iteration time
	_iterations = int(_curTime / _maxTime);
	double interval_time = _curTime - ((float)_iterations * _maxTime);
	
	//compute the current frame
	_curFrame = (int)(interval_time / _frameTime);
	
	if(_curFrame < 0) _curFrame = 0;
	if(_curFrame >= _numFrames[currentAnim]) _curFrame = _numFrames[currentAnim]-1;

	//set the image
	reloadView();
	
	if ((_curFrame == _numFrames[currentAnim] - 1) && !_isLoop)
	{
		stop();
	}
}

void AnimObject::reloadView()
{
	if(_viewHandle>0){
		wViewSetVisible(_viewHandle, isVisible());
		wViewSetImage(_viewHandle, _frames[currentAnim][_curFrame]);
		ViewSetxy(_viewHandle, (int)_imagepos.x, (int)_imagepos.y);
		ViewSetSize(_viewHandle, (int)(ViewGetWidth(_viewHandle)*zoomPer/100.0), (int)(ViewGetHeight(_viewHandle)*zoomPer/100.0));
	}
}

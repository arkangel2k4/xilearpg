#include <stdlib.h>
#include <math.h>
#define RAND() (((double)rand())/RAND_MAX)
#define RAND100() (100*RAND())

#define LEFT 0
#define RIGHT 1
enum DIRECTIONS
{
	DIR_S,
	DIR_SW,
	DIR_W,
	DIR_NW,
	DIR_N,
	DIR_NE,
	DIR_E,
	DIR_SE,
	//leave the line below:
	NUM_DIRECTIONS
};

//For character names, title names, guilds, maps, etc.
//Includes null-terminator as it is the length of the array.
#define NAME_LENGTH (23 + 1)

//Important stuf...
#define MAX_LEVEL 60
#define MAX_PC_BONUS 10
#define MAX_OBJECTS 30

//For sprites
#define NUM_VIEWHANDLES_PER_OBJ 4
#define NUM_TEXTHANDLES_PER_OBJ 4
#define NUM_PLAYERS 1
#define NUM_HEADS 7
#define NUM_MOBS 2
#define NUM_WEATHER 1
#define NUM_ITEMS 9

//For item names, which tend to have much longer names.
#define ITEM_NAME_LENGTH 50
#define MAX_SLOTS 4
#define MAX_INVENTORY 100
struct item {
	int item_id;
	int view;
	char identify;
	char refine;
	char attribute;
	short amount;
	bool equip;
	int slot[MAX_SLOTS];
};

#define BLOCK_SIZE 50
#define MAX(a,b) (a>b ? a : b)

//Classes
enum CLASS_IDS
{
	CLASS_KNIGHT,
	CLASS_MAGE,
	//leave the line below:
	NUM_CLASSES
};
#define CLASS_STR(a) (a==CLASS_KNIGHT ? "Knight" : \
					  a==CLASS_MAGE ? "Mage" : "")

//Id's
enum STATS
{
	ATK,
	CRI,
	HP,
	DEF,
	SPEED,
	EXP,
	LOOP,
	//leave the line below:
	NUM_STATS
};

//Id's
enum SKILL_IDS
{
	SKILL_FIRE,
	SKILL_BESERK,
	SKILL_BUSTER,
	SKILL_REPEL,
	SKILL_TELEPORT,
	//leave the line below:
	NUM_SKILLS
};

//Object types
enum OBJ_TYPE
{
	TYPE_PLAYER,
	TYPE_MOB
};

enum {
	ELE_NEUTRAL=0,
	ELE_WATER,
	ELE_EARTH,
	ELE_FIRE,
	ELE_WIND,
	ELE_POISON,
	ELE_HOLY,
	ELE_DARK,
	ELE_GHOST,
	ELE_UNDEAD,
	ELE_MAX
};

//Equip indexes constants. (eg: sd->equip_index[EQI_AMMO] returns the index
//where the arrows are equipped)
enum EQUIP_TYPE {
	EQI_HEAD_LOW=0,
	EQI_HEAD_MID,
	EQI_HEAD_TOP,
	EQI_WEAPON,
	EQI_ARMOR,
	EQI_SHIELD,
	EQI_NECK,
	EQI_SHOES,
	EQI_RING,
	NUM_EQI
};
#ifndef _DRAGONWRAPPER_
#define _DRAGONWRAPPER_

#define WRAPPER_LOG_NOTHING 0
#define WRAPPER_LOG_WARNINGS 1
#define WRAPPER_LOG_EVERYTHING 2

void DragonWrapperStart(int logType);
void DragonWrapperEnd();

int wImageAdd(char *filename);

int wViewAdd(int im,int x,int y);
int wViewAdd(char *filename,int x,int y);
int wViewAdd(char *filename,int x,int y,int (*callback)(int id,int event,int x,int y),int id);

int wViewSetImage(int vw,int im);
int wViewSetVisible(int vw,int flag);

int wTextAdd(int x,int y,char *text,int font);
int wTextSetText(int tx,char *text);

#endif

// SaveData.h -- data and functions for easily & securely saving and loading game data

typedef struct _PLAYERDATA
{
	bool free;
	int level;
	int id_class, id_sex, id_body, id_head;
	char name[10];
	struct item inventory[MAX_INVENTORY];
} PLAYERDATA;

typedef struct _GAMEDATA
{
	// Include variables for all data that needs to be saved here.
	// Remember NOT to include transient data, such as class objects
	// and/or pointers.  Also remember to include a default value for
	// each variable in the SetDefaultGameData() function.

	// example: (you can delete this)
	PLAYERDATA player_slot[3];

} GAMEDATA,*PGAMEDATA;

extern GAMEDATA gdGlobal;
extern GAMEDATA gdDefault;


// game data plus timestamp
typedef struct _GAMEDATA_TS
{
	// game data
	GAMEDATA gdData;
	// timestamp
	time_t ttStamp;
} GAMEDATA_TS,*PGAMEDATA_TS;

#define MAX_GD_STRING 27 // sets the maximum length of the game data filename

// prototypes //

void SaveGameData(const char* pFilename);
void LoadGameData(const char* pFilename);
char ComputeGameDataHash(PGAMEDATA_TS pGTData);
void CreateGameDataChecksums(PGAMEDATA_TS pGTData,
							 PGAMEDATA_TS pGTCheck);
void ValidateGameData(PGAMEDATA_TS pGTData,
					  PGAMEDATA_TS pGTCheck);
void InterlaceGameData(PGAMEDATA_TS pGTData,
					   PGAMEDATA_TS pGTCheck,
					   unsigned short* p16Data);
void DeinterlaceGameData(PGAMEDATA_TS pGTData,
					     PGAMEDATA_TS pGTCheck,
					     unsigned short* p16Data);
void SaveGameDataFile(char* pFilename,
					  unsigned short* p16Data);
void LoadGameDataFile(char* pFilename,
					  unsigned short* p16Data);

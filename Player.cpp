#include <string.h>
#include "mmo.h"
#include "map.h"
#include "Resources.h"
#include "GameObject.h"
#include "Player.h"
#include "Interface.h"
#include "item.h"
#include "SaveData.h"

Player::Player(int x, int y) : GameObject()
{
	//starting map
	map = 0;
	map_stage = 0;

	type = TYPE_PLAYER;
	//base stats
	status.level = 1;
	status.str = 5;
	status.vit = 5;
	status.speed = 4;
	//indirect stats
	status.batk = 40;
	status.cri = 5;
	status.hp = status.max_hp = 200;
	status.def = 0;
	//weapon stats
	status.rhw.atk = 2;
	status.rhw.atk2 = 10;
	status.rhw.ele = ELE_NEUTRAL;
	status.rhw.range = 1;
	
	//other vars
	_pos.x = x;
	_pos.y = y;
	direction = RIGHT;
	viewHPDisplay = wViewAdd("Images/hpDisplay.png", (int)_pos.x+2, (int)_pos.y-10);
	invisibleTimeCount=0;
	lasthitTimeCount=0;

	//add animations
	_heads = playerResources[0]._heads[0];
	_frames = playerResources[0]._frames;
	_frameTime = playerResources[0]._frameTime;
	_maxTime = playerResources[0]._maxTime;
	_numFrames = playerResources[0]._numFrames;
	//handles.view[0] = wViewAdd(_frames[IDLE_RIGHT][0], (int)_imagepos.x, (int)_imagepos.y);
	//handles.view[1] = wViewAdd(_heads[DIR_S], (int)_imagepos.x, (int)_imagepos.y);
	wViewSetImage(handles.view[0], _frames[IDLE_RIGHT][0]);
	ViewSetxy(handles.view[0], (int)_imagepos.x, (int)_imagepos.y);
	wViewSetImage(handles.view[1], _heads[DIR_S]);
	ViewSetxy(handles.view[1], (int)_imagepos.x, (int)_imagepos.y);

	view_img2load[NUM_VIEWHANDLES_PER_OBJ-1]=0;
	//skill anims are shifted
	view_shift_x[NUM_VIEWHANDLES_PER_OBJ-1] = -40;
	view_shift_y[NUM_VIEWHANDLES_PER_OBJ-1] = -92;

	dir = DIR_S;
	isSitting = false;
	skillInUse = -1;

	//init...
	init(ViewGetWidth(handles.view[0]), ViewGetHeight(handles.view[0]), _pos.x, _pos.y); //setup init position and size of the sprite
	setVisible(true);
	setCurrentAnim(IDLE_DOWN);
	setHeadDir(dir);

}

void Player::save()
{
	//save game...
	gdGlobal.player_slot[selectedPlayer].level = status.level;
	gdGlobal.player_slot[selectedPlayer].id_head = id_head;
	memcpy(gdGlobal.player_slot[selectedPlayer].inventory, inventory, MAX_INVENTORY*sizeof(struct item));
	SaveGameData("ixile_save");
}

void Player::addItem(int item_id, int amount)
{
	int i;
	for(i=0 ; i<MAX_INVENTORY ; i++){
		if(inventory[i].item_id==-1){
			inventory[i].item_id = item_id;
			inventory[i].amount = amount;
			inventory[i].equip = false;
			break;
		}
	}
	if(!gameStarted) return;
	if(i<MAX_INVENTORY)
		textLogLoot(item_id); //display loot text
	else
		; //display inventory full

	//save...
	save();
}

void Player::setLevel(int lvl)
{
	if(lvl>MAX_LEVEL) return;
	//new stats
	status.level = lvl;
	computeStats();
	status.hp = status.max_hp;
	status.exp = 0;
	//new delays
	//if(skillDelay[SKILL_TELEPORT] > 6*30) skillDelay[SKILL_TELEPORT] -= 5*30;

	//save...
	save();
}

void Player::computeStats()
{
	//base stats
	status.batk		= 40;
	status.cri		= 5;
	status.max_hp	= 200;
	status.def		= 0;
	status.speed	= 100;

	//levelups
	int lvlups = status.level-1;
	status.batk		+= 5*lvlups;
	status.cri		+= lvlups;
	status.max_hp	+= 10*lvlups;
	status.def		+= lvlups;

	//items
	for(int i=0 ; i<MAX_INVENTORY ; i++)
		if(inventory[i].item_id!=-1 && inventory[i].equip){
			status.batk		+= item_db[ inventory[i].item_id ].bonus[ATK];
			status.cri		+= item_db[ inventory[i].item_id ].bonus[CRI];
			status.max_hp	+= item_db[ inventory[i].item_id ].bonus[HP];
			status.def		+= item_db[ inventory[i].item_id ].bonus[DEF];
			status.speed	+= item_db[ inventory[i].item_id ].bonus[SPEED];
		}
}

void Player::checkPositionLimits()
{
	//check off-screen limits

	if(player->_pos.x < player->posZero2Feet.x)
		player->_pos.x = player->posZero2Feet.x;

	if(player->_pos.x > sizeBg.x - player->posZero2Feet.x)
		player->_pos.x = sizeBg.x - player->posZero2Feet.x;

	if(player->_pos.y < player->posZero2Feet.y)
		player->_pos.y = player->posZero2Feet.y;

	if(player->_pos.y > sizeBg.y)
		player->_pos.y = sizeBg.y;
}

void Player::setVisible(bool vis)
{
	GameObject::setVisible(vis);
	for(int i=1 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++)
		wViewSetVisible(handles.view[i], vis);
	//wViewSetVisible(viewHPDisplay, vis ? 1 : 0);
}

void Player::setHead(int head)
{
	if(head<0 || head>=NUM_HEADS) return;
	id_head = head;
	_heads = playerResources[0]._heads[head];
	setHeadDir(dir);
}


void Player::setHeadDir(int dir_)
{
	if(currentAnim==SKILL_LEFT)
		view_img2load[1] = _heads[DIR_SW];
	else if(currentAnim==SKILL_RIGHT)
		view_img2load[1] = _heads[DIR_SE];
	else
		view_img2load[1] = _heads[dir_];

	if(isSitting) view_shift_y[1] = 18;
	else view_shift_y[1] = 0;
}

void Player::setDir(int dir_)
{
	dir=dir_;

	if(dir>=DIR_S && dir<=DIR_NW)
		direction=LEFT;
	else
		direction=RIGHT;
}

void Player::idle()
{
	if(isSitting)
		setCurrentAnim(directionAnimSit[dir]);
	else
		setCurrentAnim(directionAnimIdle[dir]);
	setHeadDir(dir);
}

void Player::useSkill(int skillId)
{
	skillInUse = skillId;
	skillCounter = 0;
	if(skillId==-1){
		setCurrentAnim(dir); //IDLE_ANIM ids = DIR ids
	}else{
		if(direction==LEFT) 
			setCurrentAnim(SKILL_LEFT);
		else 
			setCurrentAnim(SKILL_RIGHT);
		if(skillId!=SKILL_FIRE)
			lockedForAnimation = true;
	}
}

void Player::updateAnims()
{
	int anim=-1;

	//dead?
	if(isDead){
		for(int i=1 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++)
			view_img2load[i] = 0;
		return;
	}

	//skills?
	switch(skillInUse){
	case -1:
		//view_img2load[2]=0;
		break;
	case SKILL_FIRE:
		if(direction==RIGHT) anim=SHOOT_RIGHT;
		else anim=SHOOT_LEFT;
		break;
	case SKILL_BESERK:
		if(direction==RIGHT) anim=BESERK_RIGHT;
		else anim=BESERK_LEFT;
		break;
	case SKILL_BUSTER:
		if(direction==RIGHT) anim=BUSTER_RIGHT;
		else anim=BUSTER_LEFT;
		break;
	case SKILL_REPEL:
		if(direction==RIGHT) anim=REPEL_RIGHT;
		else anim=REPEL_LEFT;
		break;
	case SKILL_TELEPORT:
		if(direction==RIGHT) anim=TELEPORT_RIGHT;
		else anim=TELEPORT_LEFT;
		break;
	}
	
	if(anim>=0){
		//play once and stop skill anim
		if(lockedForAnimation){ 
			//this kind of animation stops with last frame
			if(skillCounter >= _numFrames[anim]){
				setDir(DIR_S); setHeadDir(DIR_S);
				setCurrentAnim(dir); //IDLE_ANIM ids = DIR ids
				skillInUse=-1;
				skillCounter = 0;
				view_img2load[NUM_VIEWHANDLES_PER_OBJ-1] = 0;
				lockedForAnimation=false;
			}else{
				view_img2load[NUM_VIEWHANDLES_PER_OBJ-1]=_frames[anim][skillCounter++];
			}
		}else{
			//this kind of animation loops
			if(skillCounter >= _numFrames[anim])
				skillCounter = 0;
			view_img2load[NUM_VIEWHANDLES_PER_OBJ-1]=_frames[anim][skillCounter++];
		}
	}else{
		skillCounter = 0;
		view_img2load[NUM_VIEWHANDLES_PER_OBJ-1] = 0;
		lockedForAnimation=false;
	}

	//items?
	if(buttonEquId[EQI_HEAD_MID]!=-1){
		if(skillInUse==-1){
			view_img2load[2] = itemResources[8]._frames[dir][0];
			if(isSitting)
				view_shift_y[2] = 18;
			else
				view_shift_y[2] = 0;
		}else{
			if(direction==LEFT)
				view_img2load[2] = itemResources[8]._frames[DIR_SW][0];
			else
				view_img2load[2] = itemResources[8]._frames[DIR_SE][0];
		}
	}else{
		view_img2load[2] = 0;
	}
}

void Player::update()
{
	//is he dead?
	if(isDead && expiryDate>0)
		expiryDate--;
	if(!isDead && status.hp<=0){
		setDead(true);
		view_img2load[1] = 0; //hide head
		currentButtonId=-1;
		player->skillInUse=-1;
		for(int i=0 ; i<NUM_SKILLS ; i++){
			skillTimeCount[i]=-1;
			skillIsActive[i]=false;
			skillIsPressed[i]=false;
			wViewSetImage(buttonHandle[buttonIdOfSkill[i]], imgbuttonHandle1[buttonIdOfSkill[i]]);
		}
	}

	//invisible?
	if(!isVisible())
		invisibleTimeCount++;

	//last time player was hit
	lasthitTimeCount++;

	//level up?
	if(status.exp >= 100)
		setLevel(status.level+1);

	//sprite position
	/*
	Vector2 shift(0,0);
	if(lockedForAnimation){ //for skills (not fire)
		shift.y=-27;
	}
	_imagepos = posBg + (_pos-(posZero2Feet+shift))*(zoomPer/100.0);
	*/
	Vector2 diff = _imagepos-(posBg + (_pos-(posZero2Feet))*(zoomPer/100.0));
	if(diff.Length()>50)
		printf("sprite jump!!\n");
	_imagepos = posBg + (_pos-(posZero2Feet))*(zoomPer/100.0);

	//updates...
	setHeadDir(dir);
	updateAnims(); //skills and stuff
	GameObject::update();

}

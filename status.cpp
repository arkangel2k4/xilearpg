//Inflicts damage on the target with the according walkdelay.
//If flag&1, damage is passive and does not triggers cancelling status changes.
//If flag&2, fail if target does not has enough to substract.
//If flag&4, if killed, mob must not give exp/loot.
//int status_damage(struct block_list *src,struct block_list *target,int hp, int sp, int walkdelay, int flag);
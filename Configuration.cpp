#include <string.h>
#include "mmo.h"
#include "status.h"
#include "map.h"
#include "item.h"
#include "Resources.h"
#include "Configuration.h"

int skillDelay[NUM_SKILLS];
int skillRange[NUM_SKILLS];

int mobStats[NUM_MOBS][NUM_STATS];
int itemStats[NUM_ITEMS][NUM_STATS];

int statusDuration[NUM_STATUS];

void load_config(){
	load_map_config();
	load_skill_config();
	load_mob_config();
	load_item_config();
	load_status_config();
}

void load_map_config(){
	// set map stages (map_id,mob_id,quantity)
	//map 0...
	map_add_stage(0, 1, 1);
	map_add_stage(0, 1, 2);
	map_add_stage(0, 1, 3);
	map_add_stage(0, 1, 4);
	map_add_stage(0, 0, 1);
	maps[0].next_map = 0;
	//map 1...
	//...
}

void load_skill_config(){
	// default values...
	for(int i=0;i<NUM_SKILLS;i++){
		skillRange[i]=1;
		skillDelay[i]=20; //1hit per sec
	}

	// set skill range (in cell number)
	skillRange[SKILL_FIRE] =		1; //right now depends on weapon, not this value here...
	skillRange[SKILL_BESERK] =		2;
	skillRange[SKILL_BUSTER] =		2;
	skillRange[SKILL_REPEL] =		2;
	skillRange[SKILL_TELEPORT] =	1;

	// set skill delay (in frames, delay 30 = 1 hit/sec)
	skillDelay[SKILL_FIRE] =		20;
	skillDelay[SKILL_BESERK] =		30*10;
	skillDelay[SKILL_BUSTER] =		30*10;
	skillDelay[SKILL_REPEL] =		30*10;
	skillDelay[SKILL_TELEPORT] =	30*10;
}

void load_mob_config(){
	// default values...
	for(int i=0;i<NUM_MOBS;i++)
		load_mob_config_single(i, 20, 300, 0, 0, 50, 4, false);
	
	// set mob stats individually (id,atk,cri,hp,def,speed,exp,loopOnDeath)
	load_mob_config_single(0, 40, 0, 3200, 0, 4, 100, true); //boss
	load_mob_config_single(1, 20, 0, 800, 0, 4, 34, true); //first mob
}

void load_item_config(){
	// default values...
	for(int i=0;i<NUM_ITEMS;i++)
		load_item_config_single(i, "unnamed_item", 0, 0, 0, 0, 0, 0, 0);

	// set item stats individually (id,name,equip_index,atk,cri,hp,def,speed,exp)
	load_item_config_single(0, "Armor1", EQI_ARMOR, 5, 0, 10, 0, 25, 0);
	load_item_config_single(1, "Shoes1", EQI_SHOES, 5, 0, 10, 0, 25, 0);
	load_item_config_single(2, "Neck1",  EQI_NECK, 5, 0, 10, 0, 0, 0);
	load_item_config_single(3, "Ring1",  EQI_RING, 5, 0, 10, 0, 0, 0);
	load_item_config_single(4, "Armor2", EQI_ARMOR, 5, 0, 10, 0, 25, 0);
	load_item_config_single(5, "Shoes2", EQI_SHOES, 5, 0, 10, 0, 25, 0);
	load_item_config_single(6, "Neck2",  EQI_NECK, 5, 0, 10, 0, 0, 0);
	load_item_config_single(7, "Ring2",  EQI_RING, 5, 0, 10, 0, 0, 0);
	load_item_config_single(8, "MidHead1",  EQI_HEAD_MID, 0, 0, 0, 0, 0, 0);
}

void load_status_config(){
	// default values...
	for(int i=0;i<NUM_STATUS;i++){
		statusDuration[i] = 15*3;
	}

	// set status duration
	statusDuration[STATUS_STUN] = 15*3; //3secs
	statusDuration[STATUS_BESERKED] = 15*5; //5secs
}



//-----------------------------------------------------------------------

void load_mob_config_single(int id, int atk, int cri, int hp, int def, int speed, int exp, bool loop){
	mobStats[id][ATK]=atk;
	mobStats[id][CRI]=cri;
	mobStats[id][HP]=hp;
	mobStats[id][DEF]=def;
	mobStats[id][SPEED]=speed;
	mobStats[id][EXP]=exp;
	mobStats[id][LOOP]=loop;
}

void load_item_config_single(int id, char* name, int equip_index, int atk, int cri, int hp, int def, int speed, int exp){
	//name[ITEM_NAME_LENGTH-1]='\0'; //TODO
	strcpy(item_db[id].name, name);
	item_db[id].bonus[ATK]=atk;
	item_db[id].bonus[CRI]=cri;
	item_db[id].bonus[HP]=hp;
	item_db[id].bonus[DEF]=def;
	item_db[id].bonus[SPEED]=speed;
	item_db[id].bonus[EXP]=exp;
	item_db[id].equip_index=equip_index;
}

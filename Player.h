#ifndef _PLAYER_H_
#define _PLAYER_H_

class Player : public GameObject
{
public:

	//int level, exp;
	int map, map_stage;
	struct item inventory[MAX_INVENTORY];

	int invisibleTimeCount;
	int lasthitTimeCount;
	bool lockedForAnimation;

	int id_class, id_sex, id_body, id_head;

	int dir;
	int *_heads;
	int _headHandle;
	bool isSitting;
	int skillInUse;
	int skillCounter;

	int viewHPDisplay;

	Player(int x, int y);
	void save();

	void addItem(int item_id, int amount);
	void setLevel(int lvl);
	void computeStats();

	void checkPositionLimits();
	void setVisible(bool vis);
	void setHead(int head);
	void setHeadDir(int dir);
	void setDir(int dir);
	void idle();
	void useSkill(int skillId);
	void updateAnims();
	void update();
};

#endif
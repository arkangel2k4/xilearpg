#include <math.h>
#include "Vector2.h"
///////////////////////////////////////


/////////////////////////////////////////////////
const double Vector2::EPSILON = 1e-6;
const double Vector2::_INFINITY = 1.7976931348623158e+308;
const Vector2 Vector2::ZERO(0,0);
const Vector2 Vector2::UNIT_X(1,0);
const Vector2 Vector2::UNIT_Y(0,1);


///////////////////////////////////////////////////////////////////////////////
// construction and destruction
///////////////////////////////////////////////////////////////////////////////
Vector2::Vector2() : x(0),y(0)
{}

Vector2::Vector2(double fx, double fy) : x(fx), y(fy)
{}

Vector2::Vector2(const Vector2 &v) : x(v.x), y(v.y)
{}

Vector2::~Vector2()
{ }

///////////////////////////////////////////////////////////////////////////////
//  assignment & comparison
///////////////////////////////////////////////////////////////////////////////
Vector2& Vector2::operator= (const Vector2 &v)
{
	x = v.x;
	y = v.y;
	return *this;
}

bool Vector2::operator== (const Vector2 &v) const
{
	if(x == v.x && y == v.y) return true;
	return false;
}

bool Vector2::operator!= (const Vector2 &v) const
{
	if(x != v.x || y != v.y) return true;
	return false;
}


///////////////////////////////////////////////////////////////////////////////
// arithmetic operations
///////////////////////////////////////////////////////////////////////////////
Vector2 Vector2::operator+ (const Vector2 &v) const
{
	Vector2 sum;
	sum.x = x + v.x;
	sum.y = y + v.y;
	return sum;
}

Vector2 Vector2::operator- (const Vector2 &v) const
{
	Vector2 diff;
	diff.x = x - v.x;
	diff.y = y - v.y;
	return diff;
}

Vector2 Vector2::operator- () const
{
	Vector2 neg;
	neg.x = -x;
	neg.y = -y;
	return neg;
}

Vector2 Vector2::operator* (double f) const
{
	Vector2 prod;
	prod.x = x * f;
	prod.y = y * f;
	return prod;
}

Vector2 operator* (double f, const Vector2 &v)
{
	Vector2 prod;
	prod.x = v.x * f;
	prod.y = v.y * f;
	return prod;
}

Vector2 operator/ (double f, const Vector2 &v)
{
	Vector2 prod;
	prod.x = v.x / f;
	prod.y = v.y / f;
	return prod;
}

Vector2 Vector2::operator/ (double f) const
{
	Vector2 quot;

	if(f != 0)
	{
		double invquot = 1/f;
		quot.x = x * invquot;
		quot.y = y * invquot;
	}
	else
	{
		//tried to divide by 0.0, so return the closest thing
		//we can have to infinity
		quot.x = Vector2::_INFINITY;
		quot.y = Vector2::_INFINITY;
	}

	return quot;
}


///////////////////////////////////////////////////////////////////////////////
// arithmetic updates
///////////////////////////////////////////////////////////////////////////////
Vector2& Vector2::operator+= (const Vector2 &v)
{
	x += v.x;
	y += v.y;
	return *this;
}

Vector2& Vector2::operator-= (const Vector2 &v)
{
	x -= v.x;
	y -= v.y;
	return *this;
}
 
Vector2& Vector2::operator*= (double f)
{
	x *= f;
	y *= f;
	return *this;
}

Vector2& Vector2::operator/= (double f)
{
	if(f != 0)
	{
		double invquot = 1/f;
		x *= invquot;
		y *= invquot;
	}
	else
	{
		x = Vector2::_INFINITY;
		y = Vector2::_INFINITY;
	}

	return *this;
}
 

///////////////////////////////////////////////////////////////////////////////
// vector operations
///////////////////////////////////////////////////////////////////////////////
double Vector2::Length() const
{
	return sqrt((x*x) + (y*y));
}

double Vector2::LengthSquared() const
{
	return (x*x) + (y*y);
}

double Vector2::Unitize()
{
    double len = sqrt((x*x) + (y*y));

    if(len > Vector2::EPSILON)
    {
        double invlen = 1/len;
        x *= invlen;
        y *= invlen;
    }
    else
    {
        len = 0;
    }

    return len;
}

double Vector2::Dot(const Vector2 &v) const
{
    return (x*v.x) + (y*v.y);
}

double Vector2::Distance(const Vector2 &v) const
{
	double dx = v.x-x;
	double dy = v.y-y;
    return ( sqrt( dx*dx + dy*dy ) );
}

double Vector2::DistanceSquared(const Vector2 &v) const
{
	double dx = v.x-x;
	double dy = v.y-y;
    return ( dx*dx + dy*dy );
}

//void Vector2::Normalize()
//{
//
//}

Vector2 Vector2::Lerp(const Vector2 &v0, const Vector2 &v1, double T)
{
	//linear interpolation
	Vector2 trans;
	trans.x = v0.x + T*(v1.x - v0.x);
	trans.y = v0.y + T*(v1.y - v0.y);
	return trans;
}


/*****************************************************************************/
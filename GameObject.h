#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_
/*****************************************************************************/

#include "Matrix.h"
#include "DragonFireSDK.h"
#include "__Rect.h"
#include "Vector2.h"
#include "status.h"
#include "Resources.h"

class GameObject
{

public:

	bool isDead;
	
	//data
	int **_frames;
	int *_numFrames;
	int currentAnim;
	int _viewHandle;
	ObjectHandles handles;
	int view_img2load[NUM_VIEWHANDLES_PER_OBJ];
	int view_shift_x[NUM_VIEWHANDLES_PER_OBJ];
	int view_shift_y[NUM_VIEWHANDLES_PER_OBJ];

	//aux
	int _curFrame;
	bool _isLoop;
	double _frameTime;
	double _curTime;
	double _maxTime;
	int _iterations;
	bool _paused;
	double _rotDeg;
	Vector2 _scale;
	bool _visible;
	
	//martim
	Vector2 _pos;
	Vector2 _imagepos;
	Vector2 posZero2Feet;
	bool loopOnDeath;
	struct status_data status;
	enum OBJ_TYPE type;
	int direction;
	int expiryDate;
	int dmgType;
	int textHandlerDmg[2];
	int textHandlerCrit[2];
	int textTimer;
	char textStr[10];

	//constructors & destructors
	//All images that match the baseFilename will be loaded,
	//starting with index 0, so for example:
	//image0.png, image1.png, image2.png
	GameObject(double frameTime = 0.0333, bool loop = true);
	~GameObject();
	
	void addAnim(int animId, char *baseFilename);
	void setCurrentAnim(int animId);

	void setDead(bool dead);
	
	void init(int width, int height, int x, int y);

	//set the visisble/hidden status of the sprite. 
	//*** Note that sprites start out hidden
	virtual void setVisible(bool bVis);
	bool isVisible() const;
	
	//set/get the screen position
	bool setPosition(Vector2 pos);
	bool setPosition(int _x, int _y);
	Vector2 getPosition() const;
	
	//set/get the sprite rotation
	void setRotation(double deg);
	double getRotation() const;

	//set/get the sprite loop
	void setLoop(bool loop);
	bool getLoop() const;

	bool isPaused() const;
	
	//set/get the scaling values from 0.0 to 1.0
	void setScale(Vector2 &scale);
	Vector2 getScale() const;
	
	//get the number of iterations this sprite has made
	//since it started or since it was reset
	int getIterations();
	
	//start the animation, if stopped
	void start();
	//stop/pause the animation
	void stop();
	//reset the sprite to time 0
	void reset();

	//update the current frame
	void update(double dt = 0.0333);
	void reloadView();
	void reloadText();
};

//martim
extern Vector2 posBg;
extern Vector2 sizeBg;
//extern GameObject ***mapCells;
extern Matrix<GameObject*> mapCells;
//--end


/*****************************************************************************/
#endif
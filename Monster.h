#ifndef _MONSTER_H_
#define _MONSTER_H_

//Mob status
enum MOB_STATUS
{
	MOB_STATUS_IDLE,
	MOB_STATUS_ROAM,
	MOB_STATUS_FOLLOW,
	MOB_STATUS_ATTACK
};

enum MOB_TARGET_TYPE{
	MOB_TARGET_PLAYER, 
	MOB_TARGET_CELL 
};

//class...
class Monster : public GameObject
{
public:
	//int attackPower;
	//int level;
	//int hp;
	//int viewHPDisplay;
	
	struct mob_data md;
	
	enum MOB_TARGET_TYPE targetType;
	Vector2 targetCell;

	int timerIdle;
	int skillTimeCount;

	Monster(int x, int y, int id);
	~Monster();

	void walkTo(Vector2 walkVec, float speed);
	int runAI();

	void setVisible(bool vis);
	void update();
};

#endif
#include <string.h>
#include "mmo.h"
#include "map.h"
#include "mob.h"
#include "Resources.h"
#include "GameObject.h"
#include "Monster.h"
#include "Player.h"
#include "Interface.h"
#include "Configuration.h"
#include "battle.h"

extern Player *player;

Monster::Monster(int x, int y, int id) : GameObject()
{
	if(x==0) x = sizeBg.x*RAND();
	if(y==0) y = 100+(sizeBg.y-100)*RAND();

	md.bl.next = NULL;
	md.bl.prev = bl_tail;
	bl_tail = bl_tail->next = &md.bl;
	bl_count++;

	md.bl.id=id;
	md.bl.x=x;
	md.bl.y=y;
	md.bl.obj=this;

	targetType=MOB_TARGET_PLAYER;
	targetCell.x=-1;

	timerIdle=0;
	skillTimeCount=-1;
	loopOnDeath = mobStats[id][LOOP];

	type = TYPE_MOB;
	//base stats
	status.level = md.level = 1;
	status.batk = mobStats[id][ATK];
	status.cri = mobStats[id][CRI];
	status.max_hp = status.hp = mobStats[id][HP];
	status.def = mobStats[id][DEF];
	status.speed = mobStats[id][SPEED];
	status.exp = mobStats[id][EXP];
	//weapon stats
	status.rhw.atk = 2;
	status.rhw.atk2 = 4;
	status.rhw.ele = ELE_NEUTRAL;
	status.rhw.range = 1;
		
	_pos.x = x;
	_pos.y = y;
	direction = RIGHT;
	//viewHPDisplay = wViewAdd("Images/hpDisplay.png", (int)_pos.x+2, (int)_pos.y-10);

	//add animations
	_frames = mobResources[id]._frames;
	_frameTime = mobResources[id]._frameTime;
	_maxTime = mobResources[id]._maxTime;
	_numFrames = mobResources[id]._numFrames;
	//handles.view[0] = wViewAdd(_frames[IDLE_RIGHT][0], (int)_imagepos.x, (int)_imagepos.y);
	wViewSetImage(handles.view[0], _frames[IDLE_RIGHT][0]);
	ViewSetxy(handles.view[0], (int)_imagepos.x, (int)_imagepos.y);
	view_img2load[1]=0;
	view_img2load[2]=0;

	//init...
	init(ViewGetWidth(handles.view[0]), ViewGetHeight(handles.view[0]), _pos.x, _pos.y); //setup init position and size of the sprite
	setVisible(true);
	setCurrentAnim(IDLE_RIGHT);

	update();
}

Monster::~Monster()
{
	//block_list...
	if(md.bl.next) 
		md.bl.next->prev = md.bl.prev;
	if(md.bl.prev) 
		md.bl.prev->next = md.bl.next;
	if(bl_tail==&md.bl) 
		bl_tail = md.bl.prev;
	bl_count--;

	//update map
	mapCells[(int)_pos.y/BLOCK_SIZE][(int)_pos.x/BLOCK_SIZE]=NULL;

	TextSetVisible(textHandlerDmg[0], 0);
	TextSetVisible(textHandlerCrit[0], 0);
	TextSetVisible(textHandlerDmg[1], 0);
	TextSetVisible(textHandlerCrit[1], 0);
	//call GameObject destructor ?
}

void Monster::walkTo(Vector2 walkVec, float speed)
{
	Vector2 next;

	//direction
	if(walkVec.x>0){
		direction=RIGHT;
		setCurrentAnim(WALK_RIGHT);
	}else{
		direction=LEFT;
		setCurrentAnim(WALK_LEFT);
	}
	
	//simplest greedy approach...
	walkVec.Unitize();
	next.x = _pos.x + (int)(walkVec.x*speed);
	next.y = _pos.y + (int)(walkVec.y*speed);
	
	//dont leave borders... how to deal with this?
	if(next.x < 0) next.x = 0;
	if(next.y < 0) next.y = 0;
	if(next.x >= sizeBg.x) next.x = sizeBg.x;
	if(next.y >= sizeBg.y) next.y = sizeBg.y;

	//is it occupied (by someone else)?

	GameObject *next_who = mapCells[(int)next.y/BLOCK_SIZE][(int)next.x/BLOCK_SIZE];
	if(next_who!=NULL && next_who!=this){
		next.x = _pos.x + (int)(walkVec.y*speed);
		next.y = _pos.y - (int)(walkVec.x*speed);
		if(next.x<0 || next.x>=sizeBg.x || next.y<0 || next.y>=sizeBg.y){
			next.x = _pos.x - (int)(walkVec.y*speed);
			next.y = _pos.y + (int)(walkVec.x*speed);
		}
		if(next.x<0 || next.x>=sizeBg.x || next.y<0 || next.y>=sizeBg.y){
			next.x = _pos.x;
			next.y = _pos.y;
		}
	}
	
	//update...
	mapCells[(int)_pos.y/BLOCK_SIZE][(int)_pos.x/BLOCK_SIZE]=NULL;
	mapCells[(int)next.y/BLOCK_SIZE][(int)next.x/BLOCK_SIZE]=this;

	_pos = next;
}

int Monster::runAI()
{
	if(isDead || status.effect[STATUS_STUN]) 
		return MOB_STATUS_IDLE;

	Vector2 diff = player->_pos - _pos;

	//can see player?
	if(diff.Length() / BLOCK_SIZE <= 5 && player->isVisible()){
		targetType = MOB_TARGET_PLAYER;
		//can attack?
		if(attack(SKILL_FIRE,this,player)==-1){
			walkTo(diff, status.speed);
			return MOB_STATUS_FOLLOW;
		}else{
			return MOB_STATUS_ATTACK;
		}
	}else{
		targetType = MOB_TARGET_CELL;
		//idle?
		if(timerIdle>0){
			timerIdle--;
			return MOB_STATUS_IDLE;
		}else{
			//is cell chosen already?
			if(targetCell.x!=-1){
				//move towards cell (slower speed?)
				walkTo(targetCell-_pos, 0.75*status.speed);
				//reached cell?
				if((targetCell-_pos).Length() < 20){
					targetCell.x = -1;
				}
				return MOB_STATUS_ROAM;
			}else{
				//choose cell
				targetCell.x = sizeBg.x*RAND();
				targetCell.y = 100+(sizeBg.y-100)*RAND();
				timerIdle=60;
				return MOB_STATUS_IDLE;
			}
		}
	}
}

void Monster::setVisible(bool vis)
{
	GameObject::setVisible(vis);
	//wViewSetVisible(viewHPDisplay, vis ? 1 : 0);
}

void Monster::update()
{
	//status effects...
	if(!isDead && status.hp>0)
	for(int i=0 ; i<NUM_STATUS ; i++)
		if(status.effect[i]){
			switch(i){
			case STATUS_BESERKED: if(status.effect[i] % 15 == 0) status.hp -= 20;
				if(status.hp<=0){
					status.hp=0;
					if(status.effect_src[i]!=NULL) //our player ID is 1...
						status.effect_src[i]->exp += status.exp;
				}
				break;
			case STATUS_STUN:
				break;
			}
			status.effect[i]--;
		}

	//is he dead?
	if(isDead && expiryDate>0)
		expiryDate--;
	if(!isDead && status.hp<=0)
		setDead(true);

	//sprite position
	_imagepos = posBg + (_pos-posZero2Feet)*(zoomPer/100.0);

	//updates...
	GameObject::update();

}

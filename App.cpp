//------------------------------------------------------------
// MANIFEST BUILDER OUTPUT FILE :: App.cpp
// 
// Created with Manifest Builder v1.0
// Generated on 02/18/12 at 04:14:42
// 
// Manifest Builder (C)2010 Jon Howard - All rights reserved
// See http://www.whitetreegames.com or contact jon@whitetreegames.com for more details
//------------------------------------------------------------


//--------------------------------------------------------------------------------------
// FINAL APP...
//--------------------------------------------------------------------------------------
#define FINAL_BUILD
#include "DragonFireSDK.h"
#include <math.h>
#include <float.h>
#include <list>
#include <utility>
// martim...
#include <vector>
#include <string.h>
#include <stdlib.h>
//--------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
//====================================================
// Include Files...
//====================================================
//----------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : DragonWrapper.h  (FILTER = ON)

#ifndef _DRAGONWRAPPER_
#define _DRAGONWRAPPER_

#define WRAPPER_LOG_NOTHING 0
#define WRAPPER_LOG_WARNINGS 1
#define WRAPPER_LOG_EVERYTHING 2

void DragonWrapperStart(int logType);
void DragonWrapperEnd();

int wImageAdd(char *filename);

int wViewAdd(int im,int x,int y);
int wViewAdd(char *filename,int x,int y);
int wViewAdd(char *filename,int x,int y,int (*callback)(int id,int event,int x,int y),int id);

int wViewSetImage(int vw,int im);
int wViewSetVisible(int vw,int flag);

int wTextAdd(int x,int y,char *text,int font);
int wTextSetText(int tx,char *text);

#endif


// END OF INCLUDED FILE : DragonWrapper.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Matrix.h  (FILTER = ON)

// LINE FILTERED :: #include <vector>

template <typename T>
class Matrix
{
public:
  Matrix(){};
  Matrix(int rows, int cols)
  {
	rows_=rows;
	cols_=cols;
    for(int i=0; i<rows; ++i)
    {
      data_.push_back(std::vector<T>(cols));
    }
  }
  
  int rows(){ return rows_; }
  int cols(){ return cols_; }

  // other ctors ....

  inline std::vector<T> & operator[](int i) { return data_[i]; }

  inline const std::vector<T> & operator[] (int i) const { return data_[i]; }

  // other accessors, like at() ...

  void resize(int rows, int cols)
  {
    data_.resize(rows);
    for(int i = 0; i < rows; ++i)
      data_[i].resize(cols);
  }

  // other member functions, like reserve()....

private:
  std::vector<std::vector<T> > data_;
  int rows_,cols_;
};


// END OF INCLUDED FILE : Matrix.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Vector2.h  (FILTER = ON)

#ifndef _Vector2_H_
#define _Vector2_H_
/*****************************************************************************/

class Vector2
{

public:

	//constructors & destructors
	Vector2();
	Vector2(double x, double y);
	Vector2(const Vector2 &v);
	~Vector2();

	//assignment & comparison
    Vector2& operator= (const Vector2 &v);
    bool operator== (const Vector2 &v) const;
    bool operator!= (const Vector2 &v) const;

    //arithmetic & unary operations
	Vector2 operator+ (const Vector2 &v) const;
	Vector2 operator- (const Vector2 &v) const;
	Vector2 operator* (double f) const;
	Vector2 operator/ (double f) const;
	Vector2 operator- () const;

	//arithmetic with assignment operations
	Vector2& operator+= (const Vector2 &v);
	Vector2& operator-= (const Vector2 &v);
	Vector2& operator*= (double f);
	Vector2& operator/= (double f);
 
    //vector operations
    double Length() const;
    double LengthSquared() const;
    double Dot(const Vector2 &v) const;
    double Unitize();
	double Distance(const Vector2 &v) const;
	double DistanceSquared(const Vector2 &v) const;

 	//linear interpolation
	static Vector2 Lerp(const Vector2 &v0, const Vector2 &v1, double T);

	//special vectors
    static const Vector2 ZERO;
    static const Vector2 UNIT_X;
    static const Vector2 UNIT_Y;
 
	//special constants
	static const double EPSILON;
	static const double _INFINITY;

	//data
	double x;
	double y;
};

//these need to be global
Vector2 operator* (double f, const Vector2 &v);
Vector2 operator/ (double f, const Vector2 &v);

/*****************************************************************************/
#endif

// END OF INCLUDED FILE : Vector2.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : __Point.h  (FILTER = ON)

#ifndef ___POINT_H_
#define ___POINT_H_

class __Point
{
public:

	int x;
	int y;

	__Point()
	{
		x = 0; y = 0;
	}

	__Point(int _x, int _y)
	{
		x = _x; y = _y;
	}
};

#endif

// END OF INCLUDED FILE : __Point.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : __Rect.h  (FILTER = ON)

#ifndef ___RECT_H_
#define ___RECT_H_

class __Rect
{
public:

	int left;
	int top;
	int width;
	int height;

	__Rect()
	{
		left = 0; top = 0; width = 0; height = 0;
	}

	__Rect(int _left, int _top, int _width, int _height)
	{
		left = _left;
		top = _top;
		width = _width;
		height = _height;
	}

	bool intersect(const __Rect &rhs)
	{
		int X1, Y1, X2, Y2;
		int X1Left, X1Right, Y1Top, Y1Bottom, X2Left, X2Right, Y2Top, Y2Bottom;

		//grab top left X and Y
		X1 = left;
		Y1 = top;
		X2 = rhs.left;
		Y2 = rhs.top;

		//Figure out the box details for Object1
		X1Left = X1;
		X1Right = X1Left + width;
		Y1Top = Y1;
		Y1Bottom=Y1Top + height;

		//Figure out the box details for Object2
		X2Left = X2;
		X2Right = X2Left + rhs.width;
		Y2Top = Y2;
		Y2Bottom= Y2Top + rhs.height;
		
		if ((((Y1Top <= Y2Bottom) && (Y1Bottom >= Y2Top)) || 
			((Y2Top <= Y1Bottom) && (Y2Bottom >= Y1Top))) && 
			(((X1Right >= X2Left) && (X1Left <= X2Right)) || 
			((X2Right >= X1Left) && (X2Left <= X1Right))))
		{
			return true;
		}

		return false;
	}
};

#endif

// END OF INCLUDED FILE : __Rect.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Vector2.h  (FILTER = ON)

#ifndef _Vector2_H_
#define _Vector2_H_
/*****************************************************************************/

class Vector2
{

public:

	//constructors & destructors
	Vector2();
	Vector2(double x, double y);
	Vector2(const Vector2 &v);
	~Vector2();

	//assignment & comparison
    Vector2& operator= (const Vector2 &v);
    bool operator== (const Vector2 &v) const;
    bool operator!= (const Vector2 &v) const;

    //arithmetic & unary operations
	Vector2 operator+ (const Vector2 &v) const;
	Vector2 operator- (const Vector2 &v) const;
	Vector2 operator* (double f) const;
	Vector2 operator/ (double f) const;
	Vector2 operator- () const;

	//arithmetic with assignment operations
	Vector2& operator+= (const Vector2 &v);
	Vector2& operator-= (const Vector2 &v);
	Vector2& operator*= (double f);
	Vector2& operator/= (double f);
 
    //vector operations
    double Length() const;
    double LengthSquared() const;
    double Dot(const Vector2 &v) const;
    double Unitize();
	double Distance(const Vector2 &v) const;
	double DistanceSquared(const Vector2 &v) const;

 	//linear interpolation
	static Vector2 Lerp(const Vector2 &v0, const Vector2 &v1, double T);

	//special vectors
    static const Vector2 ZERO;
    static const Vector2 UNIT_X;
    static const Vector2 UNIT_Y;
 
	//special constants
	static const double EPSILON;
	static const double _INFINITY;

	//data
	double x;
	double y;
};

//these need to be global
Vector2 operator* (double f, const Vector2 &v);
Vector2 operator/ (double f, const Vector2 &v);

/*****************************************************************************/
#endif

// END OF INCLUDED FILE : Vector2.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : mmo.h  (FILTER = ON)

// LINE FILTERED :: #include <stdlib.h>
// LINE FILTERED :: #include <math.h>
#define RAND() (((double)rand())/RAND_MAX)
#define RAND100() (100*RAND())

#define LEFT 0
#define RIGHT 1
enum DIRECTIONS
{
	DIR_S,
	DIR_SW,
	DIR_W,
	DIR_NW,
	DIR_N,
	DIR_NE,
	DIR_E,
	DIR_SE,
	//leave the line below:
	NUM_DIRECTIONS
};

//For character names, title names, guilds, maps, etc.
//Includes null-terminator as it is the length of the array.
#define NAME_LENGTH (23 + 1)

//Important stuf...
#define MAX_LEVEL 60
#define MAX_PC_BONUS 10
#define MAX_OBJECTS 30

//For sprites
#define NUM_VIEWHANDLES_PER_OBJ 4
#define NUM_TEXTHANDLES_PER_OBJ 4
#define NUM_PLAYERS 1
#define NUM_HEADS 7
#define NUM_MOBS 2
#define NUM_WEATHER 1
#define NUM_ITEMS 9

//For item names, which tend to have much longer names.
#define ITEM_NAME_LENGTH 50
#define MAX_SLOTS 4
#define MAX_INVENTORY 100
struct item {
	int item_id;
	int view;
	char identify;
	char refine;
	char attribute;
	short amount;
	bool equip;
	int slot[MAX_SLOTS];
};

#define BLOCK_SIZE 50
#define MAX(a,b) (a>b ? a : b)

//Classes
enum CLASS_IDS
{
	CLASS_KNIGHT,
	CLASS_MAGE,
	//leave the line below:
	NUM_CLASSES
};
#define CLASS_STR(a) (a==CLASS_KNIGHT ? "Knight" : \
					  a==CLASS_MAGE ? "Mage" : "")

//Id's
enum STATS
{
	ATK,
	CRI,
	HP,
	DEF,
	SPEED,
	EXP,
	LOOP,
	//leave the line below:
	NUM_STATS
};

//Id's
enum SKILL_IDS
{
	SKILL_FIRE,
	SKILL_BESERK,
	SKILL_BUSTER,
	SKILL_REPEL,
	SKILL_TELEPORT,
	//leave the line below:
	NUM_SKILLS
};

//Object types
enum OBJ_TYPE
{
	TYPE_PLAYER,
	TYPE_MOB
};

enum {
	ELE_NEUTRAL=0,
	ELE_WATER,
	ELE_EARTH,
	ELE_FIRE,
	ELE_WIND,
	ELE_POISON,
	ELE_HOLY,
	ELE_DARK,
	ELE_GHOST,
	ELE_UNDEAD,
	ELE_MAX
};

//Equip indexes constants. (eg: sd->equip_index[EQI_AMMO] returns the index
//where the arrows are equipped)
enum EQUIP_TYPE {
	EQI_HEAD_LOW=0,
	EQI_HEAD_MID,
	EQI_HEAD_TOP,
	EQI_WEAPON,
	EQI_ARMOR,
	EQI_SHIELD,
	EQI_NECK,
	EQI_SHOES,
	EQI_RING,
	NUM_EQI
};

// END OF INCLUDED FILE : mmo.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : status.h  (FILTER = ON)

#ifndef _STATUS_H_
#define _STATUS_H_

enum STATUS_EFFECTS{
	STATUS_STUN,
	STATUS_BESERKED,
	//leave the line below:
	NUM_STATUS
};

//Basic damage info of a weapon
//Required because players have two of these, one in status_data
//and another for their left hand weapon.
struct weapon_atk {
	unsigned short atk, atk2;
	unsigned short range;
	unsigned char ele;
};

//For holding basic status (which can be modified by status changes)
struct status_data {
	int
		level,
		hp, sp,
		max_hp, max_sp;
	int
		exp;
	unsigned short
		str, agi, vit, int_, dex, luk,
		batk,
		matk_min, matk_max,
		speed,
		amotion, adelay, dmotion,
		mode;
	short 
		hit, flee, cri, flee2,
		def2, mdef2,
		aspd_rate;
	unsigned char
		def_ele, ele_lv,
		size, race;
	signed char
		def, mdef;
	struct weapon_atk rhw, lhw; //Right Hand/Left Hand Weapon.

	int effect[NUM_STATUS];
	struct status_data **effect_src;
};

#endif


// END OF INCLUDED FILE : status.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Resources.h  (FILTER = ON)

#ifndef _RESOURCES_H_
#define _RESOURCES_H_
/*****************************************************************************/

//#include "DragonFireSDK.h"
//#define MAX_ANIMS 50

//sounds...
enum SONG_IDS
{
	SONG_MENU,
	SONG_LVL1,
	//leave the line below:
	NUM_SONGS
};
enum SOUND_IDS
{
	SOUND_ATK0,
	SOUND_ATK1,
	SOUND_ATK2,
	//leave the line below:
	NUM_SOUNDS
};

//animations...
enum PLAYER_ANIM
{
	IDLE_DOWN,
	IDLE_SW,
	IDLE_LEFT,
	IDLE_NW,
	IDLE_UP,
	IDLE_NE,
	IDLE_RIGHT,
	IDLE_SE,
	WALK_DOWN,
	WALK_SW,
	WALK_LEFT,
	WALK_NW,
	WALK_UP,
	WALK_NE,
	WALK_RIGHT,
	WALK_SE,
	SIT_DOWN,
	SIT_SW,
	SIT_LEFT,
	SIT_NW,
	SIT_UP,
	SIT_NE,
	SIT_RIGHT,
	SIT_SE,
	DEATH_RIGHT,
	DEATH_LEFT,
	SKILL_RIGHT,
	SKILL_LEFT,
	SHOOT_RIGHT,
	SHOOT_LEFT,
	WALK_SHOOT_RIGHT,
	WALK_SHOOT_LEFT,
	BESERK_RIGHT,
	BESERK_LEFT,
	BUSTER_RIGHT,
	BUSTER_LEFT,
	REPEL_RIGHT,
	REPEL_LEFT,
	TELEPORT_RIGHT,
	TELEPORT_LEFT,
	//leave the line below:
	NUM_ANIMS
};

class Resources
{
public:
	//data
	int **_frames;
	int _numFrames[NUM_ANIMS];
	double _frameTime;
	double _maxTime;
	int _heads[NUM_HEADS][NUM_DIRECTIONS];
	
	//constructors & destructors
	//All images that matach the baseFilename will be loaded,
	//starting with index 0, so for example:
	//image0.png, image1.png, image2.png
	Resources();
	~Resources();
	
	void addHead(int animId, int dir, char *filename);
	void addAnim(int animId, const char *baseFilename);
};

//handles...
typedef struct {
	int view[NUM_VIEWHANDLES_PER_OBJ];
	int text[NUM_TEXTHANDLES_PER_OBJ];
} ObjectHandles;

extern std::vector<ObjectHandles> used_handles;

void init_object_handles_storage();
ObjectHandles popHandles();
void pushHandles(ObjectHandles hnd);

//vars...
extern int music[NUM_SONGS];
extern int sound[NUM_SOUNDS];
void load_sounds();
extern Resources playerResources[NUM_PLAYERS];
void load_head_resources_id(int id);
void load_head_resources();
void load_player_resources_avatar();
void load_player_resources_id(int id);
void load_player_resources();
extern Resources mobResources[NUM_MOBS];
void load_mob_resources_id(int id);
void load_mob_resources();
extern Resources weatherResources[NUM_WEATHER];
void load_weather_resources();
extern Resources itemResources[NUM_ITEMS];
extern int inventoryResources[NUM_ITEMS];
void load_item_resources();

/*****************************************************************************/
#endif

// END OF INCLUDED FILE : Resources.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : AnimObject.h  (FILTER = ON)

#ifndef _ANIMOBJECT_H_
#define _ANIMOBJECT_H_

// LINE FILTERED :: #include "Vector2.h"

class AnimObject
{

public:
	//data
	int **_frames;
	int *_numFrames;
	int currentAnim;
	int _viewHandle;

	//aux
	int _curFrame;
	bool _isLoop;
	double _frameTime;
	double _curTime;
	double _maxTime;
	int _iterations;
	bool _paused;
	double _rotDeg;
	Vector2 _scale;
	bool _visible;

	Vector2 _pos;
	Vector2 _imagepos;

	//constructors
	AnimObject(double frameTime = 0.0333, bool loop = true);
	~AnimObject();
	
	void init();

	//functions
	void addAnim(int animId, char *baseFilename);
	void setCurrentAnim(int animId);

	void init(int width, int height, int x, int y);

	virtual void setVisible(bool bVis);
	bool isVisible() const;

	bool setPosition(int _x, int _y);

	void setLoop(bool loop);
	bool getLoop() const;
	bool isPaused() const;
	int getIterations();
	
	void start();
	void stop();
	void reset();

	void update(double dt = 0.0333);
	void reloadView();
};

#endif


// END OF INCLUDED FILE : AnimObject.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : GameObject.h  (FILTER = ON)

#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_
/*****************************************************************************/

// LINE FILTERED :: #include "Matrix.h"
// LINE FILTERED :: #include "DragonFireSDK.h"
// LINE FILTERED :: #include "__Rect.h"
// LINE FILTERED :: #include "Vector2.h"
// LINE FILTERED :: #include "status.h"
// LINE FILTERED :: #include "Resources.h"

class GameObject
{

public:

	bool isDead;
	
	//data
	int **_frames;
	int *_numFrames;
	int currentAnim;
	int _viewHandle;
	ObjectHandles handles;
	int view_img2load[NUM_VIEWHANDLES_PER_OBJ];
	int view_shift_x[NUM_VIEWHANDLES_PER_OBJ];
	int view_shift_y[NUM_VIEWHANDLES_PER_OBJ];

	//aux
	int _curFrame;
	bool _isLoop;
	double _frameTime;
	double _curTime;
	double _maxTime;
	int _iterations;
	bool _paused;
	double _rotDeg;
	Vector2 _scale;
	bool _visible;
	
	//martim
	Vector2 _pos;
	Vector2 _imagepos;
	Vector2 posZero2Feet;
	bool loopOnDeath;
	struct status_data status;
	enum OBJ_TYPE type;
	int direction;
	int expiryDate;
	int dmgType;
	int textHandlerDmg[2];
	int textHandlerCrit[2];
	int textTimer;
	char textStr[10];

	//constructors & destructors
	//All images that match the baseFilename will be loaded,
	//starting with index 0, so for example:
	//image0.png, image1.png, image2.png
	GameObject(double frameTime = 0.0333, bool loop = true);
	~GameObject();
	
	void addAnim(int animId, char *baseFilename);
	void setCurrentAnim(int animId);

	void setDead(bool dead);
	
	void init(int width, int height, int x, int y);

	//set the visisble/hidden status of the sprite. 
	//*** Note that sprites start out hidden
	virtual void setVisible(bool bVis);
	bool isVisible() const;
	
	//set/get the screen position
	bool setPosition(Vector2 pos);
	bool setPosition(int _x, int _y);
	Vector2 getPosition() const;
	
	//set/get the sprite rotation
	void setRotation(double deg);
	double getRotation() const;

	//set/get the sprite loop
	void setLoop(bool loop);
	bool getLoop() const;

	bool isPaused() const;
	
	//set/get the scaling values from 0.0 to 1.0
	void setScale(Vector2 &scale);
	Vector2 getScale() const;
	
	//get the number of iterations this sprite has made
	//since it started or since it was reset
	int getIterations();
	
	//start the animation, if stopped
	void start();
	//stop/pause the animation
	void stop();
	//reset the sprite to time 0
	void reset();

	//update the current frame
	void update(double dt = 0.0333);
	void reloadView();
	void reloadText();
};

//martim
extern Vector2 posBg;
extern Vector2 sizeBg;
//extern GameObject ***mapCells;
extern Matrix<GameObject*> mapCells;
//--end


/*****************************************************************************/
#endif

// END OF INCLUDED FILE : GameObject.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : map.h  (FILTER = ON)

// LINE FILTERED :: #include "GameObject.h"
// LINE FILTERED :: #include <vector>

struct block_list {
	struct block_list *next,*prev;
	int id;
	short m,x,y;
	GameObject *obj;
};

extern struct block_list bl_head;
extern struct block_list *bl_tail;
extern int bl_count;

extern struct map_data maps[1];

struct map_stage {
	int mob_id;
	int quant;
};
struct map_data {
	char background[100];
	Vector2 size;
	std::vector<struct map_stage> stages;
	int next_map;
};

int map_init();
int map_spawn(int *map_id, int *stage);
void map_add_stage(int map, int mob, int quantity);

/*
struct weapon_data {
	int atkmods[3];
	// all the variables except atkmods get zero'ed in each call of status_calc_pc
	// NOTE: if you want to add a non-zeroed variable, you need to update the memset call
	//  in status_calc_pc as well! All the following are automatically zero'ed. [Skotlex]
	int overrefine;
	int star;
	int ignore_def_ele;
	int ignore_def_race;
	int def_ratio_atk_ele;
	int def_ratio_atk_race;
	int addele[ELE_MAX];
	int addsize[3];
};

struct map_session_data {
	struct block_list *bl;
	struct status_data *status;

	struct item_data* inventory_data[MAX_INVENTORY]; // direct pointers to itemdb entries (faster than doing item_id lookups)
	short equip_index[11];
	unsigned int weight,max_weight;
	int cart_weight,cart_num;
	int fd;
	unsigned short mapindex;
	unsigned char head_dir; //0: Look forward. 1: Look right, 2: Look left.

	int invincible_timer;

	short weapontype1,weapontype2;
	short disguise; // [Valaris]

	struct weapon_data right_weapon, left_weapon;

	int weapon_atk_rate[16];
	int arrow_atk,arrow_ele,arrow_cri,arrow_hit;

	int castrate,delayrate,hprate,sprate,dsprate;
	int atk_rate;
	int hprecov_rate,sprecov_rate;
	int matk_rate;
	int critical_rate,hit_rate,flee_rate,flee2_rate,def_rate,def2_rate,mdef_rate,mdef2_rate;

	int itemid;
	short itemindex;	//Used item's index in sd->inventory [Skotlex]

	short catch_target_class; // pet catching, stores a pet class to catch (short now) [zzo]
};
*/

// END OF INCLUDED FILE : map.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : mob.h  (FILTER = ON)

struct mob_data {
	struct block_list bl;
	char name[NAME_LENGTH];
	struct {
		unsigned size : 2; //Small/Big monsters.
		unsigned ai : 2; //Special ai for summoned monsters.
							//0: Normal mob.
							//1: Standard summon, attacks mobs.
							//2: Alchemist Marine Sphere
							//3: Alchemist Summon Flora
	} special_state; //Special mob information that does not needs to be zero'ed on mob respawn.

	short class_;
	unsigned boss : 1;
	unsigned int tdmg; //Stores total damage given to the mob, for exp calculations. [Skotlex]
	int level;
	int target_id,attacked_id;
};

// END OF INCLUDED FILE : mob.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Player.h  (FILTER = ON)

#ifndef _PLAYER_H_
#define _PLAYER_H_

class Player : public GameObject
{
public:

	//int level, exp;
	int map, map_stage;
	struct item inventory[MAX_INVENTORY];

	int invisibleTimeCount;
	int lasthitTimeCount;
	bool lockedForAnimation;

	int id_class, id_sex, id_body, id_head;

	int dir;
	int *_heads;
	int _headHandle;
	bool isSitting;
	int skillInUse;
	int skillCounter;

	int viewHPDisplay;

	Player(int x, int y);
	void save();

	void addItem(int item_id, int amount);
	void setLevel(int lvl);
	void computeStats();

	void checkPositionLimits();
	void setVisible(bool vis);
	void setHead(int head);
	void setHeadDir(int dir);
	void setDir(int dir);
	void idle();
	void useSkill(int skillId);
	void updateAnims();
	void update();
};

#endif

// END OF INCLUDED FILE : Player.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Monster.h  (FILTER = ON)

#ifndef _MONSTER_H_
#define _MONSTER_H_

//Mob status
enum MOB_STATUS
{
	MOB_STATUS_IDLE,
	MOB_STATUS_ROAM,
	MOB_STATUS_FOLLOW,
	MOB_STATUS_ATTACK
};

enum MOB_TARGET_TYPE{
	MOB_TARGET_PLAYER, 
	MOB_TARGET_CELL 
};

//class...
class Monster : public GameObject
{
public:
	//int attackPower;
	//int level;
	//int hp;
	//int viewHPDisplay;
	
	struct mob_data md;
	
	enum MOB_TARGET_TYPE targetType;
	Vector2 targetCell;

	int timerIdle;
	int skillTimeCount;

	Monster(int x, int y, int id);
	~Monster();

	void walkTo(Vector2 walkVec, float speed);
	int runAI();

	void setVisible(bool vis);
	void update();
};

#endif

// END OF INCLUDED FILE : Monster.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Configuration.h  (FILTER = ON)

extern int skillDelay[NUM_SKILLS];
extern int skillRange[NUM_SKILLS];

extern int mobStats[NUM_MOBS][NUM_STATS];
extern int itemStats[NUM_ITEMS][NUM_STATS];

extern int statusDuration[NUM_STATUS];

void load_config();
void load_map_config();
void load_skill_config();
void load_mob_config();
void load_item_config();
void load_status_config();

void load_mob_config_single(int id, int atk, int vit, int cri, int def, int exp, int speed, bool loop);
void load_item_config_single(int id, char* name, int equip_index, int atk, int vit, int cri, int def, int exp, int speed);


// END OF INCLUDED FILE : Configuration.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Interface.h  (FILTER = ON)

//buttons...
enum BUTTON_IDS
{
/*
	PB_LEFT_BTN,
	PB_RIGHT_BTN,
	PB_TOP_BTN,
	PB_DOWN_BTN,
	PB_NW_BTN,
	PB_NE_BTN,
	PB_SW_BTN,
	PB_SE_BTN,
*/
	PB_DOWN_BTN,
	PB_SW_BTN,
	PB_LEFT_BTN,
	PB_NW_BTN,
	PB_TOP_BTN,
	PB_NE_BTN,
	PB_RIGHT_BTN,
	PB_SE_BTN,
	PB_FIRE_BTN,
	PB_BESERK_BTN,
	PB_BUSTER_BTN,
	PB_REPEL_BTN,
	PB_TELEPORT_BTN,
	BTN_ZOOMIN,
	BTN_ZOOMOUT,
	BTN_SIT,
	BTN_PAUSE,
	MENU1,
	MENU2,
	MENU3,
	MENU4,
	MENU5,
	BTN_CHAR_C_LEFT1,
	BTN_CHAR_C_RIGHT1,
	BTN_CHAR_C_LEFT2,
	BTN_CHAR_C_RIGHT2,
	BTN_CHAR_C_LEFT3,
	BTN_CHAR_C_RIGHT3,
	BTN_CHAR_C_LEFT4,
	BTN_CHAR_C_RIGHT4,
	BTN_CHAR_C_NAME,
	BTN_CHAR_C_CREATE,
	BTN_CHAR_C_RETURN,
	BTN_CHAR_S_SELECT1,
	BTN_CHAR_S_SELECT2,
	BTN_CHAR_S_SELECT3,
	BTN_CHAR_S_RETURN,
	BTN_INV_ROTLEFT,
	BTN_INV_ROTRIGHT,
	BTN_INV_PREV,
	BTN_INV_NEXT,
	BTN_INV_HOTKEY1,
	BTN_INV_HOTKEY2,
	BTN_INV_RETURN,
	BTN_PAUSE_CHAR,
	BTN_PAUSE_EQUIP,
	BTN_PAUSE_QUESTS,
	BTN_PAUSE_SKILLS,
	BTN_PAUSE_SYSTEM,
	BTN_PAUSE_RETURN,
	JOYSTICK,
	//this one should stay just before NUM_BUTTONS:
	TOUCH,
	//leave the line below:
	NUM_BUTTONS
};
#define NUM_BUTTONS_INV 12
#define NUM_BUTTONS_EQU 9

//panels
enum PANEL_IDS
{
	PANEL_MENU,
	PANEL_LOADING,
	PANEL_PAUSE,
	PANEL_CLEAR,
	PANEL_CHAR_SELECT,
	PANEL_CHAR_CREATE,
	PANEL_INV,
	//leave the line below:
	NUM_PANELS
};
enum TRANSITION_TYPE
{
	TRANSITION_NOP, //NOP=do nothing
	TRANSITION_NORMAL,
	TRANSITION_BLACK,
	//leave the line below:
	NUM_TRANSITIONS
};

//fonts
enum FONT_IDS
{
	FONT_NORMAL,
	FONT_CHAR,
	FONT_CHAR2,
	FONT_DMG,
	FONT_DMG_SHADOW,
	FONT_CRIT,
	FONT_CRIT_SHADOW,
	FONT_LOG1,
	FONT_LOG2,
	FONT_LOG_SHADOW,
	//leave the line below:
	NUM_FONTS
};

//external to Interface
void load_game();
void unload_game();
void load_resources();
extern unsigned int frameCount;
extern Player* player;
extern bool appLoaded;
extern bool avatarsLoaded;
extern bool resourcesLoaded;
extern bool gameStarted;
extern int levelToLoad;
extern int skillTimeCount[NUM_SKILLS];
extern int skillDelay[NUM_SKILLS];
extern int skillRange[NUM_SKILLS];
extern bool skillIsActive[NUM_SKILLS];
extern bool skillIsPressed[NUM_SKILLS];
extern bool directionIsPressed;
//--end

void load_interface_resources();
void load_interface();
void load_menu();
void load_interface_player();

void panelSetVisible(int panel, bool vis, int transition_type);
void panelSetAlpha(int panel, int alpha);
void updateHPbar(int hp);
void updateEXPbar(int exp);
void textLogUpdate();
void textLogLoot(int item_id);
void centerPlayer();
void mapPanning();
void showSkillAnim(GameObject *obj, int skill);
void showDamage(GameObject *dst, int damage, int font);

void inventoryDisplay(bool vis);

int EquipmentPressed(int id,int event,int x,int y);
int InventoryPressed(int id,int event,int x,int y);
int MenuPressed(int id,int event,int x,int y);
int SkillPressed(int id,int event,int x,int y);
int JoystickPressed(int id,int event,int x,int y);
int DirectionPressed(int id,int event,int x,int y);
int function_touch(int id,int event,int x,int y);
int ZoomPressed(int id,int event,int x,int y);
int SitPressed(int id,int event,int x,int y);
int PausePressed(int id,int event,int x,int y);

extern int viewBg;
extern int viewBlack;

extern int zoomPer;
extern int zoomFade;

extern bool autoAttack;
extern int autoNextSkill;
extern GameObject *autoTarget;

extern int viewFont[NUM_FONTS];
extern int viewPanel[NUM_PANELS];
extern int imgPanel[NUM_PANELS];
extern bool panelVisible[NUM_PANELS];
extern int panelIdOfButton[NUM_BUTTONS];
extern int panelAnimCurrent;
extern int panelAnimNext;
extern int panelAnimCurrent_alpha;
extern int panelAnimNext_alpha;

extern int selectedPlayer;
extern int viewPlayer, viewPlayerHead;
extern int viewPlayerDir;
extern char viewPlayerRot;

extern int panel_black;
extern int panel_avatar, view_panel_avatar;
extern int panel_hp, view_panel_hp;
extern int panel_hp_hide, view_panel_hp_hide, view_hp_black;
extern int panel_exp, view_panel_exp;
extern int panel_exp_hide, view_panel_exp_hide, view_exp_black;
extern int panel_level;
extern int panel_joystick, view_panel_joystick;
extern int view_touch;
extern Vector2 joystick_dir;

extern int currentButtonId;
extern int buttonHandle[NUM_BUTTONS];
extern int imgbuttonHandle1[NUM_BUTTONS];
extern int imgbuttonHandle2[NUM_BUTTONS];
extern int buttonEquId[NUM_BUTTONS_EQU];

extern int directionAnim[8];
extern int directionAnimIdle[8];
extern int directionAnimSit[8];

extern int skillIdOfButton[NUM_BUTTONS];
extern int buttonIdOfSkill[NUM_SKILLS];
extern int buttonMinX[NUM_BUTTONS];
extern int buttonMaxX[NUM_BUTTONS];
extern int buttonMinY[NUM_BUTTONS];
extern int buttonMaxY[NUM_BUTTONS];



// END OF INCLUDED FILE : Interface.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : item.h  (FILTER = ON)

struct item_data {
	char name[ITEM_NAME_LENGTH];
	//Do not add stuff between value_buy and wlv (see how getiteminfo works)
	int value_buy;
	int value_sell;
	int type;
	int equip_index;
	int weight;
	int bonus[NUM_STATS];
	int slots;
};

extern struct item_data item_db[NUM_ITEMS];


// END OF INCLUDED FILE : item.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : battle.h  (FILTER = ON)

bool inRange(int skill, GameObject *src, GameObject *dst, float *d);
int attack(int skill, GameObject *src, GameObject *dst);
int reachObject(GameObject *src, GameObject *dst);
int battle_calc_base_damage(struct status_data *status, OBJ_TYPE obj_type, struct weapon_atk *wa, int flag);


// END OF INCLUDED FILE : battle.h
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : SaveData.h  (FILTER = ON)

// SaveData.h -- data and functions for easily & securely saving and loading game data

typedef struct _PLAYERDATA
{
	bool free;
	int level;
	int id_class, id_sex, id_body, id_head;
	char name[10];
	struct item inventory[MAX_INVENTORY];
} PLAYERDATA;

typedef struct _GAMEDATA
{
	// Include variables for all data that needs to be saved here.
	// Remember NOT to include transient data, such as class objects
	// and/or pointers.  Also remember to include a default value for
	// each variable in the SetDefaultGameData() function.

	// example: (you can delete this)
	PLAYERDATA player_slot[3];

} GAMEDATA,*PGAMEDATA;

extern GAMEDATA gdGlobal;
extern GAMEDATA gdDefault;


// game data plus timestamp
typedef struct _GAMEDATA_TS
{
	// game data
	GAMEDATA gdData;
	// timestamp
	time_t ttStamp;
} GAMEDATA_TS,*PGAMEDATA_TS;

#define MAX_GD_STRING 27 // sets the maximum length of the game data filename

// prototypes //

void SaveGameData(const char* pFilename);
void LoadGameData(const char* pFilename);
char ComputeGameDataHash(PGAMEDATA_TS pGTData);
void CreateGameDataChecksums(PGAMEDATA_TS pGTData,
							 PGAMEDATA_TS pGTCheck);
void ValidateGameData(PGAMEDATA_TS pGTData,
					  PGAMEDATA_TS pGTCheck);
void InterlaceGameData(PGAMEDATA_TS pGTData,
					   PGAMEDATA_TS pGTCheck,
					   unsigned short* p16Data);
void DeinterlaceGameData(PGAMEDATA_TS pGTData,
					     PGAMEDATA_TS pGTCheck,
					     unsigned short* p16Data);
void SaveGameDataFile(char* pFilename,
					  unsigned short* p16Data);
void LoadGameDataFile(char* pFilename,
					  unsigned short* p16Data);


// END OF INCLUDED FILE : SaveData.h
//--------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//====================================================
// Source Files...
//====================================================
//----------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : DragonWrapper.cpp  (FILTER = ON)

// LINE FILTERED :: #include <stdio.h>
// LINE FILTERED :: #include <string>
// LINE FILTERED :: #include "DragonFireSDK.h"

/* TODO: switch to this writing system... (to close the file before app crashes)
long FileSeekStart(int fh){ return FileSeek(fh,0); }
long FileSize(int fh){
	FileSeekStart(fh);
	int iBytesRead,iPos; char szByte[1];
	for (iPos=0;;iPos++){ iBytesRead = FileRead(fh, szByte, 1); if (iBytesRead == 0) return iPos; }
	return 0;
}
long FileSeekEnd(int fh){ FileSeekStart(fh); return FileSeek(fh,FileSize(fh)); }
long FileAppend(int fh, char *buf){ FileSeekEnd(fh); return FileWrite(fh, buf, strlen(buf)); }

int fp_dbg=-1;
void opendbg(){ if(fp_dbg==-1) fp_dbg = FileOpen("debug.txt"); }
void closedbg(){ if(fp_dbg!=-1) FileClose(fp_dbg); fp_dbg=-1; }
void initdbg(){ fp_dbg=FileOpen("debug.txt"); if(fp_dbg==0) fp_dbg=FileCreate("debug.txt"); closedbg(); }
void printdbg(const char *txt){ opendbg(); FileAppend(fp_dbg, (char*)txt); closedbg(); }
void vardbg(int var){ opendbg(); char txt[100]; sprintf(txt,"%d",var); FileAppend(fp_dbg, txt); closedbg(); }
*/

int fout = 0;
char sout[1000];
bool uselessLogsActivated = false;
bool warningLogsActivated = false;

//-------------------------------------------------------

void DragonWrapperStart(int logType){
	if(logType==WRAPPER_LOG_WARNINGS)
		warningLogsActivated = true;
	if(logType==WRAPPER_LOG_EVERYTHING)
		uselessLogsActivated = true;

	if(logType==WRAPPER_LOG_WARNINGS || logType==WRAPPER_LOG_EVERYTHING)
		fout = FileCreate("wrapper_log.txt");

	if(fout==0){
		uselessLogsActivated = false;
		warningLogsActivated = false;
	}
}
void DragonWrapperEnd(){
	if(fout!=0)
		FileClose(fout);
}

int wImageAdd(char *filename){
	int r = ImageAdd(filename);
	if(uselessLogsActivated){
		sprintf(sout, "ImageAdd(%s) = %d\r\n", filename, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0){
			sprintf(sout, "ImageAdd(%s) = %d\r\n", filename, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}

int wViewAdd(int im,int x,int y){
	int r = ViewAdd(im,x,y);
	if(uselessLogsActivated){
		sprintf(sout, "ViewAdd(%d,%d,%d) = %d\r\n", im, x, y, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0 || im==0){
			sprintf(sout, "ViewAdd(%d,%d,%d) = %d\r\n", im, x, y, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wViewAdd(char *filename,int x,int y){
	int r = ViewAdd(filename,x,y);
	if(uselessLogsActivated){
		sprintf(sout, "ViewAdd(%s,%d,%d) = %d\r\n", filename, x, y, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0){
			sprintf(sout, "ViewAdd(%s,%d,%d) = %d\r\n", filename, x, y, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wViewAdd(char *filename,int x,int y,int (*callback)(int,int,int,int),int id){
	int r = ViewAdd(filename,x,y,callback,id);
	if(uselessLogsActivated){
		sprintf(sout, "ViewAdd(%s,%d,%d,callback_func,%d) = %d\r\n", filename, x, y, id, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0){
			sprintf(sout, "ViewAdd(%s,%d,%d,callback_func,%d) = %d\r\n", filename, x, y, id, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wViewSetImage(int vw,int im){
	int r = ViewSetImage(vw,im);
	if(uselessLogsActivated){
		sprintf(sout, "ViewSetImage(%d,%d) = %d\r\n", vw, im, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0 || vw==0 || im==0){
			sprintf(sout, "ViewSetImage(%d,%d) = %d\r\n", vw, im, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wViewSetVisible(int vw,int flag){
	int r = ViewSetVisible(vw,flag);
	if(uselessLogsActivated){
		sprintf(sout, "ViewSetVisible(%d,%d) = %d\r\n", vw, flag, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0 || vw==0){
			sprintf(sout, "ViewSetVisible(%d,%d) = %d\r\n", vw, flag, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}

int wTextAdd(int x,int y,char *text,int font){
	int r = TextAdd(x,y,text,font);
	if(uselessLogsActivated){
		sprintf(sout, "TextAdd(%d,%d,%s,%d) = %d\r\n", x, y, text, font, r);
		FileWrite(fout, sout, strlen(sout));
	}else if(warningLogsActivated)
		if(r==0){
			sprintf(sout, "TextAdd(%d,%d,%s,%d) = %d\r\n", x, y, text, font, r);
			FileWrite(fout, sout, strlen(sout));
		}
	return r;
}
int wTextSetText(int tx,char *text){
	int r = TextSetText(tx,text);
	//if(uselessLogsActivated) 
	//	fprintf(fout, "TextSetText(%d,%s) = %d\r\n", tx, text, r);
	return r;
}


// END OF INCLUDED FILE : DragonWrapper.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Vector2.cpp  (FILTER = ON)

// LINE FILTERED :: #include <math.h>
// LINE FILTERED :: #include "Vector2.h"
///////////////////////////////////////


/////////////////////////////////////////////////
const double Vector2::EPSILON = 1e-6;
const double Vector2::_INFINITY = 1.7976931348623158e+308;
const Vector2 Vector2::ZERO(0,0);
const Vector2 Vector2::UNIT_X(1,0);
const Vector2 Vector2::UNIT_Y(0,1);


///////////////////////////////////////////////////////////////////////////////
// construction and destruction
///////////////////////////////////////////////////////////////////////////////
Vector2::Vector2() : x(0),y(0)
{}

Vector2::Vector2(double fx, double fy) : x(fx), y(fy)
{}

Vector2::Vector2(const Vector2 &v) : x(v.x), y(v.y)
{}

Vector2::~Vector2()
{ }

///////////////////////////////////////////////////////////////////////////////
//  assignment & comparison
///////////////////////////////////////////////////////////////////////////////
Vector2& Vector2::operator= (const Vector2 &v)
{
	x = v.x;
	y = v.y;
	return *this;
}

bool Vector2::operator== (const Vector2 &v) const
{
	if(x == v.x && y == v.y) return true;
	return false;
}

bool Vector2::operator!= (const Vector2 &v) const
{
	if(x != v.x || y != v.y) return true;
	return false;
}


///////////////////////////////////////////////////////////////////////////////
// arithmetic operations
///////////////////////////////////////////////////////////////////////////////
Vector2 Vector2::operator+ (const Vector2 &v) const
{
	Vector2 sum;
	sum.x = x + v.x;
	sum.y = y + v.y;
	return sum;
}

Vector2 Vector2::operator- (const Vector2 &v) const
{
	Vector2 diff;
	diff.x = x - v.x;
	diff.y = y - v.y;
	return diff;
}

Vector2 Vector2::operator- () const
{
	Vector2 neg;
	neg.x = -x;
	neg.y = -y;
	return neg;
}

Vector2 Vector2::operator* (double f) const
{
	Vector2 prod;
	prod.x = x * f;
	prod.y = y * f;
	return prod;
}

Vector2 operator* (double f, const Vector2 &v)
{
	Vector2 prod;
	prod.x = v.x * f;
	prod.y = v.y * f;
	return prod;
}

Vector2 operator/ (double f, const Vector2 &v)
{
	Vector2 prod;
	prod.x = v.x / f;
	prod.y = v.y / f;
	return prod;
}

Vector2 Vector2::operator/ (double f) const
{
	Vector2 quot;

	if(f != 0)
	{
		double invquot = 1/f;
		quot.x = x * invquot;
		quot.y = y * invquot;
	}
	else
	{
		//tried to divide by 0.0, so return the closest thing
		//we can have to infinity
		quot.x = Vector2::_INFINITY;
		quot.y = Vector2::_INFINITY;
	}

	return quot;
}


///////////////////////////////////////////////////////////////////////////////
// arithmetic updates
///////////////////////////////////////////////////////////////////////////////
Vector2& Vector2::operator+= (const Vector2 &v)
{
	x += v.x;
	y += v.y;
	return *this;
}

Vector2& Vector2::operator-= (const Vector2 &v)
{
	x -= v.x;
	y -= v.y;
	return *this;
}
 
Vector2& Vector2::operator*= (double f)
{
	x *= f;
	y *= f;
	return *this;
}

Vector2& Vector2::operator/= (double f)
{
	if(f != 0)
	{
		double invquot = 1/f;
		x *= invquot;
		y *= invquot;
	}
	else
	{
		x = Vector2::_INFINITY;
		y = Vector2::_INFINITY;
	}

	return *this;
}
 

///////////////////////////////////////////////////////////////////////////////
// vector operations
///////////////////////////////////////////////////////////////////////////////
double Vector2::Length() const
{
	return sqrt((x*x) + (y*y));
}

double Vector2::LengthSquared() const
{
	return (x*x) + (y*y);
}

double Vector2::Unitize()
{
    double len = sqrt((x*x) + (y*y));

    if(len > Vector2::EPSILON)
    {
        double invlen = 1/len;
        x *= invlen;
        y *= invlen;
    }
    else
    {
        len = 0;
    }

    return len;
}

double Vector2::Dot(const Vector2 &v) const
{
    return (x*v.x) + (y*v.y);
}

double Vector2::Distance(const Vector2 &v) const
{
	double dx = v.x-x;
	double dy = v.y-y;
    return ( sqrt( dx*dx + dy*dy ) );
}

double Vector2::DistanceSquared(const Vector2 &v) const
{
	double dx = v.x-x;
	double dy = v.y-y;
    return ( dx*dx + dy*dy );
}

//void Vector2::Normalize()
//{
//
//}

Vector2 Vector2::Lerp(const Vector2 &v0, const Vector2 &v1, double T)
{
	//linear interpolation
	Vector2 trans;
	trans.x = v0.x + T*(v1.x - v0.x);
	trans.y = v0.y + T*(v1.y - v0.y);
	return trans;
}


/*****************************************************************************/

// END OF INCLUDED FILE : Vector2.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : status.cpp  (FILTER = ON)

//Inflicts damage on the target with the according walkdelay.
//If flag&1, damage is passive and does not triggers cancelling status changes.
//If flag&2, fail if target does not has enough to substract.
//If flag&4, if killed, mob must not give exp/loot.
//int status_damage(struct block_list *src,struct block_list *target,int hp, int sp, int walkdelay, int flag);

// END OF INCLUDED FILE : status.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : map.cpp  (FILTER = ON)

// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "map.h"
// LINE FILTERED :: #include "mob.h"
// LINE FILTERED :: #include "Monster.h"

struct block_list bl_head;
struct block_list *bl_tail = &bl_head;
int bl_count=0;

struct map_data maps[1];

int map_init(){
	bl_head.id=0;
	bl_head.next=0;
	bl_head.prev=0;
	return 0;
}

int map_spawn(int *map_id, int *map_stage){
	int map = *map_id;
	int stage = *map_stage;
	//map finished?
	if(stage >= maps[map].stages.size()){
		*map_id = maps[map].next_map;
		*map_stage = 0;
		map_spawn(map_id,map_stage);
		return 1;
	}else{
		//spawn points?
		Vector2 pos[4];
		pos[0].x = maps[map].size.x/2;	pos[0].y = 100;
		pos[1].x = maps[map].size.x-50;	pos[1].y = maps[map].size.y/2;
		pos[2].x = maps[map].size.x/2;	pos[2].y = maps[map].size.y;
		pos[3].x = 50;					pos[3].y = maps[map].size.y/2;
		int nextpos;
		for(int i=0 ; i<maps[map].stages[stage].quant ; i++){
			nextpos=(int)(RAND()*3.99999);
			new Monster(pos[nextpos].x, pos[nextpos].y, maps[map].stages[stage].mob_id);
		}
		return 0;
	}

}

void map_add_stage(int map_id, int mob_id, int quantity){
	struct map_stage newstage;
	newstage.mob_id=mob_id;
	newstage.quant=quantity;
	maps[map_id].stages.push_back(newstage);
}


// END OF INCLUDED FILE : map.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : AnimObject.cpp  (FILTER = ON)

// LINE FILTERED :: #include <vector>
// LINE FILTERED :: #include "DragonFireSDK.h"
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "AnimObject.h"
// LINE FILTERED :: #include "Resources.h"

extern Vector2 posBg;
extern Vector2 sizeBg;
extern int zoomPer;

AnimObject::AnimObject(double frameTime, bool loop) : 
	_rotDeg(0),_visible(false), _frames(0),
	_curTime(0), _curFrame(0), _maxTime(0),
	_viewHandle(0), _iterations(0), _paused(false)
{
	_isLoop = loop;	
	currentAnim = 0;
	_frameTime = frameTime;
	init();
	_viewHandle = wViewAdd("Images/dummy.png",0,0);
	_imagepos.x = 0;
	_imagepos.y = 0;
}

AnimObject::~AnimObject()
{
	//delete [] _frames; //martim... we have resources on memory now
	_frames = 0;
	setVisible(false); //martim... can't delete the view??
}

void AnimObject::init()
{
	_frames = weatherResources[0]._frames;
	_numFrames = weatherResources[0]._numFrames;
	_frameTime = weatherResources[0]._frameTime;
	_maxTime = weatherResources[0]._maxTime;
}

void AnimObject::setCurrentAnim(int animId)
{
	if(animId == currentAnim)
		return;

	currentAnim = animId;
	_curTime = 0;
	_maxTime = _numFrames[animId] * _frameTime;
}

void AnimObject::addAnim(int animId, char *baseFilename)
{
	//open files starting with 0 and open as many as can be found
	//we assume a .png extension
	int im = 0;
	int im_cnt = 0;
	char fn[512];
	int tmp[1000];

	sprintf(fn,"%s0.png",baseFilename,0);
	im = wImageAdd(fn);

	while(im && im_cnt < 1000)
	{
		tmp[im_cnt] = im;
		im_cnt++;
		
		sprintf(fn,"%s%d.png",baseFilename,im_cnt);
		im = wImageAdd(fn);
	}
	
	//copy over
	if(im_cnt > 0)
	{
		_frames[animId] = new int[im_cnt];
		_numFrames[animId] = im_cnt;

		for(int i=0; i<im_cnt; i++)
		{
			_frames[animId][i] = tmp[i];
		}
	}

	_maxTime = _numFrames[animId] * _frameTime;

}

void AnimObject::setVisible(bool vis)
{
	_visible = vis;
	wViewSetVisible(_viewHandle, vis);
}

bool AnimObject::isVisible() const
{
	return _visible;
}

bool AnimObject::setPosition(int _x, int _y)
{
	//dont leave borders... how to deal with this?
	if(_x < 0) _x = 0;
	if(_y < 0) _y = 0;
	if(_x >= sizeBg.x) _x = sizeBg.x;
	if(_y >= sizeBg.y) _y = sizeBg.y;

	//update
	_imagepos.x = _x;
	_imagepos.y = _y;
	return true;
}

bool AnimObject::isPaused() const
{
	return _paused;
}

int AnimObject::getIterations()
{
	return _iterations;
}

void AnimObject::setLoop(bool loop)
{
	_isLoop = loop;
}

bool AnimObject::getLoop() const
{
	return _isLoop;
}

///////////////////////////////////////////////////////////////////////////////
// start / stop / reset
///////////////////////////////////////////////////////////////////////////////
void AnimObject::start()
{
	_paused = false;
	update(0.0);
}

void AnimObject::stop()
{
	_paused = true;
	update(0.0);
}

void AnimObject::reset()
{
	_curTime = 0;
	update(0.0);
}

///////////////////////////////////////////////////////////////////////////////
// update
///////////////////////////////////////////////////////////////////////////////
void AnimObject::update(double dt)
{
	if(!isVisible())
		return;

	if(_paused){
		//martim: viewHandle could have changed from Z-order... update image even if paused
		reloadView();
		return;
	}else
		_curTime += dt;

	//compute the current iteration time
	_iterations = int(_curTime / _maxTime);
	double interval_time = _curTime - ((float)_iterations * _maxTime);
	
	//compute the current frame
	_curFrame = (int)(interval_time / _frameTime);
	
	if(_curFrame < 0) _curFrame = 0;
	if(_curFrame >= _numFrames[currentAnim]) _curFrame = _numFrames[currentAnim]-1;

	//set the image
	reloadView();
	
	if ((_curFrame == _numFrames[currentAnim] - 1) && !_isLoop)
	{
		stop();
	}
}

void AnimObject::reloadView()
{
	if(_viewHandle>0){
		wViewSetVisible(_viewHandle, isVisible());
		wViewSetImage(_viewHandle, _frames[currentAnim][_curFrame]);
		ViewSetxy(_viewHandle, (int)_imagepos.x, (int)_imagepos.y);
		ViewSetSize(_viewHandle, (int)(ViewGetWidth(_viewHandle)*zoomPer/100.0), (int)(ViewGetHeight(_viewHandle)*zoomPer/100.0));
	}
}


// END OF INCLUDED FILE : AnimObject.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : GameObject.cpp  (FILTER = ON)

//#include "DragonFireSDK.h"
// LINE FILTERED :: #include <string.h>
// LINE FILTERED :: #include <vector>
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "Resources.h"
// LINE FILTERED :: #include "GameObject.h"
// LINE FILTERED :: #include "Player.h"
// LINE FILTERED :: #include "Interface.h"
///////////////////////////////////////

//martim...
Vector2 posBg;
Vector2 sizeBg;

//GameObject ***mapCells;
Matrix<GameObject*> mapCells;
//--end


///////////////////////////////////////////////////////////////////////////////
// construction and destruction
///////////////////////////////////////////////////////////////////////////////
GameObject::GameObject(double frameTime, bool loop) : 
	_rotDeg(0),_visible(false), _frames(0),
	_curTime(0), _curFrame(0), _maxTime(0),
	_viewHandle(0), _iterations(0), _paused(false),
	isDead(false)
{
	_isLoop = loop;	
	currentAnim = 0;
	_frameTime = frameTime;
	//martim
	_frames = 0;
	_numFrames = 0;
	_viewHandle = -1;
	expiryDate = 0;
	loopOnDeath = true;
	//views
	for(int i=0 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++){
		view_shift_x[i]=0;
		view_shift_y[i]=0;
	}
	//handles...
	handles = popHandles();
	textHandlerDmg[0] = handles.text[0];
	textHandlerDmg[1] = handles.text[1];
	textHandlerCrit[0] = handles.text[2];
	textHandlerCrit[1] = handles.text[3];
	textTimer = 0;
	dmgType = FONT_DMG;
	//status data
	memset(&status, 0, sizeof(struct status_data));
	status.effect_src = new struct status_data*[NUM_STATUS];
	for(int i=0; i<NUM_STATUS ; i++)
		status.effect_src[i] = NULL;
}

GameObject::~GameObject()
{
	//handles...
	pushHandles(handles);

	//clean stuff...
	delete[] status.effect_src;
	//delete [] _frames; //martim... we have resources on memory now
	_frames = 0;
	setVisible(false); //martim... can't delete the view??
}

void GameObject::setCurrentAnim(int animId)
{
	//if dead, we can only set death animations
	if(isDead && animId!=DEATH_LEFT && animId!=DEATH_RIGHT)
		return;

	if(animId == currentAnim)
		return;

	currentAnim = animId;
	_curTime = 0;
	_maxTime = _numFrames[animId] * _frameTime;
	//type;
	if(handles.view[0]>0){ //maybe make a better check...
		posZero2Feet.x = ImageGetWidth(_frames[animId][0])/2;
		posZero2Feet.y = ImageGetHeight(_frames[animId][0]);
		//_imagepos = _pos-posZero2Feet; //oct2011 trying to fix flashes
	}	
}

void GameObject::addAnim(int animId, char *baseFilename)
{
	//open files starting with 0 and open as many as can be found
	//we assume a .png extension
	int im = 0;
	int im_cnt = 0;
	char fn[512];
	int tmp[1000];

	sprintf(fn,"%s0.png",baseFilename,0);
	im = wImageAdd(fn);

	while(im && im_cnt < 1000)
	{
		tmp[im_cnt] = im;
		im_cnt++;
		
		sprintf(fn,"%s%d.png",baseFilename,im_cnt);
		im = wImageAdd(fn);
	}
	
	//copy over
	if(im_cnt > 0)
	{
		_frames[animId] = new int[im_cnt];
		_numFrames[animId] = im_cnt;

		for(int i=0; i<im_cnt; i++)
		{
			_frames[animId][i] = tmp[i];
		}
	}

	_maxTime = _numFrames[animId] * _frameTime;

	if(handles.view[0]==-1) //martim: make only one handler per object!
		handles.view[0] = wViewAdd(_frames[animId][0], (int)_imagepos.x, (int)_imagepos.y);
	wViewSetVisible(handles.view[0],0);
}

void GameObject::setDead(bool dead)
{
	isDead = dead;
	if(dead){
		//martim... animation
		expiryDate=60;
		if(direction==1) setCurrentAnim(DEATH_RIGHT);
		else setCurrentAnim(DEATH_LEFT);
		if(!loopOnDeath) _isLoop=false;
	}else{
		setVisible(true);
		_paused=false;
		_isLoop=true;
	}
}

void GameObject::init(int width, int height, int x, int y)
{
	posZero2Feet.x = width/2;
	posZero2Feet.y = height;
	_pos.x = x;
	_pos.y = y;
	_imagepos = _pos-posZero2Feet;

	setVisible(false);
	setDead(false);
	ViewSetxy(handles.view[0], (int)_imagepos.x, (int)_imagepos.y);
}

///////////////////////////////////////////////////////////////////////////////
// visible
///////////////////////////////////////////////////////////////////////////////
void GameObject::setVisible(bool vis)
{
	_visible = vis;
	wViewSetVisible(handles.view[0], vis ? 1 : 0);
	//text...
	if(textTimer>0){
		if(dmgType==FONT_DMG){
			TextSetVisible(textHandlerDmg[0], vis);
			TextSetVisible(textHandlerDmg[1], vis);
		}else if(dmgType==FONT_CRIT){
			TextSetVisible(textHandlerCrit[0], vis);
			TextSetVisible(textHandlerCrit[1], vis);
		}
	}
	//visual effects...
}

bool GameObject::isVisible() const
{
	return _visible;
}

///////////////////////////////////////////////////////////////////////////////
// position
///////////////////////////////////////////////////////////////////////////////
bool GameObject::setPosition(Vector2 pos)
{
	return setPosition(pos.x, pos.y);
}

bool GameObject::setPosition(int _x, int _y)
{
	//dont leave borders... how to deal with this?
	if(_x < 0) _x = 0;
	if(_y < 0) _y = 0;
	if(_x >= sizeBg.x) _x = sizeBg.x;
	if(_y >= sizeBg.y) _y = sizeBg.y;

	//is it occupied (by someone else)?
	GameObject *next_who = mapCells[_y/BLOCK_SIZE][_x/BLOCK_SIZE];
	if(next_who!=NULL && next_who!=this){
		return false;
	}

	//update cells
	mapCells[(int)_pos.y/BLOCK_SIZE][(int)_pos.x/BLOCK_SIZE]=NULL;
	mapCells[_y/BLOCK_SIZE][_x/BLOCK_SIZE]=this;

	//update
	_pos.x = _x;
	_pos.y = _y;
	return true;
}

Vector2 GameObject::getPosition() const
{
	return _pos;
}

bool GameObject::isPaused() const
{
	return _paused;
}

///////////////////////////////////////////////////////////////////////////////
// rotation
///////////////////////////////////////////////////////////////////////////////
void GameObject::setRotation(double deg)
{
	_rotDeg = deg;
	/*ViewSetRotate(handles.view[0],(int)deg);*/
}

double GameObject::getRotation() const
{
	return _rotDeg;
}

///////////////////////////////////////////////////////////////////////////////
// scale
///////////////////////////////////////////////////////////////////////////////
void GameObject::setScale(Vector2 &scale)
{
	_scale = scale;
}

Vector2 GameObject::getScale() const
{
	return _scale;
}

///////////////////////////////////////////////////////////////////////////////
// iterations
///////////////////////////////////////////////////////////////////////////////
int GameObject::getIterations()
{
	return _iterations;
}

//set/get the sprite loop
void GameObject::setLoop(bool loop)
{
	_isLoop = loop;
}

bool GameObject::getLoop() const
{
	return _isLoop;
}

///////////////////////////////////////////////////////////////////////////////
// start / stop / reset
///////////////////////////////////////////////////////////////////////////////
void GameObject::start()
{
	_paused = false;
	update(0.0);
}

void GameObject::stop()
{
	_paused = true;
	update(0.0);
}

void GameObject::reset()
{
	_curTime = 0;
	update(0.0);
}

///////////////////////////////////////////////////////////////////////////////
// update
///////////////////////////////////////////////////////////////////////////////
void GameObject::update(double dt)
{
	//martim
	if(textTimer>0){
		int t = _imagepos.y-30, b = _imagepos.y-5, f;
		if(textTimer>5)
			f = t + textTimer*(b-t)/(30-5);
		else
			f = t + 5*(b-t)/(30-5);
		if(dmgType==FONT_DMG){
			if(textTimer==1){
				TextSetVisible(textHandlerDmg[0], 0);
				TextSetVisible(textHandlerDmg[1], 0);
			}else{
				TextSetxy(textHandlerDmg[0], (int)(_imagepos.x-10+0.5*ViewGetWidth(handles.view[0])), f);
				TextSetxy(textHandlerDmg[1], (int)(_imagepos.x-10+0.5*ViewGetWidth(handles.view[0]))+2, f+2);
			}
		}else if(dmgType==FONT_CRIT){
			if(textTimer==1){
				TextSetVisible(textHandlerCrit[0], 0);
				TextSetVisible(textHandlerCrit[1], 0);
			}else{
				TextSetxy(textHandlerCrit[0], (int)(_imagepos.x-14+0.5*ViewGetWidth(handles.view[0])), f-8);
				TextSetxy(textHandlerCrit[1], (int)(_imagepos.x-14+0.5*ViewGetWidth(handles.view[0]))+2, f-8+2);
			}
		}
		textTimer--;
	}
	//--orig:

	if(_paused){
		//martim: viewHandle could have changed from Z-order... update image even if paused
		reloadView();
		return;
	}else
		_curTime += dt;

	//compute the current iteration time
	_iterations = int(_curTime / _maxTime);
	double interval_time = _curTime - ((float)_iterations * _maxTime);
	
	//compute the current frame
	_curFrame = (int)(interval_time / _frameTime);
	
	if(_curFrame < 0) _curFrame = 0;
	if(_curFrame >= _numFrames[currentAnim]) _curFrame = _numFrames[currentAnim]-1;
	type;
	//set the image
	reloadView();
	
	//if(type==TYPE_PLAYER && currentAnim==BUSTER_LEFT || currentAnim==BUSTER_RIGHT)
		//return;

	if ((_curFrame == _numFrames[currentAnim] - 1) && !_isLoop)
	{
		stop();
		//setVisible(false); //martim
	}
}

void GameObject::reloadView()
{
	view_img2load[0] = _frames[currentAnim][_curFrame];

	for(int i=0 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++)
		if(view_img2load[i]>0){
			wViewSetVisible(handles.view[i], isVisible());
			wViewSetImage(handles.view[i], view_img2load[i]);
			ViewSetxy(handles.view[i], (int)_imagepos.x+(view_shift_x[i]*zoomPer/100.0), (int)_imagepos.y+(view_shift_y[i]*zoomPer/100.0));
			ViewSetSize(handles.view[i], (int)(ViewGetWidth(handles.view[i])*zoomPer/100.0), (int)(ViewGetHeight(handles.view[i])*zoomPer/100.0));
		}else{
			wViewSetVisible(handles.view[i], 0);
		}
}

void GameObject::reloadText()
{
	textHandlerDmg[0] = handles.text[0];
	textHandlerDmg[1] = handles.text[1];
	textHandlerCrit[0] = handles.text[2];
	textHandlerCrit[1] = handles.text[3];
	if(textTimer>1){
		if(dmgType==FONT_DMG){
			wTextSetText(textHandlerDmg[0], textStr);
			wTextSetText(textHandlerDmg[1], textStr);
			TextSetVisible(textHandlerDmg[0], 1);
			TextSetVisible(textHandlerDmg[1], 1);
			TextSetVisible(textHandlerCrit[0], 0);
			TextSetVisible(textHandlerCrit[1], 0);
		}else if(dmgType==FONT_CRIT){
			wTextSetText(textHandlerCrit[0], textStr);
			wTextSetText(textHandlerCrit[1], textStr);
			TextSetVisible(textHandlerDmg[0], 0);
			TextSetVisible(textHandlerDmg[1], 0);
			TextSetVisible(textHandlerCrit[0], 1);
			TextSetVisible(textHandlerCrit[1], 1);
		}
	}else{
		TextSetVisible(textHandlerDmg[0], 0);
		TextSetVisible(textHandlerDmg[1], 0);
		TextSetVisible(textHandlerCrit[0], 0);
		TextSetVisible(textHandlerCrit[1], 0);
	}
}


/*****************************************************************************/

// END OF INCLUDED FILE : GameObject.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Player.cpp  (FILTER = ON)

// LINE FILTERED :: #include <string.h>
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "map.h"
// LINE FILTERED :: #include "Resources.h"
// LINE FILTERED :: #include "GameObject.h"
// LINE FILTERED :: #include "Player.h"
// LINE FILTERED :: #include "Interface.h"
// LINE FILTERED :: #include "item.h"
// LINE FILTERED :: #include "SaveData.h"

Player::Player(int x, int y) : GameObject()
{
	//starting map
	map = 0;
	map_stage = 0;

	type = TYPE_PLAYER;
	//base stats
	status.level = 1;
	status.str = 5;
	status.vit = 5;
	status.speed = 4;
	//indirect stats
	status.batk = 40;
	status.cri = 5;
	status.hp = status.max_hp = 200;
	status.def = 0;
	//weapon stats
	status.rhw.atk = 2;
	status.rhw.atk2 = 10;
	status.rhw.ele = ELE_NEUTRAL;
	status.rhw.range = 1;
	
	//other vars
	_pos.x = x;
	_pos.y = y;
	direction = RIGHT;
	viewHPDisplay = wViewAdd("Images/hpDisplay.png", (int)_pos.x+2, (int)_pos.y-10);
	invisibleTimeCount=0;
	lasthitTimeCount=0;

	//add animations
	_heads = playerResources[0]._heads[0];
	_frames = playerResources[0]._frames;
	_frameTime = playerResources[0]._frameTime;
	_maxTime = playerResources[0]._maxTime;
	_numFrames = playerResources[0]._numFrames;
	//handles.view[0] = wViewAdd(_frames[IDLE_RIGHT][0], (int)_imagepos.x, (int)_imagepos.y);
	//handles.view[1] = wViewAdd(_heads[DIR_S], (int)_imagepos.x, (int)_imagepos.y);
	wViewSetImage(handles.view[0], _frames[IDLE_RIGHT][0]);
	ViewSetxy(handles.view[0], (int)_imagepos.x, (int)_imagepos.y);
	wViewSetImage(handles.view[1], _heads[DIR_S]);
	ViewSetxy(handles.view[1], (int)_imagepos.x, (int)_imagepos.y);

	view_img2load[NUM_VIEWHANDLES_PER_OBJ-1]=0;
	//skill anims are shifted
	view_shift_x[NUM_VIEWHANDLES_PER_OBJ-1] = -40;
	view_shift_y[NUM_VIEWHANDLES_PER_OBJ-1] = -92;

	dir = DIR_S;
	isSitting = false;
	skillInUse = -1;

	//init...
	init(ViewGetWidth(handles.view[0]), ViewGetHeight(handles.view[0]), _pos.x, _pos.y); //setup init position and size of the sprite
	setVisible(true);
	setCurrentAnim(IDLE_DOWN);
	setHeadDir(dir);

}

void Player::save()
{
	//save game...
	gdGlobal.player_slot[selectedPlayer].level = status.level;
	gdGlobal.player_slot[selectedPlayer].id_head = id_head;
	memcpy(gdGlobal.player_slot[selectedPlayer].inventory, inventory, MAX_INVENTORY*sizeof(struct item));
	SaveGameData("ixile_save");
}

void Player::addItem(int item_id, int amount)
{
	int i;
	for(i=0 ; i<MAX_INVENTORY ; i++){
		if(inventory[i].item_id==-1){
			inventory[i].item_id = item_id;
			inventory[i].amount = amount;
			inventory[i].equip = false;
			break;
		}
	}
	if(!gameStarted) return;
	if(i<MAX_INVENTORY)
		textLogLoot(item_id); //display loot text
	else
		; //display inventory full

	//save...
	save();
}

void Player::setLevel(int lvl)
{
	if(lvl>MAX_LEVEL) return;
	//new stats
	status.level = lvl;
	computeStats();
	status.hp = status.max_hp;
	status.exp = 0;
	//new delays
	//if(skillDelay[SKILL_TELEPORT] > 6*30) skillDelay[SKILL_TELEPORT] -= 5*30;

	//save...
	save();
}

void Player::computeStats()
{
	//base stats
	status.batk		= 40;
	status.cri		= 5;
	status.max_hp	= 200;
	status.def		= 0;
	status.speed	= 100;

	//levelups
	int lvlups = status.level-1;
	status.batk		+= 5*lvlups;
	status.cri		+= lvlups;
	status.max_hp	+= 10*lvlups;
	status.def		+= lvlups;

	//items
	for(int i=0 ; i<MAX_INVENTORY ; i++)
		if(inventory[i].item_id!=-1 && inventory[i].equip){
			status.batk		+= item_db[ inventory[i].item_id ].bonus[ATK];
			status.cri		+= item_db[ inventory[i].item_id ].bonus[CRI];
			status.max_hp	+= item_db[ inventory[i].item_id ].bonus[HP];
			status.def		+= item_db[ inventory[i].item_id ].bonus[DEF];
			status.speed	+= item_db[ inventory[i].item_id ].bonus[SPEED];
		}
}

void Player::checkPositionLimits()
{
	//check off-screen limits

	if(player->_pos.x < player->posZero2Feet.x)
		player->_pos.x = player->posZero2Feet.x;

	if(player->_pos.x > sizeBg.x - player->posZero2Feet.x)
		player->_pos.x = sizeBg.x - player->posZero2Feet.x;

	if(player->_pos.y < player->posZero2Feet.y)
		player->_pos.y = player->posZero2Feet.y;

	if(player->_pos.y > sizeBg.y)
		player->_pos.y = sizeBg.y;
}

void Player::setVisible(bool vis)
{
	GameObject::setVisible(vis);
	for(int i=1 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++)
		wViewSetVisible(handles.view[i], vis);
	//wViewSetVisible(viewHPDisplay, vis ? 1 : 0);
}

void Player::setHead(int head)
{
	if(head<0 || head>=NUM_HEADS) return;
	id_head = head;
	_heads = playerResources[0]._heads[head];
	setHeadDir(dir);
}


void Player::setHeadDir(int dir_)
{
	if(currentAnim==SKILL_LEFT)
		view_img2load[1] = _heads[DIR_SW];
	else if(currentAnim==SKILL_RIGHT)
		view_img2load[1] = _heads[DIR_SE];
	else
		view_img2load[1] = _heads[dir_];

	if(isSitting) view_shift_y[1] = 18;
	else view_shift_y[1] = 0;
}

void Player::setDir(int dir_)
{
	dir=dir_;

	if(dir>=DIR_S && dir<=DIR_NW)
		direction=LEFT;
	else
		direction=RIGHT;
}

void Player::idle()
{
	if(isSitting)
		setCurrentAnim(directionAnimSit[dir]);
	else
		setCurrentAnim(directionAnimIdle[dir]);
	setHeadDir(dir);
}

void Player::useSkill(int skillId)
{
	skillInUse = skillId;
	skillCounter = 0;
	if(skillId==-1){
		setCurrentAnim(dir); //IDLE_ANIM ids = DIR ids
	}else{
		if(direction==LEFT) 
			setCurrentAnim(SKILL_LEFT);
		else 
			setCurrentAnim(SKILL_RIGHT);
		if(skillId!=SKILL_FIRE)
			lockedForAnimation = true;
	}
}

void Player::updateAnims()
{
	int anim=-1;

	//dead?
	if(isDead){
		for(int i=1 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++)
			view_img2load[i] = 0;
		return;
	}

	//skills?
	switch(skillInUse){
	case -1:
		//view_img2load[2]=0;
		break;
	case SKILL_FIRE:
		if(direction==RIGHT) anim=SHOOT_RIGHT;
		else anim=SHOOT_LEFT;
		break;
	case SKILL_BESERK:
		if(direction==RIGHT) anim=BESERK_RIGHT;
		else anim=BESERK_LEFT;
		break;
	case SKILL_BUSTER:
		if(direction==RIGHT) anim=BUSTER_RIGHT;
		else anim=BUSTER_LEFT;
		break;
	case SKILL_REPEL:
		if(direction==RIGHT) anim=REPEL_RIGHT;
		else anim=REPEL_LEFT;
		break;
	case SKILL_TELEPORT:
		if(direction==RIGHT) anim=TELEPORT_RIGHT;
		else anim=TELEPORT_LEFT;
		break;
	}
	
	if(anim>=0){
		//play once and stop skill anim
		if(lockedForAnimation){ 
			//this kind of animation stops with last frame
			if(skillCounter >= _numFrames[anim]){
				setDir(DIR_S); setHeadDir(DIR_S);
				setCurrentAnim(dir); //IDLE_ANIM ids = DIR ids
				skillInUse=-1;
				skillCounter = 0;
				view_img2load[NUM_VIEWHANDLES_PER_OBJ-1] = 0;
				lockedForAnimation=false;
			}else{
				view_img2load[NUM_VIEWHANDLES_PER_OBJ-1]=_frames[anim][skillCounter++];
			}
		}else{
			//this kind of animation loops
			if(skillCounter >= _numFrames[anim])
				skillCounter = 0;
			view_img2load[NUM_VIEWHANDLES_PER_OBJ-1]=_frames[anim][skillCounter++];
		}
	}else{
		skillCounter = 0;
		view_img2load[NUM_VIEWHANDLES_PER_OBJ-1] = 0;
		lockedForAnimation=false;
	}

	//items?
	if(buttonEquId[EQI_HEAD_MID]!=-1){
		if(skillInUse==-1){
			view_img2load[2] = itemResources[8]._frames[dir][0];
			if(isSitting)
				view_shift_y[2] = 18;
			else
				view_shift_y[2] = 0;
		}else{
			if(direction==LEFT)
				view_img2load[2] = itemResources[8]._frames[DIR_SW][0];
			else
				view_img2load[2] = itemResources[8]._frames[DIR_SE][0];
		}
	}else{
		view_img2load[2] = 0;
	}
}

void Player::update()
{
	//is he dead?
	if(isDead && expiryDate>0)
		expiryDate--;
	if(!isDead && status.hp<=0){
		setDead(true);
		view_img2load[1] = 0; //hide head
		currentButtonId=-1;
		player->skillInUse=-1;
		for(int i=0 ; i<NUM_SKILLS ; i++){
			skillTimeCount[i]=-1;
			skillIsActive[i]=false;
			skillIsPressed[i]=false;
			wViewSetImage(buttonHandle[buttonIdOfSkill[i]], imgbuttonHandle1[buttonIdOfSkill[i]]);
		}
	}

	//invisible?
	if(!isVisible())
		invisibleTimeCount++;

	//last time player was hit
	lasthitTimeCount++;

	//level up?
	if(status.exp >= 100)
		setLevel(status.level+1);

	//sprite position
	/*
	Vector2 shift(0,0);
	if(lockedForAnimation){ //for skills (not fire)
		shift.y=-27;
	}
	_imagepos = posBg + (_pos-(posZero2Feet+shift))*(zoomPer/100.0);
	*/
	Vector2 diff = _imagepos-(posBg + (_pos-(posZero2Feet))*(zoomPer/100.0));
	if(diff.Length()>50)
		printf("sprite jump!!\n");
	_imagepos = posBg + (_pos-(posZero2Feet))*(zoomPer/100.0);

	//updates...
	setHeadDir(dir);
	updateAnims(); //skills and stuff
	GameObject::update();

}


// END OF INCLUDED FILE : Player.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Monster.cpp  (FILTER = ON)

// LINE FILTERED :: #include <string.h>
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "map.h"
// LINE FILTERED :: #include "mob.h"
// LINE FILTERED :: #include "Resources.h"
// LINE FILTERED :: #include "GameObject.h"
// LINE FILTERED :: #include "Monster.h"
// LINE FILTERED :: #include "Player.h"
// LINE FILTERED :: #include "Interface.h"
// LINE FILTERED :: #include "Configuration.h"
// LINE FILTERED :: #include "battle.h"

extern Player *player;

Monster::Monster(int x, int y, int id) : GameObject()
{
	if(x==0) x = sizeBg.x*RAND();
	if(y==0) y = 100+(sizeBg.y-100)*RAND();

	md.bl.next = NULL;
	md.bl.prev = bl_tail;
	bl_tail = bl_tail->next = &md.bl;
	bl_count++;

	md.bl.id=id;
	md.bl.x=x;
	md.bl.y=y;
	md.bl.obj=this;

	targetType=MOB_TARGET_PLAYER;
	targetCell.x=-1;

	timerIdle=0;
	skillTimeCount=-1;
	loopOnDeath = mobStats[id][LOOP];

	type = TYPE_MOB;
	//base stats
	status.level = md.level = 1;
	status.batk = mobStats[id][ATK];
	status.cri = mobStats[id][CRI];
	status.max_hp = status.hp = mobStats[id][HP];
	status.def = mobStats[id][DEF];
	status.speed = mobStats[id][SPEED];
	status.exp = mobStats[id][EXP];
	//weapon stats
	status.rhw.atk = 2;
	status.rhw.atk2 = 4;
	status.rhw.ele = ELE_NEUTRAL;
	status.rhw.range = 1;
		
	_pos.x = x;
	_pos.y = y;
	direction = RIGHT;
	//viewHPDisplay = wViewAdd("Images/hpDisplay.png", (int)_pos.x+2, (int)_pos.y-10);

	//add animations
	_frames = mobResources[id]._frames;
	_frameTime = mobResources[id]._frameTime;
	_maxTime = mobResources[id]._maxTime;
	_numFrames = mobResources[id]._numFrames;
	//handles.view[0] = wViewAdd(_frames[IDLE_RIGHT][0], (int)_imagepos.x, (int)_imagepos.y);
	wViewSetImage(handles.view[0], _frames[IDLE_RIGHT][0]);
	ViewSetxy(handles.view[0], (int)_imagepos.x, (int)_imagepos.y);
	view_img2load[1]=0;
	view_img2load[2]=0;

	//init...
	init(ViewGetWidth(handles.view[0]), ViewGetHeight(handles.view[0]), _pos.x, _pos.y); //setup init position and size of the sprite
	setVisible(true);
	setCurrentAnim(IDLE_RIGHT);

	update();
}

Monster::~Monster()
{
	//block_list...
	if(md.bl.next) 
		md.bl.next->prev = md.bl.prev;
	if(md.bl.prev) 
		md.bl.prev->next = md.bl.next;
	if(bl_tail==&md.bl) 
		bl_tail = md.bl.prev;
	bl_count--;

	//update map
	mapCells[(int)_pos.y/BLOCK_SIZE][(int)_pos.x/BLOCK_SIZE]=NULL;

	TextSetVisible(textHandlerDmg[0], 0);
	TextSetVisible(textHandlerCrit[0], 0);
	TextSetVisible(textHandlerDmg[1], 0);
	TextSetVisible(textHandlerCrit[1], 0);
	//call GameObject destructor ?
}

void Monster::walkTo(Vector2 walkVec, float speed)
{
	Vector2 next;

	//direction
	if(walkVec.x>0){
		direction=RIGHT;
		setCurrentAnim(WALK_RIGHT);
	}else{
		direction=LEFT;
		setCurrentAnim(WALK_LEFT);
	}
	
	//simplest greedy approach...
	walkVec.Unitize();
	next.x = _pos.x + (int)(walkVec.x*speed);
	next.y = _pos.y + (int)(walkVec.y*speed);
	
	//dont leave borders... how to deal with this?
	if(next.x < 0) next.x = 0;
	if(next.y < 0) next.y = 0;
	if(next.x >= sizeBg.x) next.x = sizeBg.x;
	if(next.y >= sizeBg.y) next.y = sizeBg.y;

	//is it occupied (by someone else)?

	GameObject *next_who = mapCells[(int)next.y/BLOCK_SIZE][(int)next.x/BLOCK_SIZE];
	if(next_who!=NULL && next_who!=this){
		next.x = _pos.x + (int)(walkVec.y*speed);
		next.y = _pos.y - (int)(walkVec.x*speed);
		if(next.x<0 || next.x>=sizeBg.x || next.y<0 || next.y>=sizeBg.y){
			next.x = _pos.x - (int)(walkVec.y*speed);
			next.y = _pos.y + (int)(walkVec.x*speed);
		}
		if(next.x<0 || next.x>=sizeBg.x || next.y<0 || next.y>=sizeBg.y){
			next.x = _pos.x;
			next.y = _pos.y;
		}
	}
	
	//update...
	mapCells[(int)_pos.y/BLOCK_SIZE][(int)_pos.x/BLOCK_SIZE]=NULL;
	mapCells[(int)next.y/BLOCK_SIZE][(int)next.x/BLOCK_SIZE]=this;

	_pos = next;
}

int Monster::runAI()
{
	if(isDead || status.effect[STATUS_STUN]) 
		return MOB_STATUS_IDLE;

	Vector2 diff = player->_pos - _pos;

	//can see player?
	if(diff.Length() / BLOCK_SIZE <= 5 && player->isVisible()){
		targetType = MOB_TARGET_PLAYER;
		//can attack?
		if(attack(SKILL_FIRE,this,player)==-1){
			walkTo(diff, status.speed);
			return MOB_STATUS_FOLLOW;
		}else{
			return MOB_STATUS_ATTACK;
		}
	}else{
		targetType = MOB_TARGET_CELL;
		//idle?
		if(timerIdle>0){
			timerIdle--;
			return MOB_STATUS_IDLE;
		}else{
			//is cell chosen already?
			if(targetCell.x!=-1){
				//move towards cell (slower speed?)
				walkTo(targetCell-_pos, 0.75*status.speed);
				//reached cell?
				if((targetCell-_pos).Length() < 20){
					targetCell.x = -1;
				}
				return MOB_STATUS_ROAM;
			}else{
				//choose cell
				targetCell.x = sizeBg.x*RAND();
				targetCell.y = 100+(sizeBg.y-100)*RAND();
				timerIdle=60;
				return MOB_STATUS_IDLE;
			}
		}
	}
}

void Monster::setVisible(bool vis)
{
	GameObject::setVisible(vis);
	//wViewSetVisible(viewHPDisplay, vis ? 1 : 0);
}

void Monster::update()
{
	//status effects...
	if(!isDead && status.hp>0)
	for(int i=0 ; i<NUM_STATUS ; i++)
		if(status.effect[i]){
			switch(i){
			case STATUS_BESERKED: if(status.effect[i] % 15 == 0) status.hp -= 20;
				if(status.hp<=0){
					status.hp=0;
					if(status.effect_src[i]!=NULL) //our player ID is 1...
						status.effect_src[i]->exp += status.exp;
				}
				break;
			case STATUS_STUN:
				break;
			}
			status.effect[i]--;
		}

	//is he dead?
	if(isDead && expiryDate>0)
		expiryDate--;
	if(!isDead && status.hp<=0)
		setDead(true);

	//sprite position
	_imagepos = posBg + (_pos-posZero2Feet)*(zoomPer/100.0);

	//updates...
	GameObject::update();

}


// END OF INCLUDED FILE : Monster.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Resources.cpp  (FILTER = ON)

// LINE FILTERED :: #include <vector>
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "DragonFireSDK.h"
// LINE FILTERED :: #include "Resources.h"
// LINE FILTERED :: #include "GameObject.h"
// LINE FILTERED :: #include "Player.h"
// LINE FILTERED :: #include "Interface.h"

// construction and destruction
Resources::Resources() : 
	_frameTime(0.0333), _maxTime(0)
{
	_frames = new int*[NUM_ANIMS];
	for(int i=0 ; i<NUM_ANIMS ; i++){
		_numFrames[i] = 0;
		_frames[i] = NULL;
	}
}

Resources::~Resources()
{ 
	delete [] _frames;
	_frames = 0;
}

// add head
void Resources::addHead(int headId, int dir, char *filename)
{
	_heads[headId][dir] = wImageAdd(filename);
}

// add animation
void Resources::addAnim(int animId, const char *baseFilename)
{
	//check if animation was not added already
	if(_numFrames[animId]>0)
		return;

	//open files starting with 0 and open as many as can be found
	//we assume a .png extension
	int im = 0;
	int im_cnt = 0;
	char fn[512];
	int tmp[1000];

	sprintf(fn,"%s0.png",baseFilename,0);
	im = wImageAdd(fn);

	while(im && im_cnt < 1000)
	{
		tmp[im_cnt] = im;
		im_cnt++;
		
		sprintf(fn,"%s%d.png",baseFilename,im_cnt);
		im = wImageAdd(fn);
	}
	
	//copy over
	if(im_cnt > 0)
	{
		_frames[animId] = new int[im_cnt];
		_numFrames[animId] = im_cnt;

		for(int i=0; i<im_cnt; i++)
		{
			_frames[animId][i] = tmp[i];
		}
	}

	_maxTime = _numFrames[animId] * _frameTime;

}

//---------------------------------------------------------------
// Load Handles...

std::vector<ObjectHandles> used_handles;

//storage of view handles and text handles
std::vector<ObjectHandles> object_handles_storage(MAX_OBJECTS);

//load all the view/text handles that may show during the game
void init_object_handles_storage(){
	//view handles (for object images)
	for(int i=0 ; i<MAX_OBJECTS ; i++)
		for(int j=0 ; j<NUM_VIEWHANDLES_PER_OBJ ; j++){
			object_handles_storage[i].view[j] = wViewAdd("Images/dummy.png",0,0);
			wViewSetVisible(object_handles_storage[i].view[j], 0);
		}
	//text handles (on top of images)
	for(int i=0 ; i<MAX_OBJECTS ; i++){
		object_handles_storage[i].text[3] = wTextAdd(0,0,"   ",viewFont[FONT_CRIT_SHADOW]);
		object_handles_storage[i].text[2] = wTextAdd(0,0,"   ",viewFont[FONT_CRIT]);
		object_handles_storage[i].text[1] = wTextAdd(0,0,"   ",viewFont[FONT_DMG_SHADOW]);
		object_handles_storage[i].text[0] = wTextAdd(0,0,"   ",viewFont[FONT_DMG]);
		TextSetVisible(object_handles_storage[i].text[3], 0);
		TextSetVisible(object_handles_storage[i].text[2], 0);
		TextSetVisible(object_handles_storage[i].text[1], 0);
		TextSetVisible(object_handles_storage[i].text[0], 0);
	}
}

ObjectHandles popHandles(){
	ObjectHandles hnd = object_handles_storage.back();
	object_handles_storage.pop_back();

	//used handles array should always be sorted...
	int i=0;
	while(i<used_handles.size() && used_handles[i].view[0]<hnd.view[0]) i++;
	used_handles.insert(used_handles.begin()+i, hnd);

	return hnd;
}

void pushHandles(ObjectHandles hnd){
	object_handles_storage.push_back(hnd);

	for(int i=0 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++)
		wViewSetVisible(hnd.view[i], 0);
	for(int i=0 ; i<NUM_TEXTHANDLES_PER_OBJ ; i++)
		TextSetVisible(hnd.text[i], 0);

	for(int i=0 ; i<used_handles.size() ; i++){
		if(used_handles[i].view[0]==hnd.view[0])
			used_handles.erase(used_handles.begin()+i);
	}
}

//---------------------------------------------------------------
// Load Resources...

// Sounds...
int music[NUM_SONGS];
int sound[NUM_SOUNDS];

void load_sounds(){
	//Songs
	music[SONG_MENU] = Mp3Add("Sounds/menu0.mp3");
	music[SONG_LVL1] = Mp3Add("Sounds/level0.mp3");
	//Sounds
	sound[SOUND_ATK0] = SoundAdd("Sounds/player-atk-01/atk0.snd");
	sound[SOUND_ATK1] = SoundAdd("Sounds/player-atk-01/atk1.snd");
	sound[SOUND_ATK2] = SoundAdd("Sounds/player-atk-01/atk2.snd");
}

// Players...
Resources playerResources[NUM_PLAYERS];

void load_head_resources_id(int id){
	switch(id)
	{
	case 0:
		//male head 01
		playerResources[0].addHead(0, DIR_S,	"Images/Players/male01/head01/down/h0.png");
		playerResources[0].addHead(0, DIR_SW,	"Images/Players/male01/head01/sw/h0.png");
		playerResources[0].addHead(0, DIR_W,	"Images/Players/male01/head01/left/h0.png");
		playerResources[0].addHead(0, DIR_NW,	"Images/Players/male01/head01/nw/h0.png");
		playerResources[0].addHead(0, DIR_N,	"Images/Players/male01/head01/up/h0.png");
		playerResources[0].addHead(0, DIR_NE,	"Images/Players/male01/head01/ne/h0.png");
		playerResources[0].addHead(0, DIR_E,	"Images/Players/male01/head01/right/h0.png");
		playerResources[0].addHead(0, DIR_SE,	"Images/Players/male01/head01/se/h0.png");
		break;
	case 1:
		//male head 02
		playerResources[0].addHead(1, DIR_S,	"Images/Players/male01/head02/down/h0.png");
		playerResources[0].addHead(1, DIR_SW,	"Images/Players/male01/head02/sw/h0.png");
		playerResources[0].addHead(1, DIR_W,	"Images/Players/male01/head02/left/h0.png");
		playerResources[0].addHead(1, DIR_NW,	"Images/Players/male01/head02/nw/h0.png");
		playerResources[0].addHead(1, DIR_N,	"Images/Players/male01/head02/up/h0.png");
		playerResources[0].addHead(1, DIR_NE,	"Images/Players/male01/head02/ne/h0.png");
		playerResources[0].addHead(1, DIR_E,	"Images/Players/male01/head02/right/h0.png");
		playerResources[0].addHead(1, DIR_SE,	"Images/Players/male01/head02/se/h0.png");
		break;
	case 2:
		//male head 03
		playerResources[0].addHead(2, DIR_S,	"Images/Players/male01/head03/down/h0.png");
		playerResources[0].addHead(2, DIR_SW,	"Images/Players/male01/head03/sw/h0.png");
		playerResources[0].addHead(2, DIR_W,	"Images/Players/male01/head03/left/h0.png");
		playerResources[0].addHead(2, DIR_NW,	"Images/Players/male01/head03/nw/h0.png");
		playerResources[0].addHead(2, DIR_N,	"Images/Players/male01/head03/up/h0.png");
		playerResources[0].addHead(2, DIR_NE,	"Images/Players/male01/head03/ne/h0.png");
		playerResources[0].addHead(2, DIR_E,	"Images/Players/male01/head03/right/h0.png");
		playerResources[0].addHead(2, DIR_SE,	"Images/Players/male01/head03/se/h0.png");
		break;
	case 3:
		//male head 04
		playerResources[0].addHead(3, DIR_S,	"Images/Players/male01/head04/down/h0.png");
		playerResources[0].addHead(3, DIR_SW,	"Images/Players/male01/head04/sw/h0.png");
		playerResources[0].addHead(3, DIR_W,	"Images/Players/male01/head04/left/h0.png");
		playerResources[0].addHead(3, DIR_NW,	"Images/Players/male01/head04/nw/h0.png");
		playerResources[0].addHead(3, DIR_N,	"Images/Players/male01/head04/up/h0.png");
		playerResources[0].addHead(3, DIR_NE,	"Images/Players/male01/head04/ne/h0.png");
		playerResources[0].addHead(3, DIR_E,	"Images/Players/male01/head04/right/h0.png");
		playerResources[0].addHead(3, DIR_SE,	"Images/Players/male01/head04/se/h0.png");
		break;
	case 4:
		//male head 05
		playerResources[0].addHead(4, DIR_S,	"Images/Players/male01/head05/down/h0.png");
		playerResources[0].addHead(4, DIR_SW,	"Images/Players/male01/head05/sw/h0.png");
		playerResources[0].addHead(4, DIR_W,	"Images/Players/male01/head05/left/h0.png");
		playerResources[0].addHead(4, DIR_NW,	"Images/Players/male01/head05/nw/h0.png");
		playerResources[0].addHead(4, DIR_N,	"Images/Players/male01/head05/up/h0.png");
		playerResources[0].addHead(4, DIR_NE,	"Images/Players/male01/head05/ne/h0.png");
		playerResources[0].addHead(4, DIR_E,	"Images/Players/male01/head05/right/h0.png");
		playerResources[0].addHead(4, DIR_SE,	"Images/Players/male01/head05/se/h0.png");
		break;
	case 5:
		//male head 06
		playerResources[0].addHead(5, DIR_S,	"Images/Players/male01/head06/down/h0.png");
		playerResources[0].addHead(5, DIR_SW,	"Images/Players/male01/head06/sw/h0.png");
		playerResources[0].addHead(5, DIR_W,	"Images/Players/male01/head06/left/h0.png");
		playerResources[0].addHead(5, DIR_NW,	"Images/Players/male01/head06/nw/h0.png");
		playerResources[0].addHead(5, DIR_N,	"Images/Players/male01/head06/up/h0.png");
		playerResources[0].addHead(5, DIR_NE,	"Images/Players/male01/head06/ne/h0.png");
		playerResources[0].addHead(5, DIR_E,	"Images/Players/male01/head06/right/h0.png");
		playerResources[0].addHead(5, DIR_SE,	"Images/Players/male01/head06/se/h0.png");
		break;
	case 6:
		//male head 07
		playerResources[0].addHead(6, DIR_S,	"Images/Players/male01/head07/down/h0.png");
		playerResources[0].addHead(6, DIR_SW,	"Images/Players/male01/head07/sw/h0.png");
		playerResources[0].addHead(6, DIR_W,	"Images/Players/male01/head07/left/h0.png");
		playerResources[0].addHead(6, DIR_NW,	"Images/Players/male01/head07/nw/h0.png");
		playerResources[0].addHead(6, DIR_N,	"Images/Players/male01/head07/up/h0.png");
		playerResources[0].addHead(6, DIR_NE,	"Images/Players/male01/head07/ne/h0.png");
		playerResources[0].addHead(6, DIR_E,	"Images/Players/male01/head07/right/h0.png");
		playerResources[0].addHead(6, DIR_SE,	"Images/Players/male01/head07/se/h0.png");
		break;
	}
}

void load_head_resources(){
	//load all heads
	for(int i=0 ; i<NUM_HEADS ; i++)
		load_head_resources_id(i);
}

void load_player_resources_avatar(){
	playerResources[0].addAnim(IDLE_DOWN,	"Images/Players/male01/body01/south/idle-down/idle");
}

void load_player_resources_id(int id){
	switch(id)
	{
	case 0:
		//male 01
		playerResources[0].addAnim(IDLE_UP,			"Images/Players/male01/body01/north/idle-up/idle");
		playerResources[0].addAnim(IDLE_DOWN,		"Images/Players/male01/body01/south/idle-down/idle");
		playerResources[0].addAnim(IDLE_RIGHT,		"Images/Players/male01/body01/right/idle-right/idle"); 
		playerResources[0].addAnim(IDLE_LEFT,		"Images/Players/male01/body01/left/idle-left/idle");
		playerResources[0].addAnim(WALK_UP,			"Images/Players/male01/body01/north/walk-up/walk");
		playerResources[0].addAnim(WALK_DOWN,		"Images/Players/male01/body01/south/walk-down/walk");
		playerResources[0].addAnim(WALK_RIGHT,		"Images/Players/male01/body01/right/walk-right/walk"); 
		playerResources[0].addAnim(WALK_LEFT,		"Images/Players/male01/body01/left/walk-left/walk");
		playerResources[0].addAnim(IDLE_NW,			"Images/Players/male01/body01/north/idle-nw/idle");
		playerResources[0].addAnim(IDLE_NE,			"Images/Players/male01/body01/north/idle-ne/idle");
		playerResources[0].addAnim(IDLE_SW,			"Images/Players/male01/body01/south/idle-sw/idle");
		playerResources[0].addAnim(IDLE_SE,			"Images/Players/male01/body01/south/idle-se/idle");
		playerResources[0].addAnim(WALK_NW,			"Images/Players/male01/body01/north/walk-nw/walk");
		playerResources[0].addAnim(WALK_NE,			"Images/Players/male01/body01/north/walk-ne/walk");
		playerResources[0].addAnim(WALK_SW,			"Images/Players/male01/body01/south/walk-sw/walk");
		playerResources[0].addAnim(WALK_SE,			"Images/Players/male01/body01/south/walk-se/walk");
		playerResources[0].addAnim(SIT_UP,			"Images/Players/male01/body01/sit/idle-sit-up/sit");
		playerResources[0].addAnim(SIT_DOWN,		"Images/Players/male01/body01/sit/idle-sit-down/sit");
		playerResources[0].addAnim(SIT_RIGHT,		"Images/Players/male01/body01/sit/idle-sit-right/sit"); 
		playerResources[0].addAnim(SIT_LEFT,		"Images/Players/male01/body01/sit/idle-sit-left/sit");
		playerResources[0].addAnim(SIT_NW,			"Images/Players/male01/body01/sit/idle-sit-nw/sit");
		playerResources[0].addAnim(SIT_NE,			"Images/Players/male01/body01/sit/idle-sit-ne/sit");
		playerResources[0].addAnim(SIT_SW,			"Images/Players/male01/body01/sit/idle-sit-sw/sit");
		playerResources[0].addAnim(SIT_SE,			"Images/Players/male01/body01/sit/idle-sit-se/sit");
		playerResources[0].addAnim(DEATH_RIGHT,		"Images/Players/male01/body01/right/die-right/die");
		playerResources[0].addAnim(DEATH_LEFT,		"Images/Players/male01/body01/left/die-left/die");
		playerResources[0].addAnim(SKILL_RIGHT,		"Images/Players/male01/body01/right/skill-right/body"); 
		playerResources[0].addAnim(SKILL_LEFT,		"Images/Players/male01/body01/left/skill-left/body");
		playerResources[0].addAnim(SHOOT_RIGHT,		"Images/Players/male01/body01/right/shoot-right/shoot"); 
		playerResources[0].addAnim(SHOOT_LEFT,		"Images/Players/male01/body01/left/shoot-left/shoot");
		playerResources[0].addAnim(BESERK_RIGHT,	"Images/Players/male01/body01/skills/skill-beserk-right/skill");
		playerResources[0].addAnim(BESERK_LEFT,		"Images/Players/male01/body01/skills/skill-beserk-left/skill");
		playerResources[0].addAnim(BUSTER_RIGHT,	"Images/Players/male01/body01/skills/skill-buster-right/skill");
		playerResources[0].addAnim(BUSTER_LEFT,		"Images/Players/male01/body01/skills/skill-buster-left/skill");
		playerResources[0].addAnim(REPEL_RIGHT,		"Images/Players/male01/body01/skills/skill-repel-right/skill");
		playerResources[0].addAnim(REPEL_LEFT,		"Images/Players/male01/body01/skills/skill-repel-left/skill");
		playerResources[0].addAnim(TELEPORT_RIGHT,	"Images/Players/male01/body01/skills/skill-teleport-right/skill");
		playerResources[0].addAnim(TELEPORT_LEFT,	"Images/Players/male01/body01/skills/skill-teleport-left/skill");
		break;
	}
}

void load_player_resources(){
	//load all players
	for(int i=0 ; i<NUM_PLAYERS ; i++)
		load_player_resources_id(i);
}

// Mobs...
Resources mobResources[NUM_MOBS];

void load_mob_resources_id(int id){
	switch(id)
	{
	case 0:
		//mob01
		mobResources[0].addAnim(IDLE_RIGHT,		"Images/Monsters/mob01/idle-right/walk");
		mobResources[0].addAnim(IDLE_LEFT,		"Images/Monsters/mob01/idle-left/walk");
		mobResources[0].addAnim(WALK_RIGHT,		"Images/Monsters/mob01/walk-right/walk");
		mobResources[0].addAnim(WALK_LEFT,		"Images/Monsters/mob01/walk-left/walk");
		mobResources[0].addAnim(DEATH_RIGHT,	"Images/Monsters/mob01/die-right/die");
		mobResources[0].addAnim(DEATH_LEFT,		"Images/Monsters/mob01/die-left/die");
		mobResources[0].addAnim(SHOOT_RIGHT,	"Images/Monsters/mob01/atk-right/atk");
		mobResources[0].addAnim(SHOOT_LEFT,		"Images/Monsters/mob01/atk-left/atk");
		break;
	case 1:
		//mob02
		mobResources[1].addAnim(IDLE_RIGHT,		"Images/Monsters/mob02/walk-right/walk");
		mobResources[1].addAnim(IDLE_LEFT,		"Images/Monsters/mob02/walk-left/walk");
		mobResources[1].addAnim(WALK_RIGHT,		"Images/Monsters/mob02/walk-right/walk");
		mobResources[1].addAnim(WALK_LEFT,		"Images/Monsters/mob02/walk-left/walk");
		mobResources[1].addAnim(DEATH_RIGHT,	"Images/Monsters/mob02/die-right/die");
		mobResources[1].addAnim(DEATH_LEFT,		"Images/Monsters/mob02/die-left/die");
		mobResources[1].addAnim(SHOOT_RIGHT,	"Images/Monsters/mob02/atk-right/atk");
		mobResources[1].addAnim(SHOOT_LEFT,		"Images/Monsters/mob02/atk-left/atk");
		break;
	}
}

void load_mob_resources(){
	//loads all mobs
	for(int i=0 ; i<NUM_MOBS ; i++)
		load_mob_resources_id(i);
}

// Weather...
Resources weatherResources[NUM_WEATHER];

void load_weather_resources(){

	weatherResources[0].addAnim(0, "Images/effects/rain0/rain");
}

// Items...
Resources itemResources[NUM_ITEMS];
int inventoryResources[NUM_ITEMS];

void load_item_resources(){
	//load inventory images...
	inventoryResources[0] = wImageAdd("Images/inventory/armor/armor0.png");
	inventoryResources[1] = wImageAdd("Images/inventory/boots/boots0.png");
	inventoryResources[2] = wImageAdd("Images/inventory/necklace/neck0.png");
	inventoryResources[3] = wImageAdd("Images/inventory/ring/ring0.png");
	inventoryResources[4] = wImageAdd("Images/inventory/armor/armor5.png");
	inventoryResources[5] = wImageAdd("Images/inventory/boots/boots1.png");
	inventoryResources[6] = wImageAdd("Images/inventory/necklace/neck2.png");
	inventoryResources[7] = wImageAdd("Images/inventory/ring/ring1.png");

	inventoryResources[8] = wImageAdd("Images/inventory/armor/armor0.png");
	itemResources[8].addAnim(DIR_S,	"Images/inventory/midheadgear/01/down/mid");
	itemResources[8].addAnim(DIR_SW,"Images/inventory/midheadgear/01/sw/mid");
	itemResources[8].addAnim(DIR_W,	"Images/inventory/midheadgear/01/left/mid");
	itemResources[8].addAnim(DIR_NW,"Images/inventory/midheadgear/01/nw/mid");
	itemResources[8].addAnim(DIR_N,	"Images/inventory/midheadgear/01/up/mid");
	itemResources[8].addAnim(DIR_NE,"Images/inventory/midheadgear/01/ne/mid");
	itemResources[8].addAnim(DIR_E,	"Images/inventory/midheadgear/01/right/mid");
	itemResources[8].addAnim(DIR_SE,"Images/inventory/midheadgear/01/se/mid");
}


// END OF INCLUDED FILE : Resources.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Interface.cpp  (FILTER = ON)

// LINE FILTERED :: #include <math.h>
// LINE FILTERED :: #include <float.h>

// "server side" stuff
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "map.h"
// LINE FILTERED :: #include "mob.h"
// LINE FILTERED :: #include "battle.h"
// LINE FILTERED :: #include "item.h"

// "client side" stuff
// LINE FILTERED :: #include "Resources.h"
// LINE FILTERED :: #include "GameObject.h"
// LINE FILTERED :: #include "Player.h"
// LINE FILTERED :: #include "Monster.h"
// LINE FILTERED :: #include "Interface.h"
// LINE FILTERED :: #include "SaveData.h"

//background
int viewBg;
int viewBlack;

int zoomPer=100;
int zoomFade=-1;

bool autoAttack=true;
int autoNextSkill=-1;
GameObject *autoTarget=NULL;

//fonts
int viewFont[NUM_FONTS];

//panels
int imgPanel[NUM_PANELS];
int viewPanel[NUM_PANELS];
bool panelVisible[NUM_PANELS];
int panelIdOfButton[NUM_BUTTONS];
int panelAnimCurrent=-1;
int panelAnimNext=-1;
int panelAnimCurrent_alpha;
int panelAnimNext_alpha;
int inventorySection=0;

//player
int selectedPlayer=0;
int viewPlayer, viewPlayerHead;
int viewPlayerDir=0;
char viewPlayerRot=' ';
int viewCharSelectPlayer[3], viewCharSelectPlayerHead[3];
int char_create_head=0;

//buttons
int panel_black;
int panel_avatar, view_panel_avatar;
int panel_hp, view_panel_hp;
int panel_hp_hide, view_panel_hp_hide, view_hp_black;
int panel_exp, view_panel_exp;
int panel_exp_hide, view_panel_exp_hide, view_exp_black;
int panel_joystick, view_panel_joystick;
int view_touch;

//text
int edit_name;
int textCharSelectName[3];
int textCharSelectLevel[3];
int textCharSelectClass[3];
int panel_level;
int text_inv_section;
int text_log1[2], text_log2[2];
int text_log_timer=0;

//joystick
Vector2 joystick_dir;

//buttons
int currentButtonId=-1;
int buttonHandle[NUM_BUTTONS];
int imgbuttonHandle1[NUM_BUTTONS];
int imgbuttonHandle2[NUM_BUTTONS];
int buttonHandleInv[NUM_BUTTONS_INV];
int buttonHandleEqu[NUM_BUTTONS_EQU];
int buttonInvId[NUM_BUTTONS_INV];
int buttonEquId[NUM_BUTTONS_EQU];
int skillIdOfButton[NUM_BUTTONS];
int buttonIdOfSkill[NUM_SKILLS];
int buttonMinX[NUM_BUTTONS];
int buttonMaxX[NUM_BUTTONS];
int buttonMinY[NUM_BUTTONS];
int buttonMaxY[NUM_BUTTONS];

int directionAnim[8];
int directionAnimIdle[8];
int directionAnimSit[8];

//--------------------------------------------------------------

class ButtonHistory {
public:
	ButtonHistory(int history_size);
	void addToHistory(int button, int timestamp);
	bool checkCombo(std::vector<int> combo, int time_between);
private:
	int lastButtonIdx;
	std::vector<int> history;
	std::vector<int> timestamps;
};

ButtonHistory::ButtonHistory(int history_size){
	for(int i=0 ; i<history_size ; i++){
		history.push_back(-1);
		timestamps.push_back(-1);
	}
	lastButtonIdx = history_size-1;
}
void ButtonHistory::addToHistory(int button, int timestamp){
	if(history.size()<=0) return;
	lastButtonIdx = (lastButtonIdx+1) % history.size();
	history[lastButtonIdx] = button;
	timestamps[lastButtonIdx] = timestamp;
	
	int i=lastButtonIdx, c = 3;
	while(c>0){
		printf("%d ", history[i]);
		i = (i>0)?(i-1):(history.size()-1);
		c--;
	}
	printf("\n");
}
bool ButtonHistory::checkCombo(std::vector<int> combo, int time_between){
	if(combo.size()>history.size()) 
		return false;
	int i=lastButtonIdx, j, c=combo.size()-1;
	while(c>=0){
		if(history[i]!=combo[c])
			return false;
		c--;
		j=i; 
		i = (i>0)?(i-1):(history.size()-1);
		if(c>=0 && timestamps[j]-timestamps[i] > time_between)
			return false;
	}
	return true;
}

ButtonHistory buttonHistory(3);

//--------------------------------------------------------------

void load_menu(){
	//menu...
	Mp3Loop(music[SONG_MENU]);
	panelVisible[PANEL_MENU] = true;
}

void load_interface_resources(){  //imgbuttonhandles (1 for normal, 2 for down)
	//main menu
	imgPanel[PANEL_MENU] = wImageAdd("Images/Interface/menu_background.png");
	imgbuttonHandle1[MENU1] = wImageAdd("Images/Interface/play01.png");
	imgbuttonHandle1[MENU2] = wImageAdd("Images/Interface/online01.png");
	imgbuttonHandle1[MENU3] = wImageAdd("Images/Interface/options01.png");
	imgbuttonHandle1[MENU4] = wImageAdd("Images/Interface/help01.png");
	imgbuttonHandle1[MENU5] = wImageAdd("Images/Interface/shop01.png");
	imgbuttonHandle2[MENU1] = wImageAdd("Images/Interface/play02.png");
	imgbuttonHandle2[MENU2] = wImageAdd("Images/Interface/online02.png");
	imgbuttonHandle2[MENU3] = wImageAdd("Images/Interface/options02.png");
	imgbuttonHandle2[MENU4] = wImageAdd("Images/Interface/help02.png");
	imgbuttonHandle2[MENU5] = wImageAdd("Images/Interface/shop02.png");
	//loading panel
	imgPanel[PANEL_LOADING] = wImageAdd("Images/iXileMenus/iXile_Load.png");
	//pause
	imgbuttonHandle1[BTN_PAUSE] = wImageAdd("Images/iXileMenus/Pause_Button.png");
	imgbuttonHandle2[BTN_PAUSE] = wImageAdd("Images/iXileMenus/Pause_Button_Select.png");
	//sit and stand
	imgbuttonHandle1[BTN_SIT] = wImageAdd("Images/iXileMenus/Sit_Button.png");
	imgbuttonHandle2[BTN_SIT] = wImageAdd("Images/iXileMenus/Stand_Button.png");
	//pause menu
	imgPanel[PANEL_PAUSE] = wImageAdd("Images/iXileMenus/PauseMenu/iXile_Menu_Screen_Static.png");
	imgbuttonHandle1[BTN_PAUSE_CHAR] = wImageAdd("Images/iXileMenus/PauseMenu/Character_Button.png");
	imgbuttonHandle2[BTN_PAUSE_CHAR] = wImageAdd("Images/iXileMenus/PauseMenu/Character_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_EQUIP] = wImageAdd("Images/iXileMenus/PauseMenu/Equipment_Button.png");
	imgbuttonHandle2[BTN_PAUSE_EQUIP] = wImageAdd("Images/iXileMenus/PauseMenu/Equipment_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_QUESTS] = wImageAdd("Images/iXileMenus/PauseMenu/Quests_Button.png");
	imgbuttonHandle2[BTN_PAUSE_QUESTS] = wImageAdd("Images/iXileMenus/PauseMenu/Quests_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_SKILLS] = wImageAdd("Images/iXileMenus/PauseMenu/Skills_Button.png");
	imgbuttonHandle2[BTN_PAUSE_SKILLS] = wImageAdd("Images/iXileMenus/PauseMenu/Skills_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_SYSTEM] = wImageAdd("Images/iXileMenus/PauseMenu/System_Button.png");
	imgbuttonHandle2[BTN_PAUSE_SYSTEM] = wImageAdd("Images/iXileMenus/PauseMenu/System_Button_Select.png");
	imgbuttonHandle1[BTN_PAUSE_RETURN] = wImageAdd("Images/iXileMenus/ReturnButton/Return_Button.png");
	imgbuttonHandle2[BTN_PAUSE_RETURN] = wImageAdd("Images/iXileMenus/ReturnButton/Return_Button_Select.png");
	//character select menu
	imgPanel[PANEL_CHAR_SELECT] = wImageAdd("Images/iXileMenus/CharSelect/iXile_Character_Selection_Screen_BLANK.png");
	imgbuttonHandle1[BTN_CHAR_S_SELECT1] = wImageAdd("Images/iXileMenus/CharSelect/Character_Select_WND.png");
	imgbuttonHandle2[BTN_CHAR_S_SELECT1] = wImageAdd("Images/iXileMenus/CharSelect/Character_Select_WND_Select.png");
	imgbuttonHandle1[BTN_CHAR_S_SELECT3] = imgbuttonHandle1[BTN_CHAR_S_SELECT2]=imgbuttonHandle1[BTN_CHAR_S_SELECT1];
	imgbuttonHandle2[BTN_CHAR_S_SELECT3] = imgbuttonHandle2[BTN_CHAR_S_SELECT2]=imgbuttonHandle2[BTN_CHAR_S_SELECT1];
	imgbuttonHandle1[BTN_CHAR_S_RETURN] = imgbuttonHandle1[BTN_PAUSE_RETURN];
	imgbuttonHandle2[BTN_CHAR_S_RETURN] = imgbuttonHandle2[BTN_PAUSE_RETURN];
	//character create menu
	imgPanel[PANEL_CHAR_CREATE] = wImageAdd("Images/iXileMenus/CharCreation/iXile_Character_Creation_Screen.png");
	imgbuttonHandle1[BTN_CHAR_C_LEFT1] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Left.png");
	imgbuttonHandle2[BTN_CHAR_C_LEFT1] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Left_Select.png");
	imgbuttonHandle1[BTN_CHAR_C_LEFT4] = imgbuttonHandle1[BTN_CHAR_C_LEFT3]=imgbuttonHandle1[BTN_CHAR_C_LEFT2]=imgbuttonHandle1[BTN_CHAR_C_LEFT1];
	imgbuttonHandle2[BTN_CHAR_C_LEFT4] = imgbuttonHandle2[BTN_CHAR_C_LEFT3]=imgbuttonHandle2[BTN_CHAR_C_LEFT2]=imgbuttonHandle2[BTN_CHAR_C_LEFT1];
	imgbuttonHandle1[BTN_CHAR_C_RIGHT1] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Right.png");
	imgbuttonHandle2[BTN_CHAR_C_RIGHT1] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Right_Select.png");
	imgbuttonHandle1[BTN_CHAR_C_RIGHT4] = imgbuttonHandle1[BTN_CHAR_C_RIGHT3]=imgbuttonHandle1[BTN_CHAR_C_RIGHT2]=imgbuttonHandle1[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle2[BTN_CHAR_C_RIGHT4] = imgbuttonHandle2[BTN_CHAR_C_RIGHT3]=imgbuttonHandle2[BTN_CHAR_C_RIGHT2]=imgbuttonHandle2[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle1[BTN_CHAR_C_NAME] = wImageAdd("Images/iXileMenus/CharCreation/Name_Plate_Entry.png");
	imgbuttonHandle2[BTN_CHAR_C_NAME] = wImageAdd("Images/iXileMenus/CharCreation/Name_Plate_Entry_Select.png");
	imgbuttonHandle1[BTN_CHAR_C_CREATE] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Right.png");
	imgbuttonHandle2[BTN_CHAR_C_CREATE] = wImageAdd("Images/iXileMenus/CharCreation/Selection_Arrow_Button_Right_Select.png");
	imgbuttonHandle1[BTN_CHAR_C_RETURN] = imgbuttonHandle1[BTN_PAUSE_RETURN];
	imgbuttonHandle2[BTN_CHAR_C_RETURN] = imgbuttonHandle2[BTN_PAUSE_RETURN];
	//inventory menu
	imgPanel[PANEL_INV] = wImageAdd("Images/iXileMenus/Inventory/iXile_Character_Menu.png");
	imgbuttonHandle1[BTN_INV_ROTLEFT] = wImageAdd("Images/iXileMenus/Inventory/Arrow_Rotate_Left_Button.png");
	imgbuttonHandle2[BTN_INV_ROTLEFT] = wImageAdd("Images/iXileMenus/Inventory/Arrow_Rotate_Left_Button_Select.png");
	imgbuttonHandle1[BTN_INV_ROTRIGHT] = wImageAdd("Images/iXileMenus/Inventory/Arrow_Rotate_Right_Button.png");
	imgbuttonHandle2[BTN_INV_ROTRIGHT] = wImageAdd("Images/iXileMenus/Inventory/Arrow_Rotate_Right_Button_Select.png");
	imgbuttonHandle1[BTN_INV_PREV] = imgbuttonHandle1[BTN_CHAR_C_LEFT1];
	imgbuttonHandle2[BTN_INV_PREV] = imgbuttonHandle2[BTN_CHAR_C_LEFT1];
	imgbuttonHandle1[BTN_INV_NEXT] = imgbuttonHandle1[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle2[BTN_INV_NEXT] = imgbuttonHandle2[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle1[BTN_INV_HOTKEY1] = wImageAdd("Images/iXileMenus/Inventory/Hotkey_Button.png");
	imgbuttonHandle2[BTN_INV_HOTKEY1] = wImageAdd("Images/iXileMenus/Inventory/Hotkey_Button_Select.png");
	imgbuttonHandle1[BTN_INV_HOTKEY2] = imgbuttonHandle1[BTN_INV_HOTKEY1];
	imgbuttonHandle2[BTN_INV_HOTKEY2] = imgbuttonHandle2[BTN_INV_HOTKEY1];
	imgbuttonHandle1[BTN_INV_RETURN] = imgbuttonHandle1[BTN_PAUSE_RETURN];
	imgbuttonHandle2[BTN_INV_RETURN] = imgbuttonHandle2[BTN_PAUSE_RETURN];
	//game buttons
	imgbuttonHandle1[BTN_ZOOMOUT] = imgbuttonHandle1[BTN_CHAR_C_LEFT1];
	imgbuttonHandle2[BTN_ZOOMOUT] = imgbuttonHandle2[BTN_CHAR_C_LEFT1];
	imgbuttonHandle1[BTN_ZOOMIN] = imgbuttonHandle1[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle2[BTN_ZOOMIN] = imgbuttonHandle2[BTN_CHAR_C_RIGHT1];
	imgbuttonHandle1[JOYSTICK] = wImageAdd("Images/Interface/interface_joystick_pod.png");
	imgbuttonHandle2[JOYSTICK] = imgbuttonHandle1[JOYSTICK];
	imgbuttonHandle1[PB_LEFT_BTN] = wImageAdd("Images/controller/left1.png");
	imgbuttonHandle2[PB_LEFT_BTN] = wImageAdd("Images/controller/left2.png");
	imgbuttonHandle1[PB_RIGHT_BTN] = wImageAdd("Images/controller/right1.png");
	imgbuttonHandle2[PB_RIGHT_BTN] = wImageAdd("Images/controller/right2.png");
	imgbuttonHandle1[PB_TOP_BTN] = wImageAdd("Images/controller/up1.png");
	imgbuttonHandle2[PB_TOP_BTN] = wImageAdd("Images/controller/up2.png");
	imgbuttonHandle1[PB_DOWN_BTN] = wImageAdd("Images/controller/down1.png");
	imgbuttonHandle2[PB_DOWN_BTN] = wImageAdd("Images/controller/down2.png");
	imgbuttonHandle1[PB_NW_BTN] = wImageAdd("Images/controller/nw1.png");
	imgbuttonHandle2[PB_NW_BTN] = wImageAdd("Images/controller/nw2.png");
	imgbuttonHandle1[PB_NE_BTN] = wImageAdd("Images/controller/ne1.png");
	imgbuttonHandle2[PB_NE_BTN] = wImageAdd("Images/controller/ne2.png");
	imgbuttonHandle1[PB_SW_BTN] = wImageAdd("Images/controller/sw1.png");
	imgbuttonHandle2[PB_SW_BTN] = wImageAdd("Images/controller/sw2.png");
	imgbuttonHandle1[PB_SE_BTN] = wImageAdd("Images/controller/se1.png");
	imgbuttonHandle2[PB_SE_BTN] = wImageAdd("Images/controller/se2.png");
	imgbuttonHandle1[PB_FIRE_BTN] = wImageAdd("Images/controller/fire1.png");
	imgbuttonHandle2[PB_FIRE_BTN] = wImageAdd("Images/controller/fire2.png");
	imgbuttonHandle1[PB_BESERK_BTN] = wImageAdd("Images/controller/beserk1.png");
	imgbuttonHandle2[PB_BESERK_BTN] = wImageAdd("Images/controller/beserk2.png");
	imgbuttonHandle1[PB_BUSTER_BTN] = wImageAdd("Images/controller/buster1.png");
	imgbuttonHandle2[PB_BUSTER_BTN] = wImageAdd("Images/controller/buster2.png");
	imgbuttonHandle1[PB_REPEL_BTN] = wImageAdd("Images/controller/repel1.png");
	imgbuttonHandle2[PB_REPEL_BTN] = wImageAdd("Images/controller/repel2.png");
	imgbuttonHandle1[PB_TELEPORT_BTN] = wImageAdd("Images/controller/teleport1.png");
	imgbuttonHandle2[PB_TELEPORT_BTN] = wImageAdd("Images/controller/teleport2.png");
}

void load_interface(){
	//where should this be?
	for(int i=0;i<NUM_BUTTONS;i++)
		panelIdOfButton[i] = -1;

	//fonts
	viewFont[FONT_NORMAL] = FontAdd("Arial","Regular",18,0xFFFFFF); //FontAdd("Images/font-numbers");
	viewFont[FONT_CHAR] = FontAdd("Arial","Regular",16,0x000000);
	viewFont[FONT_CHAR2] = FontAdd("Arial","Regular",11,0x000000);
	viewFont[FONT_DMG] = FontAdd("Arial","Regular",18,0xFF0000);
	viewFont[FONT_DMG_SHADOW] = FontAdd("Arial","Regular",18,0x000000);
	viewFont[FONT_CRIT] = FontAdd("Arial","Regular",26,0xFFFF00);
	viewFont[FONT_CRIT_SHADOW] = FontAdd("Arial","Regular",26,0x000000);
	viewFont[FONT_LOG1] = FontAdd("Arial","Regular",20,0xECCA11);
	//viewFont[FONT_LOG2] = FontAdd("Arial","Regular",20,0x1E9A05);
	viewFont[FONT_LOG_SHADOW] = FontAdd("Arial","Regular",20,0x000000);

	//add panels
	view_panel_avatar = wViewAdd("Images/Interface/interface_avatar.png", 0, 0);
	view_panel_hp = wViewAdd("Images/Interface/progress_life.png", 81, 6);
	view_panel_hp_hide = wViewAdd("Images/Interface/hit1.png", 213, 2);
	view_hp_black = wViewAdd("Images/Interface/black.png", 213, 9);
	view_panel_exp = wViewAdd("Images/Interface/progress_exp.png", 83, 31);
	view_panel_exp_hide = wViewAdd("Images/Interface/exp1.png", 211, 27);
	view_exp_black = wViewAdd("Images/Interface/black.png", 211, 30);

	panel_level = wTextAdd(4,1,"99", viewFont[FONT_NORMAL]);
	updateHPbar(100);

	//text log
	text_log1[1] = wTextAdd(190+2,280+2,"Loot: ", viewFont[FONT_LOG_SHADOW]);
	text_log1[0] = wTextAdd(190,280,"Loot: ", viewFont[FONT_LOG1]);
	text_log2[1] = wTextAdd(240+2,280+2,"          ", viewFont[FONT_LOG_SHADOW]);
	text_log2[0] = wTextAdd(240,280,"          ", viewFont[FONT_LOG1]);
	TextSetVisible(text_log1[0], 0);
	TextSetVisible(text_log1[1], 0);
	TextSetVisible(text_log2[0], 0);
	TextSetVisible(text_log2[1], 0);

	//add actual control buttons with respective callback functions
	buttonHandle[BTN_ZOOMIN] = wViewAdd("Images/dummy.png", 40, 65, ZoomPressed, BTN_ZOOMIN);
	buttonHandle[BTN_ZOOMOUT] = wViewAdd("Images/dummy.png", 0, 65, ZoomPressed, BTN_ZOOMOUT);
	buttonHandle[JOYSTICK] = wViewAdd("Images/dummy.png", 9, 199, JoystickPressed, JOYSTICK);
	buttonHandle[PB_LEFT_BTN] = wViewAdd("Images/dummy.png", 13, 241);
	buttonHandle[PB_RIGHT_BTN] = wViewAdd("Images/dummy.png", 82, 241);
	buttonHandle[PB_TOP_BTN] = wViewAdd("Images/dummy.png", 50, 210);
	buttonHandle[PB_DOWN_BTN] = wViewAdd("Images/dummy.png", 50, 265);
	buttonHandle[PB_NW_BTN] = wViewAdd("Images/dummy.png", 28, 218);
	buttonHandle[PB_NE_BTN] = wViewAdd("Images/dummy.png", 92, 218);
	buttonHandle[PB_SW_BTN] = wViewAdd("Images/dummy.png", 26, 282);
	buttonHandle[PB_SE_BTN] = wViewAdd("Images/dummy.png", 91, 281);
	buttonHandle[PB_FIRE_BTN] = wViewAdd("Images/dummy.png", 410, 255, SkillPressed, PB_FIRE_BTN);
	buttonHandle[PB_BESERK_BTN] = wViewAdd("Images/dummy.png", 350, 275, SkillPressed, PB_BESERK_BTN);
	buttonHandle[PB_BUSTER_BTN] = wViewAdd("Images/dummy.png", 363, 234, SkillPressed, PB_BUSTER_BTN);
	buttonHandle[PB_REPEL_BTN] = wViewAdd("Images/dummy.png", 394, 205, SkillPressed, PB_REPEL_BTN);
	buttonHandle[PB_TELEPORT_BTN] = wViewAdd("Images/dummy.png", 435, 195, SkillPressed, PB_TELEPORT_BTN);
	buttonHandle[BTN_SIT] = wViewAdd("Images/dummy.png", 435, 45, SitPressed, BTN_SIT);
	buttonHandle[BTN_PAUSE] = wViewAdd("Images/dummy.png", 435, 7, PausePressed, BTN_PAUSE);

	//black background
	viewBlack = wViewAdd("Images/Interface/black.png", 0, 0);
	ViewSetSize(viewBlack,480,320);

	// martim: panels
	viewPanel[PANEL_MENU] = wViewAdd(imgPanel[PANEL_MENU], 0, 0);
	viewPanel[PANEL_LOADING] = wViewAdd(imgPanel[PANEL_LOADING], 0, 0);
	viewPanel[PANEL_PAUSE] = wViewAdd(imgPanel[PANEL_PAUSE], 0, 0);
	viewPanel[PANEL_CHAR_SELECT] = wViewAdd(imgPanel[PANEL_CHAR_SELECT], 0, 0);
	viewPanel[PANEL_CHAR_CREATE] = wViewAdd(imgPanel[PANEL_CHAR_CREATE], 0, 0);
	viewPanel[PANEL_INV] = wViewAdd(imgPanel[PANEL_INV], 0, 0);
	buttonHandle[MENU1] = wViewAdd("Images/Interface/play01.png", 370, 10, MenuPressed, MENU1);
    buttonHandle[MENU2] = wViewAdd("Images/Interface/online01.png", 370, 70, MenuPressed, MENU2);
    buttonHandle[MENU3] = wViewAdd("Images/Interface/options01.png", 370, 130, MenuPressed, MENU3);
    buttonHandle[MENU4] = wViewAdd("Images/Interface/help01.png", 370, 190, MenuPressed, MENU4);
    buttonHandle[MENU5] = wViewAdd("Images/Interface/shop01.png", 370, 250, MenuPressed, MENU5);
	buttonHandle[BTN_PAUSE_CHAR] = wViewAdd("Images/dummy.png", 240-145, 45, MenuPressed, BTN_PAUSE_CHAR);
	buttonHandle[BTN_PAUSE_EQUIP] = wViewAdd("Images/dummy.png", 240-145, 90, MenuPressed, BTN_PAUSE_EQUIP);
	buttonHandle[BTN_PAUSE_QUESTS] = wViewAdd("Images/dummy.png", 240-145, 135, MenuPressed, BTN_PAUSE_QUESTS);
	buttonHandle[BTN_PAUSE_SKILLS] = wViewAdd("Images/dummy.png", 240-145, 180, MenuPressed, BTN_PAUSE_SKILLS);
	buttonHandle[BTN_PAUSE_SYSTEM] = wViewAdd("Images/dummy.png", 240-145, 225, MenuPressed, BTN_PAUSE_SYSTEM);
	buttonHandle[BTN_PAUSE_RETURN] = wViewAdd("Images/dummy.png", 48, 258, MenuPressed, BTN_PAUSE_RETURN);
	buttonHandle[BTN_CHAR_C_LEFT1] = wViewAdd("Images/dummy.png", 250, 116, MenuPressed, BTN_CHAR_C_LEFT1);
	buttonHandle[BTN_CHAR_C_RIGHT1] = wViewAdd("Images/dummy.png", 338, 116, MenuPressed, BTN_CHAR_C_RIGHT1);
	buttonHandle[BTN_CHAR_C_LEFT2] = wViewAdd("Images/dummy.png", 250, 146, MenuPressed, BTN_CHAR_C_LEFT2);
	buttonHandle[BTN_CHAR_C_RIGHT2] = wViewAdd("Images/dummy.png", 338, 146, MenuPressed, BTN_CHAR_C_RIGHT2);
	buttonHandle[BTN_CHAR_C_LEFT3] = wViewAdd("Images/dummy.png", 250, 176, MenuPressed, BTN_CHAR_C_LEFT3);
	buttonHandle[BTN_CHAR_C_RIGHT3] = wViewAdd("Images/dummy.png", 338, 176, MenuPressed, BTN_CHAR_C_RIGHT3);
	buttonHandle[BTN_CHAR_C_LEFT4] = wViewAdd("Images/dummy.png", 250, 206, MenuPressed, BTN_CHAR_C_LEFT4);
	buttonHandle[BTN_CHAR_C_RIGHT4] = wViewAdd("Images/dummy.png", 338, 206, MenuPressed, BTN_CHAR_C_RIGHT4);
	buttonHandle[BTN_CHAR_C_NAME] = wViewAdd("Images/dummy.png", 125, 215, MenuPressed, BTN_CHAR_C_NAME);
	buttonHandle[BTN_CHAR_C_CREATE] = wViewAdd("Images/dummy.png", 420, 290, MenuPressed, BTN_CHAR_C_CREATE);
	buttonHandle[BTN_CHAR_C_RETURN] = wViewAdd("Images/dummy.png", 48, 258, MenuPressed, BTN_CHAR_C_RETURN);
	buttonHandle[BTN_CHAR_S_SELECT1] = wViewAdd("Images/dummy.png", 85, 116, MenuPressed, BTN_CHAR_S_SELECT1);
	buttonHandle[BTN_CHAR_S_SELECT2] = wViewAdd("Images/dummy.png", 190, 116, MenuPressed, BTN_CHAR_S_SELECT2);
	buttonHandle[BTN_CHAR_S_SELECT3] = wViewAdd("Images/dummy.png", 295, 116, MenuPressed, BTN_CHAR_S_SELECT3);
	buttonHandle[BTN_CHAR_S_RETURN] = wViewAdd("Images/dummy.png", 48, 258, MenuPressed, BTN_CHAR_S_RETURN);
	buttonHandle[BTN_INV_ROTLEFT] = wViewAdd("Images/dummy.png", 195, 170, MenuPressed, BTN_INV_ROTLEFT);
	buttonHandle[BTN_INV_ROTRIGHT] = wViewAdd("Images/dummy.png", 252, 170, MenuPressed, BTN_INV_ROTRIGHT);
	buttonHandle[BTN_INV_PREV] = wViewAdd("Images/dummy.png", 295, 241, MenuPressed, BTN_INV_PREV);
	buttonHandle[BTN_INV_NEXT] = wViewAdd("Images/dummy.png", 355, 241, MenuPressed, BTN_INV_NEXT);
	buttonHandle[BTN_INV_HOTKEY1] = wViewAdd("Images/dummy.png", 200, 270, MenuPressed, BTN_INV_HOTKEY1);
	buttonHandle[BTN_INV_HOTKEY2] = wViewAdd("Images/dummy.png", 250, 270, MenuPressed, BTN_INV_HOTKEY2);
	buttonHandle[BTN_INV_RETURN] = wViewAdd("Images/dummy.png", 48, 258, MenuPressed, BTN_INV_RETURN);
	panelIdOfButton[MENU1] = PANEL_MENU;
	panelIdOfButton[MENU2] = PANEL_MENU;
	panelIdOfButton[MENU3] = PANEL_MENU;
	panelIdOfButton[MENU4] = PANEL_MENU;
	panelIdOfButton[MENU5] = PANEL_MENU;
	panelIdOfButton[BTN_PAUSE_CHAR] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_EQUIP] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_QUESTS] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_SKILLS] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_SYSTEM] = PANEL_PAUSE;
	panelIdOfButton[BTN_PAUSE_RETURN] = PANEL_PAUSE;
	panelIdOfButton[BTN_CHAR_C_LEFT1] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_LEFT2] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_LEFT3] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_LEFT4] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RIGHT1] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RIGHT2] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RIGHT3] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RIGHT4] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_NAME] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_RETURN] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_C_CREATE] = PANEL_CHAR_CREATE;
	panelIdOfButton[BTN_CHAR_S_SELECT1] = PANEL_CHAR_SELECT;
	panelIdOfButton[BTN_CHAR_S_SELECT2] = PANEL_CHAR_SELECT;
	panelIdOfButton[BTN_CHAR_S_SELECT3] = PANEL_CHAR_SELECT;
	panelIdOfButton[BTN_CHAR_S_RETURN] = PANEL_CHAR_SELECT;
	panelIdOfButton[BTN_INV_ROTLEFT] = PANEL_INV;
	panelIdOfButton[BTN_INV_ROTRIGHT] = PANEL_INV;
	panelIdOfButton[BTN_INV_PREV] = PANEL_INV;
	panelIdOfButton[BTN_INV_NEXT] = PANEL_INV;
	panelIdOfButton[BTN_INV_HOTKEY1] = PANEL_INV;
	panelIdOfButton[BTN_INV_HOTKEY2] = PANEL_INV;
	panelIdOfButton[BTN_INV_RETURN] = PANEL_INV;

	// inventory
	for(int i=0 ; i<NUM_BUTTONS_INV ; i++){
		buttonInvId[i] = -1;
		buttonHandleInv[i] = wViewAdd("Images/dummy.png", 297+35*(i%3), 97+35*(i/3), InventoryPressed, i);
		wViewSetVisible(buttonHandleInv[i],0);
	}
	for(int i=0 ; i<NUM_BUTTONS_EQU ; i++){
		buttonEquId[i] = -1;
		buttonHandleEqu[i] = wViewAdd("Images/dummy.png", 89+35*(i%3), 98+45*(i/3), EquipmentPressed, i);
		wViewSetVisible(buttonHandleEqu[i],0);
	}

	// inventory and char create player preview
	viewPlayer = wViewAdd("Images/dummy.png",0,0);
	viewPlayerHead = wViewAdd("Images/dummy.png",0,0);
	wViewSetVisible(viewPlayer,0);
	wViewSetVisible(viewPlayerHead,0);

	// char select player preview
	for(int i=0 ; i<3 ; i++){
		viewCharSelectPlayer[i] = wViewAdd("Images/dummy.png",0,0);
		viewCharSelectPlayerHead[i] = wViewAdd("Images/dummy.png",0,0);
		wViewSetVisible(viewCharSelectPlayer[i],0);
		wViewSetVisible(viewCharSelectPlayerHead[i],0);
	}

	// martim: text boxes
	edit_name = EditAdd(135, 217, 75);
	EditSetLabel(edit_name, "Name");
	textCharSelectName[0] = wTextAdd(123,100,"     ",viewFont[FONT_CHAR]);
	textCharSelectName[1] = wTextAdd(227,100,"     ",viewFont[FONT_CHAR]);
	textCharSelectName[2] = wTextAdd(333,100,"     ",viewFont[FONT_CHAR]);
	textCharSelectLevel[0] = wTextAdd(130,213,"     ",viewFont[FONT_CHAR2]);
	textCharSelectLevel[1] = wTextAdd(235,213,"     ",viewFont[FONT_CHAR2]);
	textCharSelectLevel[2] = wTextAdd(340,213,"     ",viewFont[FONT_CHAR2]);
	textCharSelectClass[0] = wTextAdd(130,223,"     ",viewFont[FONT_CHAR2]);
	textCharSelectClass[1] = wTextAdd(235,223,"     ",viewFont[FONT_CHAR2]);
	textCharSelectClass[2] = wTextAdd(340,223,"     ",viewFont[FONT_CHAR2]);
	text_inv_section = wTextAdd(336,240,"00", viewFont[FONT_CHAR]);
	TextSetVisible(text_inv_section, 0);

	// hide panels
	panelSetVisible(PANEL_LOADING, false, TRANSITION_BLACK);
	panelSetVisible(PANEL_PAUSE, false, TRANSITION_BLACK);
	panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
	panelSetVisible(PANEL_CHAR_CREATE, false, TRANSITION_BLACK);
	panelSetVisible(PANEL_INV, false, TRANSITION_BLACK);

	// martim: screen touch
	view_touch = TouchAdd(0,0,480,320,function_touch,TOUCH);
	TouchSetVisible(view_touch,0);

	// martim: which animation when button is pressed
	directionAnim[PB_LEFT_BTN] = (int)WALK_LEFT;
	directionAnim[PB_RIGHT_BTN] = (int)WALK_RIGHT;
	directionAnim[PB_TOP_BTN] = (int)WALK_UP;
	directionAnim[PB_DOWN_BTN] = (int)WALK_DOWN;
	directionAnim[PB_NW_BTN] = (int)WALK_NW;
	directionAnim[PB_NE_BTN] = (int)WALK_NE;
	directionAnim[PB_SW_BTN] = (int)WALK_SW;
	directionAnim[PB_SE_BTN] = (int)WALK_SE;
	directionAnimIdle[PB_LEFT_BTN] = (int)IDLE_LEFT;
	directionAnimIdle[PB_RIGHT_BTN] = (int)IDLE_RIGHT;
	directionAnimIdle[PB_TOP_BTN] = (int)IDLE_UP;
	directionAnimIdle[PB_DOWN_BTN] = (int)IDLE_DOWN;
	directionAnimIdle[PB_NW_BTN] = (int)IDLE_NW;
	directionAnimIdle[PB_NE_BTN] = (int)IDLE_NE;
	directionAnimIdle[PB_SW_BTN] = (int)IDLE_SW;
	directionAnimIdle[PB_SE_BTN] = (int)IDLE_SE;
	directionAnimSit[PB_LEFT_BTN] = (int)SIT_LEFT;
	directionAnimSit[PB_RIGHT_BTN] = (int)SIT_RIGHT;
	directionAnimSit[PB_TOP_BTN] = (int)SIT_UP;
	directionAnimSit[PB_DOWN_BTN] = (int)SIT_DOWN;
	directionAnimSit[PB_NW_BTN] = (int)SIT_NW;
	directionAnimSit[PB_NE_BTN] = (int)SIT_NE;
	directionAnimSit[PB_SW_BTN] = (int)SIT_SW;
	directionAnimSit[PB_SE_BTN] = (int)SIT_SE;

	// martim: buttonId
	currentButtonId=-1;

	// martim: link skills to buttons
	skillIdOfButton[PB_FIRE_BTN]=SKILL_FIRE;
	skillIdOfButton[PB_BESERK_BTN]=SKILL_BESERK;
	skillIdOfButton[PB_BUSTER_BTN]=SKILL_BUSTER;
	skillIdOfButton[PB_REPEL_BTN]=SKILL_REPEL;
	skillIdOfButton[PB_TELEPORT_BTN]=SKILL_TELEPORT;
	buttonIdOfSkill[SKILL_FIRE]=PB_FIRE_BTN;
	buttonIdOfSkill[SKILL_BESERK]=PB_BESERK_BTN;
	buttonIdOfSkill[SKILL_BUSTER]=PB_BUSTER_BTN;
	buttonIdOfSkill[SKILL_REPEL]=PB_REPEL_BTN;
	buttonIdOfSkill[SKILL_TELEPORT]=PB_TELEPORT_BTN;

	// martim: set button images and get bounding box of each button
	for(int i=0;i<NUM_BUTTONS-1;i++){ //(last button is TOUCH, no image)
		wViewSetImage(buttonHandle[i], imgbuttonHandle1[i]);
		buttonMinX[i] = ViewGetx(buttonHandle[i]);
		buttonMinY[i] = ViewGety(buttonHandle[i]);
		buttonMaxX[i] = buttonMinX[i] + ViewGetWidth(buttonHandle[i]);
		buttonMaxY[i] = buttonMinY[i] + ViewGetHeight(buttonHandle[i]);
	}
}

void load_interface_player(){
	// inventory and char create player preview
	wViewSetImage(viewPlayer, playerResources[0]._frames[IDLE_DOWN][0]);
	wViewSetImage(viewPlayerHead, playerResources[0]._heads[0][DIR_S]);
	wViewSetVisible(viewPlayer,0);
	wViewSetVisible(viewPlayerHead,0);

	// char select player preview
	for(int i=0 ; i<3 ; i++){
		wViewSetImage(viewCharSelectPlayer[i], playerResources[0]._frames[IDLE_DOWN][0]);
		wViewSetImage(viewCharSelectPlayerHead[i], playerResources[0]._heads[0][DIR_S]);
		wViewSetVisible(viewCharSelectPlayer[i],0);
		wViewSetVisible(viewCharSelectPlayerHead[i],0);
	}
}

void updateHPbar(int hp){
	//hp in percentage...
	if(hp>96){
		wViewSetVisible(view_hp_black,0);
		wViewSetVisible(view_panel_hp_hide,0);
		wViewSetVisible(view_panel_hp,1);
	}else if(hp>=1){
		wViewSetVisible(view_hp_black,1);
		wViewSetVisible(view_panel_hp_hide,1);
		wViewSetVisible(view_panel_hp,1);
		int pos = (214-96*(214-92)/(96-1)) + hp*(214-92)/(96-1);
		//change hp bar
		ViewSetxy(view_hp_black, pos, 9);
		ViewSetSize(view_hp_black, 214-pos, 15);
	}else{
		wViewSetVisible(view_hp_black,0);
		wViewSetVisible(view_panel_hp_hide,1);
		wViewSetVisible(view_panel_hp,0);
	}
}

void updateEXPbar(int exp){
	//exp in percentage...
	if(exp>96){
		wViewSetVisible(view_exp_black,0);
		wViewSetVisible(view_panel_exp_hide,0);
		wViewSetVisible(view_panel_exp,1);
	}else if(exp>=1){
		wViewSetVisible(view_exp_black,1);
		wViewSetVisible(view_panel_exp_hide,1);
		wViewSetVisible(view_panel_exp,1);
		int pos = (212-96*(212-92)/(96-1)) + exp*(212-92)/(96-1);
		//change exp bar
		ViewSetxy(view_exp_black, pos, 30);
		ViewSetSize(view_exp_black, 212-pos, 10);
	}else{
		wViewSetVisible(view_exp_black,0);
		wViewSetVisible(view_panel_exp_hide,1);
		wViewSetVisible(view_panel_exp,0);
	}
}

void textLogUpdate(){
	if(text_log_timer<1) return;
	text_log_timer--;
	if(text_log_timer==0){
		TextSetVisible(text_log1[0], 0);
		TextSetVisible(text_log1[1], 0);
		TextSetVisible(text_log2[0], 0);
		TextSetVisible(text_log2[1], 0);
	}
}

void textLogLoot(int item_id){
	wTextSetText(text_log1[0], "Loot:");
	wTextSetText(text_log1[1], "Loot:");
	wTextSetText(text_log2[0], item_db[item_id].name);
	wTextSetText(text_log2[1], item_db[item_id].name);
	TextSetVisible(text_log1[0], 1);
	TextSetVisible(text_log1[1], 1);
	TextSetVisible(text_log2[0], 1);
	TextSetVisible(text_log2[1], 1);
	text_log_timer=60;
}

void centerPlayer(){
	int bgminx=-(sizeBg.x*zoomPer/100.0)+480;
	int bgminy=-(sizeBg.y*zoomPer/100.0)+320;
	posBg.x = -(player->_pos.x*zoomPer/100.0) + 240;
	posBg.y = -(player->_pos.y*zoomPer/100.0) + 200;

	if(posBg.x < bgminx) posBg.x = bgminx;
	if(posBg.y < bgminy) posBg.y = bgminy;
	
	if(posBg.x > 0) posBg.x = 0;
	if(posBg.y > 0) posBg.y = 0;

	ViewSetxy(viewBg, posBg.x, posBg.y);	
}

void mapPanning(){
	int playerw=ViewGetWidth(player->handles.view[0]);
	int playerh=ViewGetHeight(player->handles.view[0]);
	int bgy=ViewGety(viewBg);
	int bgminy=-ViewGetHeight(viewBg)+320;
	int bgx=ViewGetx(viewBg);
	int bgminx=-ViewGetWidth(viewBg)+480;
	//map panning when near borders
	if(player->_pos.x < 0.5*playerw - posBg.x){
		if(bgx!=0)
			ViewSetxy(viewBg, (bgx<-4 ? bgx+4 : 0), bgy);
	}
	if(player->_pos.x > 480-0.5*playerw - posBg.x){
		if(bgx!=bgminx)
			ViewSetxy(viewBg, (bgx>bgminx+4 ? bgx-4 : bgminx), bgy);
	}
	bgx=ViewGetx(viewBg);
	if(player->_pos.y < playerh - posBg.y){
		if(bgy!=0)
			ViewSetxy(viewBg, bgx, (bgy<-4 ? bgy+4 : 0));
	}
	if(player->_pos.y > 320 - posBg.y){
		if(bgy!=bgminy)
			ViewSetxy(viewBg, bgx, (bgy>bgminy+4 ? bgy-4 : bgminy));
	}
	//move objects as well
	posBg.x = ViewGetx(viewBg);
	posBg.y = ViewGety(viewBg);
}

void showSkillAnim(GameObject *obj, int skill){
	if(obj->isDead) return;

	if(obj->type==TYPE_PLAYER){
		((Player*)obj)->useSkill(skill);
		return;
	}

	switch(skill){
	case -1:
		if(obj->direction==RIGHT) obj->setCurrentAnim(IDLE_RIGHT);
		else obj->setCurrentAnim(IDLE_LEFT);
		break;
	case SKILL_FIRE:
		if(obj->direction==RIGHT) obj->setCurrentAnim(SHOOT_RIGHT);
		else obj->setCurrentAnim(SHOOT_LEFT);
		break;
	case SKILL_BESERK:
		if(obj->direction==RIGHT) obj->setCurrentAnim(BESERK_RIGHT);
		else obj->setCurrentAnim(BESERK_LEFT);
		break;
	case SKILL_BUSTER:
		if(obj->direction==RIGHT) obj->setCurrentAnim(BUSTER_RIGHT);
		else obj->setCurrentAnim(BUSTER_LEFT);
		break;
	case SKILL_REPEL:
		if(obj->direction==RIGHT) obj->setCurrentAnim(REPEL_RIGHT);
		else obj->setCurrentAnim(REPEL_LEFT);
		break;
	case SKILL_TELEPORT:
		if(obj->direction==RIGHT) obj->setCurrentAnim(TELEPORT_RIGHT);
		else obj->setCurrentAnim(TELEPORT_LEFT);
		break;
	}
}

void showDamage(GameObject *dst, int damage, int font){
	sprintf(dst->textStr,"%d", damage);
	//hide last dmg
	if(dst->textTimer>0 && dst->dmgType!=font){
		if(font==FONT_DMG){
			TextSetVisible(dst->textHandlerCrit[0], 0);
			TextSetVisible(dst->textHandlerCrit[1], 0);
		}else if(font==FONT_CRIT){
			TextSetVisible(dst->textHandlerDmg[0], 0);
			TextSetVisible(dst->textHandlerDmg[1], 0);
		}
	}
	//show new dmg
	if(font==FONT_DMG){
		wTextSetText(dst->textHandlerDmg[0], dst->textStr);
		TextSetVisible(dst->textHandlerDmg[0], 1);
		wTextSetText(dst->textHandlerDmg[1], dst->textStr);
		TextSetVisible(dst->textHandlerDmg[1], 1);
	}else if(font==FONT_CRIT){
		wTextSetText(dst->textHandlerCrit[0], dst->textStr);
		TextSetVisible(dst->textHandlerCrit[0], 1);
		wTextSetText(dst->textHandlerCrit[1], dst->textStr);
		TextSetVisible(dst->textHandlerCrit[1], 1);
	}
	dst->dmgType = font;
	dst->textTimer = 30;
}

int SkillPressed(int id,int event,int x,int y){
	//button history
	if(event==1 && x!=0 && y!=0){
		buttonHistory.addToHistory(id, frameCount);
		std::vector<int> combo; 
		combo.push_back(PB_BESERK_BTN);
		combo.push_back(PB_BUSTER_BTN);
		combo.push_back(PB_BESERK_BTN);
		if(buttonHistory.checkCombo(combo, 30)){
			printf("Beserk-Buster-Beserk combo!\n");
			if(player) player->addItem(8,1);
		}
	}

	if(!gameStarted || !player || player->isDead || player->lockedForAnimation) return 0;

	int skill_id = skillIdOfButton[id];

	// auto-target...
	if(autoAttack && skill_id!=SKILL_TELEPORT){
		//only if skill cooldown is over. fire can always be re-cast
		if(skill_id==SKILL_FIRE || !skillIsActive[skill_id]){
			if(event==2){
				return 0;
			}else if(event==1){
				block_list *aux=bl_head.next;
				float d, min_d=FLT_MAX;

				autoTarget=NULL;
				autoNextSkill=skill_id;
				// is no monster in range?
				while(aux){
					if(aux->obj->isDead){ aux=aux->next; continue; }
					if(inRange(skill_id, player, aux->obj, &d)){
						autoTarget=NULL;
						break;
					}else{
						//target the closest monster
						if(d<min_d){
							min_d=d;
							autoTarget=aux->obj;
						}
					}
					aux=aux->next;
				}
			}

			if(autoTarget!=NULL){
				//buttons are only shown as pressed when we are actually attacking
				player->skillInUse=-1;
				skillIsPressed[SKILL_FIRE]=false;
				wViewSetImage(buttonHandle[PB_FIRE_BTN], imgbuttonHandle1[PB_FIRE_BTN]);
				return 0;
			}
		}
	}

	// fire skill is special (spammable)
	if(skill_id==SKILL_FIRE){
/*		//if moved outside button [removed] fire sticks now...
		if(event==3 || !(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id])){
			skillIsPressed[skill_id]=false;
			wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
			if(player->direction==RIGHT) player->setCurrentAnim(IDLE_RIGHT);
			else player->setCurrentAnim(IDLE_LEFT);
		}else{
*/
			if(!skillIsActive[skill_id])
				skillTimeCount[skill_id]=skillDelay[skill_id];
			skillIsPressed[skill_id]=true;
			skillIsActive[skill_id]=true;
			wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
//		}
		return 0;
	}
		
	//if you press any skill, stop attack
	if(event==1 && skillIsPressed[SKILL_FIRE]){
		skillIsPressed[SKILL_FIRE]=false;
		player->skillInUse=-1;
		wViewSetImage(buttonHandle[PB_FIRE_BTN], imgbuttonHandle1[PB_FIRE_BTN]);
		if(player->direction==RIGHT) player->setCurrentAnim(IDLE_RIGHT);
		else player->setCurrentAnim(IDLE_LEFT);
	}

	// if we're not touching anymore...
	if(event==3)
		skillIsPressed[skill_id]=false;

	// if cooldown is over, and we released the button...
	if((event == 1 || event == 2) && skillIsActive[skill_id]==false /*&& skillIsPressed[skill_id]==false*/)
	{
		skillIsPressed[skill_id]=true;
		skillIsActive[skill_id]=true;
		skillTimeCount[skill_id]=0;

		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);

		block_list *aux=bl_head.next;

		switch(id){
		case PB_FIRE_BTN:
			break;
		case PB_BESERK_BTN:
			while(aux){
				attack(skill_id, player, aux->obj);
				aux=aux->next;
			}
			break;
		case PB_BUSTER_BTN:
			while(aux){
				attack(skill_id, player, aux->obj);
				aux=aux->next;
			}
			break;
		case PB_REPEL_BTN:
			while(aux){
				attack(skill_id, player, aux->obj);
				aux=aux->next;
			}
			break;
		case PB_TELEPORT_BTN:
			player->setVisible(false);
			TouchSetVisible(view_touch,1);
			for(int i=0 ; i<MENU1 ; i++)
				wViewSetVisible(buttonHandle[i],0);
			wViewSetVisible(buttonHandle[JOYSTICK],0);
			break;
		}

		showSkillAnim(player, skill_id);
	}

	return 0;
}

int JoystickPressed(int id,int event,int x,int y)
{
	if(!gameStarted || id!=JOYSTICK || player->lockedForAnimation) return 0;

	//if you press any direction, stop attack
	if(event==1 && skillIsPressed[SKILL_FIRE]){
		player->skillInUse=-1;
		skillIsPressed[SKILL_FIRE]=false;
		wViewSetImage(buttonHandle[PB_FIRE_BTN], imgbuttonHandle1[PB_FIRE_BTN]);
		//player->setCurrentAnim(directionAnimIdle[player->dir]);
	}

	//if you press any direction, stop auto-target
	if(event==1 && autoAttack){
		autoTarget=NULL;
	}

	int w = ViewGetWidth(buttonHandle[id])/2;
	int dx = x - (ViewGetx(buttonHandle[id]) + w);
	int dy = y - (ViewGety(buttonHandle[id]) + ViewGetHeight(buttonHandle[id])/2);

	joystick_dir.x = dx;
	joystick_dir.y = dy;

	// moved away...
	if((event==1 || event==2) && joystick_dir.Length()>w+4){
		//event=3; //old version
		joystick_dir = joystick_dir/(w+4); //new version. let user go out of the joystick area
	}

/*	if(event==1 || event==2){
		//moved away...
		if( !(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]) ){
			event=3;
		}
	}
*/
	//release touch...
	if(event==3){
		joystick_dir.x = 0;
		joystick_dir.y = 0;

		if(currentButtonId!=-1){
			player->idle();
			wViewSetImage(buttonHandle[currentButtonId], imgbuttonHandle1[currentButtonId]);
			currentButtonId=-1;
		}
		return 0;
	}

	
	joystick_dir.Unitize();

	float ang = 180 + atan2((float)dy,(float)-dx)*180/3.14;
	int lastid = currentButtonId;
	int headDir;

	if(ang <= 22.5 || ang > 337.5){
		headDir = DIR_E;
		currentButtonId = PB_RIGHT_BTN;
	}else if(ang <= 67.5 && ang > 22.5){
		headDir = DIR_NE;
		currentButtonId = PB_NE_BTN;
	}else if(ang <= 112.5 && ang > 67.5){
		headDir = DIR_N;
		currentButtonId = PB_TOP_BTN;
	}else if(ang <= 157.5 && ang > 112.5){
		headDir = DIR_NW;
		currentButtonId = PB_NW_BTN;
	}else if(ang <= 202.5 && ang > 157.5){
		headDir = DIR_W;
		currentButtonId = PB_LEFT_BTN;
	}else if(ang <= 247.5 && ang > 202.5){
		headDir = DIR_SW;
		currentButtonId = PB_SW_BTN;
	}else if(ang <= 292.5 && ang > 247.5){
		headDir = DIR_S;
		currentButtonId = PB_DOWN_BTN;
	}else{
		headDir = DIR_SE;
		currentButtonId = PB_SE_BTN;
	}

	if(lastid!=currentButtonId){
		if(lastid!=-1) wViewSetImage(buttonHandle[lastid], imgbuttonHandle1[lastid]);
		wViewSetImage(buttonHandle[currentButtonId], imgbuttonHandle2[currentButtonId]);
	}
	player->setDir(headDir);
	player->setHeadDir(headDir);
	if(!player->isSitting) player->setCurrentAnim(directionAnim[player->dir]);
	else player->setCurrentAnim(directionAnimSit[player->dir]);

	return 0;
}

int DirectionPressed(int id,int event,int x,int y)
{
	if(!player || player->isDead || !gameStarted) return 0;
	int i;

	// which button are we pressing?
	if(currentButtonId==-1)
		currentButtonId=id;
	else 
		id=currentButtonId;

	//if(currentButtonId<0 || currentButtonId>=NUM_DIRECTIONS) return 0;

	// if we're not touching anymore...
	if(event==3)
		currentButtonId=-1;

	// touch move...
	if(event==2){
		if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
			;
		}else{
			//if we moved to another button...
			for(i=0;i<NUM_DIRECTIONS;i++){
				if(x>buttonMinX[i] && x<buttonMaxX[i] && y>buttonMinY[i] && y<buttonMaxY[i]){
					wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
					currentButtonId=id=i; break;
				}
			}
			//no button...
			if(i>=NUM_DIRECTIONS)
				event=3;
		}
	}

	if(event==3){
		directionIsPressed = false;
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		player->setCurrentAnim(directionAnimIdle[id]);
	}else{
		directionIsPressed = true;
		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
		player->setCurrentAnim(directionAnim[id]);

		switch(id){
		case PB_LEFT_BTN:
		case PB_NW_BTN:
		case PB_SW_BTN:
			player->direction = LEFT; break;
		case PB_RIGHT_BTN:
		case PB_NE_BTN:
		case PB_SE_BTN:
			player->direction = RIGHT; break;
		case PB_TOP_BTN:
		case PB_DOWN_BTN:
			break;
		}
	}

	return 0;
}

int function_touch(int id,int event,int x,int y){
	if(id!=TOUCH || !gameStarted) return 0;

	// if we touch
	if(event==1){
		TouchSetVisible(view_touch,0);
		player->setVisible(true);
		player->_pos.x = -posBg.x + x;
		player->_pos.y = -posBg.y + y;
		centerPlayer();
		showSkillAnim(player, SKILL_TELEPORT);

		for(int i=0 ; i<MENU1 ; i++)
			wViewSetVisible(buttonHandle[i],1);
		wViewSetVisible(buttonHandle[JOYSTICK],1);
	}

	return 0;
}

void panelSetVisible(int panel, bool vis, int transition_type){
	//black background for transition
	if(transition_type==TRANSITION_BLACK){
		wViewSetVisible(viewBlack,1);
		ViewSetAlpha(viewBlack,100);
	}else if(transition_type==TRANSITION_NORMAL){
		wViewSetVisible(viewBlack,0);
	}

	//zoom (exception)
	if(panel==PANEL_CLEAR){
		//player
		if(player) player->setVisible(!vis);
		//monsters
		block_list *aux=bl_head.next;
		while(aux){
			aux->obj->setVisible(!vis);
			aux=aux->next;
		}
		return;
	}

	//text and items disappear right away
	if(!vis && panelAnimCurrent==-1){
		if(panel==PANEL_INV){
			inventoryDisplay(false);
		}else if(panel==PANEL_CHAR_CREATE){
			EditSetVisible(edit_name,false);
		}else if(panel==PANEL_CHAR_SELECT){
			for(int i=0 ; i<3 ; i++){
				TextSetVisible(textCharSelectName[i],false);
				TextSetVisible(textCharSelectLevel[i],false);
				TextSetVisible(textCharSelectClass[i],false);
			}
		}
	}

	//alpha...
	if(appLoaded){
		//set up transitions
		if(vis){
			panelAnimNext = panel;
			panelAnimNext_alpha = 0;
			panelSetAlpha(panel, 0);
		}else{
			if(panelAnimCurrent==-1){
				panelAnimCurrent = panel;
				panelAnimCurrent_alpha = 100;
				return;
			}else{
				panelAnimCurrent=-1;
			}
		}
	}

	//if we're opening a panel or going back to game...
	if(vis || panelAnimNext==-1){
		//player
		if(player) player->setVisible(!vis);
		//monsters
		block_list *aux=bl_head.next;
		while(aux){
			aux->obj->setVisible(!vis);
			aux=aux->next;
		}
	}

	//panel
	wViewSetVisible(viewPanel[panel], vis);
	panelVisible[panel]=vis;

	//buttons
	for(int i=0 ; i<NUM_BUTTONS ; i++){
		if(panelIdOfButton[i]==panel)
			wViewSetVisible(buttonHandle[i], vis);
	}
	//player previews
	if(panel==PANEL_INV){
		wViewSetVisible(viewPlayer,vis);
		wViewSetVisible(viewPlayerHead,vis);
		ViewSetxy(viewPlayer,240-0.5*ViewGetWidth(viewPlayer),180-ViewGetHeight(viewPlayer));
		ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
		//if(!vis) inventoryDisplay(false);
	}else if(panel==PANEL_CHAR_CREATE){
		wViewSetVisible(viewPlayer,vis);
		wViewSetVisible(viewPlayerHead,vis);
		ViewSetxy(viewPlayer,165-0.5*ViewGetWidth(viewPlayer),210-ViewGetHeight(viewPlayer));
		ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
	}else if(panel==PANEL_CHAR_SELECT){
		int head_, view_, name_, level_, class_;
		char str[10];
		for(int i=0 ; i<3 ; i++){
			head_=viewCharSelectPlayerHead[i]; view_=viewCharSelectPlayer[i]; name_=textCharSelectName[i]; level_=textCharSelectLevel[i]; class_=textCharSelectClass[i];
			if(gdGlobal.player_slot[i].free){
				//...
			}else{
				wViewSetVisible(view_,vis);
				wViewSetVisible(head_,vis);
				sprintf(str,"%d",gdGlobal.player_slot[i].level);
				ViewSetxy(view_,85+48+(105*i)-0.5*ViewGetWidth(view_),218-ViewGetHeight(view_));
				ViewSetxy(head_,ViewGetx(view_),ViewGety(view_));
				wTextSetText(name_,gdGlobal.player_slot[i].name);
				wTextSetText(level_,str);
				wTextSetText(class_, const_cast<char*> CLASS_STR(gdGlobal.player_slot[i].id_class) );
				if(vis) wViewSetImage(head_, playerResources[0]._heads[gdGlobal.player_slot[i].id_head][DIR_S]);
			}
		}
	}
}

void panelSetAlpha(int panel, int alpha){
	//zoom (exception)
	if(panel==PANEL_CLEAR){
		ViewSetAlpha(viewBlack, alpha);
		return;
	}

	//items and text show when alpha=100
	if(alpha==100){
		if(panel==PANEL_INV){
			inventoryDisplay(true);
		}else if(panel==PANEL_CHAR_CREATE){
			EditSetVisible(edit_name,true);
		}else if(panel==PANEL_CHAR_SELECT){
			for(int i=0 ; i<3 ; i++){
				TextSetVisible(textCharSelectName[i],true);
				TextSetVisible(textCharSelectLevel[i],true);
				TextSetVisible(textCharSelectClass[i],true);
			}
		}
	}
	//panel
	ViewSetAlpha(viewPanel[panel], alpha);
	//buttons
	for(int i=0 ; i<NUM_BUTTONS ; i++){
		if(panelIdOfButton[i]==panel)
			ViewSetAlpha(buttonHandle[i], alpha);
	}
	//player previews
	if(panel==PANEL_INV){
		ViewSetAlpha(viewPlayer,alpha);
		ViewSetAlpha(viewPlayerHead,alpha);
		ViewSetxy(viewPlayer,240-0.5*ViewGetWidth(viewPlayer),180-ViewGetHeight(viewPlayer));
		ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
		//if(alpha==100) inventoryDisplay(true);
	}else if(panel==PANEL_CHAR_CREATE){
		ViewSetAlpha(viewPlayer,alpha);
		ViewSetAlpha(viewPlayerHead,alpha);
		ViewSetxy(viewPlayer,165-0.5*ViewGetWidth(viewPlayer),210-ViewGetHeight(viewPlayer));
		ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
		//EditSetVisible(edit_name,vis);
	}else if(panel==PANEL_CHAR_SELECT){
		for(int i=0 ; i<3 ; i++){
			if(gdGlobal.player_slot[i].free){
				//...
			}else{
				ViewSetAlpha(viewCharSelectPlayer[i],alpha);
				ViewSetAlpha(viewCharSelectPlayerHead[i],alpha);
			}
		}
	}
}

void inventoryDisplay(bool vis){
	if(!player) return;
	
	if(!vis){
		int i;
		TextSetVisible(text_inv_section,0);
		for(i=0 ; i<NUM_BUTTONS_INV ; i++){
			wViewSetVisible(buttonHandleInv[i], 0);
		}
		for(i=0 ; i<NUM_BUTTONS_EQU ; i++){
			wViewSetVisible(buttonHandleEqu[i], 0);
		}
		return;
	}
	
	TextSetVisible(text_inv_section,1);
	char str[5]; sprintf(str, "%02d", inventorySection+1);
	wTextSetText(text_inv_section, str);

	int i, inv=0, num_notequipped=0;
	for(i=0 ; i<MAX_INVENTORY ; i++){
		if(player->inventory[i].item_id!=-1)
			if(!player->inventory[i].equip){
				//not equipped
				if(num_notequipped < inventorySection*NUM_BUTTONS_INV){
					num_notequipped++;
					continue;
				}
				if(inv>=NUM_BUTTONS_INV) 
					continue;
				buttonInvId[inv] = i;
				wViewSetImage(buttonHandleInv[inv], inventoryResources[ player->inventory[i].item_id ]);
				wViewSetVisible(buttonHandleInv[inv], 1);
				inv++;
			}else{
				//equipped
				int id=player->inventory[i].item_id;
				int equip_index=item_db[id].equip_index;
				buttonEquId[equip_index] = i;
				wViewSetImage(buttonHandleEqu[equip_index], inventoryResources[id]);
				wViewSetVisible(buttonHandleEqu[equip_index], 1);
			}
	}
	if(inv<NUM_BUTTONS_INV)
		for( ; inv<NUM_BUTTONS_INV ; inv++){
			buttonInvId[inv] = -1;
			wViewSetVisible(buttonHandleInv[inv], 0);
		}
}

void inventoryUnequip(int equip_index){
	if(!player) return;

	int inv_index = buttonEquId[equip_index];
	buttonEquId[equip_index] = -1;
	
	ViewSetxy(buttonHandleEqu[equip_index], 89+35*(equip_index%3), 98+45*(equip_index/3));
	wViewSetVisible(buttonHandleEqu[equip_index], 0);

	player->inventory[inv_index].equip = false;
	player->computeStats();
	inventoryDisplay(true);
}

void inventoryEquip(int inv){
	if(!player) return;

	int inv_index = buttonInvId[inv];
	int equip_index = item_db[player->inventory[inv_index].item_id].equip_index;
	//if something else is already equipped
	if(buttonEquId[equip_index]!=-1) inventoryUnequip(equip_index);
	
	buttonEquId[equip_index] = inv_index;
	ViewSetxy(buttonHandleEqu[equip_index], 89+35*(equip_index%3), 98+45*(equip_index/3));
	wViewSetVisible(buttonHandleEqu[equip_index], 1);

	player->inventory[inv_index].equip = true;
	player->computeStats();
	inventoryDisplay(true);
}

int EquipmentPressed(int id,int event,int x,int y){
	if(!player) return 0;
	if(event==2){
		ViewSetxy(buttonHandleEqu[id],x-12,y-12);
	}else if(event==3){
		ViewSetxy(buttonHandleEqu[id], 89+35*(id%3), 98+45*(id/3));
		if(!(x>84 && x<189 && y>64 && y<247)){
			inventoryUnequip(id);
		}
	}
	return 0;
}

int InventoryPressed(int id,int event,int x,int y){
	if(!player) return 0;
	//move item with finger
	if(event==2){
		ViewSetxy(buttonHandleInv[id],x-12,y-12);
	}else if(event==3){
		/*
		int i;
		for(i=BTN_INV_HOTKEY1 ; i<=BTN_INV_HOTKEY2 ; i++){
			if(x>buttonMinX[i] && x<buttonMaxX[i] && y>buttonMinY[i] && y<buttonMaxY[i]){
				ViewSetxy(buttonHandleInv[id], buttonMinX[i], buttonMinY[i]);
				break;
			}
		}
		*/
		int j;
		for(j=0 ; j<9 ; j++)
			if(x>84 && x<189 && y>64 && y<247){
				inventoryEquip(id);
				break;
			}
		//if(i>BTN_INV_HOTKEY2)
		ViewSetxy(buttonHandleInv[id], 297+35*(id%3), 97+35*(id/3));
	}
	return 0;
}

int MenuPressed(int id,int event,int x,int y){
	// which button are we pressing?
	if(currentButtonId==-1)
		currentButtonId=id;
	else 
		id=currentButtonId;

	// fading in process?
	if(panelAnimCurrent!=-1 || panelAnimNext!=-1)
		return 0;

	// init
	viewPlayerRot=' ';

	// check touch
	if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
		// if we're touching
		if(event==1 || event==2){
			switch(currentButtonId){
			case BTN_INV_ROTLEFT:
				viewPlayerRot='L';
				break;
			case BTN_INV_ROTRIGHT:
				viewPlayerRot='R';
				break;
			}
		}
	}else{
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		//if we moved to another button...
		int i;
		for(i=MENU1;i<=BTN_PAUSE_RETURN;i++)
			if(panelIdOfButton[i]!=-1 && panelVisible[panelIdOfButton[i]] && x>buttonMinX[i] && x<buttonMaxX[i] && y>buttonMinY[i] && y<buttonMaxY[i]){
				wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
				currentButtonId=id=i; break;
			}
		if(i>BTN_PAUSE_RETURN)
			return 0;
	}

	// if we release
	if(event==3){
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		currentButtonId=-1;
		
		switch(id){
		case MENU1:
			levelToLoad=-1;
			if(!avatarsLoaded)
				panelSetVisible(PANEL_LOADING, true, TRANSITION_BLACK);
			else
				panelSetVisible(PANEL_CHAR_SELECT, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_MENU, false, TRANSITION_BLACK);
			break;
		case MENU2:
			AppExit();
			exit(0);
			break;
		case BTN_PAUSE_EQUIP:
			wViewSetImage(viewPlayerHead, player->_heads[viewPlayerDir]);
			panelSetVisible(PANEL_INV, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_PAUSE, false, TRANSITION_BLACK);
			break;
		case BTN_PAUSE_SYSTEM:
			panelSetVisible(PANEL_MENU, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_PAUSE, false, TRANSITION_BLACK);
			unload_game();
			Mp3Loop(music[SONG_MENU]);
			break;
		case BTN_PAUSE_RETURN:
			panelSetVisible(PANEL_PAUSE, false, TRANSITION_NORMAL);
			gameStarted = !gameStarted;
			break;
		case BTN_INV_PREV:
			if(inventorySection>0) inventorySection--;
			inventoryDisplay(true);
			break;
		case BTN_INV_NEXT:
			inventorySection++;
			if(inventorySection>MAX_INVENTORY/12) inventorySection=0;
			inventoryDisplay(true);
			break;
		case BTN_INV_RETURN:
			panelSetVisible(PANEL_PAUSE, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_INV, false, TRANSITION_BLACK);
			break;
		case BTN_CHAR_C_RETURN:
			panelSetVisible(PANEL_CHAR_SELECT, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_CHAR_CREATE, false, TRANSITION_BLACK);
			break;
		case BTN_CHAR_S_RETURN:
			panelSetVisible(PANEL_MENU, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
			break;
		case BTN_CHAR_S_SELECT1:
			selectedPlayer=0;
			if(gdGlobal.player_slot[0].free){
				char_create_head=0;
				wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
				panelSetVisible(PANEL_CHAR_CREATE, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
			}else{
				panelSetVisible(PANEL_LOADING, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
				levelToLoad=0;
				resourcesLoaded=false;
			}
			break;
		case BTN_CHAR_S_SELECT2:
			selectedPlayer=1;
			if(gdGlobal.player_slot[0].free){
				char_create_head=0;
				wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
				panelSetVisible(PANEL_CHAR_CREATE, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
			}else{
				panelSetVisible(PANEL_LOADING, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
				levelToLoad=0;
				resourcesLoaded=false;
			}
			break;
		case BTN_CHAR_S_SELECT3:
			selectedPlayer=2;
			if(gdGlobal.player_slot[0].free){
				char_create_head=0;
				wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
				panelSetVisible(PANEL_CHAR_CREATE, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
			}else{
				panelSetVisible(PANEL_LOADING, true, TRANSITION_BLACK);
				panelSetVisible(PANEL_CHAR_SELECT, false, TRANSITION_BLACK);
				levelToLoad=0;
				resourcesLoaded=false;
			}
			break;
		case BTN_CHAR_C_LEFT3:
			if(char_create_head>0) char_create_head--;
			wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
			break;
		case BTN_CHAR_C_RIGHT3:
			if(char_create_head<NUM_HEADS-1) char_create_head++;
			wViewSetImage(viewPlayerHead, playerResources[0]._heads[char_create_head][DIR_S]);
			break;
		case BTN_CHAR_C_CREATE:
			EditGetText(edit_name, gdGlobal.player_slot[selectedPlayer].name);
			if(gdGlobal.player_slot[selectedPlayer].name[0]=='\0')
				return 0;
			gdGlobal.player_slot[selectedPlayer].free=false;
			gdGlobal.player_slot[selectedPlayer].level=1;
			gdGlobal.player_slot[selectedPlayer].id_class=CLASS_KNIGHT;
			gdGlobal.player_slot[selectedPlayer].id_sex=0;
			gdGlobal.player_slot[selectedPlayer].id_body=0;
			gdGlobal.player_slot[selectedPlayer].id_head=char_create_head;
			for(int i=0 ; i<MAX_INVENTORY ; i++){
				gdGlobal.player_slot[selectedPlayer].inventory[i].item_id = -1;
				gdGlobal.player_slot[selectedPlayer].inventory[i].equip = false;
				gdGlobal.player_slot[selectedPlayer].inventory[i].amount = 0;
			}
			//for(int i=0 ; i<16 ; i++){
			//	gdGlobal.player_slot[selectedPlayer].inventory[i].item_id = i/5;
			//	gdGlobal.player_slot[selectedPlayer].inventory[i].equip = false;
			//	gdGlobal.player_slot[selectedPlayer].inventory[i].amount = 1;
			//}
			panelSetVisible(PANEL_CHAR_SELECT, true, TRANSITION_BLACK);
			panelSetVisible(PANEL_CHAR_CREATE, false, TRANSITION_BLACK);
			break;
		default:
			break;
		}
	}

	return 0;
}

int ZoomPressed(int id,int event,int x,int y){
	if(id!=BTN_ZOOMIN && id!=BTN_ZOOMOUT) return 0;

	if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
	}else{
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		return 0;
	}

	// if we release
	if(event==3){
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);

		if(id==BTN_ZOOMIN) 
			zoomPer += 20;
		else if(id==BTN_ZOOMOUT) 
			zoomPer -= 20;
		
		if(zoomPer<80) zoomPer=80;
		if(zoomPer>120) zoomPer=120;

		zoomFade = 100;
		gameStarted=false;
	}

	return 0;
}

int SitPressed(int id,int event,int x,int y){
	if(!gameStarted || !player) return 0;

	if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
		;
	}else{
		;
		return 0;
	}
	
	// stop attacking
	if(event==1 && skillIsPressed[SKILL_FIRE]){
		player->skillInUse=-1;
		skillIsPressed[SKILL_FIRE]=false;
		wViewSetImage(buttonHandle[PB_FIRE_BTN], imgbuttonHandle1[PB_FIRE_BTN]);
	}else if(event==1 && autoAttack){
		autoTarget=NULL;
	}

	// if we release
	if(event==3){
		player->isSitting = !player->isSitting;
		for(int i=PB_FIRE_BTN ; i<BTN_ZOOMIN ; i++)
			wViewSetVisible(buttonHandle[i], !player->isSitting);
		if(player->isSitting){
			wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
			player->setCurrentAnim(directionAnimSit[player->dir]);
		}else{
			wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
			player->setCurrentAnim(directionAnimIdle[player->dir]);
		}
	}

	return 0;
}

int PausePressed(int id,int event,int x,int y){
	if(id!=BTN_PAUSE || !gameStarted) return 0;

	if(x>buttonMinX[id] && x<buttonMaxX[id] && y>buttonMinY[id] && y<buttonMaxY[id]){
		wViewSetImage(buttonHandle[id], imgbuttonHandle2[id]);
	}else{
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		return 0;
	}
	
	// if we release
	if(event==3){
		wViewSetImage(buttonHandle[id], imgbuttonHandle1[id]);
		panelSetVisible(PANEL_PAUSE, true, TRANSITION_NORMAL);
		gameStarted = !gameStarted;
	}

	return 0;
}


// END OF INCLUDED FILE : Interface.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Configuration.cpp  (FILTER = ON)

// LINE FILTERED :: #include <string.h>
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "status.h"
// LINE FILTERED :: #include "map.h"
// LINE FILTERED :: #include "item.h"
// LINE FILTERED :: #include "Resources.h"
// LINE FILTERED :: #include "Configuration.h"

int skillDelay[NUM_SKILLS];
int skillRange[NUM_SKILLS];

int mobStats[NUM_MOBS][NUM_STATS];
int itemStats[NUM_ITEMS][NUM_STATS];

int statusDuration[NUM_STATUS];

void load_config(){
	load_map_config();
	load_skill_config();
	load_mob_config();
	load_item_config();
	load_status_config();
}

void load_map_config(){
	// set map stages (map_id,mob_id,quantity)
	//map 0...
	map_add_stage(0, 1, 1);
	map_add_stage(0, 1, 2);
	map_add_stage(0, 1, 3);
	map_add_stage(0, 1, 4);
	map_add_stage(0, 0, 1);
	maps[0].next_map = 0;
	//map 1...
	//...
}

void load_skill_config(){
	// default values...
	for(int i=0;i<NUM_SKILLS;i++){
		skillRange[i]=1;
		skillDelay[i]=20; //1hit per sec
	}

	// set skill range (in cell number)
	skillRange[SKILL_FIRE] =		1; //right now depends on weapon, not this value here...
	skillRange[SKILL_BESERK] =		2;
	skillRange[SKILL_BUSTER] =		2;
	skillRange[SKILL_REPEL] =		2;
	skillRange[SKILL_TELEPORT] =	1;

	// set skill delay (in frames, delay 30 = 1 hit/sec)
	skillDelay[SKILL_FIRE] =		20;
	skillDelay[SKILL_BESERK] =		30*10;
	skillDelay[SKILL_BUSTER] =		30*10;
	skillDelay[SKILL_REPEL] =		30*10;
	skillDelay[SKILL_TELEPORT] =	30*10;
}

void load_mob_config(){
	// default values...
	for(int i=0;i<NUM_MOBS;i++)
		load_mob_config_single(i, 20, 300, 0, 0, 50, 4, false);
	
	// set mob stats individually (id,atk,cri,hp,def,speed,exp,loopOnDeath)
	load_mob_config_single(0, 40, 0, 3200, 0, 4, 100, true); //boss
	load_mob_config_single(1, 20, 0, 800, 0, 4, 34, true); //first mob
}

void load_item_config(){
	// default values...
	for(int i=0;i<NUM_ITEMS;i++)
		load_item_config_single(i, "unnamed_item", 0, 0, 0, 0, 0, 0, 0);

	// set item stats individually (id,name,equip_index,atk,cri,hp,def,speed,exp)
	load_item_config_single(0, "Armor1", EQI_ARMOR, 5, 0, 10, 0, 25, 0);
	load_item_config_single(1, "Shoes1", EQI_SHOES, 5, 0, 10, 0, 25, 0);
	load_item_config_single(2, "Neck1",  EQI_NECK, 5, 0, 10, 0, 0, 0);
	load_item_config_single(3, "Ring1",  EQI_RING, 5, 0, 10, 0, 0, 0);
	load_item_config_single(4, "Armor2", EQI_ARMOR, 5, 0, 10, 0, 25, 0);
	load_item_config_single(5, "Shoes2", EQI_SHOES, 5, 0, 10, 0, 25, 0);
	load_item_config_single(6, "Neck2",  EQI_NECK, 5, 0, 10, 0, 0, 0);
	load_item_config_single(7, "Ring2",  EQI_RING, 5, 0, 10, 0, 0, 0);
	load_item_config_single(8, "MidHead1",  EQI_HEAD_MID, 0, 0, 0, 0, 0, 0);
}

void load_status_config(){
	// default values...
	for(int i=0;i<NUM_STATUS;i++){
		statusDuration[i] = 15*3;
	}

	// set status duration
	statusDuration[STATUS_STUN] = 15*3; //3secs
	statusDuration[STATUS_BESERKED] = 15*5; //5secs
}



//-----------------------------------------------------------------------

void load_mob_config_single(int id, int atk, int cri, int hp, int def, int speed, int exp, bool loop){
	mobStats[id][ATK]=atk;
	mobStats[id][CRI]=cri;
	mobStats[id][HP]=hp;
	mobStats[id][DEF]=def;
	mobStats[id][SPEED]=speed;
	mobStats[id][EXP]=exp;
	mobStats[id][LOOP]=loop;
}

void load_item_config_single(int id, char* name, int equip_index, int atk, int cri, int hp, int def, int speed, int exp){
	//name[ITEM_NAME_LENGTH-1]='\0'; //TODO
	strcpy(item_db[id].name, name);
	item_db[id].bonus[ATK]=atk;
	item_db[id].bonus[CRI]=cri;
	item_db[id].bonus[HP]=hp;
	item_db[id].bonus[DEF]=def;
	item_db[id].bonus[SPEED]=speed;
	item_db[id].bonus[EXP]=exp;
	item_db[id].equip_index=equip_index;
}


// END OF INCLUDED FILE : Configuration.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : item.cpp  (FILTER = ON)

// LINE FILTERED :: #include <vector>
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "status.h"
// LINE FILTERED :: #include "Resources.h"
// LINE FILTERED :: #include "Configuration.h"
// LINE FILTERED :: #include "item.h"

struct item_data item_db[NUM_ITEMS];


// END OF INCLUDED FILE : item.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : battle.cpp  (FILTER = ON)

// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "map.h"
// LINE FILTERED :: #include "mob.h"
// LINE FILTERED :: #include "Resources.h"
// LINE FILTERED :: #include "GameObject.h"
// LINE FILTERED :: #include "Player.h"
// LINE FILTERED :: #include "Monster.h"
// LINE FILTERED :: #include "Configuration.h"
// LINE FILTERED :: #include "Interface.h"
// LINE FILTERED :: #include "battle.h"

bool inRange(int skill, GameObject *src, GameObject *dst, float *d){
	int range = (skill==SKILL_FIRE ? src->status.rhw.range : skillRange[skill]);
	Vector2 dist = src->_pos - dst->_pos;
	float dd=dist.Length();
	if(d!=NULL) *d=dd;

	if(dd > range*BLOCK_SIZE || !dst->isVisible())
		return false;
	else
		return true;
}

int attack(int skill, GameObject *src, GameObject *dst){
	int damage = 0;
	int font = FONT_DMG;

	if(dst->isDead || dst->status.hp<=0){
		showSkillAnim(src, -1);
		return -2;
	}

	//is in range ?
	int range = (skill==SKILL_FIRE ? src->status.rhw.range : skillRange[skill]);
	Vector2 dist = src->_pos - dst->_pos;
	if(dist.Length() > range*BLOCK_SIZE || !dst->isVisible())
		return -1;

	//is it time ? wait for skill delay...
	switch(src->type){
	case TYPE_MOB:
		if( ((Monster*)src)->skillTimeCount < skillDelay[skill] ){
			((Monster*)src)->skillTimeCount++;
			return -2;
		}
		showSkillAnim(src, -1);
		((Monster*)src)->skillTimeCount = 0;
		break;
	case TYPE_PLAYER:
		if( skillTimeCount[skill] > 0 )
			return -2;
		break;
	}

	//animation...
	showSkillAnim(src, skill);

	//damage and effects...
	switch(skill){
	case SKILL_FIRE:
		damage = battle_calc_base_damage(&src->status, src->type, &src->status.rhw, 0);
		//critical?
		if(RAND100() < src->status.cri){
			damage *= 2;
			font = FONT_CRIT;
		}
		break;
	case SKILL_BESERK:
		damage = 100 + 25*(src->status.level-1);
		dst->status.effect[STATUS_BESERKED] = statusDuration[STATUS_BESERKED];
		dst->status.effect_src[STATUS_BESERKED] = &src->status;
		break;
	case SKILL_BUSTER:
		damage = 150 + 35*(src->status.level-1);
		if(RAND100() < 10 + 5*(src->status.level-1))
			dst->status.effect[STATUS_STUN] = statusDuration[STATUS_STUN];
		break;
	case SKILL_REPEL:
		if(RAND100() < 20 + 8*(src->status.level-1))
			dst->status.effect[STATUS_STUN] = statusDuration[STATUS_STUN];
		Vector2 dir=-dist; dir.Unitize();
		dst->setPosition(dst->_pos + dir*BLOCK_SIZE*2);
		return 0;
		break;
	}

	//defense...
	damage -= dst->status.def;
	//limits on final damage... everyone or monsters only ???
	if(src->type==TYPE_MOB && damage < src->status.batk) damage = src->status.batk;
	if(dst->type==TYPE_MOB && damage > 0.20*dst->status.max_hp) damage = (int)(0.20*dst->status.max_hp);

	//take damage...
	dst->status.hp = MAX(0, dst->status.hp - damage);
	showDamage(dst, damage, font);

	//hit...
	if(dst->type==TYPE_PLAYER && damage>0){
		((Player*)dst)->lasthitTimeCount = 0; //last hit timer
	}

	//killed?
	if(dst->status.hp<=0){
		src->status.exp += dst->status.exp;
		//loot:
		for(int i=0 ; i<NUM_ITEMS ; i++)
			if(RAND100()<50){ player->addItem(i,1); break; } //TODO: loop only through items dropable by current mob
	}

	//TODO: if src==mob and dist>0.5*range return -1 (to get a little closer)
	return 0;
}

int reachObject(GameObject *src, GameObject *dst){
	Vector2 dir = dst->_pos - src->_pos;
	Vector2 next;
	
	//if target is too far away, then mobs dont see him
	if(src->type==TYPE_MOB && dir.Length() / BLOCK_SIZE > 5 || !dst->isVisible())
		return 0;

	dir.Unitize();

	//direction
	if(src->type==TYPE_PLAYER){
		float ang = 180 + atan2((float)dir.y,(float)-dir.x)*180/3.14;
		int auxbut, headDir;
		if(ang <= 22.5 || ang > 337.5){
			headDir = DIR_E;
			auxbut = PB_RIGHT_BTN;
		}else if(ang <= 67.5 && ang > 22.5){
			headDir = DIR_NE;
			auxbut = PB_NE_BTN;
		}else if(ang <= 112.5 && ang > 67.5){
			headDir = DIR_N;
			auxbut = PB_TOP_BTN;
		}else if(ang <= 157.5 && ang > 112.5){
			headDir = DIR_NW;
			auxbut = PB_NW_BTN;
		}else if(ang <= 202.5 && ang > 157.5){
			headDir = DIR_W;
			auxbut = PB_LEFT_BTN;
		}else if(ang <= 247.5 && ang > 202.5){
			headDir = DIR_SW;
			auxbut = PB_SW_BTN;
		}else if(ang <= 292.5 && ang > 247.5){
			headDir = DIR_S;
			auxbut = PB_DOWN_BTN;
		}else{
			headDir = DIR_SE;
			auxbut = PB_SE_BTN;
		}
		player->setDir(headDir);
		player->setCurrentAnim(directionAnim[auxbut]);
		player->setHeadDir(headDir);
	}else{
		if(dir.x>0){
			src->direction=RIGHT;
			src->setCurrentAnim(WALK_RIGHT);
		}else{
			src->direction=LEFT;
			src->setCurrentAnim(WALK_LEFT);
		}
	}

	//this is player's speed...
	next = src->_pos + dir*4*(src->status.speed/100.0);
	
	//dont leave borders... how to deal with this?
	if(next.x < 0) next.x = 0;
	if(next.y < 0) next.y = 0;
	if(next.x >= sizeBg.x) next.x = sizeBg.x;
	if(next.y >= sizeBg.y) next.y = sizeBg.y;

	//is it occupied (by someone else)?
	GameObject *next_who = mapCells[(int)next.y/BLOCK_SIZE][(int)next.x/BLOCK_SIZE];
	if(next_who!=NULL && next_who!=src){
		next.x = src->_pos.x + (int)(dir.y*4*(src->status.speed/100.0));
		next.y = src->_pos.y - (int)(dir.x*4*(src->status.speed/100.0));
		if(next.x<0 || next.x>=sizeBg.x || next.y<0 || next.y>=sizeBg.y){
			next.x = src->_pos.x - (int)(dir.y*4*(src->status.speed/100.0));
			next.y = src->_pos.y + (int)(dir.x*4*(src->status.speed/100.0));
		}
		if(next.x<0 || next.x>=sizeBg.x || next.y<0 || next.y>=sizeBg.y){
			next = src->_pos;
		}
	}
	
	//update...
	mapCells[(int)src->_pos.y/BLOCK_SIZE][(int)src->_pos.x/BLOCK_SIZE]=NULL;
	mapCells[(int)next.y/BLOCK_SIZE][(int)next.x/BLOCK_SIZE]=src;
	src->_pos = next;

	return 0;
}

/*==========================================
 * Calculates the standard damage of a normal attack assuming it hits.
 *------------------------------------------
 * Pass damage2 as NULL to not calc it.
 * Flag values:
 * &1: Critical hit
 * &2: Arrow attack
 */
int battle_calc_base_damage(struct status_data *status, OBJ_TYPE obj_type, struct weapon_atk *wa, int flag)
{
	unsigned short atkmin=0, atkmax=0;
	short type = 0;
	int damage = 0;

	//for now, use jon's formula
	return status->batk;

	if(obj_type==TYPE_MOB){
		//Mobs/Pets
		atkmin = wa->atk;
		atkmax = wa->atk2;

		if (atkmin > atkmax)
			atkmin = atkmax;
	} else {
		//PCs
		atkmax = wa->atk;
		type = EQI_WEAPON;

		if (!(flag&1) || (flag&2))
		{	//Normal attacks
			atkmin = status->dex;
			/*
			if (sd->equip_index[type] >= 0 && sd->inventory_data[sd->equip_index[type]])
				atkmin = atkmin*(80 + sd->inventory_data[sd->equip_index[type]]->wlv*20)/100;

			if (atkmin > atkmax)
				atkmin = atkmax;
			*/
		}
	}
		
	//Weapon Damage calculation
	if (!(flag&1))
		damage = (atkmax>atkmin? rand()%(atkmax-atkmin):0)+atkmin;
	else 
		damage = atkmax;
		
	//Finally, add baseatk
	damage += status->batk;
	
	return damage;
}


// END OF INCLUDED FILE : battle.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : SaveData.cpp  (FILTER = ON)

// LINE FILTERED :: #include <stdio.h>
// LINE FILTERED :: #include <string.h>
// LINE FILTERED :: #include <time.h>
// LINE FILTERED :: #include "DragonFireSDK.h"
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "SaveData.h"

// game data objects
GAMEDATA gdGlobal;
GAMEDATA gdDefault;

// SetDefaultGameData() -- Use this function to provide a default
//						   value for every variable contained in
//						   the GAMEDATA structure.  This information
//						   will be used to provide game data for
//						   the first time your app is run on the
//                         device or any time the data becomes
//						   corrupted (which should be never, but
//						   we all know how computers can be).

void SetDefaultGameData(void)
{
	// Fill every variable in the gdDefault object here.  The values
	// in gdDefault should not be used or modified anywhere else in
	// your app.  For accessing and changing global data values
	// elsewhere, use the gdGlobal structure, since that is the
	// information that will be saved to disk.  Just to emphasize:
	// Only use gdDefault in this function!  Use gdGlobal everywhere
	// else in your app.

	// example: (you can delete this)
	gdDefault.player_slot[0].free=true;
	gdDefault.player_slot[1].free=true;
	gdDefault.player_slot[2].free=true;
}

///////////////////////////////////////////////////////////////////////
// The functions below perform all of the necessary operations for
// saving and loading the data.  When you want to save the data,
// such as when the user makes an options selection, reaches a
// significant point in the game, or during AppExit(), call the
// SaveGameData() function.  The LoadGameData() function should be
// called early in AppMain() and in most cases will not need to be
// called again.  Remember to provide a filename for SaveGameData()
// and LoadGameData() that does NOT have a file extension!  The file
// extension will be added for you (.sav for the main save and .bak
// for the backup).  For example, use SaveGameData("scores"), but do
// not use SaveGameData("scores.dat").

// SaveGameData() -- redundantly save timestamped & encrypted game data to disk
void SaveGameData(const char* pFilename)
{
	// make sure there is data to save
	if(sizeof(GAMEDATA)==0)
		return;

	// make sure the filename is not too long
	if(strlen(pFilename)>MAX_GD_STRING)
	{
		// output debug message
		printf("SaveGameData() : Filename contains too many characters.\n");

		// stop here
		return;
	}

	// save both files (original and backup)
	for(int n=0;n<2;n++)
	{
		// game data save objects
		GAMEDATA_TS gtSaveData;
		GAMEDATA_TS gtSaveCheck;

		// we need a buffer 2x the size of GAMEDATA_TS, so use an array
		GAMEDATA_TS gtBuffer[2];

		// now cast the buffer to an array of 16-bit values
		unsigned short* p16Data=(unsigned short*)gtBuffer;

		// copy global data to local variable
		gtSaveData.gdData=gdGlobal;

		// set timestamp for save
		gtSaveData.ttStamp=time(0);
		
		// create checksums for save
		CreateGameDataChecksums(&gtSaveData,
								&gtSaveCheck);

		// encode the data
		InterlaceGameData(&gtSaveData,
						  &gtSaveCheck,
						  p16Data);

		// string for filename maniplation
		char pStr[MAX_GD_STRING+5]; // +5 allows bytes for .ext and null terminator

		// add the extenstion to the filename
		sprintf(pStr,
				"%s%s",
				pFilename,
				(n==1) ? ".sav" : ".bak");

		// save the file
		SaveGameDataFile(pStr,
						 p16Data);
	}
}

// LoadGameData() -- load most recent & valid game data from disk
void LoadGameData(const char* pFilename)
{
	// make sure there is data to load
	if(sizeof(GAMEDATA)==0)
		return;

	// make sure the filename is not too long
	if(strlen(pFilename)>MAX_GD_STRING)
	{
		// output debug message
		printf("LoadGameData() : Filename contains too many characters.\n");

		// stop here
		return;
	}

	// objects for storing loaded game data
	GAMEDATA_TS gtLoadData[2];

	// load both files (original and backup)
	for(int n=0;n<2;n++)
	{
		// we need a buffer 2x the size of GAMEDATA_TS, so use an array
		GAMEDATA_TS gtBuffer[2];

		// now cast the buffer to an array of 16-bit values
		unsigned short* p16Data=(unsigned short*)gtBuffer;

		// string for filename maniplation
		char pStr[MAX_GD_STRING+5]; // +5 allows bytes for .ext and null terminator

		// add the extenstion to the filename
		sprintf(pStr,
				"%s%s",
				pFilename,
				(n==1) ? ".sav" : ".bak");

		// load the file
		LoadGameDataFile(pStr,
						 p16Data);

		// this holds the checksums
		GAMEDATA_TS gtLoadCheck;

		// decode the data image
		DeinterlaceGameData(&gtLoadData[n],
						    &gtLoadCheck,
						    p16Data);
		
		// validate the loaded data
		ValidateGameData(&gtLoadData[n],
						 &gtLoadCheck);
	}

	// copy the most recent validated data to the global struct
	gdGlobal=(gtLoadData[0].ttStamp>gtLoadData[1].ttStamp) ? gtLoadData[0].gdData : gtLoadData[1].gdData;
}

// ComputeGameDataHash() -- create a unique number based on global game data
char ComputeGameDataHash(PGAMEDATA_TS pGTData)
{
	// set hash value to 0
	char cHash=0;
	
	// cast the game data to a char buffer
	char* pBuf=(char*)pGTData;

	// add all the bytes together to produce a unique number
	for(int n=0;n<sizeof(GAMEDATA_TS);n++)
		cHash+=pBuf[n];

	// return the number
	return(cHash);
}

// CreateGameDataChecksums() -- create checksum list for game data
void CreateGameDataChecksums(PGAMEDATA_TS pGTData,
							 PGAMEDATA_TS pGTCheck)
{
	// get the starting number
	char cHash=ComputeGameDataHash(pGTData);

	// cast the checksum data to a char buffer
	char* pCheckBuf=(char*)pGTCheck;

	// process each byte
	for(int n=0;n<sizeof(GAMEDATA_TS);n++)
	{
		// get a random number from 0-7
		char cRand=Random(8);

		// add the random value to the hash & truncate to 4 bits
		cHash=(cHash+cRand)&0x0F;

		// add the lower 3 bits of the byte number to the random number
		cRand+=(n&0x07);

		// store the combined hash (lower 4 bits) and the random value (upper 4 bits)
		pCheckBuf[n]=(cRand<<4)|cHash;
	}
}

// ValidateGameData() -- check the game data and set to default if needed
void ValidateGameData(PGAMEDATA_TS pGTData,
					  PGAMEDATA_TS pGTCheck)
{
	// flag for valid default data
	static bool bDefDataIsValid=false;

	// check default data
	if(!bDefDataIsValid)
	{
		// set the default data
		SetDefaultGameData();

		// set the flag
		bDefDataIsValid=true;
	}

	// get the starting number
	char cHash=ComputeGameDataHash(pGTData);

	// cast the checksum data to a char buffer
	char* pCheckBuf=(char*)pGTCheck;

	// flag to indicate failure
	bool bFail=false;

	// validate each byte
	for(int n=0;n<sizeof(GAMEDATA_TS);n++)
	{
		// extract the random value from the buffer
		int cRand=(pCheckBuf[n]&0xF0)>>4;

		// subract the lower 3 bits of the byte number from the random number;
		// this yields the original random number
		cRand-=(n&0x07);

		// add the random value to the hash & truncate to 4 bits
		cHash=(cHash+cRand)&0x0F;

		// compare this value with the stored hash (lower 4 bits)
		if(cHash!=(pCheckBuf[n]&0x0F))
		{
			// invalid byte, so throw the flag
			bFail=true;

			// stop the loop
			break;
		}
	}

	// check for failure
	if(bFail)
	{
		// output debug message
		printf("ValidateGameData() : Invalid checksum; using default game data.\n");

		// copy the default data
		pGTData->gdData=gdDefault;

		// set the timestamp to 0 (this way, if the other file is still
		// valid, its data will be used instead)
		pGTData->ttStamp=0;
	}
	else
	{
		// the data is valid, so leave it alone

		// output debug message
		printf("ValidateGameData() : Successfully loaded game data.\n");
	}
}

// InterlaceGameData() -- encode data and checksums for saving
void InterlaceGameData(PGAMEDATA_TS pGTData,
					   PGAMEDATA_TS pGTCheck,
					   unsigned short* p16Data)
{
	// cast data & checksums as uchar buffers
	unsigned char* cDataBuf  =(unsigned char*)pGTData;
	unsigned char* cCheckBuf =(unsigned char*)pGTCheck;

	// process each value
	for(int n=0;n<sizeof(GAMEDATA_TS);n++)
	{
		// encode the game data with a bitwise not (okay, so it's not exactly Ft. Knox)
		cDataBuf[n]=~cDataBuf[n];

		// shred the data for interlacing
		unsigned short n16Val1 =(((unsigned short)cDataBuf[n]) &0x000F)<<12; // bits 12-15
		unsigned short n16Val2 =(((unsigned short)cDataBuf[n]) &0x00F0)>>4;  // bits 0-3
		unsigned short n16Val3 =(((unsigned short)cCheckBuf[n])&0x000F)<<8;  // bits 8-11
		unsigned short n16Val4 =(((unsigned short)cCheckBuf[n])&0x00F0);     // bits 4-7

		// store the interlaced value
		p16Data[n]=n16Val1|n16Val2|n16Val3|n16Val4;
	}
}

// DeinterlaceGameData() -- decode the image back into data and checksums
void DeinterlaceGameData(PGAMEDATA_TS pGTData,
					     PGAMEDATA_TS pGTCheck,
					     unsigned short* p16Data)
{
	// cast data & checksums as uchar buffers
	unsigned char* cDataBuf  =(unsigned char*)pGTData;
	unsigned char* cCheckBuf =(unsigned char*)pGTCheck;

	// process each value
	for(int n=0;n<sizeof(GAMEDATA_TS);n++)
	{
		// un-shred the interlaced data
		unsigned short n16Val1 =(p16Data[n]&0xF000)>>12;
		unsigned short n16Val2 =(p16Data[n]&0x000F)<<4;
		unsigned short n16Val3 =(p16Data[n]&0x0F00)>>8;
		unsigned short n16Val4 =(p16Data[n]&0x00F0);

		// put the data back in its proper places
		cDataBuf[n]  =(unsigned char)(n16Val1|n16Val2);
		cCheckBuf[n] =(unsigned char)(n16Val3|n16Val4);

		// decode the game data with a bitwise not
		cDataBuf[n]=~cDataBuf[n];
	}
}

// SaveGameDataFile() -- save the encoded data image to disk
void SaveGameDataFile(char* pFilename,
					  unsigned short* p16Data)
{
	// create the file
	int hFile=FileCreate(pFilename);

	// output the data
	FileWrite(hFile,
			  (char*)p16Data,
			  sizeof(GAMEDATA_TS)*sizeof(short));

	// close the file
	FileClose(hFile);
}

// LoadGameDataFile() -- load encoded game data image from disk
void LoadGameDataFile(char* pFilename,
					  unsigned short* p16Data)
{
	// open the file
	int hFile=FileOpen(pFilename);

	// input the data
	FileRead(hFile,
			 (char*)p16Data,
			 sizeof(GAMEDATA_TS)*sizeof(short));

	// close the file
	FileClose(hFile);
}


// END OF INCLUDED FILE : SaveData.cpp
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// INCLUDED FILE : Main.cpp  (FILTER = ON)

// LINE FILTERED :: #include <string.h>

// "server side" stuff
// LINE FILTERED :: #include "mmo.h"
// LINE FILTERED :: #include "map.h"
// LINE FILTERED :: #include "mob.h"

// "client side" stuff
// LINE FILTERED :: #include "Resources.h"
// LINE FILTERED :: #include "AnimObject.h"
// LINE FILTERED :: #include "GameObject.h"
// LINE FILTERED :: #include "Player.h"
// LINE FILTERED :: #include "Monster.h"
// LINE FILTERED :: #include "Configuration.h"
// LINE FILTERED :: #include "Interface.h"
// LINE FILTERED :: #include "battle.h"
// LINE FILTERED :: #include "item.h"
// LINE FILTERED :: #include "SaveData.h"

unsigned int frameCount=0;
int frameCountInv=0;
bool appLoaded=false;
bool avatarsLoaded=false;
bool resourcesLoaded=false;
bool gameStarted=false;
Player* player=NULL;

int levelToLoad=0;

int skillTimeCount[NUM_SKILLS];

bool skillIsActive[NUM_SKILLS];
bool skillIsPressed[NUM_SKILLS];
bool directionIsPressed=false;

AnimObject *weather=NULL;

//-------------------------------------------------------

void restart_game(){
	//delete monsters...
	block_list *aux=bl_head.next, *lastaux;
	while(aux){
		lastaux=aux;
		aux=aux->next;
		delete ((Monster*)lastaux->obj);
	}
	// clear map cells (to be safe)
	for(int i=0 ; i<1+(int)(sizeBg.y/BLOCK_SIZE) ; i++)
		for(int j=0 ; j<1+(int)(sizeBg.x/BLOCK_SIZE) ; j++)
			mapCells[i][j]=NULL;

	//spawn monsters...
	map_spawn(&player->map, &player->map_stage);

	//player...
	player->status.hp = player->status.max_hp;
	player->invisibleTimeCount=0;
	player->lasthitTimeCount=0;
	player->setDead(false);
	player->_pos.x = 250; player->_pos.y = 200;
	centerPlayer();
	player->idle();
}

void load_resources(int level_id){

	if(level_id==-1 && !avatarsLoaded){
		load_player_resources_avatar();
		load_head_resources();
		load_interface_player();

		load_item_resources(); //TODO: load when going to inventory; only if not loaded yet
		avatarsLoaded=true;

		panelSetVisible(PANEL_CHAR_SELECT, true, TRANSITION_BLACK);
		panelSetVisible(PANEL_LOADING, false, TRANSITION_BLACK);
	}else if(level_id==0){
		//load images...
		load_player_resources();
		load_mob_resources();
		load_weather_resources();
		weather->init();

		//handles...
		init_object_handles_storage();

		//start game
		load_game();
		gameStarted=true;

		panelSetVisible(PANEL_LOADING, false, TRANSITION_NORMAL);
	}else{
		panelSetVisible(PANEL_LOADING, false, TRANSITION_NORMAL);
	}

	//done...
	resourcesLoaded=true;
}

void load_level(){
	Mp3Stop();
	//Mp3Loop(music[SONG_LVL1]);

	centerPlayer();

	// martim: map cells
	mapCells.resize( 1+(int)(sizeBg.y/BLOCK_SIZE), 1+(int)(sizeBg.x/BLOCK_SIZE) );
	for(int i=0 ; i<mapCells.rows() ; i++)
		for(int j=0 ; j<mapCells.cols() ; j++)
			mapCells[i][j]=NULL;
	/*
	mapCells = new GameObject**[1+(int)(sizeBg.y/BLOCK_SIZE)];
	for(int i=0 ; i<1+(int)(sizeBg.y/BLOCK_SIZE) ; i++){
		mapCells[i] = new GameObject*[1+(int)(sizeBg.x/BLOCK_SIZE)];
		for(int j=0 ; j<1+(int)(sizeBg.x/BLOCK_SIZE) ; j++)
			mapCells[i][j]=NULL;
	}
	*/

	//spawn monsters...
	map_spawn(&player->map, &player->map_stage);

	// martim: skill variables
	player->skillInUse=-1;
	for(int i=0;i<NUM_SKILLS;i++){
		skillTimeCount[i]=-1;
		skillIsActive[i]=false;
		skillIsPressed[i]=false;
	}
}

void load_game(){
	wViewSetVisible(viewBlack,0);
	if(player) delete player;
	player = new Player(250, 200); //create new player
	player->status.level = gdGlobal.player_slot[selectedPlayer].level;
	player->setHead(gdGlobal.player_slot[selectedPlayer].id_head);
	if(!gdGlobal.player_slot[selectedPlayer].free){
		memcpy(player->inventory, gdGlobal.player_slot[selectedPlayer].inventory, MAX_INVENTORY*sizeof(struct item));
	}
	player->computeStats();

	//fill in equipment slots
	inventoryDisplay(true);
	inventoryDisplay(false);

	load_level();
}

void unload_level(){
	//delete stuff...
	block_list *aux=bl_head.next, *lastaux;
	while(aux){
		lastaux=aux;
		aux=aux->next;
		delete ((Monster*)lastaux->obj);
	}
	/*
	if(mapCells){
		for(int i=0 ; i<1+(int)(sizeBg.y/BLOCK_SIZE) ; i++){
			if(mapCells[i]) delete[] mapCells[i];
		}
		delete[] mapCells;
		mapCells=NULL;
	}
	*/
}

void unload_game(){
	gameStarted=false;

	//save game...
	if(player)
		player->save();

	//unload everything...
	Mp3Stop();
	unload_level();
	if(player){ delete player; player=NULL; }
}

//===============================================
void AppMain()
{
	DragonWrapperStart(WRAPPER_LOG_WARNINGS);
	LandscapeMode();
	posBg.x=0; posBg.y=0;

	//background
	strcpy(maps[0].background, "Images/Maps/Background.png");
	viewBg = wViewAdd(maps[0].background, 0, 0);
	sizeBg.x=ViewGetWidth(viewBg); sizeBg.y=ViewGetHeight(viewBg);
	maps[0].size = sizeBg;

	//weather
	weather = new AnimObject();
	weather->setCurrentAnim(0);
	weather->setVisible(false);

	//resources
	map_init();
	load_interface_resources();
	load_interface();
	load_config();
	load_sounds();
	load_menu();

	LoadGameData("ixile_save");
	//gdGlobal.player_slot[0].free=true;
	//gdGlobal.player_slot[1].free=true;
	//gdGlobal.player_slot[2].free=true;

	appLoaded=true;

	return;
}

void AppExit()
{
	DragonWrapperEnd();
	unload_game();
	if(weather){ delete weather; }
}

void updateZorder(){
	block_list *aux;
	GameObject **sortedObjs = new GameObject*[bl_count+1], *obj;
	int i=0, swaps=0;

	//temporary vectors (with all monsters and the player)
	aux=bl_head.next;
	while(aux){
		sortedObjs[i++] = aux->obj;
		aux=aux->next;
	}
	sortedObjs[i] = (GameObject*)player;
	
	//sort handles? No. that array should already be sorted...

	//sort objects
	do{
		swaps=0;
		for(i=0;i<bl_count;i++)
			if(sortedObjs[i]->_pos.y > sortedObjs[i+1]->_pos.y){
				obj=sortedObjs[i]; sortedObjs[i]=sortedObjs[i+1]; sortedObjs[i+1]=obj; swaps++;
			}
	}while(swaps>0);

	//swap handles
	for(i=0;i<bl_count+1;i++){
		if(sortedObjs[i]->handles.view[0] != used_handles[i].view[0]){
			sortedObjs[i]->handles = used_handles[i];
			sortedObjs[i]->reloadText();
		}
	}

	delete[] sortedObjs; //i should...right?
	return;
}

void restrictPlayer(){
	//check off-screen limits
	int playerw=ViewGetWidth(player->handles.view[0]);
	int playerh=ViewGetHeight(player->handles.view[0]);
	if(player->_pos.x < 0.5*playerw - posBg.x)
		player->_pos.x = 0.5*playerw - posBg.x;
	if(player->_pos.x > 480-0.5*playerw - posBg.x)
		player->_pos.x = 480-0.5*playerw - posBg.x;
	if(player->_pos.y < playerh - posBg.y)
		player->_pos.y = playerh - posBg.y;
	if(player->_pos.y > 320 - posBg.y)
		player->_pos.y = 320 - posBg.y;
}

//===============================================
void OnTimer()
{
	int i;

	//loading screen
	if(panelVisible[PANEL_LOADING] && panelAnimNext==-1){
		if(!resourcesLoaded)
			load_resources(levelToLoad);
	}

	//menu fading
	if(panelAnimCurrent!=-1){ //first finish fade out
		panelAnimCurrent_alpha-=20;
		if(panelAnimCurrent_alpha>0)
			panelSetAlpha(panelAnimCurrent, panelAnimCurrent_alpha);
		else
			panelSetVisible(panelAnimCurrent, false, TRANSITION_NOP);
	}else if(panelAnimNext!=-1){ //then fade in
		panelAnimNext_alpha+=20;
		if(panelAnimNext_alpha<=100)
			panelSetAlpha(panelAnimNext, panelAnimNext_alpha);
		else
			panelAnimNext=-1;
	}
	//zoom fading
	if(zoomFade>=0){
		if(zoomFade==100){
			panelSetVisible(PANEL_CLEAR, true, TRANSITION_BLACK);
			panelSetAlpha(PANEL_CLEAR, 0);
		}else if(zoomFade>50){
			panelSetAlpha(PANEL_CLEAR, 200-2*zoomFade);
		}else if(zoomFade>0){
			if(zoomFade==50){
				ViewSetSize(viewBg, (int)(sizeBg.x*zoomPer/100.0), (int)(sizeBg.y*zoomPer/100.0));
				centerPlayer();
				player->update();
			}
			panelSetAlpha(PANEL_CLEAR, 2*zoomFade);
		}else{
			panelSetVisible(PANEL_CLEAR, false, TRANSITION_NORMAL);
			gameStarted=true;
		}
		zoomFade-=10;
	}

	//game is not running
	if(!gameStarted){
		//inventory animations
		if(panelVisible[PANEL_INV]){
			if ((frameCountInv++)%3 != 0) return;
			if(viewPlayerRot=='L'){
				if(--viewPlayerDir <= -1) viewPlayerDir=7;
				wViewSetImage(viewPlayer, player->_frames[viewPlayerDir][0]);
				wViewSetImage(viewPlayerHead, player->_heads[viewPlayerDir]);
				ViewSetxy(viewPlayer,240-0.5*ViewGetWidth(viewPlayer),180-ViewGetHeight(viewPlayer));
				ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
			}else if(viewPlayerRot=='R'){
				if(++viewPlayerDir >= 8) viewPlayerDir=0;
				wViewSetImage(viewPlayer, player->_frames[viewPlayerDir][0]);
				wViewSetImage(viewPlayerHead, player->_heads[viewPlayerDir]);
				ViewSetxy(viewPlayer,240-0.5*ViewGetWidth(viewPlayer),180-ViewGetHeight(viewPlayer));
				ViewSetxy(viewPlayerHead,ViewGetx(viewPlayer),ViewGety(viewPlayer));
			}
		}
		//stop here.
		return;
	}

	//------------------------------------------------------
	// Game is running from here on

	frameCount++;

	// invisible for teleport skill... max 5secs
	if(skillIsActive[SKILL_TELEPORT] && player->invisibleTimeCount > 15*5){
		TouchSetVisible(view_touch,0);
		player->setVisible(true);
		player->invisibleTimeCount=0;
		for(int i=0 ; i<MENU1 ; i++)
			wViewSetVisible(buttonHandle[i],1);
		wViewSetVisible(buttonHandle[JOYSTICK],1);
	}

	// last time player was hit for hp regen... after 5secs no hit, regen 10HP every 2secs
	if(!player->isDead && player->lasthitTimeCount - 15*5 > 0 && (player->lasthitTimeCount - 15*5)%(15*2)==0){
		player->status.hp += 10;
		if(player->status.hp > player->status.max_hp)
			player->status.hp = player->status.max_hp;
	}

	// fire skill active ? (pressed or counting delay)
	if(skillIsActive[SKILL_FIRE]){
		// if delay has passed...
		if(skillTimeCount[SKILL_FIRE] >= skillDelay[SKILL_FIRE]){
			skillTimeCount[SKILL_FIRE] = 0;
			// attack if button is pressed
			if(skillIsPressed[SKILL_FIRE]){
				block_list *aux=bl_head.next;
				while(aux){
					attack(SKILL_FIRE,player, aux->obj);
					aux=aux->next;
				}
				showSkillAnim(player, SKILL_FIRE);
				SoundPlay(sound[SOUND_ATK0]);
			}
			// deactivate
			skillIsActive[SKILL_FIRE] = false;
		}
		skillTimeCount[SKILL_FIRE]++;
		// activate if button is pressed
		if(skillIsPressed[SKILL_FIRE])
			skillIsActive[SKILL_FIRE] = true;
	}

	// process skill delays...
	if(player->isVisible() && !player->isDead)
	for(i=0;i<NUM_SKILLS;i++){
		if(skillIsActive[i] && i!=SKILL_FIRE){
			skillTimeCount[i]++;
			/*
			// stop animation after x frames
			if(skillTimeCount[i] == 9){
				if(directionIsPressed)
					player->setCurrentAnim(directionAnim[currentButtonId]);
				else
					showSkillAnim(player, -1);
			}
			*/
			// unlock skill after "delay" frames
			if(skillTimeCount[i] == skillDelay[i]){
				skillIsActive[i]=false;
				wViewSetImage(buttonHandle[buttonIdOfSkill[i]], imgbuttonHandle1[buttonIdOfSkill[i]]);
			}
		}
	}

	//auto-target
	if(autoTarget!=NULL && autoAttack && !player->isDead){
		if(inRange(autoNextSkill, player, autoTarget, NULL)){
			//autoTarget=NULL;
			SkillPressed(buttonIdOfSkill[autoNextSkill],1,0,0);
		}else{
			reachObject(player, autoTarget);
		}
	}

	//walk
	if(!player->isDead && !player->isSitting)
		player->_pos += joystick_dir*4*(player->status.speed/100.0);
	player->checkPositionLimits();

	//map movement...
	centerPlayer();

	//update animations 15fps
	if (frameCount % 2 == 0)
	{
		//update panels...
		updateHPbar(100 * player->status.hp / player->status.max_hp); //TODO: update only when damaged
		updateEXPbar(player->status.exp);
		textLogUpdate();

		char level_txt[3];
		sprintf(level_txt,"%2d",player->status.level);
		wTextSetText(panel_level, level_txt);

		//update Z-order...
		updateZorder();

		//update monsters...
		block_list *aux=bl_head.next, *lastaux; int num_monsters=0;
		while(aux){
			//count monsters. TODO: keep it on a global var?
			if(aux->obj->type==TYPE_MOB) num_monsters++;

			//attack/walk...
			((Monster*)aux->obj)->runAI();
			((Monster*)aux->obj)->update();

			lastaux=aux;
			aux=aux->next;
			//delete monster if it was dead
			if(lastaux->obj->isDead && !lastaux->obj->expiryDate) 
				delete ((Monster*)lastaux->obj);
		}

		//map stage completed?
		if(num_monsters==0){
			player->map_stage++;
			map_spawn(&player->map, &player->map_stage);
		}
		
		//update player...
		player->update();

		if(weather) weather->update();

		//game over?
		if(player->isDead && player->expiryDate==0)
			restart_game();

		//frameCount = 0;
	}	
}


// END OF INCLUDED FILE : Main.cpp
//--------------------------------------------------------------------------------

//
// End of COMPILATION file... 


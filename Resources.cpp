#include <vector>
#include "mmo.h"
#include "DragonFireSDK.h"
#include "Resources.h"
#include "GameObject.h"
#include "Player.h"
#include "Interface.h"

// construction and destruction
Resources::Resources() : 
	_frameTime(0.0333), _maxTime(0)
{
	_frames = new int*[NUM_ANIMS];
	for(int i=0 ; i<NUM_ANIMS ; i++){
		_numFrames[i] = 0;
		_frames[i] = NULL;
	}
}

Resources::~Resources()
{ 
	delete [] _frames;
	_frames = 0;
}

// add head
void Resources::addHead(int headId, int dir, char *filename)
{
	_heads[headId][dir] = wImageAdd(filename);
}

// add animation
void Resources::addAnim(int animId, const char *baseFilename)
{
	//check if animation was not added already
	if(_numFrames[animId]>0)
		return;

	//open files starting with 0 and open as many as can be found
	//we assume a .png extension
	int im = 0;
	int im_cnt = 0;
	char fn[512];
	int tmp[1000];

	sprintf(fn,"%s0.png",baseFilename,0);
	im = wImageAdd(fn);

	while(im && im_cnt < 1000)
	{
		tmp[im_cnt] = im;
		im_cnt++;
		
		sprintf(fn,"%s%d.png",baseFilename,im_cnt);
		im = wImageAdd(fn);
	}
	
	//copy over
	if(im_cnt > 0)
	{
		_frames[animId] = new int[im_cnt];
		_numFrames[animId] = im_cnt;

		for(int i=0; i<im_cnt; i++)
		{
			_frames[animId][i] = tmp[i];
		}
	}

	_maxTime = _numFrames[animId] * _frameTime;

}

//---------------------------------------------------------------
// Load Handles...

std::vector<ObjectHandles> used_handles;

//storage of view handles and text handles
std::vector<ObjectHandles> object_handles_storage(MAX_OBJECTS);

//load all the view/text handles that may show during the game
void init_object_handles_storage(){
	//view handles (for object images)
	for(int i=0 ; i<MAX_OBJECTS ; i++)
		for(int j=0 ; j<NUM_VIEWHANDLES_PER_OBJ ; j++){
			object_handles_storage[i].view[j] = wViewAdd("Images/dummy.png",0,0);
			wViewSetVisible(object_handles_storage[i].view[j], 0);
		}
	//text handles (on top of images)
	for(int i=0 ; i<MAX_OBJECTS ; i++){
		object_handles_storage[i].text[3] = wTextAdd(0,0,"   ",viewFont[FONT_CRIT_SHADOW]);
		object_handles_storage[i].text[2] = wTextAdd(0,0,"   ",viewFont[FONT_CRIT]);
		object_handles_storage[i].text[1] = wTextAdd(0,0,"   ",viewFont[FONT_DMG_SHADOW]);
		object_handles_storage[i].text[0] = wTextAdd(0,0,"   ",viewFont[FONT_DMG]);
		TextSetVisible(object_handles_storage[i].text[3], 0);
		TextSetVisible(object_handles_storage[i].text[2], 0);
		TextSetVisible(object_handles_storage[i].text[1], 0);
		TextSetVisible(object_handles_storage[i].text[0], 0);
	}
}

ObjectHandles popHandles(){
	ObjectHandles hnd = object_handles_storage.back();
	object_handles_storage.pop_back();

	//used handles array should always be sorted...
	int i=0;
	while(i<used_handles.size() && used_handles[i].view[0]<hnd.view[0]) i++;
	used_handles.insert(used_handles.begin()+i, hnd);

	return hnd;
}

void pushHandles(ObjectHandles hnd){
	object_handles_storage.push_back(hnd);

	for(int i=0 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++)
		wViewSetVisible(hnd.view[i], 0);
	for(int i=0 ; i<NUM_TEXTHANDLES_PER_OBJ ; i++)
		TextSetVisible(hnd.text[i], 0);

	for(int i=0 ; i<used_handles.size() ; i++){
		if(used_handles[i].view[0]==hnd.view[0])
			used_handles.erase(used_handles.begin()+i);
	}
}

//---------------------------------------------------------------
// Load Resources...

// Sounds...
int music[NUM_SONGS];
int sound[NUM_SOUNDS];

void load_sounds(){
	//Songs
	music[SONG_MENU] = Mp3Add("Sounds/menu0.mp3");
	music[SONG_LVL1] = Mp3Add("Sounds/level0.mp3");
	//Sounds
	sound[SOUND_ATK0] = SoundAdd("Sounds/player-atk-01/atk0.snd");
	sound[SOUND_ATK1] = SoundAdd("Sounds/player-atk-01/atk1.snd");
	sound[SOUND_ATK2] = SoundAdd("Sounds/player-atk-01/atk2.snd");
}

// Players...
Resources playerResources[NUM_PLAYERS];

void load_head_resources_id(int id){
	switch(id)
	{
	case 0:
		//male head 01
		playerResources[0].addHead(0, DIR_S,	"Images/Players/male01/head01/down/h0.png");
		playerResources[0].addHead(0, DIR_SW,	"Images/Players/male01/head01/sw/h0.png");
		playerResources[0].addHead(0, DIR_W,	"Images/Players/male01/head01/left/h0.png");
		playerResources[0].addHead(0, DIR_NW,	"Images/Players/male01/head01/nw/h0.png");
		playerResources[0].addHead(0, DIR_N,	"Images/Players/male01/head01/up/h0.png");
		playerResources[0].addHead(0, DIR_NE,	"Images/Players/male01/head01/ne/h0.png");
		playerResources[0].addHead(0, DIR_E,	"Images/Players/male01/head01/right/h0.png");
		playerResources[0].addHead(0, DIR_SE,	"Images/Players/male01/head01/se/h0.png");
		break;
	case 1:
		//male head 02
		playerResources[0].addHead(1, DIR_S,	"Images/Players/male01/head02/down/h0.png");
		playerResources[0].addHead(1, DIR_SW,	"Images/Players/male01/head02/sw/h0.png");
		playerResources[0].addHead(1, DIR_W,	"Images/Players/male01/head02/left/h0.png");
		playerResources[0].addHead(1, DIR_NW,	"Images/Players/male01/head02/nw/h0.png");
		playerResources[0].addHead(1, DIR_N,	"Images/Players/male01/head02/up/h0.png");
		playerResources[0].addHead(1, DIR_NE,	"Images/Players/male01/head02/ne/h0.png");
		playerResources[0].addHead(1, DIR_E,	"Images/Players/male01/head02/right/h0.png");
		playerResources[0].addHead(1, DIR_SE,	"Images/Players/male01/head02/se/h0.png");
		break;
	case 2:
		//male head 03
		playerResources[0].addHead(2, DIR_S,	"Images/Players/male01/head03/down/h0.png");
		playerResources[0].addHead(2, DIR_SW,	"Images/Players/male01/head03/sw/h0.png");
		playerResources[0].addHead(2, DIR_W,	"Images/Players/male01/head03/left/h0.png");
		playerResources[0].addHead(2, DIR_NW,	"Images/Players/male01/head03/nw/h0.png");
		playerResources[0].addHead(2, DIR_N,	"Images/Players/male01/head03/up/h0.png");
		playerResources[0].addHead(2, DIR_NE,	"Images/Players/male01/head03/ne/h0.png");
		playerResources[0].addHead(2, DIR_E,	"Images/Players/male01/head03/right/h0.png");
		playerResources[0].addHead(2, DIR_SE,	"Images/Players/male01/head03/se/h0.png");
		break;
	case 3:
		//male head 04
		playerResources[0].addHead(3, DIR_S,	"Images/Players/male01/head04/down/h0.png");
		playerResources[0].addHead(3, DIR_SW,	"Images/Players/male01/head04/sw/h0.png");
		playerResources[0].addHead(3, DIR_W,	"Images/Players/male01/head04/left/h0.png");
		playerResources[0].addHead(3, DIR_NW,	"Images/Players/male01/head04/nw/h0.png");
		playerResources[0].addHead(3, DIR_N,	"Images/Players/male01/head04/up/h0.png");
		playerResources[0].addHead(3, DIR_NE,	"Images/Players/male01/head04/ne/h0.png");
		playerResources[0].addHead(3, DIR_E,	"Images/Players/male01/head04/right/h0.png");
		playerResources[0].addHead(3, DIR_SE,	"Images/Players/male01/head04/se/h0.png");
		break;
	case 4:
		//male head 05
		playerResources[0].addHead(4, DIR_S,	"Images/Players/male01/head05/down/h0.png");
		playerResources[0].addHead(4, DIR_SW,	"Images/Players/male01/head05/sw/h0.png");
		playerResources[0].addHead(4, DIR_W,	"Images/Players/male01/head05/left/h0.png");
		playerResources[0].addHead(4, DIR_NW,	"Images/Players/male01/head05/nw/h0.png");
		playerResources[0].addHead(4, DIR_N,	"Images/Players/male01/head05/up/h0.png");
		playerResources[0].addHead(4, DIR_NE,	"Images/Players/male01/head05/ne/h0.png");
		playerResources[0].addHead(4, DIR_E,	"Images/Players/male01/head05/right/h0.png");
		playerResources[0].addHead(4, DIR_SE,	"Images/Players/male01/head05/se/h0.png");
		break;
	case 5:
		//male head 06
		playerResources[0].addHead(5, DIR_S,	"Images/Players/male01/head06/down/h0.png");
		playerResources[0].addHead(5, DIR_SW,	"Images/Players/male01/head06/sw/h0.png");
		playerResources[0].addHead(5, DIR_W,	"Images/Players/male01/head06/left/h0.png");
		playerResources[0].addHead(5, DIR_NW,	"Images/Players/male01/head06/nw/h0.png");
		playerResources[0].addHead(5, DIR_N,	"Images/Players/male01/head06/up/h0.png");
		playerResources[0].addHead(5, DIR_NE,	"Images/Players/male01/head06/ne/h0.png");
		playerResources[0].addHead(5, DIR_E,	"Images/Players/male01/head06/right/h0.png");
		playerResources[0].addHead(5, DIR_SE,	"Images/Players/male01/head06/se/h0.png");
		break;
	case 6:
		//male head 07
		playerResources[0].addHead(6, DIR_S,	"Images/Players/male01/head07/down/h0.png");
		playerResources[0].addHead(6, DIR_SW,	"Images/Players/male01/head07/sw/h0.png");
		playerResources[0].addHead(6, DIR_W,	"Images/Players/male01/head07/left/h0.png");
		playerResources[0].addHead(6, DIR_NW,	"Images/Players/male01/head07/nw/h0.png");
		playerResources[0].addHead(6, DIR_N,	"Images/Players/male01/head07/up/h0.png");
		playerResources[0].addHead(6, DIR_NE,	"Images/Players/male01/head07/ne/h0.png");
		playerResources[0].addHead(6, DIR_E,	"Images/Players/male01/head07/right/h0.png");
		playerResources[0].addHead(6, DIR_SE,	"Images/Players/male01/head07/se/h0.png");
		break;
	}
}

void load_head_resources(){
	//load all heads
	for(int i=0 ; i<NUM_HEADS ; i++)
		load_head_resources_id(i);
}

void load_player_resources_avatar(){
	playerResources[0].addAnim(IDLE_DOWN,	"Images/Players/male01/body01/south/idle-down/idle");
}

void load_player_resources_id(int id){
	switch(id)
	{
	case 0:
		//male 01
		playerResources[0].addAnim(IDLE_UP,			"Images/Players/male01/body01/north/idle-up/idle");
		playerResources[0].addAnim(IDLE_DOWN,		"Images/Players/male01/body01/south/idle-down/idle");
		playerResources[0].addAnim(IDLE_RIGHT,		"Images/Players/male01/body01/right/idle-right/idle"); 
		playerResources[0].addAnim(IDLE_LEFT,		"Images/Players/male01/body01/left/idle-left/idle");
		playerResources[0].addAnim(WALK_UP,			"Images/Players/male01/body01/north/walk-up/walk");
		playerResources[0].addAnim(WALK_DOWN,		"Images/Players/male01/body01/south/walk-down/walk");
		playerResources[0].addAnim(WALK_RIGHT,		"Images/Players/male01/body01/right/walk-right/walk"); 
		playerResources[0].addAnim(WALK_LEFT,		"Images/Players/male01/body01/left/walk-left/walk");
		playerResources[0].addAnim(IDLE_NW,			"Images/Players/male01/body01/north/idle-nw/idle");
		playerResources[0].addAnim(IDLE_NE,			"Images/Players/male01/body01/north/idle-ne/idle");
		playerResources[0].addAnim(IDLE_SW,			"Images/Players/male01/body01/south/idle-sw/idle");
		playerResources[0].addAnim(IDLE_SE,			"Images/Players/male01/body01/south/idle-se/idle");
		playerResources[0].addAnim(WALK_NW,			"Images/Players/male01/body01/north/walk-nw/walk");
		playerResources[0].addAnim(WALK_NE,			"Images/Players/male01/body01/north/walk-ne/walk");
		playerResources[0].addAnim(WALK_SW,			"Images/Players/male01/body01/south/walk-sw/walk");
		playerResources[0].addAnim(WALK_SE,			"Images/Players/male01/body01/south/walk-se/walk");
		playerResources[0].addAnim(SIT_UP,			"Images/Players/male01/body01/sit/idle-sit-up/sit");
		playerResources[0].addAnim(SIT_DOWN,		"Images/Players/male01/body01/sit/idle-sit-down/sit");
		playerResources[0].addAnim(SIT_RIGHT,		"Images/Players/male01/body01/sit/idle-sit-right/sit"); 
		playerResources[0].addAnim(SIT_LEFT,		"Images/Players/male01/body01/sit/idle-sit-left/sit");
		playerResources[0].addAnim(SIT_NW,			"Images/Players/male01/body01/sit/idle-sit-nw/sit");
		playerResources[0].addAnim(SIT_NE,			"Images/Players/male01/body01/sit/idle-sit-ne/sit");
		playerResources[0].addAnim(SIT_SW,			"Images/Players/male01/body01/sit/idle-sit-sw/sit");
		playerResources[0].addAnim(SIT_SE,			"Images/Players/male01/body01/sit/idle-sit-se/sit");
		playerResources[0].addAnim(DEATH_RIGHT,		"Images/Players/male01/body01/right/die-right/die");
		playerResources[0].addAnim(DEATH_LEFT,		"Images/Players/male01/body01/left/die-left/die");
		playerResources[0].addAnim(SKILL_RIGHT,		"Images/Players/male01/body01/right/skill-right/body"); 
		playerResources[0].addAnim(SKILL_LEFT,		"Images/Players/male01/body01/left/skill-left/body");
		playerResources[0].addAnim(SHOOT_RIGHT,		"Images/Players/male01/body01/right/shoot-right/shoot"); 
		playerResources[0].addAnim(SHOOT_LEFT,		"Images/Players/male01/body01/left/shoot-left/shoot");
		playerResources[0].addAnim(BESERK_RIGHT,	"Images/Players/male01/body01/skills/skill-beserk-right/skill");
		playerResources[0].addAnim(BESERK_LEFT,		"Images/Players/male01/body01/skills/skill-beserk-left/skill");
		playerResources[0].addAnim(BUSTER_RIGHT,	"Images/Players/male01/body01/skills/skill-buster-right/skill");
		playerResources[0].addAnim(BUSTER_LEFT,		"Images/Players/male01/body01/skills/skill-buster-left/skill");
		playerResources[0].addAnim(REPEL_RIGHT,		"Images/Players/male01/body01/skills/skill-repel-right/skill");
		playerResources[0].addAnim(REPEL_LEFT,		"Images/Players/male01/body01/skills/skill-repel-left/skill");
		playerResources[0].addAnim(TELEPORT_RIGHT,	"Images/Players/male01/body01/skills/skill-teleport-right/skill");
		playerResources[0].addAnim(TELEPORT_LEFT,	"Images/Players/male01/body01/skills/skill-teleport-left/skill");
		break;
	}
}

void load_player_resources(){
	//load all players
	for(int i=0 ; i<NUM_PLAYERS ; i++)
		load_player_resources_id(i);
}

// Mobs...
Resources mobResources[NUM_MOBS];

void load_mob_resources_id(int id){
	switch(id)
	{
	case 0:
		//mob01
		mobResources[0].addAnim(IDLE_RIGHT,		"Images/Monsters/mob01/idle-right/walk");
		mobResources[0].addAnim(IDLE_LEFT,		"Images/Monsters/mob01/idle-left/walk");
		mobResources[0].addAnim(WALK_RIGHT,		"Images/Monsters/mob01/walk-right/walk");
		mobResources[0].addAnim(WALK_LEFT,		"Images/Monsters/mob01/walk-left/walk");
		mobResources[0].addAnim(DEATH_RIGHT,	"Images/Monsters/mob01/die-right/die");
		mobResources[0].addAnim(DEATH_LEFT,		"Images/Monsters/mob01/die-left/die");
		mobResources[0].addAnim(SHOOT_RIGHT,	"Images/Monsters/mob01/atk-right/atk");
		mobResources[0].addAnim(SHOOT_LEFT,		"Images/Monsters/mob01/atk-left/atk");
		break;
	case 1:
		//mob02
		mobResources[1].addAnim(IDLE_RIGHT,		"Images/Monsters/mob02/walk-right/walk");
		mobResources[1].addAnim(IDLE_LEFT,		"Images/Monsters/mob02/walk-left/walk");
		mobResources[1].addAnim(WALK_RIGHT,		"Images/Monsters/mob02/walk-right/walk");
		mobResources[1].addAnim(WALK_LEFT,		"Images/Monsters/mob02/walk-left/walk");
		mobResources[1].addAnim(DEATH_RIGHT,	"Images/Monsters/mob02/die-right/die");
		mobResources[1].addAnim(DEATH_LEFT,		"Images/Monsters/mob02/die-left/die");
		mobResources[1].addAnim(SHOOT_RIGHT,	"Images/Monsters/mob02/atk-right/atk");
		mobResources[1].addAnim(SHOOT_LEFT,		"Images/Monsters/mob02/atk-left/atk");
		break;
	}
}

void load_mob_resources(){
	//loads all mobs
	for(int i=0 ; i<NUM_MOBS ; i++)
		load_mob_resources_id(i);
}

// Weather...
Resources weatherResources[NUM_WEATHER];

void load_weather_resources(){

	weatherResources[0].addAnim(0, "Images/effects/rain0/rain");
}

// Items...
Resources itemResources[NUM_ITEMS];
int inventoryResources[NUM_ITEMS];

void load_item_resources(){
	//load inventory images...
	inventoryResources[0] = wImageAdd("Images/inventory/armor/armor0.png");
	inventoryResources[1] = wImageAdd("Images/inventory/boots/boots0.png");
	inventoryResources[2] = wImageAdd("Images/inventory/necklace/neck0.png");
	inventoryResources[3] = wImageAdd("Images/inventory/ring/ring0.png");
	inventoryResources[4] = wImageAdd("Images/inventory/armor/armor5.png");
	inventoryResources[5] = wImageAdd("Images/inventory/boots/boots1.png");
	inventoryResources[6] = wImageAdd("Images/inventory/necklace/neck2.png");
	inventoryResources[7] = wImageAdd("Images/inventory/ring/ring1.png");

	inventoryResources[8] = wImageAdd("Images/inventory/armor/armor0.png");
	itemResources[8].addAnim(DIR_S,	"Images/inventory/midheadgear/01/down/mid");
	itemResources[8].addAnim(DIR_SW,"Images/inventory/midheadgear/01/sw/mid");
	itemResources[8].addAnim(DIR_W,	"Images/inventory/midheadgear/01/left/mid");
	itemResources[8].addAnim(DIR_NW,"Images/inventory/midheadgear/01/nw/mid");
	itemResources[8].addAnim(DIR_N,	"Images/inventory/midheadgear/01/up/mid");
	itemResources[8].addAnim(DIR_NE,"Images/inventory/midheadgear/01/ne/mid");
	itemResources[8].addAnim(DIR_E,	"Images/inventory/midheadgear/01/right/mid");
	itemResources[8].addAnim(DIR_SE,"Images/inventory/midheadgear/01/se/mid");
}

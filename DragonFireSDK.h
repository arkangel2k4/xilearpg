//===============================================
// DragonFireSDK.h
//===============================================
#include <stdio.h>

#pragma comment(lib,"C:/DragonFireSDK/DragonFireSDK.lib")

//===============================================
// App.cpp must provide these functions
void AppMain(); // app starts here
void OnTimer(); // 30 times per sec - update game here
void AppExit(); // app ends here - save game if desired

//===============================================


int WorldSetxy(int x,int y);

int ImageAdd(char *filename);
int ImageGetWidth(int im);
int ImageGetHeight(int im);

int ViewAdd(int im,int x,int y);
int ViewAdd(char *filename,int x,int y);
int ViewAdd(char *filename,int x,int y,int (*callback)(int id,int event,int x,int y),int id);

int ViewGetx(int vw);
int ViewGety(int vw);
int ViewGetWidth(int vw);
int ViewGetHeight(int vw);

int ViewSetxy(int vw,int x,int y);
int ViewSetImage(int vw,int im);
int ViewSetVisible(int vw,int flag);

int TouchAdd(int x,int y,int width,int height,int (*callback)(int id,int event,int x,int y),int id); // event: 1=down, 2=move, 3=up
int TouchGetxy(int tc,int &x,int &y);
int TouchSetxy(int tc,int x,int y);
int TouchSetVisible(int tc,int flag);

void RandomSetSeed(unsigned int seed);
void Randomize();
int Random(unsigned int range); // ret number from 0 to <range

int ButtonAdd(char *filename,int x,int y,int (*callback)(int id),int id);
int ButtonSetVisible(int bt,int flag);

int PushButtonAdd(char *filename,int x,int y,int (*callback)(int id),int id);
int PushButtonSetVisible(int bt,int flag);

int FontAdd(char *foldername);
int FontAdd(char *font,char *style,int points,int color);
//  Arial
//  Courier
//  Georgia
//  Helvetica
//  Times
//  Trebuchet
//  Verdana

int TextAdd(int x,int y,char *text,int font);
int TextSetText(int tx,char *text);
int TextSetxy(int tx,int x,int y);
int TextGetx(int tx);
int TextGety(int tx);
int TextSetVisible(int tx,int flag);
int TextSetColor(int tx,int color);

int FileOpenAsset(char *filename);
int FileOpen(char *filename);
int FileCreate(char *filename);
int FileRead(int fh,char *buf,int len);
int FileWrite(int fh,char *buf,int len);
int FileSeek(int fh,int seek);
int FileClose(int fh);

int SoundAdd(char *filename);
int SoundPlay(int sn);
int SoundLoop(int sn);
int SoundStop(int sn);

int Mp3Add(char *filename);
int Mp3Play(int sn);
int Mp3Loop(int sn);
int Mp3Stop(); // only one that can be playing so sn not needed


void DeckShuffle(int deck[52]);

void Printf(char* lpszFormat, ...);

void StrCopy(char *dest,char *src);
void StrAppend(char *dest,char *src);

void LandscapeMode();
void PortraitMode();

int ViewSetRotate(int vw,int degrees);
int ViewSetSize(int vw,int width,int height);

int ViewSetTouch(int vw,int tc);

int TiltGetx();
int TiltGety();
int TiltGetz();

int ViewSetAlpha(int vw,int alpha);

int EditAdd(int x,int y,int width);
int EditSetLabel(int ed,char *text);
int EditSetText(int ed,char *text);
int EditGetText(int ed,char *text);
int EditSetVisible(int ed,int flag);
int EditSetOnDone(int (*callback)(void));

#define printf Printf

#include "DragonWrapper.h"

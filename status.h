#ifndef _STATUS_H_
#define _STATUS_H_

enum STATUS_EFFECTS{
	STATUS_STUN,
	STATUS_BESERKED,
	//leave the line below:
	NUM_STATUS
};

//Basic damage info of a weapon
//Required because players have two of these, one in status_data
//and another for their left hand weapon.
struct weapon_atk {
	unsigned short atk, atk2;
	unsigned short range;
	unsigned char ele;
};

//For holding basic status (which can be modified by status changes)
struct status_data {
	int
		level,
		hp, sp,
		max_hp, max_sp;
	int
		exp;
	unsigned short
		str, agi, vit, int_, dex, luk,
		batk,
		matk_min, matk_max,
		speed,
		amotion, adelay, dmotion,
		mode;
	short 
		hit, flee, cri, flee2,
		def2, mdef2,
		aspd_rate;
	unsigned char
		def_ele, ele_lv,
		size, race;
	signed char
		def, mdef;
	struct weapon_atk rhw, lhw; //Right Hand/Left Hand Weapon.

	int effect[NUM_STATUS];
	struct status_data **effect_src;
};

#endif

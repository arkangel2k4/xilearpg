#ifndef _ANIMOBJECT_H_
#define _ANIMOBJECT_H_

#include "Vector2.h"

class AnimObject
{

public:
	//data
	int **_frames;
	int *_numFrames;
	int currentAnim;
	int _viewHandle;

	//aux
	int _curFrame;
	bool _isLoop;
	double _frameTime;
	double _curTime;
	double _maxTime;
	int _iterations;
	bool _paused;
	double _rotDeg;
	Vector2 _scale;
	bool _visible;

	Vector2 _pos;
	Vector2 _imagepos;

	//constructors
	AnimObject(double frameTime = 0.0333, bool loop = true);
	~AnimObject();
	
	void init();

	//functions
	void addAnim(int animId, char *baseFilename);
	void setCurrentAnim(int animId);

	void init(int width, int height, int x, int y);

	virtual void setVisible(bool bVis);
	bool isVisible() const;

	bool setPosition(int _x, int _y);

	void setLoop(bool loop);
	bool getLoop() const;
	bool isPaused() const;
	int getIterations();
	
	void start();
	void stop();
	void reset();

	void update(double dt = 0.0333);
	void reloadView();
};

#endif

#include "mmo.h"
#include "map.h"
#include "mob.h"
#include "Resources.h"
#include "GameObject.h"
#include "Player.h"
#include "Monster.h"
#include "Configuration.h"
#include "Interface.h"
#include "battle.h"

bool inRange(int skill, GameObject *src, GameObject *dst, float *d){
	int range = (skill==SKILL_FIRE ? src->status.rhw.range : skillRange[skill]);
	Vector2 dist = src->_pos - dst->_pos;
	float dd=dist.Length();
	if(d!=NULL) *d=dd;

	if(dd > range*BLOCK_SIZE || !dst->isVisible())
		return false;
	else
		return true;
}

int attack(int skill, GameObject *src, GameObject *dst){
	int damage = 0;
	int font = FONT_DMG;

	if(dst->isDead || dst->status.hp<=0){
		showSkillAnim(src, -1);
		return -2;
	}

	//is in range ?
	int range = (skill==SKILL_FIRE ? src->status.rhw.range : skillRange[skill]);
	Vector2 dist = src->_pos - dst->_pos;
	if(dist.Length() > range*BLOCK_SIZE || !dst->isVisible())
		return -1;

	//is it time ? wait for skill delay...
	switch(src->type){
	case TYPE_MOB:
		if( ((Monster*)src)->skillTimeCount < skillDelay[skill] ){
			((Monster*)src)->skillTimeCount++;
			return -2;
		}
		showSkillAnim(src, -1);
		((Monster*)src)->skillTimeCount = 0;
		break;
	case TYPE_PLAYER:
		if( skillTimeCount[skill] > 0 )
			return -2;
		break;
	}

	//animation...
	showSkillAnim(src, skill);

	//damage and effects...
	switch(skill){
	case SKILL_FIRE:
		damage = battle_calc_base_damage(&src->status, src->type, &src->status.rhw, 0);
		//critical?
		if(RAND100() < src->status.cri){
			damage *= 2;
			font = FONT_CRIT;
		}
		break;
	case SKILL_BESERK:
		damage = 100 + 25*(src->status.level-1);
		dst->status.effect[STATUS_BESERKED] = statusDuration[STATUS_BESERKED];
		dst->status.effect_src[STATUS_BESERKED] = &src->status;
		break;
	case SKILL_BUSTER:
		damage = 150 + 35*(src->status.level-1);
		if(RAND100() < 10 + 5*(src->status.level-1))
			dst->status.effect[STATUS_STUN] = statusDuration[STATUS_STUN];
		break;
	case SKILL_REPEL:
		if(RAND100() < 20 + 8*(src->status.level-1))
			dst->status.effect[STATUS_STUN] = statusDuration[STATUS_STUN];
		Vector2 dir=-dist; dir.Unitize();
		dst->setPosition(dst->_pos + dir*BLOCK_SIZE*2);
		return 0;
		break;
	}

	//defense...
	damage -= dst->status.def;
	//limits on final damage... everyone or monsters only ???
	if(src->type==TYPE_MOB && damage < src->status.batk) damage = src->status.batk;
	if(dst->type==TYPE_MOB && damage > 0.20*dst->status.max_hp) damage = (int)(0.20*dst->status.max_hp);

	//take damage...
	dst->status.hp = MAX(0, dst->status.hp - damage);
	showDamage(dst, damage, font);

	//hit...
	if(dst->type==TYPE_PLAYER && damage>0){
		((Player*)dst)->lasthitTimeCount = 0; //last hit timer
	}

	//killed?
	if(dst->status.hp<=0){
		src->status.exp += dst->status.exp;
		//loot:
		for(int i=0 ; i<NUM_ITEMS ; i++)
			if(RAND100()<50){ player->addItem(i,1); break; } //TODO: loop only through items dropable by current mob
	}

	//TODO: if src==mob and dist>0.5*range return -1 (to get a little closer)
	return 0;
}

int reachObject(GameObject *src, GameObject *dst){
	Vector2 dir = dst->_pos - src->_pos;
	Vector2 next;
	
	//if target is too far away, then mobs dont see him
	if(src->type==TYPE_MOB && dir.Length() / BLOCK_SIZE > 5 || !dst->isVisible())
		return 0;

	dir.Unitize();

	//direction
	if(src->type==TYPE_PLAYER){
		float ang = 180 + atan2((float)dir.y,(float)-dir.x)*180/3.14;
		int auxbut, headDir;
		if(ang <= 22.5 || ang > 337.5){
			headDir = DIR_E;
			auxbut = PB_RIGHT_BTN;
		}else if(ang <= 67.5 && ang > 22.5){
			headDir = DIR_NE;
			auxbut = PB_NE_BTN;
		}else if(ang <= 112.5 && ang > 67.5){
			headDir = DIR_N;
			auxbut = PB_TOP_BTN;
		}else if(ang <= 157.5 && ang > 112.5){
			headDir = DIR_NW;
			auxbut = PB_NW_BTN;
		}else if(ang <= 202.5 && ang > 157.5){
			headDir = DIR_W;
			auxbut = PB_LEFT_BTN;
		}else if(ang <= 247.5 && ang > 202.5){
			headDir = DIR_SW;
			auxbut = PB_SW_BTN;
		}else if(ang <= 292.5 && ang > 247.5){
			headDir = DIR_S;
			auxbut = PB_DOWN_BTN;
		}else{
			headDir = DIR_SE;
			auxbut = PB_SE_BTN;
		}
		player->setDir(headDir);
		player->setCurrentAnim(directionAnim[auxbut]);
		player->setHeadDir(headDir);
	}else{
		if(dir.x>0){
			src->direction=RIGHT;
			src->setCurrentAnim(WALK_RIGHT);
		}else{
			src->direction=LEFT;
			src->setCurrentAnim(WALK_LEFT);
		}
	}

	//this is player's speed...
	next = src->_pos + dir*4*(src->status.speed/100.0);
	
	//dont leave borders... how to deal with this?
	if(next.x < 0) next.x = 0;
	if(next.y < 0) next.y = 0;
	if(next.x >= sizeBg.x) next.x = sizeBg.x;
	if(next.y >= sizeBg.y) next.y = sizeBg.y;

	//is it occupied (by someone else)?
	GameObject *next_who = mapCells[(int)next.y/BLOCK_SIZE][(int)next.x/BLOCK_SIZE];
	if(next_who!=NULL && next_who!=src){
		next.x = src->_pos.x + (int)(dir.y*4*(src->status.speed/100.0));
		next.y = src->_pos.y - (int)(dir.x*4*(src->status.speed/100.0));
		if(next.x<0 || next.x>=sizeBg.x || next.y<0 || next.y>=sizeBg.y){
			next.x = src->_pos.x - (int)(dir.y*4*(src->status.speed/100.0));
			next.y = src->_pos.y + (int)(dir.x*4*(src->status.speed/100.0));
		}
		if(next.x<0 || next.x>=sizeBg.x || next.y<0 || next.y>=sizeBg.y){
			next = src->_pos;
		}
	}
	
	//update...
	mapCells[(int)src->_pos.y/BLOCK_SIZE][(int)src->_pos.x/BLOCK_SIZE]=NULL;
	mapCells[(int)next.y/BLOCK_SIZE][(int)next.x/BLOCK_SIZE]=src;
	src->_pos = next;

	return 0;
}

/*==========================================
 * Calculates the standard damage of a normal attack assuming it hits.
 *------------------------------------------
 * Pass damage2 as NULL to not calc it.
 * Flag values:
 * &1: Critical hit
 * &2: Arrow attack
 */
int battle_calc_base_damage(struct status_data *status, OBJ_TYPE obj_type, struct weapon_atk *wa, int flag)
{
	unsigned short atkmin=0, atkmax=0;
	short type = 0;
	int damage = 0;

	//for now, use jon's formula
	return status->batk;

	if(obj_type==TYPE_MOB){
		//Mobs/Pets
		atkmin = wa->atk;
		atkmax = wa->atk2;

		if (atkmin > atkmax)
			atkmin = atkmax;
	} else {
		//PCs
		atkmax = wa->atk;
		type = EQI_WEAPON;

		if (!(flag&1) || (flag&2))
		{	//Normal attacks
			atkmin = status->dex;
			/*
			if (sd->equip_index[type] >= 0 && sd->inventory_data[sd->equip_index[type]])
				atkmin = atkmin*(80 + sd->inventory_data[sd->equip_index[type]]->wlv*20)/100;

			if (atkmin > atkmax)
				atkmin = atkmax;
			*/
		}
	}
		
	//Weapon Damage calculation
	if (!(flag&1))
		damage = (atkmax>atkmin? rand()%(atkmax-atkmin):0)+atkmin;
	else 
		damage = atkmax;
		
	//Finally, add baseatk
	damage += status->batk;
	
	return damage;
}

#include "GameObject.h"
#include <vector>

struct block_list {
	struct block_list *next,*prev;
	int id;
	short m,x,y;
	GameObject *obj;
};

extern struct block_list bl_head;
extern struct block_list *bl_tail;
extern int bl_count;

extern struct map_data maps[1];

struct map_stage {
	int mob_id;
	int quant;
};
struct map_data {
	char background[100];
	Vector2 size;
	std::vector<struct map_stage> stages;
	int next_map;
};

int map_init();
int map_spawn(int *map_id, int *stage);
void map_add_stage(int map, int mob, int quantity);

/*
struct weapon_data {
	int atkmods[3];
	// all the variables except atkmods get zero'ed in each call of status_calc_pc
	// NOTE: if you want to add a non-zeroed variable, you need to update the memset call
	//  in status_calc_pc as well! All the following are automatically zero'ed. [Skotlex]
	int overrefine;
	int star;
	int ignore_def_ele;
	int ignore_def_race;
	int def_ratio_atk_ele;
	int def_ratio_atk_race;
	int addele[ELE_MAX];
	int addsize[3];
};

struct map_session_data {
	struct block_list *bl;
	struct status_data *status;

	struct item_data* inventory_data[MAX_INVENTORY]; // direct pointers to itemdb entries (faster than doing item_id lookups)
	short equip_index[11];
	unsigned int weight,max_weight;
	int cart_weight,cart_num;
	int fd;
	unsigned short mapindex;
	unsigned char head_dir; //0: Look forward. 1: Look right, 2: Look left.

	int invincible_timer;

	short weapontype1,weapontype2;
	short disguise; // [Valaris]

	struct weapon_data right_weapon, left_weapon;

	int weapon_atk_rate[16];
	int arrow_atk,arrow_ele,arrow_cri,arrow_hit;

	int castrate,delayrate,hprate,sprate,dsprate;
	int atk_rate;
	int hprecov_rate,sprecov_rate;
	int matk_rate;
	int critical_rate,hit_rate,flee_rate,flee2_rate,def_rate,def2_rate,mdef_rate,mdef2_rate;

	int itemid;
	short itemindex;	//Used item's index in sd->inventory [Skotlex]

	short catch_target_class; // pet catching, stores a pet class to catch (short now) [zzo]
};
*/
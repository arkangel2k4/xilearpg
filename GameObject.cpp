//#include "DragonFireSDK.h"
#include <string.h>
#include <vector>
#include "mmo.h"
#include "Resources.h"
#include "GameObject.h"
#include "Player.h"
#include "Interface.h"
///////////////////////////////////////

//martim...
Vector2 posBg;
Vector2 sizeBg;

//GameObject ***mapCells;
Matrix<GameObject*> mapCells;
//--end


///////////////////////////////////////////////////////////////////////////////
// construction and destruction
///////////////////////////////////////////////////////////////////////////////
GameObject::GameObject(double frameTime, bool loop) : 
	_rotDeg(0),_visible(false), _frames(0),
	_curTime(0), _curFrame(0), _maxTime(0),
	_viewHandle(0), _iterations(0), _paused(false),
	isDead(false)
{
	_isLoop = loop;	
	currentAnim = 0;
	_frameTime = frameTime;
	//martim
	_frames = 0;
	_numFrames = 0;
	_viewHandle = -1;
	expiryDate = 0;
	loopOnDeath = true;
	//views
	for(int i=0 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++){
		view_shift_x[i]=0;
		view_shift_y[i]=0;
	}
	//handles...
	handles = popHandles();
	textHandlerDmg[0] = handles.text[0];
	textHandlerDmg[1] = handles.text[1];
	textHandlerCrit[0] = handles.text[2];
	textHandlerCrit[1] = handles.text[3];
	textTimer = 0;
	dmgType = FONT_DMG;
	//status data
	memset(&status, 0, sizeof(struct status_data));
	status.effect_src = new struct status_data*[NUM_STATUS];
	for(int i=0; i<NUM_STATUS ; i++)
		status.effect_src[i] = NULL;
}

GameObject::~GameObject()
{
	//handles...
	pushHandles(handles);

	//clean stuff...
	delete[] status.effect_src;
	//delete [] _frames; //martim... we have resources on memory now
	_frames = 0;
	setVisible(false); //martim... can't delete the view??
}

void GameObject::setCurrentAnim(int animId)
{
	//if dead, we can only set death animations
	if(isDead && animId!=DEATH_LEFT && animId!=DEATH_RIGHT)
		return;

	if(animId == currentAnim)
		return;

	currentAnim = animId;
	_curTime = 0;
	_maxTime = _numFrames[animId] * _frameTime;
	//type;
	if(handles.view[0]>0){ //maybe make a better check...
		posZero2Feet.x = ImageGetWidth(_frames[animId][0])/2;
		posZero2Feet.y = ImageGetHeight(_frames[animId][0]);
		//_imagepos = _pos-posZero2Feet; //oct2011 trying to fix flashes
	}	
}

void GameObject::addAnim(int animId, char *baseFilename)
{
	//open files starting with 0 and open as many as can be found
	//we assume a .png extension
	int im = 0;
	int im_cnt = 0;
	char fn[512];
	int tmp[1000];

	sprintf(fn,"%s0.png",baseFilename,0);
	im = wImageAdd(fn);

	while(im && im_cnt < 1000)
	{
		tmp[im_cnt] = im;
		im_cnt++;
		
		sprintf(fn,"%s%d.png",baseFilename,im_cnt);
		im = wImageAdd(fn);
	}
	
	//copy over
	if(im_cnt > 0)
	{
		_frames[animId] = new int[im_cnt];
		_numFrames[animId] = im_cnt;

		for(int i=0; i<im_cnt; i++)
		{
			_frames[animId][i] = tmp[i];
		}
	}

	_maxTime = _numFrames[animId] * _frameTime;

	if(handles.view[0]==-1) //martim: make only one handler per object!
		handles.view[0] = wViewAdd(_frames[animId][0], (int)_imagepos.x, (int)_imagepos.y);
	wViewSetVisible(handles.view[0],0);
}

void GameObject::setDead(bool dead)
{
	isDead = dead;
	if(dead){
		//martim... animation
		expiryDate=60;
		if(direction==1) setCurrentAnim(DEATH_RIGHT);
		else setCurrentAnim(DEATH_LEFT);
		if(!loopOnDeath) _isLoop=false;
	}else{
		setVisible(true);
		_paused=false;
		_isLoop=true;
	}
}

void GameObject::init(int width, int height, int x, int y)
{
	posZero2Feet.x = width/2;
	posZero2Feet.y = height;
	_pos.x = x;
	_pos.y = y;
	_imagepos = _pos-posZero2Feet;

	setVisible(false);
	setDead(false);
	ViewSetxy(handles.view[0], (int)_imagepos.x, (int)_imagepos.y);
}

///////////////////////////////////////////////////////////////////////////////
// visible
///////////////////////////////////////////////////////////////////////////////
void GameObject::setVisible(bool vis)
{
	_visible = vis;
	wViewSetVisible(handles.view[0], vis ? 1 : 0);
	//text...
	if(textTimer>0){
		if(dmgType==FONT_DMG){
			TextSetVisible(textHandlerDmg[0], vis);
			TextSetVisible(textHandlerDmg[1], vis);
		}else if(dmgType==FONT_CRIT){
			TextSetVisible(textHandlerCrit[0], vis);
			TextSetVisible(textHandlerCrit[1], vis);
		}
	}
	//visual effects...
}

bool GameObject::isVisible() const
{
	return _visible;
}

///////////////////////////////////////////////////////////////////////////////
// position
///////////////////////////////////////////////////////////////////////////////
bool GameObject::setPosition(Vector2 pos)
{
	return setPosition(pos.x, pos.y);
}

bool GameObject::setPosition(int _x, int _y)
{
	//dont leave borders... how to deal with this?
	if(_x < 0) _x = 0;
	if(_y < 0) _y = 0;
	if(_x >= sizeBg.x) _x = sizeBg.x;
	if(_y >= sizeBg.y) _y = sizeBg.y;

	//is it occupied (by someone else)?
	GameObject *next_who = mapCells[_y/BLOCK_SIZE][_x/BLOCK_SIZE];
	if(next_who!=NULL && next_who!=this){
		return false;
	}

	//update cells
	mapCells[(int)_pos.y/BLOCK_SIZE][(int)_pos.x/BLOCK_SIZE]=NULL;
	mapCells[_y/BLOCK_SIZE][_x/BLOCK_SIZE]=this;

	//update
	_pos.x = _x;
	_pos.y = _y;
	return true;
}

Vector2 GameObject::getPosition() const
{
	return _pos;
}

bool GameObject::isPaused() const
{
	return _paused;
}

///////////////////////////////////////////////////////////////////////////////
// rotation
///////////////////////////////////////////////////////////////////////////////
void GameObject::setRotation(double deg)
{
	_rotDeg = deg;
	/*ViewSetRotate(handles.view[0],(int)deg);*/
}

double GameObject::getRotation() const
{
	return _rotDeg;
}

///////////////////////////////////////////////////////////////////////////////
// scale
///////////////////////////////////////////////////////////////////////////////
void GameObject::setScale(Vector2 &scale)
{
	_scale = scale;
}

Vector2 GameObject::getScale() const
{
	return _scale;
}

///////////////////////////////////////////////////////////////////////////////
// iterations
///////////////////////////////////////////////////////////////////////////////
int GameObject::getIterations()
{
	return _iterations;
}

//set/get the sprite loop
void GameObject::setLoop(bool loop)
{
	_isLoop = loop;
}

bool GameObject::getLoop() const
{
	return _isLoop;
}

///////////////////////////////////////////////////////////////////////////////
// start / stop / reset
///////////////////////////////////////////////////////////////////////////////
void GameObject::start()
{
	_paused = false;
	update(0.0);
}

void GameObject::stop()
{
	_paused = true;
	update(0.0);
}

void GameObject::reset()
{
	_curTime = 0;
	update(0.0);
}

///////////////////////////////////////////////////////////////////////////////
// update
///////////////////////////////////////////////////////////////////////////////
void GameObject::update(double dt)
{
	//martim
	if(textTimer>0){
		int t = _imagepos.y-30, b = _imagepos.y-5, f;
		if(textTimer>5)
			f = t + textTimer*(b-t)/(30-5);
		else
			f = t + 5*(b-t)/(30-5);
		if(dmgType==FONT_DMG){
			if(textTimer==1){
				TextSetVisible(textHandlerDmg[0], 0);
				TextSetVisible(textHandlerDmg[1], 0);
			}else{
				TextSetxy(textHandlerDmg[0], (int)(_imagepos.x-10+0.5*ViewGetWidth(handles.view[0])), f);
				TextSetxy(textHandlerDmg[1], (int)(_imagepos.x-10+0.5*ViewGetWidth(handles.view[0]))+2, f+2);
			}
		}else if(dmgType==FONT_CRIT){
			if(textTimer==1){
				TextSetVisible(textHandlerCrit[0], 0);
				TextSetVisible(textHandlerCrit[1], 0);
			}else{
				TextSetxy(textHandlerCrit[0], (int)(_imagepos.x-14+0.5*ViewGetWidth(handles.view[0])), f-8);
				TextSetxy(textHandlerCrit[1], (int)(_imagepos.x-14+0.5*ViewGetWidth(handles.view[0]))+2, f-8+2);
			}
		}
		textTimer--;
	}
	//--orig:

	if(_paused){
		//martim: viewHandle could have changed from Z-order... update image even if paused
		reloadView();
		return;
	}else
		_curTime += dt;

	//compute the current iteration time
	_iterations = int(_curTime / _maxTime);
	double interval_time = _curTime - ((float)_iterations * _maxTime);
	
	//compute the current frame
	_curFrame = (int)(interval_time / _frameTime);
	
	if(_curFrame < 0) _curFrame = 0;
	if(_curFrame >= _numFrames[currentAnim]) _curFrame = _numFrames[currentAnim]-1;
	type;
	//set the image
	reloadView();
	
	//if(type==TYPE_PLAYER && currentAnim==BUSTER_LEFT || currentAnim==BUSTER_RIGHT)
		//return;

	if ((_curFrame == _numFrames[currentAnim] - 1) && !_isLoop)
	{
		stop();
		//setVisible(false); //martim
	}
}

void GameObject::reloadView()
{
	view_img2load[0] = _frames[currentAnim][_curFrame];

	for(int i=0 ; i<NUM_VIEWHANDLES_PER_OBJ ; i++)
		if(view_img2load[i]>0){
			wViewSetVisible(handles.view[i], isVisible());
			wViewSetImage(handles.view[i], view_img2load[i]);
			ViewSetxy(handles.view[i], (int)_imagepos.x+(view_shift_x[i]*zoomPer/100.0), (int)_imagepos.y+(view_shift_y[i]*zoomPer/100.0));
			ViewSetSize(handles.view[i], (int)(ViewGetWidth(handles.view[i])*zoomPer/100.0), (int)(ViewGetHeight(handles.view[i])*zoomPer/100.0));
		}else{
			wViewSetVisible(handles.view[i], 0);
		}
}

void GameObject::reloadText()
{
	textHandlerDmg[0] = handles.text[0];
	textHandlerDmg[1] = handles.text[1];
	textHandlerCrit[0] = handles.text[2];
	textHandlerCrit[1] = handles.text[3];
	if(textTimer>1){
		if(dmgType==FONT_DMG){
			wTextSetText(textHandlerDmg[0], textStr);
			wTextSetText(textHandlerDmg[1], textStr);
			TextSetVisible(textHandlerDmg[0], 1);
			TextSetVisible(textHandlerDmg[1], 1);
			TextSetVisible(textHandlerCrit[0], 0);
			TextSetVisible(textHandlerCrit[1], 0);
		}else if(dmgType==FONT_CRIT){
			wTextSetText(textHandlerCrit[0], textStr);
			wTextSetText(textHandlerCrit[1], textStr);
			TextSetVisible(textHandlerDmg[0], 0);
			TextSetVisible(textHandlerDmg[1], 0);
			TextSetVisible(textHandlerCrit[0], 1);
			TextSetVisible(textHandlerCrit[1], 1);
		}
	}else{
		TextSetVisible(textHandlerDmg[0], 0);
		TextSetVisible(textHandlerDmg[1], 0);
		TextSetVisible(textHandlerCrit[0], 0);
		TextSetVisible(textHandlerCrit[1], 0);
	}
}


/*****************************************************************************/